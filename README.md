# PLOGS

#### 介绍
在PLOGS系统中进行工作任务计划和执行，当任务完成后会自动归档，自动生成项目工作周报 （日报、周报、月报）和人员工作周报（日报、周报、月报）并且支持项目文档在线预览，项目文档全文检索（可对文件内容检索），另外本系统提供wfs的个人网盘功能.

![输入图片说明](https://images.gitee.com/uploads/images/2021/0925/102909_18ea93f9_24089.jpeg "2e92ae730d544bcebde9d0d0788a1a88.jpg")

#### 软件架构

- jdk7
- maven
- spring4
- spring-mvc4
- hibernate4
- bootstrap
- tomcat7
- mysql


#### 安装说明

- maven部署源码（主模块：PLOGS/src/plogs-web ）编译顺序：plogs-core > plogs-parameter > plogs-report > plogs-authority > plogs-file > plogs-view > plogs-project > plogs-wfs> plogs-tag >plogs-web
- 创建数据库，数据库脚本在 PLOGS/resource/sql/目录下
- 修改数据库配置文件 PLOGS/src/plogs-web/src/main/resources/jdbc.properties
- 修改附件存储地址 参见使用说明
- 项目编译后可直接部署于tomcat7，mysql5.x中运行，支持jdk7/jdk8，如要使用tomcat8及以上版本可能会有报错，请自行修正（所以建议第一次运行在tomcat7中）


#### 注意事项

1.  建议tomcat7，tomcat8或以上版本可能会有报错，根据错误信息自行百度和修改，并不复杂
2.  目前因为数据库方言的使用，只支持mysql，如果要切换数据库系统会有一些工作量，mysql要配置为大小写不敏感（linux环境下特别注意myslq默认大小写敏感）
3.  请使用utf8字符集


#### 安装包下载

[http://www.wcpdoc.com/webspecial/home/Pub2c909b2b6eb4fe9e016f9495d1fb5ad7.html](http://www.wcpdoc.com/webspecial/home/Pub2c909b2b6eb4fe9e016f9495d1fb5ad7.html)

#### 官网地址
[http://www.wcpdoc.com/webspecial/home/Pub2c909b2b6eb4fe9e016f9495d1fb5ad7.html](http://www.wcpdoc.com/webspecial/home/Pub2c909b2b6eb4fe9e016f9495d1fb5ad7.html)

#### 使用教程

视频教程  [http://course.wcpknow.com/classweb/Pubview.do?classid=2c9180837b8aea2c017c1adbb5bc061d](http://course.wcpknow.com/classweb/Pubview.do?classid=2c9180837b8aea2c017c1adbb5bc061d)


#### 界面截图-移动端
![移动端截图1](https://images.gitee.com/uploads/images/2021/0925/091000_6c102eaf_24089.png "phone.png")
#### 界面截图-PC端
![PC1](https://images.gitee.com/uploads/images/2021/0925/102348_6f9da07c_24089.png "1.png")
![PC2](https://images.gitee.com/uploads/images/2021/0925/102358_d9f6ef69_24089.png "2.png")
![PC3](https://images.gitee.com/uploads/images/2021/0925/102408_951d5b19_24089.png "3.png")
![PC4](https://images.gitee.com/uploads/images/2021/0925/102415_4c4849c2_24089.png "4.png")
![PC5](https://images.gitee.com/uploads/images/2021/0925/102424_85e54a85_24089.png "5.png")
![PC6](https://images.gitee.com/uploads/images/2021/0925/102432_67514e8d_24089.png "6.png")

### 开源项目
	
> WCP:知识管理系统 [https://gitee.com/macplus/WCP](https://gitee.com/macplus/WCP)

> WDA:文件转换组件（附件在线预览）[https://gitee.com/macplus/WDA](https://gitee.com/macplus/WDA)

> WTS:在线答题系统 [https://gitee.com/macplus/WTS](https://gitee.com/macplus/WTS)

> WLP:在线学习系统 [https://gitee.com/macplus/WLP](https://gitee.com/macplus/WLP)

> PLOGS:项目任务日志管理系统 [https://gitee.com/macplus/plogs](https://gitee.com/macplus/plogs)

> WSCH:鉴权内容检索系统 [https://gitee.com/macplus/wsch](https://gitee.com/macplus/wsch)

## 商业版产品介绍

> 知识库/在线答題/在线学习产品介绍 [http://www.wcpknow.com/home/index.html](http://www.wcpknow.com/home/index.html)
