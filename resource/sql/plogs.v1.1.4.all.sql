/*
Navicat MySQL Data Transfer

Source Server         : plogs_wfs3377
Source Server Version : 50155
Source Host           : localhost:3377
Source Database       : plogs

Target Server Type    : MYSQL
Target Server Version : 50155
File Encoding         : 65001

Date: 2021-09-25 07:26:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `alone_app_version`
-- ----------------------------
DROP TABLE IF EXISTS `alone_app_version`;
CREATE TABLE `alone_app_version` (
  `version` varchar(32) NOT NULL DEFAULT '',
  `update_time` varchar(32) DEFAULT NULL,
  `update_user` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_app_version
-- ----------------------------
INSERT INTO `alone_app_version` VALUES ('v1.1.2', '2020-02-26 13:35:10', 'USERNAME');
INSERT INTO `alone_app_version` VALUES ('v1.1.3', '2021-03-04 14:13:09', 'USERNAME');
INSERT INTO `alone_app_version` VALUES ('v1.1.4', '2021-09-16 21:43:21', 'USERNAME');

-- ----------------------------
-- Table structure for `alone_applog`
-- ----------------------------
DROP TABLE IF EXISTS `alone_applog`;
CREATE TABLE `alone_applog` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `DESCRIBES` varchar(1024) NOT NULL,
  `APPUSER` varchar(32) NOT NULL,
  `LEVELS` varchar(32) DEFAULT NULL,
  `METHOD` varchar(128) DEFAULT NULL,
  `CLASSNAME` varchar(128) DEFAULT NULL,
  `IP` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_9` (`APPUSER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_applog
-- ----------------------------
INSERT INTO `alone_applog` VALUES ('402880e86fd0b2b9016fd0b48a210000', '20200123124130', '用户登陆系统[系统管理员/127.0.0.1] / 操作用户:系统管理员[40288b854a329988014a329a12f30002]', 'NONE', 'INFO', 'info', 'com.farm.web.log.PlogsLog', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402880e86fd1944d016fd19492120000', '20200123164612', '用户登陆系统[系统管理员/127.0.0.1] / 操作用户:系统管理员[40288b854a329988014a329a12f30002]', 'NONE', 'INFO', 'info', 'com.farm.web.log.PlogsLog', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402880e86fd1985d016fd198bddc0000', '20200123165045', '用户登陆系统[系统管理员/127.0.0.1] / 操作用户:系统管理员[40288b854a329988014a329a12f30002]', 'NONE', 'INFO', 'info', 'com.farm.web.log.PlogsLog', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('402880e86fd1985d016fd19ce6440001', '20200123165518', '用户登陆系统[系统管理员/127.0.0.1] / 操作用户:系统管理员[40288b854a329988014a329a12f30002]', 'NONE', 'INFO', 'info', 'com.farm.web.log.PlogsLog', '127.0.0.1');
INSERT INTO `alone_applog` VALUES ('40289f787c1a1dc2017c1a1f99100000', '20210925072410', '用户登陆系统[系统管理员/127.0.0.1] / 操作用户:系统管理员[40288b854a329988014a329a12f30002]', 'NONE', 'INFO', 'info', 'com.farm.web.log.PlogsLog', '127.0.0.1');

-- ----------------------------
-- Table structure for `alone_auth_action`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_action`;
CREATE TABLE `alone_auth_action` (
  `ID` varchar(32) NOT NULL,
  `AUTHKEY` varchar(128) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `CHECKIS` char(1) NOT NULL,
  `LOGINIS` char(1) NOT NULL COMMENT '默认所有ACTION都要登录',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_action';

-- ----------------------------
-- Records of alone_auth_action
-- ----------------------------
INSERT INTO `alone_auth_action` VALUES ('40288b854a38408e014a384de88a0005', 'action/list', '权限管理_权限定义', '', '20141211154357', '20141211154357', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38b3a6700017', 'actiontree/list', '权限管理_权限构造', '', '20141211173505', '20141211173505', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38b93d0a0022', 'log/list', '系统配置_系统日志', '', '20141211174111', '20150831184926', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38ba24f90024', 'parameter/list', '系统配置_参数定义', '', '20141211174210', '20150831185312', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bafb830026', 'parameter/editlist', '系统配置_系统参数', '', '20141211174305', '20151016154936', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bd26a0002a', 'dictionary/list', '系统配置_数据字典', '', '20141211174527', '20150831185552', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38beae79002d', 'user/list', '组织用户管理_用户管理', '', '20141211174708', '20150831185712', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('40288b854a38974f014a38bf10fc002f', 'organization/list', '组织用户管理_组织机构管理', '', '20141211174733', '20150831185753', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('8a2831b3573c10d0015741b2273c0018', 'user/online', '系统配置_在线用户', null, '20160919170617', '20160919170617', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('8af5c60d5c215c54015c216201ce0002', 'outuser/list', '用户管理_外部账户', null, '20170519234450', '20170519234450', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402889ac5da5d50f015da5d736060000', 'toolweb/sysbackup', '系统配置_系统备份向导', null, '20170803100531', '20170803100531', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402889ac5fc2b9a6015fc2c34e9a0001', 'fileindex/list', '检索管理_附件索引', null, '20171116105820', '20171116105820', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('8a8580496053a94e016053bbba700001', 'wdaconvertlog/list', '运维管理_预览文件转换日志', null, '20171214143500', '20171214143500', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b88169fd1b650169fd1e8fcf0002', 'fileresource/list', '文件管理_资源库管理', null, '20190408212352', '20190408212352', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b88169fd1b650169fd3a1e52000b', 'filebase/list', '文件管理_文件管理', null, '20190408215358', '20190408215358', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816a19c998016a19cd402d0002', 'viewmodel/list', '文件预览_文件模型', null, '20190414110402', '20190414110402', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816a19c998016a19ce87330004', 'viewconvertor/list', '文件预览_文件转换器', null, '20190414110526', '20190414110526', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816a19c998016a19d08d2e0006', 'viewregular/list', '文件预览_转换规则', null, '20190414110739', '20190414110739', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816a1fb82a016a1fbf78460007', 'viewtask/list', '文件预览_转换任务', null, '20190415144642', '20190415144642', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028b8816a2673b5016a2674ebd00001', 'viewdoc/list', '文件预览_预览文件', null, '20190416220237', '20190416220237', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402880e86ebfa71a016ebfaa733a0002', 'projecttype/list', '项目管理_项目类型配置', null, '20191201121409', '20191201121409', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402880e86ec043a8016ec0500cfd0003', 'projectgradation/list', '项目管理_项目阶段定义', null, '20191201151502', '20191201151502', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402880e86ec06ee4016ec0bcbb9e0006', 'tasktype/list', '项目管理_任务类型定义', null, '20191201171344', '20191201171344', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402884116ecf45d4016ecf47f1c90002', 'project/list', '项目任务管理_项目管理', '', '20191204130028', '20191204130110', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402884116ecfe8a8016ecfe9c17d0001', 'task/list', '项目任务_任务管理', null, '20191204155713', '20191204155713', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402884166f3b2fe5016f3b3278e30002', 'fieldtype/list', '项目定义_属性分类配置', null, '20191225115601', '20191225115601', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402884166f3b2fe5016f3b338bef0004', 'field/list', '项目定义_属性定义管理', null, '20191225115711', '20191225115711', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('402881ec6f79086c016f790ba3820001', 'taskfindex/list', '项目任务_任务附件索引', null, '20200106121003', '20200106121003', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');
INSERT INTO `alone_auth_action` VALUES ('4028840e77cdd4f20177cdd800a40002', 'company/list', '项目任务_业务机构', null, '20210223154341', '20210223154341', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '1', '1');

-- ----------------------------
-- Table structure for `alone_auth_actiontree`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_actiontree`;
CREATE TABLE `alone_auth_actiontree` (
  `ID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) NOT NULL COMMENT '分类、菜单、权限',
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `UUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `ACTIONID` varchar(32) DEFAULT NULL,
  `DOMAIN` varchar(64) NOT NULL,
  `ICON` varchar(64) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  `PARAMS` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_7` (`ACTIONID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`action`) REFER `alone/alone_action`';

-- ----------------------------
-- Records of alone_auth_actiontree
-- ----------------------------
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38b93d0a0023', '10', '40288b854a38408e014a384c4f3c0002', '系统日志', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38b93d0a0023', '', '2', '20141211174111', '20141211174111', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38b93d0a0022', 'alone', 'icon-tip', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38b3a6700018', '2', '40288b854a38408e014a384c4f3c0002', '权限构造', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38b3a6700018', '', '2', '20141211173505', '20141211173505', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38b3a6700017', 'alone', 'icon-category', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38408e014a384c4f3c0002', '1', 'NONE', '系统配置', '40288b854a38408e014a384c4f3c0002', '', '1', '20141211154212', '20171216131317', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-sprocket_dark', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bafb830027', '3', '40288b854a38408e014a384c4f3c0002', '参数设置', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bafb830027', '', '2', '20141211174305', '20151016155834', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bafb830026', 'alone', 'icon-sprocket_dark', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bd26a0002b', '5', '40288b854a38408e014a384c4f3c0002', '数据字典', '40288b854a38408e014a384c4f3c000240288b854a38974f014a38bd26a0002b', '', '2', '20141211174527', '20141211174527', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bd26a0002a', 'alone', 'icon-address-book', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38be1805002c', '2', 'NONE', '用户管理', '40288b854a38974f014a38be1805002c', '', '1', '20141211174629', '20170319103822', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-group_green_edit', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38beae79002e', '1', '40288b854a38974f014a38be1805002c', '用户管理', '40288b854a38974f014a38be1805002c40288b854a38974f014a38beae79002e', '', '2', '20141211174708', '20141211174708', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38beae79002d', 'alone', 'icon-hire-me', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('40288b854a38974f014a38bf10fc0030', '2', '40288b854a38974f014a38be1805002c', '组织管理', '40288b854a38974f014a38be1805002c40288b854a38974f014a38bf10fc0030', '', '2', '20141211174733', '20170319103800', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38bf10fc002f', 'alone', 'icon-customers', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888ac506eb41b01506fa9b0a30027', '1', '40288b854a38408e014a384c4f3c0002', '注册参数', '40288b854a38408e014a384c4f3c0002402888ac506eb41b01506fa9b0a30027', '', '2', '20151016160003', '20151016160003', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38974f014a38ba24f90024', 'alone', 'icon-edit', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402888a84f835c36014f835f72cb000e', '1', '40288b854a38408e014a384c4f3c0002', '权限定义', '40288b854a38408e014a384c4f3c0002402888a84f835c36014f835f72cb000e', '', '2', '20150831184834', '20150831184834', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '40288b854a38408e014a384de88a0005', 'alone', 'icon-communication', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('8a2831b3573c10d0015741b2273c0019', '6', '40288b854a38408e014a384c4f3c0002', '在线用户', '40288b854a38408e014a384c4f3c00028a2831b3573c10d0015741b2273c0019', '', '2', '20160919170617', '20160919170617', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '8a2831b3573c10d0015741b2273c0018', 'alone', 'icon-group_green_new', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('8af5c60d5c215c54015c216201ce0003', '3', '40288b854a38974f014a38be1805002c', '外部账户', '40288b854a38974f014a38be1805002c8af5c60d5c215c54015c216201ce0003', '', '2', '20170519234450', '20170519234450', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '8af5c60d5c215c54015c216201ce0002', 'alone', 'icon-group_green_new', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402889ac5da5d50f015da5d736110001', '15', '40288b854a38408e014a384c4f3c0002', '系统备份向导', '40288b854a38408e014a384c4f3c0002402889ac5da5d50f015da5d736110001', '', '2', '20170803100531', '20170803100623', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402889ac5da5d50f015da5d736060000', 'alone', 'icon-save', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b88169fd1b650169fd1d5a230001', '3', 'NONE', '文件管理', '4028b88169fd1b650169fd1d5a230001', '', '1', '20190408212233', '20190408212233', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-archives', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b88169fd1b650169fd1e8fd00003', '1', '4028b88169fd1b650169fd1d5a230001', '资源库管理', '4028b88169fd1b650169fd1d5a2300014028b88169fd1b650169fd1e8fd00003', '', '2', '20190408212352', '20190408212352', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028b88169fd1b650169fd1e8fcf0002', 'alone', 'icon-database', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b88169fd1b650169fd3a1e52000c', '2', '4028b88169fd1b650169fd1d5a230001', '文件管理', '4028b88169fd1b650169fd1d5a2300014028b88169fd1b650169fd3a1e52000c', '', '2', '20190408215358', '20190408215358', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028b88169fd1b650169fd3a1e52000b', 'alone', 'icon-blogs', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b8816a19c998016a19cb3a500001', '4', 'NONE', '文件预览', '4028b8816a19c998016a19cb3a500001', '', '1', '20190414110150', '20190414110150', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, 'alone', 'icon-networking', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b8816a19c998016a19cd402d0003', '1', '4028b8816a19c998016a19cb3a500001', '文件模型', '4028b8816a19c998016a19cb3a5000014028b8816a19c998016a19cd402d0003', '', '2', '20190414110402', '20190414110402', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028b8816a19c998016a19cd402d0002', 'alone', 'icon-document-library', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b8816a19c998016a19ce87330005', '2', '4028b8816a19c998016a19cb3a500001', '文件转换器', '4028b8816a19c998016a19cb3a5000014028b8816a19c998016a19ce87330005', '', '2', '20190414110526', '20190414110526', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028b8816a19c998016a19ce87330004', 'alone', 'icon-print', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b8816a19c998016a19d08d2e0007', '3', '4028b8816a19c998016a19cb3a500001', '转换规则', '4028b8816a19c998016a19cb3a5000014028b8816a19c998016a19d08d2e0007', '', '2', '20190414110739', '20190414110739', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028b8816a19c998016a19d08d2e0006', 'alone', 'icon-invoice', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b8816a1fb82a016a1fbf78460008', '4', '4028b8816a19c998016a19cb3a500001', '转换任务', '4028b8816a19c998016a19cb3a5000014028b8816a1fb82a016a1fbf78460008', '', '2', '20190415144642', '20190415144642', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028b8816a1fb82a016a1fbf78460007', 'alone', 'icon-future-projects', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028b8816a2673b5016a2674ebd00002', '5', '4028b8816a19c998016a19cb3a500001', '预览文件', '4028b8816a19c998016a19cb3a5000014028b8816a2673b5016a2674ebd00002', '', '2', '20190416220237', '20190416220237', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028b8816a2673b5016a2674ebd00001', 'alone', 'icon-blog', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e86ebfa71a016ebfa9d3790001', '5', 'NONE', '项目定义', '402880e86ebfa71a016ebfa9d3790001', '', '1', '20191201121328', '20191201215032', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-project', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e86ebfa71a016ebfaa733a0003', '5', '402880e86ebfa71a016ebfa9d3790001', '项目分类配置', '402880e86ebfa71a016ebfa9d3790001402880e86ebfa71a016ebfaa733a0003', '', '2', '20191201121409', '20191228133955', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402880e86ebfa71a016ebfaa733a0002', 'alone', 'icon-project', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e86ec043a8016ec0500cfd0004', '3', '402880e86ebfa71a016ebfa9d3790001', '阶段分类配置', '402880e86ebfa71a016ebfa9d3790001402880e86ec043a8016ec0500cfd0004', '', '2', '20191201151502', '20191201215553', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402880e86ec043a8016ec0500cfd0003', 'alone', 'icon-administrative-docs', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402880e86ec06ee4016ec0bcbb9e0007', '4', '402880e86ebfa71a016ebfa9d3790001', '任务分类配置', '402880e86ebfa71a016ebfa9d3790001402880e86ec06ee4016ec0bcbb9e0007', '', '2', '20191201171344', '20191228134001', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402880e86ec06ee4016ec0bcbb9e0006', 'alone', 'icon-blogs', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402884116ecf45d4016ecf474ebc0001', '6', 'NONE', '项目任务', '402884116ecf45d4016ecf474ebc0001', '', '1', '20191204125947', '20191204155641', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', 'alone', 'icon-invoice', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402884116ecf45d4016ecf47f1c90003', '1', '402884116ecf45d4016ecf474ebc0001', '项目管理', '402884116ecf45d4016ecf474ebc0001402884116ecf45d4016ecf47f1c90003', '', '2', '20191204130028', '20191204130028', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402884116ecf45d4016ecf47f1c90002', 'alone', 'icon-consulting', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402884116ecfe8a8016ecfe9c17d0002', '2', '402884116ecf45d4016ecf474ebc0001', '任务管理', '402884116ecf45d4016ecf474ebc0001402884116ecfe8a8016ecfe9c17d0002', '', '2', '20191204155713', '20191204155713', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402884116ecfe8a8016ecfe9c17d0001', 'alone', 'icon-client_account_template', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402884166f3b2fe5016f3b3278e30003', '1', '402880e86ebfa71a016ebfa9d3790001', '属性分类配置', '402880e86ebfa71a016ebfa9d3790001402884166f3b2fe5016f3b3278e30003', '', '2', '20191225115601', '20191225115828', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402884166f3b2fe5016f3b3278e30002', 'alone', 'icon-issue', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402884166f3b2fe5016f3b338bef0005', '2', '402880e86ebfa71a016ebfa9d3790001', '属性定义管理', '402880e86ebfa71a016ebfa9d3790001402884166f3b2fe5016f3b338bef0005', '', '2', '20191225115711', '20191225115838', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402884166f3b2fe5016f3b338bef0004', 'alone', 'icon-finished-work', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('402881ec6f79086c016f790ba3820002', '3', '402884116ecf45d4016ecf474ebc0001', '任务附件索引', '402884116ecf45d4016ecf474ebc0001402881ec6f79086c016f790ba3820002', '', '2', '20200106121003', '20200106121003', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '402881ec6f79086c016f790ba3820001', 'alone', 'icon-report', null, '');
INSERT INTO `alone_auth_actiontree` VALUES ('4028840e77cdd4f20177cdd800a50003', '3', '402884116ecf45d4016ecf474ebc0001', '业务机构', '402884116ecf45d4016ecf474ebc00014028840e77cdd4f20177cdd800a50003', '', '2', '20210223154341', '20210223154341', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '4028840e77cdd4f20177cdd800a40002', 'alone', 'icon-featured', null, '');

-- ----------------------------
-- Table structure for `alone_auth_organization`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_organization`;
CREATE TABLE `alone_auth_organization` (
  `ID` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `NAME` varchar(64) NOT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `STATE` char(1) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `SORT` int(11) DEFAULT NULL,
  `TYPE` char(1) NOT NULL COMMENT '组织类型：1科室、2班组、3队组、0其他',
  `APPID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='组织类型：科室、班组、队组、其他';

-- ----------------------------
-- Records of alone_auth_organization
-- ----------------------------
INSERT INTO `alone_auth_organization` VALUES ('402881ec6f8f7fee016f8f8289ec0001', '402881ec6f8f7fee016f8f8289ec0001', '', '公司', '20200110205134', '20200110205134', '1', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', 'NONE', '1', '1', null);

-- ----------------------------
-- Table structure for `alone_auth_outuser`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_outuser`;
CREATE TABLE `alone_auth_outuser` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `ACCOUNTID` varchar(64) NOT NULL,
  `ACCOUNTNAME` varchar(64) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_54` (`USERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_outuser
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_pop`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_pop`;
CREATE TABLE `alone_auth_pop` (
  `ID` varchar(32) NOT NULL,
  `POPTYPE` varchar(1) NOT NULL COMMENT '1人、2组织机构、3岗位',
  `OID` varchar(32) NOT NULL COMMENT '人ID、组织机构ID、岗位ID',
  `ONAME` varchar(128) NOT NULL COMMENT '人NAME、组织机构NAME、岗位NAME',
  `TARGETTYPE` varchar(64) NOT NULL COMMENT '权限业务类型',
  `TARGETID` varchar(32) NOT NULL COMMENT '权限业务ID',
  `TARGETNAME` varchar(128) DEFAULT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_pop
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_post`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_post`;
CREATE TABLE `alone_auth_post` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `ORGANIZATIONID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `EXTENDIS` varchar(2) NOT NULL COMMENT '0:否1:是（默认否）',
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_51` (`ORGANIZATIONID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_post
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_postaction`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_postaction`;
CREATE TABLE `alone_auth_postaction` (
  `ID` varchar(32) NOT NULL,
  `MENUID` varchar(32) NOT NULL,
  `POSTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_8` (`MENUID`),
  KEY `FK_Reference_9` (`POSTID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`actionid`) REFER `alone/alone_actio';

-- ----------------------------
-- Records of alone_auth_postaction
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_user`;
CREATE TABLE `alone_auth_user` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(64) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL COMMENT 'MD5(password+loginname)',
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) DEFAULT NULL COMMENT '1:系统用户:2其他3超级用户',
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `LOGINNAME` varchar(64) NOT NULL,
  `LOGINTIME` varchar(14) DEFAULT NULL,
  `IMGID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_user';

-- ----------------------------
-- Records of alone_auth_user
-- ----------------------------
INSERT INTO `alone_auth_user` VALUES ('40288b854a329988014a329a12f30002', '系统管理员', '6C832EDB4410D94088918F240949593F', '', '3', '20141210130925', '20200123115557', 'userId', '40288b854a329988014a329a12f30002', '1', 'sysadmin', '20210925072410', '4028b88169a85b800169a87b98a10002');

-- ----------------------------
-- Table structure for `alone_auth_userorg`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_userorg`;
CREATE TABLE `alone_auth_userorg` (
  `ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `ORGANIZATIONID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `DOC_USERID` (`USERID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; (`organizationid`) REFER `alone/alone';

-- ----------------------------
-- Records of alone_auth_userorg
-- ----------------------------
INSERT INTO `alone_auth_userorg` VALUES ('402880e86fd088c2016fd08ad6000007', '40288b854a329988014a329a12f30002', '402881ec6f8f7fee016f8f8289ec0001');

-- ----------------------------
-- Table structure for `alone_auth_userpost`
-- ----------------------------
DROP TABLE IF EXISTS `alone_auth_userpost`;
CREATE TABLE `alone_auth_userpost` (
  `ID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `POSTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_auth_userpost
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_dictionary_entity`
-- ----------------------------
DROP TABLE IF EXISTS `alone_dictionary_entity`;
CREATE TABLE `alone_dictionary_entity` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL DEFAULT '',
  `NAME` varchar(128) NOT NULL,
  `ENTITYINDEX` varchar(128) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `TYPE` char(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB';

-- ----------------------------
-- Records of alone_dictionary_entity
-- ----------------------------
INSERT INTO `alone_dictionary_entity` VALUES ('4028840e77cdd4f20177cddbd8cd0005', '202102231547', '202102231547', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '业务机构类型', 'COMPANY_TYPE_ENUM', '{私企:1, 国企:2, 政府机构:3, 军队:4}', '1');

-- ----------------------------
-- Table structure for `alone_dictionary_type`
-- ----------------------------
DROP TABLE IF EXISTS `alone_dictionary_type`;
CREATE TABLE `alone_dictionary_type` (
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `STATE` char(1) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `COMMENTS` varchar(128) DEFAULT NULL,
  `ENTITYTYPE` varchar(128) NOT NULL,
  `ENTITY` varchar(32) NOT NULL,
  `ID` varchar(32) NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TREECODE` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_8` (`ENTITY`) USING BTREE,
  CONSTRAINT `alone_dictionary_type_ibfk_1` FOREIGN KEY (`ENTITY`) REFERENCES `alone_dictionary_entity` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB alone_dictionary_type';

-- ----------------------------
-- Records of alone_dictionary_type
-- ----------------------------
INSERT INTO `alone_dictionary_type` VALUES ('202102231548', '202102231549', '40288b854a329988014a329a12f30002', '系统管理员', '1', '私企', '', '1', '4028840e77cdd4f20177cddbd8cd0005', '4028840e77cdd4f20177cddc2e460006', '1', 'NONE', '4028840e77cdd4f20177cddc2e460006');
INSERT INTO `alone_dictionary_type` VALUES ('202102231549', '202102231549', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '国企', '', '2', '4028840e77cdd4f20177cddbd8cd0005', '4028840e77cdd4f20177cddda24a0007', '2', 'NONE', '4028840e77cdd4f20177cddda24a0007');
INSERT INTO `alone_dictionary_type` VALUES ('202102231550', '202102231550', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '政府机构', '', '3', '4028840e77cdd4f20177cddbd8cd0005', '4028840e77cdd4f20177cdddf6a00008', '3', 'NONE', '4028840e77cdd4f20177cdddf6a00008');
INSERT INTO `alone_dictionary_type` VALUES ('202102231550', '202102231550', '40288b854a329988014a329a12f30002', '系统管理员', '1', '军队', '', '4', '4028840e77cdd4f20177cddbd8cd0005', '4028840e77cdd4f20177cdde1b790009', '4', 'NONE', '4028840e77cdd4f20177cdde1b790009');

-- ----------------------------
-- Table structure for `alone_parameter`
-- ----------------------------
DROP TABLE IF EXISTS `alone_parameter`;
CREATE TABLE `alone_parameter` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(12) NOT NULL,
  `UTIME` varchar(12) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `MUSER` varchar(32) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `STATE` char(1) NOT NULL,
  `PKEY` varchar(64) NOT NULL,
  `PVALUE` varchar(2048) NOT NULL,
  `RULES` varchar(256) DEFAULT NULL,
  `DOMAIN` varchar(64) DEFAULT NULL,
  `COMMENTS` varchar(256) DEFAULT NULL,
  `VTYPE` char(1) NOT NULL COMMENT ' 文本：1 枚举：2',
  `USERABLE` varchar(1) NOT NULL COMMENT '0否，1是',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 12288 kB; InnoDB free: 12288 kB alone_parameter';

-- ----------------------------
-- Records of alone_parameter
-- ----------------------------

-- ----------------------------
-- Table structure for `alone_parameter_local`
-- ----------------------------
DROP TABLE IF EXISTS `alone_parameter_local`;
CREATE TABLE `alone_parameter_local` (
  `ID` varchar(32) NOT NULL,
  `PARAMETERID` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PVALUE` varchar(2048) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_50` (`PARAMETERID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alone_parameter_local
-- ----------------------------

-- ----------------------------
-- Table structure for `plogs_field`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_field`;
CREATE TABLE `plogs_field` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `NAME` varchar(256) NOT NULL,
  `TYPEID` varchar(32) NOT NULL,
  `LIVETIME` varchar(16) NOT NULL,
  `VALTYPE` varchar(2) NOT NULL COMMENT '1.富文本，2附件，3多行文本，4单行文本，5枚举单选，6枚举多选，7公司，8人员，9时间，10日期',
  `UUID` varchar(32) NOT NULL COMMENT '原创阶段的ID和UUID值一样，拷贝的ID和UUID值不一样，拷贝的阶段名称不能编辑',
  `SORT` int(11) NOT NULL,
  `SINGLETYPE` varchar(2) NOT NULL COMMENT '1项目中唯一属性，2项目中运行多条',
  `ENUMS` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_28` (`TYPEID`),
  CONSTRAINT `FK_Reference_28` FOREIGN KEY (`TYPEID`) REFERENCES `plogs_field_type` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_field
-- ----------------------------
INSERT INTO `plogs_field` VALUES ('2c905d086f61371d016f619b90e00061', '20200101225622', '20200101232236', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '如WCP正在使用版本', '软件当前版本', '2c905d086f61371d016f619ae3990060', '20200101232236', '4', 'c06c426452fe43448d1e4850827c9251', '6', '1', '');
INSERT INTO `plogs_field` VALUES ('2c905d086f61371d016f61b4d1460083', '20200101232357', '20200101232357', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '历史版本简介', '2c905d086f61371d016f619ae3990060', '20200101232357', '3', 'a69f25cae0464aaebab52c5315e25e6c', '7', '2', '');
INSERT INTO `plogs_field` VALUES ('2c905d086f61bea7016f6510531100d7', '20200102150246', '20200102150246', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '访问地址', '402884166f3b56c8016f3b6e48470009', '20200102150246', '4', '9095483072ae4c81af37c5483ee32a09', '10', '2', '');
INSERT INTO `plogs_field` VALUES ('402880e86f4aa3fc016f4aa65c44000b', '20191228115654', '20200101164114', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '签订合同', '402880e86f4aa3fc016f4aa4a5290001', '20200101164114', '5', 'BOOKIS', '1', '1', '1:是,0:否');
INSERT INTO `plogs_field` VALUES ('402880e86f4aa3fc016f4aa65c4b000d', '20191228115654', '20191229103250', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '已开发票', '402880e86f4aa3fc016f4aa4a5290001', '20191229103250', '5', 'BILLIS', '4', '1', '1:是,0:否');
INSERT INTO `plogs_field` VALUES ('402880e86f4aa3fc016f4aa65c51000e', '20191228115654', '20200101164109', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '完成部署', '402880e86f4aa3fc016f4aa4a5290001', '20200101164109', '5', 'BUILDIS', '2', '1', '1:是,0:否');
INSERT INTO `plogs_field` VALUES ('402880e86f4aa3fc016f4aa65c54000f', '20191228115654', '20191229103250', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '合同金额', '402880e86f4aa3fc016f4aa4a5290001', '20191229103250', '4', 'BOOKPRICE', '3', '1', null);
INSERT INTO `plogs_field` VALUES ('402881ec6f4cb63d016f4cba565b0002', '20191228213757', '20200101225657', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '完成回款', '402880e86f4aa3fc016f4aa4a5290001', '20200101225657', '5', 'PAYIS', '5', '1', '1:是,0:否');
INSERT INTO `plogs_field` VALUES ('402881ec6f4f3874016f4f3c034b0001', '20191229091850', '20191229091911', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '公司名称', '402884166f3b56c8016f3b6e927b000a', '20191229091911', '4', 'd45953196f904759815abc091e3fe9db', '11', '1', '');
INSERT INTO `plogs_field` VALUES ('402881ec6f4f3874016f4f3ddb130002', '20191229092051', '20200101103319', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '已回款额', '402884166f3b56c8016f3b6e2ba50008', '20200101103319', '4', 'ec013da02fed442c86e171fb3d703d1c', '8', '2', '');
INSERT INTO `plogs_field` VALUES ('402881ec6f4fe6ed016f4ff065d20001', '20191229123552', '20200101225049', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '快递信息', '402884166f3b56c8016f3b6e927b000a', '20200101225049', '3', '6e2f29f4e0f5417db9e5a4a2a8020203', '12', '2', '');
INSERT INTO `plogs_field` VALUES ('402881ec6f507662016f509a6b33001d', '20191229154134', '20200101164137', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '交付日期', '402884166f3b56c8016f3b6e48470009', '20200101164137', '10', '882003bd9b664cbea3ae0e2ac649f15f', '9', '1', '');

-- ----------------------------
-- Table structure for `plogs_field_reptype`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_field_reptype`;
CREATE TABLE `plogs_field_reptype` (
  `ID` varchar(32) NOT NULL,
  `PROJECTTYPEID` varchar(32) NOT NULL,
  `FIELDID` varchar(32) NOT NULL,
  `READIS` varchar(1) NOT NULL COMMENT '1.默认公開,0默认隐藏',
  `WRITEIS` varchar(1) NOT NULL COMMENT '1.默认公開,0默认禁止',
  `SORT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_95` (`FIELDID`),
  KEY `FK_Reference_96` (`PROJECTTYPEID`),
  CONSTRAINT `FK_Reference_95` FOREIGN KEY (`FIELDID`) REFERENCES `plogs_field` (`ID`),
  CONSTRAINT `FK_Reference_96` FOREIGN KEY (`PROJECTTYPEID`) REFERENCES `plogs_project_type` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_field_reptype
-- ----------------------------
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6195b5bd0023', '402880e86ec198bd016ec1bc86220002', '402880e86f4aa3fc016f4aa65c44000b', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6195b5cd0024', '402880e86ec198bd016ec1bc86220002', '402880e86f4aa3fc016f4aa65c51000e', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6195b5cd0025', '402880e86ec198bd016ec1bc86220002', '402880e86f4aa3fc016f4aa65c54000f', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6195b5cd0026', '402880e86ec198bd016ec1bc86220002', '402880e86f4aa3fc016f4aa65c4b000d', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6195b5dc0027', '402880e86ec198bd016ec1bc86220002', '402881ec6f4cb63d016f4cba565b0002', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6195b5dc0028', '402880e86ec198bd016ec1bc86220002', '402881ec6f4f3874016f4f3ddb130002', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6195b5dc0029', '402880e86ec198bd016ec1bc86220002', '402881ec6f4f3874016f4f3c034b0001', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6195b5ec002a', '402880e86ec198bd016ec1bc86220002', '402881ec6f4fe6ed016f4ff065d20001', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f619736fd002b', '402880e86ec198bd016ec1cd8f6a000d', '402880e86f4aa3fc016f4aa65c44000b', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f619736fd002c', '402880e86ec198bd016ec1cd8f6a000d', '402880e86f4aa3fc016f4aa65c51000e', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6197370d002d', '402880e86ec198bd016ec1cd8f6a000d', '402880e86f4aa3fc016f4aa65c54000f', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6197370d002e', '402880e86ec198bd016ec1cd8f6a000d', '402880e86f4aa3fc016f4aa65c4b000d', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6197371d002f', '402880e86ec198bd016ec1cd8f6a000d', '402881ec6f4cb63d016f4cba565b0002', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6197371d0030', '402880e86ec198bd016ec1cd8f6a000d', '402881ec6f4f3874016f4f3ddb130002', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6197372c0031', '402880e86ec198bd016ec1cd8f6a000d', '402881ec6f507662016f509a6b33001d', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6197372c0032', '402880e86ec198bd016ec1cd8f6a000d', '402881ec6f4f3874016f4f3c034b0001', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f6197373c0033', '402880e86ec198bd016ec1cd8f6a000d', '402881ec6f4fe6ed016f4ff065d20001', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f619c6da30062', '402880e86ec198bd016ec1bc86220002', '2c905d086f61371d016f619b90e00061', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f619c956f0063', '402880e86ec198bd016ec1cd8f6a000d', '2c905d086f61371d016f619b90e00061', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f61b55a3d0084', '402880e86ec198bd016ec1cde1a8000e', '402881ec6f507662016f509a6b33001d', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f61b57e4f0085', '402880e86ec198bd016ec1cde1a8000e', '2c905d086f61371d016f619b90e00061', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61371d016f61b57e5f0086', '402880e86ec198bd016ec1cde1a8000e', '2c905d086f61371d016f61b4d1460083', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61bea7016f6510e69700d8', '402880e86ec198bd016ec1bc86220002', '2c905d086f61bea7016f6510531100d7', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61bea7016f6511298c00d9', '402880e86ec198bd016ec1cd8f6a000d', '2c905d086f61bea7016f6510531100d7', '1', '1', '99');
INSERT INTO `plogs_field_reptype` VALUES ('2c905d086f61bea7016f65115ed500da', '402880e86ec198bd016ec1cde1a8000e', '2c905d086f61bea7016f6510531100d7', '1', '1', '99');

-- ----------------------------
-- Table structure for `plogs_field_rettype`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_field_rettype`;
CREATE TABLE `plogs_field_rettype` (
  `ID` varchar(32) NOT NULL,
  `FIELDID` varchar(32) NOT NULL,
  `TASKTYPEID` varchar(32) NOT NULL,
  `READIS` varchar(1) NOT NULL COMMENT '1.公开（仅有公开，备用选项）',
  `WRITEIS` varchar(1) NOT NULL COMMENT '1.公开,0禁止',
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_32` (`TASKTYPEID`),
  KEY `FK_Reference_94` (`FIELDID`),
  CONSTRAINT `FK_Reference_32` FOREIGN KEY (`TASKTYPEID`) REFERENCES `plogs_task_type` (`ID`),
  CONSTRAINT `FK_Reference_94` FOREIGN KEY (`FIELDID`) REFERENCES `plogs_field` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_field_rettype
-- ----------------------------

-- ----------------------------
-- Table structure for `plogs_field_type`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_field_type`;
CREATE TABLE `plogs_field_type` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `NAME` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_field_type
-- ----------------------------
INSERT INTO `plogs_field_type` VALUES ('2c905d086f61371d016f619ae3990060', '20200101225538', '20200101225538', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '5', 'NONE', '产品特性', '2c905d086f61371d016f619ae3990060');
INSERT INTO `plogs_field_type` VALUES ('402880e86f4aa3fc016f4aa4a5290001', '20191228115501', '20191228115522', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', 'NONE', '系统属性', '402880e86f4aa3fc016f4aa4a5290001');
INSERT INTO `plogs_field_type` VALUES ('402884166f3b56c8016f3b6e2ba50008', '20191225130113', '20191228115508', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '4', 'NONE', '合同信息', '402884166f3b56c8016f3b6e2ba50008');
INSERT INTO `plogs_field_type` VALUES ('402884166f3b56c8016f3b6e48470009', '20191225130120', '20191225130120', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', 'NONE', '部署信息', '402884166f3b56c8016f3b6e48470009');
INSERT INTO `plogs_field_type` VALUES ('402884166f3b56c8016f3b6e927b000a', '20191225130139', '20191225130139', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', 'NONE', '公司信息', '402884166f3b56c8016f3b6e927b000a');

-- ----------------------------
-- Table structure for `plogs_project`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_project`;
CREATE TABLE `plogs_project` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `NAME` varchar(128) NOT NULL,
  `STATE` varchar(1) NOT NULL COMMENT '1.激活2.关闭3.删除',
  `LIVETIME` varchar(16) NOT NULL,
  `GRADATIONID` varchar(32) NOT NULL,
  `GRADATIONNAME` varchar(128) NOT NULL,
  `TYPEID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_project
-- ----------------------------
INSERT INTO `plogs_project` VALUES ('402880e86fd1985d016fd19d4fd70002', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '', '测试项目', '1', '20200123165545', '402880e86ec193b8016ec194c35b0002', '前期商务沟通', '402880e86ec198bd016ec1bc86220002');

-- ----------------------------
-- Table structure for `plogs_project_company`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_project_company`;
CREATE TABLE `plogs_project_company` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `LIVETIME` varchar(16) NOT NULL,
  `NAME` varchar(512) NOT NULL,
  `TYPE` varchar(128) NOT NULL,
  `TEXTFILEID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_project_company
-- ----------------------------
INSERT INTO `plogs_project_company` VALUES ('40289f787c1a1dc2017c1a204e6e0001', '20210925072457', '20210925072457', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '20210925072457', '太原扁舟科技有限责任公司', '0', null);

-- ----------------------------
-- Table structure for `plogs_project_fieldins`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_project_fieldins`;
CREATE TABLE `plogs_project_fieldins` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `LIVETIME` varchar(16) NOT NULL,
  `UUID` varchar(32) NOT NULL COMMENT '原创阶段的ID和UUID值一样，拷贝的ID和UUID值不一样，拷贝的阶段名称不能编辑',
  `SORT` int(11) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `NAME` varchar(256) NOT NULL,
  `VALTYPE` varchar(2) NOT NULL COMMENT '1.富文本，2附件，3多行文本，4单行文本，5枚举单选，6枚举多选，7公司，8人员，9时间，10日期',
  `VALTITLE` varchar(512) DEFAULT NULL,
  `VAL` varchar(32) DEFAULT NULL,
  `ENUMS` varchar(512) DEFAULT NULL,
  `HTMLDEMOID` varchar(32) DEFAULT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `PUBREAD` varchar(2) DEFAULT NULL COMMENT '0.默认隐藏，1公开信息',
  `SINGLETYPE` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_30` (`PROJECTID`),
  CONSTRAINT `FK_Reference_30` FOREIGN KEY (`PROJECTID`) REFERENCES `plogs_project` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_project_fieldins
-- ----------------------------
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50910003', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', 'BOOKIS', '1', null, '签订合同', '5', null, null, '1:是,0:否', null, '402880e86fd1985d016fd19d4fd70002', '1', '1');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d509d0004', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', 'BUILDIS', '2', null, '完成部署', '5', null, null, '1:是,0:否', null, '402880e86fd1985d016fd19d4fd70002', '1', '1');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50a60005', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', 'BOOKPRICE', '3', null, '合同金额', '4', null, null, null, null, '402880e86fd1985d016fd19d4fd70002', '1', '1');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50ad0006', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', 'BILLIS', '4', null, '已开发票', '5', null, null, '1:是,0:否', null, '402880e86fd1985d016fd19d4fd70002', '1', '1');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50b60007', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', 'PAYIS', '5', null, '完成回款', '5', null, null, '1:是,0:否', null, '402880e86fd1985d016fd19d4fd70002', '1', '1');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50bf0008', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', 'ec013da02fed442c86e171fb3d703d1c', '8', null, '已回款额', '4', null, null, '', null, '402880e86fd1985d016fd19d4fd70002', '1', '2');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50c80009', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', 'd45953196f904759815abc091e3fe9db', '11', null, '公司名称', '4', null, null, '', null, '402880e86fd1985d016fd19d4fd70002', '1', '1');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50d1000a', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', '6e2f29f4e0f5417db9e5a4a2a8020203', '12', null, '快递信息', '3', null, null, '', null, '402880e86fd1985d016fd19d4fd70002', '1', '2');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50da000b', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', 'c06c426452fe43448d1e4850827c9251', '6', null, '软件当前版本', '4', null, null, '', null, '402880e86fd1985d016fd19d4fd70002', '1', '1');
INSERT INTO `plogs_project_fieldins` VALUES ('402880e86fd1985d016fd19d50e2000c', '20200123165545', '20200123165545', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '20200123165545', '9095483072ae4c81af37c5483ee32a09', '10', null, '访问地址', '4', null, null, '', null, '402880e86fd1985d016fd19d4fd70002', '1', '2');

-- ----------------------------
-- Table structure for `plogs_project_gradation`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_project_gradation`;
CREATE TABLE `plogs_project_gradation` (
  `ID` varchar(32) NOT NULL,
  `UUID` varchar(32) NOT NULL COMMENT '原创阶段的ID和UUID值一样，拷贝的ID和UUID值不一样，拷贝的阶段名称不能编辑',
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL COMMENT '1，可用，0停用，2刪除',
  `PCONTENT` varchar(128) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `MODEL` varchar(2) NOT NULL COMMENT '0分类：可以下挂分类和类型，1类型：必须为叶子节点',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_project_gradation
-- ----------------------------
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df90006', '20191209214328', '20191209214328', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '非标准阶段', 'NONE', '2c905d086eeadbe2016eeae68df90006', '0');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086eeadbe2016eeae6d0040007', '2c905d086eeadbe2016eeae6d0040007', '20191209214345', '20200101234319', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '6', '日常', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086eeadbe2016eeae6d0040007', '1');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086eeadbe2016eeae935f80010', '2c905d086eeadbe2016eeae935f80010', '20191209214622', '20200101233914', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '9', '其他', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086eeadbe2016eeae935f80010', '1');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086eeadbe2016eeae977560011', '2c905d086eeadbe2016eeae977560011', '20191209214639', '20191209214639', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '5', '生活', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086eeadbe2016eeae977560011', '1');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086f61bea7016f61c1eee50004', '2c905d086f61bea7016f61c1eee50004', '20200101233817', '20200101233817', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '财务', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086f61bea7016f61c1eee50004', '1');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086f61bea7016f61c207ba0005', '2c905d086f61bea7016f61c207ba0005', '20200101233823', '20200101233823', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '税务', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086f61bea7016f61c207ba0005', '1');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086f61bea7016f61c238e80006', '2c905d086f61bea7016f61c238e80006', '20200101233836', '20200101233836', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '工商', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086f61bea7016f61c238e80006', '1');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086f61bea7016f61c264d70007', '2c905d086f61bea7016f61c264d70007', '20200101233847', '20200101233847', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '4', '客户', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086f61bea7016f61c264d70007', '1');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086f61bea7016f61c299ce0008', '2c905d086f61bea7016f61c299ce0008', '20200101233900', '20200101233942', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '7', '设计', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086f61bea7016f61c299ce0008', '1');
INSERT INTO `plogs_project_gradation` VALUES ('2c905d086f61bea7016f61c401130009', '2c905d086f61bea7016f61c401130009', '20200101234032', '20200101234032', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '8', '推广', '2c905d086eeadbe2016eeae68df90006', '2c905d086eeadbe2016eeae68df900062c905d086f61bea7016f61c401130009', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec18cfb016ec190d4c00002', '402880e86ec18cfb016ec190d4c00002', '20191201210524', '20191201210524', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '标准阶段', 'NONE', '402880e86ec18cfb016ec190d4c00002', '0');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec193b8016ec19482a20001', '402880e86ec193b8016ec19482a20001', '20191201210925', '20191204143116', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '自研阶段', 'NONE', '402880e86ec193b8016ec19482a20001', '0');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec193b8016ec194c35b0002', '402880e86ec193b8016ec194c35b0002', '20191201210942', '20191201210942', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '前期商务沟通', '402880e86ec18cfb016ec190d4c00002', '402880e86ec18cfb016ec190d4c00002402880e86ec193b8016ec194c35b0002', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec198bd016ec1ca9c2c0007', '402880e86ec198bd016ec1ca9c2c0007', '20191201220831', '20191201220831', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '合同签订阶段', '402880e86ec18cfb016ec190d4c00002', '402880e86ec18cfb016ec190d4c00002402880e86ec198bd016ec1ca9c2c0007', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec198bd016ec1cac40e0008', '402880e86ec198bd016ec1cac40e0008', '20191201220841', '20191201220841', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '4', '定制开发阶段', '402880e86ec18cfb016ec190d4c00002', '402880e86ec18cfb016ec190d4c00002402880e86ec198bd016ec1cac40e0008', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec198bd016ec1cafc6c0009', '402880e86ec198bd016ec1cafc6c0009', '20191201220855', '20191201220855', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '5', '部署实施阶段', '402880e86ec18cfb016ec190d4c00002', '402880e86ec18cfb016ec190d4c00002402880e86ec198bd016ec1cafc6c0009', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec198bd016ec1cb3452000a', '402880e86ec198bd016ec1cb3452000a', '20191201220910', '20191201220910', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '6', '产品验收阶段', '402880e86ec18cfb016ec190d4c00002', '402880e86ec18cfb016ec190d4c00002402880e86ec198bd016ec1cb3452000a', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec198bd016ec1cb6db8000b', '402880e86ec198bd016ec1cb6db8000b', '20191201220924', '20191201220924', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '7', '售后服务阶段', '402880e86ec18cfb016ec190d4c00002', '402880e86ec18cfb016ec190d4c00002402880e86ec198bd016ec1cb6db8000b', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402880e86ec198bd016ec1cbfe78000c', '402880e86ec198bd016ec1cbfe78000c', '20191201221001', '20191201221001', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '产品试用阶段', '402880e86ec18cfb016ec190d4c00002', '402880e86ec18cfb016ec190d4c00002402880e86ec198bd016ec1cbfe78000c', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402884116ecf885d016ecf9b5b1b0003', '402884116ecf885d016ecf9b5b1b0003', '20191204143135', '20191204143135', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '前期规划', '402880e86ec193b8016ec19482a20001', '402880e86ec193b8016ec19482a20001402884116ecf885d016ecf9b5b1b0003', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402884116ecf885d016ecf9b8a130004', '402884116ecf885d016ecf9b8a130004', '20191204143147', '20191204143147', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '项目立项', '402880e86ec193b8016ec19482a20001', '402880e86ec193b8016ec19482a20001402884116ecf885d016ecf9b8a130004', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402884116ecf885d016ecf9bcf0b0005', '402884116ecf885d016ecf9bcf0b0005', '20191204143205', '20191204143205', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '需求分析', '402880e86ec193b8016ec19482a20001', '402880e86ec193b8016ec19482a20001402884116ecf885d016ecf9bcf0b0005', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402884116ecf885d016ecf9bf0970006', '402884116ecf885d016ecf9bf0970006', '20191204143213', '20191204143213', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '4', '编码研发', '402880e86ec193b8016ec19482a20001', '402880e86ec193b8016ec19482a20001402884116ecf885d016ecf9bf0970006', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402884116ecf885d016ecf9c47430007', '402884116ecf885d016ecf9c47430007', '20191204143235', '20191204143235', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '5', '集成测试', '402880e86ec193b8016ec19482a20001', '402880e86ec193b8016ec19482a20001402884116ecf885d016ecf9c47430007', '1');
INSERT INTO `plogs_project_gradation` VALUES ('402884116ecf885d016ecf9c7bc30008', '402884116ecf885d016ecf9c7bc30008', '20191204143249', '20191204143249', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '6', '项目发布', '402880e86ec193b8016ec19482a20001', '402880e86ec193b8016ec19482a20001402884116ecf885d016ecf9c7bc30008', '1');

-- ----------------------------
-- Table structure for `plogs_project_re_company`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_project_re_company`;
CREATE TABLE `plogs_project_re_company` (
  `ID` varchar(32) NOT NULL,
  `COMPANYID` varchar(32) NOT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_97` (`COMPANYID`),
  KEY `FK_Reference_98` (`PROJECTID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_project_re_company
-- ----------------------------
INSERT INTO `plogs_project_re_company` VALUES ('40289f787c1a1dc2017c1a206e740002', '40289f787c1a1dc2017c1a204e6e0001', '402880e86fd1985d016fd19d4fd70002');

-- ----------------------------
-- Table structure for `plogs_project_type`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_project_type`;
CREATE TABLE `plogs_project_type` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `MODEL` varchar(2) NOT NULL COMMENT '0分类：可以下挂分类和类型，1类型：必须为叶子节点',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_project_type
-- ----------------------------
INSERT INTO `plogs_project_type` VALUES ('2c905d086eeadbe2016eeae54c710003', '20191209214206', '20191209214206', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '4', 'NONE', '其他工作', '2c905d086eeadbe2016eeae54c710003', '0');
INSERT INTO `plogs_project_type` VALUES ('2c905d086eeadbe2016eeae5afee0004', '20191209214231', '20191209214231', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '1', '2c905d086eeadbe2016eeae54c710003', '公司管理', '2c905d086eeadbe2016eeae54c7100032c905d086eeadbe2016eeae5afee0004', '1');
INSERT INTO `plogs_project_type` VALUES ('2c905d086eeadbe2016eeae5f18b0005', '20191209214248', '20191209214248', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '2', '2c905d086eeadbe2016eeae54c710003', '生活日常', '2c905d086eeadbe2016eeae54c7100032c905d086eeadbe2016eeae5f18b0005', '1');
INSERT INTO `plogs_project_type` VALUES ('402880e86ec198bd016ec1bc86220002', '20191201215308', '20191201215308', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '1', '402880e86ec198bd016ec1c8a5860004', '产品销售项目', '402880e86ec198bd016ec1c8a5860004402880e86ec198bd016ec1bc86220002', '1');
INSERT INTO `plogs_project_type` VALUES ('402880e86ec198bd016ec1c8a5860004', '20191201220622', '20191201220622', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '1', 'NONE', '软件产品销售', '402880e86ec198bd016ec1c8a5860004', '0');
INSERT INTO `plogs_project_type` VALUES ('402880e86ec198bd016ec1c8c72f0005', '20191201220631', '20191201220631', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '1', 'NONE', '客户定制开发', '402880e86ec198bd016ec1c8c72f0005', '0');
INSERT INTO `plogs_project_type` VALUES ('402880e86ec198bd016ec1c9317c0006', '20191201220658', '20191201220658', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '3', 'NONE', '内部产品研发', '402880e86ec198bd016ec1c9317c0006', '0');
INSERT INTO `plogs_project_type` VALUES ('402880e86ec198bd016ec1cd8f6a000d', '20191201221144', '20191201221144', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '1', '402880e86ec198bd016ec1c8c72f0005', '定制开发项目', '402880e86ec198bd016ec1c8c72f0005402880e86ec198bd016ec1cd8f6a000d', '1');
INSERT INTO `plogs_project_type` VALUES ('402880e86ec198bd016ec1cde1a8000e', '20191201221205', '20191201221205', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', null, '1', '402880e86ec198bd016ec1c9317c0006', '产品研发项目', '402880e86ec198bd016ec1c9317c0006402880e86ec198bd016ec1cde1a8000e', '1');

-- ----------------------------
-- Table structure for `plogs_project_user`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_project_user`;
CREATE TABLE `plogs_project_user` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `POPTYPE` varchar(1) NOT NULL COMMENT '1管理员2成员',
  `LIVETIME` varchar(16) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_86` (`PROJECTID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_project_user
-- ----------------------------
INSERT INTO `plogs_project_user` VALUES ('40289f787c1a1dc2017c1a20bea50003', '20210925072526', '40288b854a329988014a329a12f30002', '1', null, '402880e86fd1985d016fd19d4fd70002', '40288b854a329988014a329a12f30002', '1', '20210925072526');

-- ----------------------------
-- Table structure for `plogs_re_gradationtasktype`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_re_gradationtasktype`;
CREATE TABLE `plogs_re_gradationtasktype` (
  `ID` varchar(32) NOT NULL,
  `GRADATIONID` varchar(32) NOT NULL,
  `TASKTYPEID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_92` (`GRADATIONID`),
  KEY `FK_Reference_93` (`TASKTYPEID`),
  CONSTRAINT `FK_Reference_92` FOREIGN KEY (`GRADATIONID`) REFERENCES `plogs_project_gradation` (`ID`),
  CONSTRAINT `FK_Reference_93` FOREIGN KEY (`TASKTYPEID`) REFERENCES `plogs_task_type` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_re_gradationtasktype
-- ----------------------------
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaea57750012', '2c905d086eeadbe2016eeae6d0040007', '2c905d086eeadbe2016eeae80111000c', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaea57850013', '2c905d086eeadbe2016eeae6d0040007', '2c905d086eeadbe2016eeae820fd000d', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaea57940014', '2c905d086eeadbe2016eeae6d0040007', '2c905d086eeadbe2016eeae8408b000e', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaea57a40015', '2c905d086eeadbe2016eeae6d0040007', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaeaab040016', '2c905d086eeadbe2016eeae977560011', '2c905d086eeadbe2016eeae80111000c', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaeaab040017', '2c905d086eeadbe2016eeae977560011', '2c905d086eeadbe2016eeae820fd000d', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaeaab130018', '2c905d086eeadbe2016eeae977560011', '2c905d086eeadbe2016eeae8408b000e', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaeaab230019', '2c905d086eeadbe2016eeae977560011', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaeae2fc001a', '2c905d086eeadbe2016eeae935f80010', '2c905d086eeadbe2016eeae79e8f0009', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaeae2fc001b', '2c905d086eeadbe2016eeae935f80010', '2c905d086eeadbe2016eeae7bf94000a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaeae30b001c', '2c905d086eeadbe2016eeae935f80010', '2c905d086eeadbe2016eeae7db78000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaf16703003c', '402880e86ec198bd016ec1cafc6c0009', '2c905d086eeadbe2016eeae13a060002', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaf16713003d', '402880e86ec198bd016ec1cafc6c0009', '402881e66eda5d1d016eda7365180022', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaf16713003e', '402880e86ec198bd016ec1cafc6c0009', '402881e66ee32762016ee38c62080007', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaf17c02003f', '402880e86ec198bd016ec1cafc6c0009', '402881e66ee32762016ee38a76ef0006', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaf1dbe50040', '402880e86ec198bd016ec1cafc6c0009', '402881e66ee32762016ee38a1ee40005', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaf1dc140041', '402880e86ec198bd016ec1cafc6c0009', '402881e66ee32762016ee39e231c0019', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeaf1dc240042', '402880e86ec198bd016ec1cafc6c0009', '402881e66ee32762016ee39e6809001a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeafa39700080', '402880e86ec198bd016ec1cac40e0008', '402880e86ec193b8016ec197c5220003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeafa397f0081', '402880e86ec198bd016ec1cac40e0008', '402881e66eda5d1d016eda6ea4660006', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eeadbe2016eeafa397f0082', '402880e86ec198bd016ec1cac40e0008', '402881e66ee32762016ee3888ea80003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eedf704016ef2cea7ed0169', '402880e86ec198bd016ec1cafc6c0009', '2c905d086eedf704016ef2cdd4af0168', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eedf704016ef2cf254a016a', '402880e86ec198bd016ec1cb3452000a', '2c905d086eedf704016ef2cdd4af0168', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086eedf704016ef2cf95f6016b', '402880e86ec198bd016ec1cb6db8000b', '2c905d086eedf704016ef2cdd4af0168', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f17ca16016f1c2272b401e6', '402880e86ec198bd016ec1cb6db8000b', '402881e66ee32762016ee38a1ee40005', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f17ca16016f1c2272b401e7', '402880e86ec198bd016ec1cb6db8000b', '402881e66ee32762016ee39e231c0019', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f17ca16016f1c2272b401e8', '402880e86ec198bd016ec1cb6db8000b', '402881e66ee32762016ee39e6809001a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f17ca16016f214ca9d80492', '402880e86ec198bd016ec1cbfe78000c', '402881e66eda5d1d016eda6eeffc0007', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f17ca16016f214ca9e70493', '402880e86ec198bd016ec1cbfe78000c', '402881e66eda5d1d016eda701a1a000a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f17ca16016f214ca9f70494', '402880e86ec198bd016ec1cbfe78000c', '402881e66eda5d1d016eda70582e000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f17ca16016f214d304e0495', '402880e86ec198bd016ec1cbfe78000c', '2c905d086eeadbe2016eeae13a060002', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f17ca16016f214d305d0496', '402880e86ec198bd016ec1cbfe78000c', '402881e66eda5d1d016eda7365180022', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c74ff00013', '2c905d086f61bea7016f61c207ba0005', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c785640014', '2c905d086f61bea7016f61c207ba0005', '2c905d086eeadbe2016eeae7bf94000a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c7b2aa0015', '2c905d086f61bea7016f61c238e80006', '2c905d086eeadbe2016eeae7db78000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c7b2ba0016', '2c905d086f61bea7016f61c238e80006', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c7d7270017', '2c905d086f61bea7016f61c1eee50004', '2c905d086eeadbe2016eeae79e8f0009', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c7d7270018', '2c905d086f61bea7016f61c1eee50004', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c819d80019', '2c905d086f61bea7016f61c264d70007', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c87763001a', '2c905d086f61bea7016f61c299ce0008', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c8a42c001b', '2c905d086f61bea7016f61c401130009', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c905d086f61bea7016f61c8cf02001c', '2c905d086eeadbe2016eeae935f80010', '2c905d086eeadbe2016eeae85898000f', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c948a826ed153da016ed15822f30005', '402880e86ec198bd016ec1ca9c2c0007', '402880e86ec193b8016ec197c5220003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c948a826ed153da016ed15888ff0007', '402884116ecf885d016ecf9b5b1b0003', '402880e86ec193b8016ec197c5220003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('2c948a826ed153da016ed158890f0008', '402884116ecf885d016ecf9b5b1b0003', '402880e86ec23655016ec23cb04a0008', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402880e86ec23e52016ec243dd770007', '402880e86ec193b8016ec194c35b0002', '402880e86ec193b8016ec197c5220003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda70c685000c', '402880e86ec193b8016ec194c35b0002', '402881e66eda5d1d016eda6f41570009', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda70c68f000d', '402880e86ec193b8016ec194c35b0002', '402881e66eda5d1d016eda701a1a000a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda70c694000e', '402880e86ec193b8016ec194c35b0002', '402881e66eda5d1d016eda70582e000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda71f0970016', '402880e86ec198bd016ec1ca9c2c0007', '402880e86ec23655016ec23cb04a0008', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda71f0a30017', '402880e86ec198bd016ec1ca9c2c0007', '402881e66eda5d1d016eda6eeffc0007', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda71f0a90018', '402880e86ec198bd016ec1ca9c2c0007', '402881e66eda5d1d016eda6f24a20008', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda71f0b20019', '402880e86ec198bd016ec1ca9c2c0007', '402881e66eda5d1d016eda6f41570009', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda71f0b9001a', '402880e86ec198bd016ec1ca9c2c0007', '402881e66eda5d1d016eda70582e000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda748cc90026', '402880e86ec198bd016ec1cb3452000a', '402880e86ec193b8016ec197c5220003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda748cd40027', '402880e86ec198bd016ec1cb3452000a', '402880e86ec23655016ec23cb04a0008', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda748cda0028', '402880e86ec198bd016ec1cb3452000a', '402881e66eda5d1d016eda6e38510005', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda748ce50029', '402880e86ec198bd016ec1cb3452000a', '402881e66eda5d1d016eda6ea4660006', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda748cea002a', '402880e86ec198bd016ec1cb3452000a', '402881e66eda5d1d016eda701a1a000a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda748cf1002b', '402880e86ec198bd016ec1cb3452000a', '402881e66eda5d1d016eda70582e000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda748cf6002c', '402880e86ec198bd016ec1cb3452000a', '402881e66eda5d1d016eda7365180022', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda748cfb002d', '402880e86ec198bd016ec1cb3452000a', '402881e66eda5d1d016eda737e220023', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda75ea750030', '402880e86ec198bd016ec1cb6db8000b', '402881e66eda5d1d016eda701a1a000a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda75ea800031', '402880e86ec198bd016ec1cb6db8000b', '402881e66eda5d1d016eda70582e000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda7614600035', '402880e86ec198bd016ec1cb6db8000b', '402881e66eda5d1d016eda737e220023', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66eda5d1d016eda7640cc0038', '402880e86ec198bd016ec1cb6db8000b', '402881e66eda5d1d016eda6ea4660006', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39ae5ee0008', '402884116ecf885d016ecf9bf0970006', '402881e66ee32762016ee3888ea80003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39b20270009', '402884116ecf885d016ecf9bf0970006', '402880e86ec193b8016ec197c5220003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39b2031000a', '402884116ecf885d016ecf9bf0970006', '402881e66eda5d1d016eda6ea4660006', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39b83b4000c', '402884116ecf885d016ecf9c7bc30008', '402881e66eda5d1d016eda7365180022', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39b83bf000d', '402884116ecf885d016ecf9c7bc30008', '402881e66eda5d1d016eda737e220023', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39bcd0e000e', '402884116ecf885d016ecf9c7bc30008', '402881e66ee32762016ee38c62080007', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39c304f000f', '402884116ecf885d016ecf9b8a130004', '402880e86ec193b8016ec197c5220003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39c30620010', '402884116ecf885d016ecf9b8a130004', '402881e66eda5d1d016eda6ea4660006', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39c4e1a0011', '402884116ecf885d016ecf9b8a130004', '402881e66eda5d1d016eda70582e000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39c98820012', '402884116ecf885d016ecf9bcf0b0005', '402880e86ec193b8016ec197c5220003', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39c98930013', '402884116ecf885d016ecf9bcf0b0005', '402881e66eda5d1d016eda6ea4660006', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39cbb1d0014', '402884116ecf885d016ecf9bcf0b0005', '402881e66eda5d1d016eda70582e000b', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39d131a0015', '402884116ecf885d016ecf9bcf0b0005', '402881e66eda5d1d016eda701a1a000a', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39d44600016', '402884116ecf885d016ecf9c47430007', '402881e66eda5d1d016eda7365180022', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39d44760017', '402884116ecf885d016ecf9c47430007', '402881e66eda5d1d016eda737e220023', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39e9000001b', '402884116ecf885d016ecf9c47430007', '402881e66ee32762016ee39e231c0019', '0');
INSERT INTO `plogs_re_gradationtasktype` VALUES ('402881e66ee32762016ee39e900e001c', '402884116ecf885d016ecf9c47430007', '402881e66ee32762016ee39e6809001a', '0');

-- ----------------------------
-- Table structure for `plogs_re_typegradation`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_re_typegradation`;
CREATE TABLE `plogs_re_typegradation` (
  `ID` varchar(32) NOT NULL,
  `PROJECTTYPEID` varchar(32) NOT NULL,
  `GRADATIONID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_90` (`PROJECTTYPEID`),
  KEY `FK_Reference_91` (`GRADATIONID`),
  CONSTRAINT `FK_Reference_90` FOREIGN KEY (`PROJECTTYPEID`) REFERENCES `plogs_project_type` (`ID`),
  CONSTRAINT `FK_Reference_91` FOREIGN KEY (`GRADATIONID`) REFERENCES `plogs_project_gradation` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_re_typegradation
-- ----------------------------
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086eeadbe2016eeaeb7113001f', '2c905d086eeadbe2016eeae5f18b0005', '2c905d086eeadbe2016eeae6d0040007', '1');
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086f099679016f11ae148001b9', '402880e86ec198bd016ec1cd8f6a000d', '402880e86ec198bd016ec1cac40e0008', '4');
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086f61bea7016f61c4b69a000a', '2c905d086eeadbe2016eeae5afee0004', '2c905d086f61bea7016f61c207ba0005', '1');
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086f61bea7016f61c4b69a000b', '2c905d086eeadbe2016eeae5afee0004', '2c905d086f61bea7016f61c238e80006', '2');
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086f61bea7016f61c4b6aa000c', '2c905d086eeadbe2016eeae5afee0004', '2c905d086f61bea7016f61c1eee50004', '3');
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086f61bea7016f61c4b6aa000d', '2c905d086eeadbe2016eeae5afee0004', '2c905d086f61bea7016f61c264d70007', '4');
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086f61bea7016f61c4b6d9000e', '2c905d086eeadbe2016eeae5afee0004', '2c905d086f61bea7016f61c299ce0008', '7');
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086f61bea7016f61c4b6e8000f', '2c905d086eeadbe2016eeae5afee0004', '2c905d086f61bea7016f61c401130009', '8');
INSERT INTO `plogs_re_typegradation` VALUES ('2c905d086f61bea7016f61c4b6e80010', '2c905d086eeadbe2016eeae5afee0004', '2c905d086eeadbe2016eeae935f80010', '9');
INSERT INTO `plogs_re_typegradation` VALUES ('402881e66ec6067a016ec60eab240001', '402880e86ec198bd016ec1cd8f6a000d', '402880e86ec193b8016ec194c35b0002', '1');
INSERT INTO `plogs_re_typegradation` VALUES ('402881e66ec6067a016ec60eab380002', '402880e86ec198bd016ec1cd8f6a000d', '402880e86ec198bd016ec1ca9c2c0007', '3');
INSERT INTO `plogs_re_typegradation` VALUES ('402881e66ec6067a016ec60eab460004', '402880e86ec198bd016ec1cd8f6a000d', '402880e86ec198bd016ec1cafc6c0009', '5');
INSERT INTO `plogs_re_typegradation` VALUES ('402881e66ec6067a016ec60eab4d0005', '402880e86ec198bd016ec1cd8f6a000d', '402880e86ec198bd016ec1cb3452000a', '6');
INSERT INTO `plogs_re_typegradation` VALUES ('402881e66ec6067a016ec60eab610006', '402880e86ec198bd016ec1cd8f6a000d', '402880e86ec198bd016ec1cb6db8000b', '7');
INSERT INTO `plogs_re_typegradation` VALUES ('402881e66ec6067a016ec60eab680007', '402880e86ec198bd016ec1cd8f6a000d', '402880e86ec198bd016ec1cbfe78000c', '2');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecf9fdcc10001', '402880e86ec198bd016ec1cde1a8000e', '402884116ecf885d016ecf9b5b1b0003', '1');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecf9fdcd20002', '402880e86ec198bd016ec1cde1a8000e', '402884116ecf885d016ecf9b8a130004', '2');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecf9fdcd90003', '402880e86ec198bd016ec1cde1a8000e', '402884116ecf885d016ecf9bcf0b0005', '3');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecf9fdce30004', '402880e86ec198bd016ec1cde1a8000e', '402884116ecf885d016ecf9bf0970006', '4');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecf9fdcef0005', '402880e86ec198bd016ec1cde1a8000e', '402884116ecf885d016ecf9c47430007', '5');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecf9fdcf80006', '402880e86ec198bd016ec1cde1a8000e', '402884116ecf885d016ecf9c7bc30008', '6');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecfa0abfc0007', '402880e86ec198bd016ec1bc86220002', '402880e86ec193b8016ec194c35b0002', '1');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecfa0ac0d0008', '402880e86ec198bd016ec1bc86220002', '402880e86ec198bd016ec1ca9c2c0007', '3');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecfa0ac150009', '402880e86ec198bd016ec1bc86220002', '402880e86ec198bd016ec1cafc6c0009', '4');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecfa0ac26000b', '402880e86ec198bd016ec1bc86220002', '402880e86ec198bd016ec1cb6db8000b', '5');
INSERT INTO `plogs_re_typegradation` VALUES ('402884116ecf9f65016ecfa0ac2f000c', '402880e86ec198bd016ec1bc86220002', '402880e86ec198bd016ec1cbfe78000c', '2');

-- ----------------------------
-- Table structure for `plogs_re_viewtask`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_re_viewtask`;
CREATE TABLE `plogs_re_viewtask` (
  `ID` varchar(32) NOT NULL,
  `TASKID` varchar(32) NOT NULL,
  `VIEWID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_85` (`TASKID`),
  KEY `FK_Reference_87` (`VIEWID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_re_viewtask
-- ----------------------------

-- ----------------------------
-- Table structure for `plogs_task`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_task`;
CREATE TABLE `plogs_task` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL COMMENT '1.计划，2开始，3结束，4关闭',
  `PCONTENT` varchar(128) DEFAULT NULL,
  `TITLE` varchar(512) NOT NULL,
  `READFILEID` varchar(32) DEFAULT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL COMMENT '系统中任务处理人',
  `PLANSTIME` varchar(16) DEFAULT NULL,
  `PLANETIME` varchar(16) DEFAULT NULL,
  `DOSTIME` varchar(16) DEFAULT NULL,
  `DOETIME` varchar(16) DEFAULT NULL,
  `PLANTIMES` int(11) DEFAULT NULL COMMENT '单位小时',
  `DOTIMES` int(11) DEFAULT NULL COMMENT '单位小时',
  `SOURCEUSERID` varchar(32) DEFAULT NULL,
  `RELAUSERID` varchar(32) DEFAULT NULL COMMENT '比如配合人，沟通人等',
  `GRADATIONID` varchar(32) NOT NULL,
  `GRADATIONNAME` varchar(128) NOT NULL,
  `TASKTYPEID` varchar(32) NOT NULL,
  `TASKTYPENAME` varchar(128) NOT NULL,
  `COMPLETION` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_11` (`PROJECTID`),
  CONSTRAINT `FK_Reference_11` FOREIGN KEY (`PROJECTID`) REFERENCES `plogs_project` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_task
-- ----------------------------

-- ----------------------------
-- Table structure for `plogs_task_file`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_task_file`;
CREATE TABLE `plogs_task_file` (
  `ID` varchar(32) NOT NULL,
  `TASKID` varchar(32) DEFAULT NULL,
  `FILEID` varchar(32) DEFAULT NULL,
  `CTIME` varchar(16) NOT NULL,
  `LOGID` varchar(32) DEFAULT NULL,
  `SOURCETYPE` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_20` (`TASKID`),
  KEY `FK_Reference_88` (`LOGID`),
  CONSTRAINT `FK_Reference_20` FOREIGN KEY (`TASKID`) REFERENCES `plogs_task` (`ID`),
  CONSTRAINT `FK_Reference_88` FOREIGN KEY (`LOGID`) REFERENCES `plogs_task_log` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_task_file
-- ----------------------------

-- ----------------------------
-- Table structure for `plogs_task_log`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_task_log`;
CREATE TABLE `plogs_task_log` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `TASKID` varchar(32) NOT NULL,
  `DATEKEY` varchar(8) NOT NULL,
  `FILEID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_25` (`TASKID`),
  CONSTRAINT `FK_Reference_25` FOREIGN KEY (`TASKID`) REFERENCES `plogs_task` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_task_log
-- ----------------------------

-- ----------------------------
-- Table structure for `plogs_task_type`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_task_type`;
CREATE TABLE `plogs_task_type` (
  `ID` varchar(32) NOT NULL,
  `UUID` varchar(32) NOT NULL COMMENT '原创任务类型的ID和UUID值一样，拷贝的ID和UUID值不一样，拷贝的名称不能编辑',
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL COMMENT '1，可用，0停用，2刪除',
  `PCONTENT` varchar(128) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `MODEL` varchar(2) NOT NULL COMMENT '0分类：可以下挂分类和类型，1类型：必须为叶子节点',
  `TEMPLETEFILEID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_task_type
-- ----------------------------
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae13a060002', '2c905d086eeadbe2016eeae13a060002', '20191209213739', '20191209213739', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '补丁发布', '402881e66eda5d1d016eda7334400021', '402881e66eda5d1d016eda73344000212c905d086eeadbe2016eeae13a060002', '1', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae727d80008', '2c905d086eeadbe2016eeae727d80008', '20191209214407', '20191209214549', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '6', '工作日常', 'NONE', '2c905d086eeadbe2016eeae727d80008', '0', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae79e8f0009', '2c905d086eeadbe2016eeae79e8f0009', '20191209214438', '20191209214438', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '财务', '2c905d086eeadbe2016eeae727d80008', '2c905d086eeadbe2016eeae727d800082c905d086eeadbe2016eeae79e8f0009', '1', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae7bf94000a', '2c905d086eeadbe2016eeae7bf94000a', '20191209214446', '20191209214446', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '税务', '2c905d086eeadbe2016eeae727d80008', '2c905d086eeadbe2016eeae727d800082c905d086eeadbe2016eeae7bf94000a', '1', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae7db78000b', '2c905d086eeadbe2016eeae7db78000b', '20191209214453', '20191209214453', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '工商', '2c905d086eeadbe2016eeae727d80008', '2c905d086eeadbe2016eeae727d800082c905d086eeadbe2016eeae7db78000b', '1', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae80111000c', '2c905d086eeadbe2016eeae80111000c', '20191209214503', '20191209214503', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '4', '锻炼', '2c905d086eeadbe2016eeae727d80008', '2c905d086eeadbe2016eeae727d800082c905d086eeadbe2016eeae80111000c', '1', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae820fd000d', '2c905d086eeadbe2016eeae820fd000d', '20191209214511', '20191209214511', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '5', '社交', '2c905d086eeadbe2016eeae727d80008', '2c905d086eeadbe2016eeae727d800082c905d086eeadbe2016eeae820fd000d', '1', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae8408b000e', '2c905d086eeadbe2016eeae8408b000e', '20191209214519', '20191209214519', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '6', '家庭', '2c905d086eeadbe2016eeae727d80008', '2c905d086eeadbe2016eeae727d800082c905d086eeadbe2016eeae8408b000e', '1', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eeadbe2016eeae85898000f', '2c905d086eeadbe2016eeae85898000f', '20191209214525', '20191209214525', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '7', '其他', '2c905d086eeadbe2016eeae727d80008', '2c905d086eeadbe2016eeae727d800082c905d086eeadbe2016eeae85898000f', '1', null);
INSERT INTO `plogs_task_type` VALUES ('2c905d086eedf704016ef2cdd4af0168', '2c905d086eedf704016ef2cdd4af0168', '20191211103325', '20191211103325', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '远程配置', '402881e66ee32762016ee389dbc80004', '402881e66ee32762016ee389dbc800042c905d086eedf704016ef2cdd4af0168', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402880e86ec193b8016ec197c5220003', '402880e86ec193b8016ec197c5220003', '20191201211259', '20191209213843', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '需求分析', '402880e86ec23655016ec23bcb0b0006', '402880e86ec23655016ec23bcb0b0006402880e86ec193b8016ec197c5220003', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402880e86ec23655016ec23bcb0b0006', '402880e86ec23655016ec23bcb0b0006', '20191202001208', '20191202001208', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '研发任务', 'NONE', '402880e86ec23655016ec23bcb0b0006', '0', null);
INSERT INTO `plogs_task_type` VALUES ('402880e86ec23655016ec23bee8a0007', '402880e86ec23655016ec23bee8a0007', '20191202001217', '20191202001217', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '商务任务', 'NONE', '402880e86ec23655016ec23bee8a0007', '0', null);
INSERT INTO `plogs_task_type` VALUES ('402880e86ec23655016ec23cb04a0008', '402880e86ec23655016ec23cb04a0008', '20191202001307', '20191208112331', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '', '2', '需求说明书编写', '402880e86ec23655016ec23bcb0b0006', '402880e86ec23655016ec23bcb0b0006402880e86ec23655016ec23cb04a0008', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda6e38510005', '402881e66eda5d1d016eda6e38510005', '20191206165806', '20191208112345', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '', '3', '开发编码', '402880e86ec23655016ec23bcb0b0006', '402880e86ec23655016ec23bcb0b0006402881e66eda5d1d016eda6e38510005', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda6ea4660006', '402881e66eda5d1d016eda6ea4660006', '20191206165834', '20191208112359', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '4', '技术预研', '402880e86ec23655016ec23bcb0b0006', '402880e86ec23655016ec23bcb0b0006402881e66eda5d1d016eda6ea4660006', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda6eeffc0007', '402881e66eda5d1d016eda6eeffc0007', '20191206165853', '20191208112631', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '合同签订', '402880e86ec23655016ec23bee8a0007', '402880e86ec23655016ec23bee8a0007402881e66eda5d1d016eda6eeffc0007', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda6f24a20008', '402881e66eda5d1d016eda6f24a20008', '20191206165907', '20191208112635', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '', '2', '合同邮寄', '402880e86ec23655016ec23bee8a0007', '402880e86ec23655016ec23bee8a0007402881e66eda5d1d016eda6f24a20008', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda6f41570009', '402881e66eda5d1d016eda6f41570009', '20191206165914', '20191209214021', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '', '3', '合同谈判', '402880e86ec23655016ec23bee8a0007', '402880e86ec23655016ec23bee8a0007402881e66eda5d1d016eda6f41570009', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda701a1a000a', '402881e66eda5d1d016eda701a1a000a', '20191206170010', '20191209214040', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '4', '远程演示', '402880e86ec23655016ec23bee8a0007', '402880e86ec23655016ec23bee8a0007402881e66eda5d1d016eda701a1a000a', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda70582e000b', '402881e66eda5d1d016eda70582e000b', '20191206170026', '20191206170026', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '5', '沟通交流', '402880e86ec23655016ec23bee8a0007', '402880e86ec23655016ec23bee8a0007402881e66eda5d1d016eda70582e000b', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda7334400021', '402881e66eda5d1d016eda7334400021', '20191206170333', '20191209213932', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '实施发布', 'NONE', '402881e66eda5d1d016eda7334400021', '0', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda7365180022', '402881e66eda5d1d016eda7365180022', '20191206170345', '20191206170345', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '远程部署', '402881e66eda5d1d016eda7334400021', '402881e66eda5d1d016eda7334400021402881e66eda5d1d016eda7365180022', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda737e220023', '402881e66eda5d1d016eda737e220023', '20191206170352', '20191209213802', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '2', '', '2', '远程测试', '402881e66eda5d1d016eda7334400021', '402881e66eda5d1d016eda7334400021402881e66eda5d1d016eda737e220023', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda753328002e', '402881e66eda5d1d016eda753328002e', '20191206170544', '20191208112706', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '6', '开具发票', '402880e86ec23655016ec23bee8a0007', '402880e86ec23655016ec23bee8a0007402881e66eda5d1d016eda753328002e', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66eda5d1d016eda754b24002f', '402881e66eda5d1d016eda754b24002f', '20191206170550', '20191208112713', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '7', '项目回款', '402880e86ec23655016ec23bee8a0007', '402880e86ec23655016ec23bee8a0007402881e66eda5d1d016eda754b24002f', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66ee32762016ee3888ea80003', '402881e66ee32762016ee3888ea80003', '20191208112327', '20191208112408', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '功能开发', '402880e86ec23655016ec23bcb0b0006', '402880e86ec23655016ec23bcb0b0006402881e66ee32762016ee3888ea80003', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66ee32762016ee389dbc80004', '402881e66ee32762016ee389dbc80004', '20191208112453', '20191208112453', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '4', '技术支持', 'NONE', '402881e66ee32762016ee389dbc80004', '0', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66ee32762016ee38a1ee40005', '402881e66ee32762016ee38a1ee40005', '20191208112510', '20191208112510', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', '远程测试', '402881e66ee32762016ee39de92a0018', '402881e66ee32762016ee39de92a0018402881e66ee32762016ee38a1ee40005', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66ee32762016ee38a76ef0006', '402881e66ee32762016ee38a76ef0006', '20191208112532', '20191208112541', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '使用咨询', '402881e66ee32762016ee389dbc80004', '402881e66ee32762016ee389dbc80004402881e66ee32762016ee38a76ef0006', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66ee32762016ee38c62080007', '402881e66ee32762016ee38c62080007', '20191208112738', '20191208112738', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '3', '项目发布', '402881e66eda5d1d016eda7334400021', '402881e66eda5d1d016eda7334400021402881e66ee32762016ee38c62080007', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66ee32762016ee39de92a0018', '402881e66ee32762016ee39de92a0018', '20191208114647', '20191208114647', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '5', '运维测试', 'NONE', '402881e66ee32762016ee39de92a0018', '0', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66ee32762016ee39e231c0019', '402881e66ee32762016ee39e231c0019', '20191208114702', '20191208114702', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '1', 'bug修复', '402881e66ee32762016ee39de92a0018', '402881e66ee32762016ee39de92a0018402881e66ee32762016ee39e231c0019', '1', null);
INSERT INTO `plogs_task_type` VALUES ('402881e66ee32762016ee39e6809001a', '402881e66ee32762016ee39e6809001a', '20191208114719', '20191209213702', '40288b854a329988014a329a12f30002', '40288b854a329988014a329a12f30002', '1', '', '2', '本地测试', '402881e66ee32762016ee39de92a0018', '402881e66ee32762016ee39de92a0018402881e66ee32762016ee39e6809001a', '1', null);

-- ----------------------------
-- Table structure for `plogs_task_user`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_task_user`;
CREATE TABLE `plogs_task_user` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PROJECTID` varchar(32) NOT NULL,
  `TASKID` varchar(32) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `POPTYPE` varchar(1) NOT NULL COMMENT '1管理人（唯一）2执行人（唯一）3监视人',
  `LIVETIME` varchar(16) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_100` (`TASKID`),
  KEY `FK_Reference_99` (`PROJECTID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_task_user
-- ----------------------------

-- ----------------------------
-- Table structure for `plogs_userview`
-- ----------------------------
DROP TABLE IF EXISTS `plogs_userview`;
CREATE TABLE `plogs_userview` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `NAME` varchar(128) NOT NULL,
  `USERID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  `TYPE` varchar(2) NOT NULL COMMENT '1.用户自定义',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plogs_userview
-- ----------------------------

-- ----------------------------
-- Table structure for `wdap_file`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_file`;
CREATE TABLE `wdap_file` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(1) NOT NULL COMMENT '0.临时1.持久2永久9删除\r\n            ',
  `EXNAME` varchar(32) NOT NULL,
  `RELATIVEPATH` varchar(1024) NOT NULL COMMENT 'relative',
  `SECRET` varchar(32) NOT NULL,
  `FILENAME` varchar(128) NOT NULL,
  `TITLE` varchar(256) NOT NULL,
  `FILESIZE` int(11) NOT NULL,
  `SYSNAME` varchar(128) NOT NULL,
  `RESOURCEID` varchar(32) NOT NULL,
  `APPID` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RE_WDAP_FILE_RESOURCE` (`RESOURCEID`),
  CONSTRAINT `FK_RE_WDAP_FILE_RESOURCE` FOREIGN KEY (`RESOURCEID`) REFERENCES `wdap_file_resource` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wdap_file
-- ----------------------------

-- ----------------------------
-- Table structure for `wdap_file_resource`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_file_resource`;
CREATE TABLE `wdap_file_resource` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PATH` varchar(1024) NOT NULL,
  `STATE` varchar(1) NOT NULL COMMENT '1读写，2只读，0禁用',
  `TITLE` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='resource';

-- ----------------------------
-- Records of wdap_file_resource
-- ----------------------------
INSERT INTO `wdap_file_resource` VALUES ('4028b88169fd1b650169fd331e56000a', '20190408214619', '20200123124245', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '', 'D:\\wfsServer\\resource\\file', '1', '资源库');

-- ----------------------------
-- Table structure for `wdap_file_text`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_file_text`;
CREATE TABLE `wdap_file_text` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `FILETEXT` longtext NOT NULL,
  `FILEID` varchar(32) NOT NULL,
  `COMPLETEIS` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RE_WDAP_TEXT_FILE` (`FILEID`),
  CONSTRAINT `FK_RE_WDAP_TEXT_FILE` FOREIGN KEY (`FILEID`) REFERENCES `wdap_file` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wdap_file_text
-- ----------------------------

-- ----------------------------
-- Table structure for `wdap_file_visit`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_file_visit`;
CREATE TABLE `wdap_file_visit` (
  `ID` varchar(32) NOT NULL,
  `DOWNUM` int(11) NOT NULL,
  `VIEWNUM` int(11) NOT NULL,
  `FILEID` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RE_WDAP_VISITE_FILE` (`FILEID`),
  CONSTRAINT `FK_RE_WDAP_VISITE_FILE` FOREIGN KEY (`FILEID`) REFERENCES `wdap_file` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wdap_file_visit
-- ----------------------------

-- ----------------------------
-- Table structure for `wdap_view_convertor`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_view_convertor`;
CREATE TABLE `wdap_view_convertor` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `CLASSKEY` varchar(512) NOT NULL,
  `OMODELID` varchar(32) NOT NULL,
  `TMODELID` varchar(32) NOT NULL,
  `TITLE` varchar(64) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RE_WDAP_CONVERTOR_OMODEL` (`OMODELID`),
  KEY `FK_RE_WDAP_CONVERTOR_TMODEL` (`TMODELID`),
  CONSTRAINT `FK_RE_WDAP_CONVERTOR_OMODEL` FOREIGN KEY (`OMODELID`) REFERENCES `wdap_view_filemodel` (`ID`),
  CONSTRAINT `FK_RE_WDAP_CONVERTOR_TMODEL` FOREIGN KEY (`TMODELID`) REFERENCES `wdap_view_filemodel` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wdap_view_convertor
-- ----------------------------
INSERT INTO `wdap_view_convertor` VALUES ('402881e66ee99428016ee99550330002', '20191209153507', '20191209153507', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'NONE', '4028b8816a1eb5b1016a1eb5f8db0001', '402881e66ee99abb016ee99b002c0001', 'IHTML原始文件-To-原始IHTML文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a1f378b016a1f421ff90004', '20190415122948', '20190415122948', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'NONE', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a203ab2016a2069ae890006', 'PDF原始文件-To-原始PDF文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a1fb82a016a1fb929c90004', '20190415143949', '20190415143949', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'THUMBNAIL_IMG', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a1fb82a016a1fb88b240001', '图片原始文件-To-大缩略图');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a1fb82a016a1fba0ffc0005', '20190415144048', '20190415144048', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'THUMBNAIL_IMG', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a1fb82a016a1fb88b380002', '图片原始文件-To-中缩略图');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a1fb82a016a1fba2ecb0006', '20190415144056', '20190415144056', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'THUMBNAIL_IMG', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a1fb82a016a1fb88b420003', '图片原始文件-To-小缩略图');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a30d1d3016a30e6830f009f', '20190418224254', '20190418224254', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'NONE', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a30d1d3016a30e469b2009b', 'MP4原始文件-To-原始MP4文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a30d1d3016a30e6e28500a0', '20190418224318', '20190418224318', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'NONE', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a30d1d3016a30e469b8009c', 'MP3原始文件-To-原始MP3文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a311054016a3112933f0002', '20190418233102', '20190418233102', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'PDF_TO_IMG', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a311054016a3110c6240001', 'PDF原始文件-To-图片册');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a4f21d9016a4f2721760002', '20190424194205', '20190424194205', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'OPENOFFICE_TO_FILE', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a4f21d9016a4f256cea0001', 'OFFICE原始文件-To-PDF预览文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a4f39fe016a4f4225140006', '20190424201136', '20190424201136', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'PDF_TO_IMG', '4028b8816a4f21d9016a4f256cea0001', '4028b8816a311054016a3110c6240001', 'PDF预览文件-To-图片册');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a4f7e90016a4f807ef60001', '20190424211942', '20190424211942', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'OPENOFFICE_TO_FILE', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a4f7874016a4f78d8340001', 'OFFICE原始文件-To-HTML预览文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a52ebc0016a52eca3fb0001', '20190425131641', '20190425131641', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'ZIPRAR_TO_TXT', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a52e999016a52ea20fd0002', 'ZIP|RAR原始文件-To-txt预览文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a52edb2016a531d61d3005c', '20190425140955', '20190425140955', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'OPENOFFICE_TO_FILE', '4028b8816a52e999016a52ea20fd0002', '4028b8816a4f7874016a4f78d8340001', 'TXT预览文件-To-HTML预览文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816a549e9b016a54c46c8e002f', '20190425215200', '20190425215200', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'NONE', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816a52e999016a52ea20f00001', 'TXT原始文件-To-原始txt文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816c14e8c9016c155c1ceb00bf', '20190722002733', '20190722002733', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'OPENOFFICE_TO_FILE', '4028b8816a52e999016a52ea20fd0002', '4028b8816a4f21d9016a4f256cea0001', 'TXT预览文件-To-PDF预览文件');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816c9ac891016c9ad56a280014', '20190816222933', '20190816222933', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'NONE', '4028b8816a1eb5b1016a1eb5f8db0001', '4028b8816c9af63b016c9af6bd940001', '图片原始文件-To-原始图片');
INSERT INTO `wdap_view_convertor` VALUES ('4028b8816d06bfb7016d06c0b8460001', '20190906212556', '20190906212556', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', 'TXT_TO_HTML', '4028b8816a52e999016a52ea20f00001', '4028b8816a4f7874016a4f78d8340001', '原始txt文件-To-HTML预览文件(按编码读取)');

-- ----------------------------
-- Table structure for `wdap_view_doc`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_view_doc`;
CREATE TABLE `wdap_view_doc` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL COMMENT '0排队1转换中2完成9错误\r\n            ',
  `PCONTENT` varchar(128) DEFAULT NULL,
  `FILEID` varchar(32) NOT NULL,
  `WEBPATH` varchar(256) NOT NULL,
  `MODELID` varchar(32) NOT NULL,
  `VIEWIS` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RE_WDAP_VIEWDOC_FILE` (`FILEID`),
  KEY `FK_RE_WDAP_VIEWDOC_MODEL` (`MODELID`),
  CONSTRAINT `FK_RE_WDAP_VIEWDOC_FILE` FOREIGN KEY (`FILEID`) REFERENCES `wdap_file` (`ID`),
  CONSTRAINT `FK_RE_WDAP_VIEWDOC_MODEL` FOREIGN KEY (`MODELID`) REFERENCES `wdap_view_filemodel` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wdap_view_doc
-- ----------------------------

-- ----------------------------
-- Table structure for `wdap_view_filemodel`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_view_filemodel`;
CREATE TABLE `wdap_view_filemodel` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `TITLE` varchar(64) NOT NULL,
  `MODELKEY` varchar(32) NOT NULL COMMENT '在后台的枚举项',
  `SORTNUM` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wdap_view_filemodel
-- ----------------------------
INSERT INTO `wdap_view_filemodel` VALUES ('402881e66ee99abb016ee99b002c0001', '20191209154119', '20191209154119', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '原始IHTML文件', 'IHTML', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a1eb5b1016a1eb5f8db0001', '20190415095643', '20190415095643', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '原始文件', 'WDAPFILE', '0');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a1fb82a016a1fb88b240001', '20190415143909', '20190415143909', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '大缩略图', 'MAX_THUMBNAIL', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a1fb82a016a1fb88b380002', '20190415143909', '20190415143909', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '中缩略图', 'MED_THUMBNAIL', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a1fb82a016a1fb88b420003', '20190415143909', '20190415143909', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '小缩略图', 'MIN_THUMBNAIL', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a203ab2016a2069ae890006', '20190415175238', '20190415175238', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '原始PDF文件', 'PDFFILE', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a30d1d3016a30e469b2009b', '20190418224036', '20190418224036', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '原始MP4文件', 'MP4FILE', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a30d1d3016a30e469b8009c', '20190418224036', '20190418224036', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '原始MP3文件', 'MP3FILE', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a311054016a3110c6240001', '20190418232904', '20190418232904', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '图片册', 'IMGS', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a4f21d9016a4f256cea0001', '20190424194013', '20190424194013', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'PDF预览文件', 'VIEW_PDF', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a4f7874016a4f78d8340001', '20190424211120', '20190424211120', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'HTML预览文件', 'VIEW_HTML', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a52e999016a52ea20f00001', '20190425131356', '20190425131356', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '原始txt文件', 'TXTFILE', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816a52e999016a52ea20fd0002', '20190425131356', '20190425131356', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', 'txt预览文件', 'VIEW_TXT', '1');
INSERT INTO `wdap_view_filemodel` VALUES ('4028b8816c9af63b016c9af6bd940001', '20190816230557', '20190816230557', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', null, '1', '原始图片', 'IMGFILE', '1');

-- ----------------------------
-- Table structure for `wdap_view_log`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_view_log`;
CREATE TABLE `wdap_view_log` (
  `ID` varchar(32) NOT NULL,
  `TASKID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `PCONTENT` varchar(512) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RE_WDAP_VIEWLOG_TASK` (`TASKID`),
  CONSTRAINT `FK_RE_WDAP_VIEWLOG_TASK` FOREIGN KEY (`TASKID`) REFERENCES `wdap_view_task` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wdap_view_log
-- ----------------------------

-- ----------------------------
-- Table structure for `wdap_view_regular`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_view_regular`;
CREATE TABLE `wdap_view_regular` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `EUSERNAME` varchar(64) NOT NULL,
  `EUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `SIZEMIN` int(11) NOT NULL,
  `SIZEMAX` int(11) NOT NULL,
  `VIEWIS` varchar(1) NOT NULL,
  `CONVERTORID` varchar(32) NOT NULL,
  `SORT` int(11) NOT NULL,
  `TITLE` varchar(64) NOT NULL,
  `FILEEXNAME` varchar(32) NOT NULL,
  `OUTTIMENUM` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RE_WDAP_REGULAR_CONVERTOR` (`CONVERTORID`),
  CONSTRAINT `FK_RE_WDAP_REGULAR_CONVERTOR` FOREIGN KEY (`CONVERTORID`) REFERENCES `wdap_view_convertor` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='regular';

-- ----------------------------
-- Records of wdap_view_regular
-- ----------------------------
INSERT INTO `wdap_view_regular` VALUES ('402881e66ee9a294016ee9a4afc40001', '20191209155154', '20191209155154', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '402881e66ee99428016ee99550330002', '1', 'ihtml:IHTML原始文件-To-原始IHTML文件', 'ihtml', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a1f8ccc016a1f8e27e20001', '20190415135251', '20190415135251', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '90000000', '0', '4028b8816a1f378b016a1f421ff90004', '1', 'pdf:PDF原始文件-To-原始PDF文件', 'pdf', '10000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a2448af016a244e4b7b0001', '20190416120112', '20190416120112', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1024', '1024000', '0', '4028b8816a1fb82a016a1fb929c90004', '0', 'png:图片原始文件-To-大缩略图', 'png', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a2448af016a244e73270002', '20190416120122', '20190416120122', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1024', '1024000', '1', '4028b8816a1fb82a016a1fba0ffc0005', '0', 'png:图片原始文件-To-中缩略图', 'png', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a2448af016a244e91fb0003', '20190416120130', '20200120103752', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1024', '1024000', '0', '4028b8816a1fb82a016a1fba2ecb0006', '0', 'png:图片原始文件-To-小缩略图', 'png', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a24a63f016a24aa225b0000', '20190416134130', '20190416134130', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1024', '30240000', '0', '4028b8816a1fb82a016a1fb929c90004', '0', 'jpg:图片原始文件-To-大缩略图', 'jpg', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a24a63f016a24aa22cb0001', '20190416134130', '20190416134130', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1024', '30240000', '1', '4028b8816a1fb82a016a1fba0ffc0005', '0', 'jpg:图片原始文件-To-中缩略图', 'jpg', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a24a63f016a24aa22f20002', '20190416134130', '20200120103752', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1024', '30240000', '0', '4028b8816a1fb82a016a1fba2ecb0006', '0', 'jpg:图片原始文件-To-小缩略图', 'jpg', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a24a63f016a24ac95ed0003', '20190416134411', '20190416134411', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1024', '1024000', '0', '4028b8816a1fb82a016a1fb929c90004', '0', 'jpeg:图片原始文件-To-大缩略图', 'jpeg', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a24a63f016a24ac96420004', '20190416134411', '20200120103752', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1024', '1024000', '0', '4028b8816a1fb82a016a1fba2ecb0006', '0', 'jpeg:图片原始文件-To-小缩略图', 'jpeg', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a24a63f016a24ac966b0005', '20190416134411', '20190416134411', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1024', '1024000', '1', '4028b8816a1fb82a016a1fba0ffc0005', '0', 'jpeg:图片原始文件-To-中缩略图', 'jpeg', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a30d1d3016a30e55789009d', '20190418224137', '20190418224137', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '1111111111', '1', '4028b8816a30d1d3016a30e6830f009f', '0', 'mp4:MP4原始文件-To-原始MP4文件', 'mp4', '20000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a30d1d3016a30e5714a009e', '20190418224144', '20190418224144', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '1111111111', '1', '4028b8816a30d1d3016a30e6e28500a0', '0', 'mp3:MP3原始文件-To-原始MP3文件', 'mp3', '20000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a311054016a311379680003', '20190418233200', '20190418233200', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '1000000000', '1', '4028b8816a311054016a3112933f0002', '2', 'pdf:PDF原始文件-To-图片册', 'pdf', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f21d9016a4f289c620003', '20190424194342', '20190424194342', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '0', '4028b8816a4f21d9016a4f2721760002', '1', 'doc:OFFICE原始文件-To-PDF预览文件', 'doc', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f21d9016a4f2afb390004', '20190424194618', '20190424194618', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '0', '4028b8816a4f21d9016a4f2721760002', '1', 'docx:OFFICE原始文件-To-PDF预览文件', 'docx', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f39fe016a4f433dfe0007', '20190424201247', '20190424201247', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816a4f39fe016a4f4225140006', '2', 'doc:PDF预览文件-To-图片册', 'doc', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f39fe016a4f4402d00008', '20190424201338', '20190424201338', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816a4f39fe016a4f4225140006', '2', 'docx:PDF预览文件-To-图片册', 'docx', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f39fe016a4f563013002f', '20190424203329', '20190424203329', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816a4f7e90016a4f807ef60001', '1', 'xls:OFFICE原始文件-To-HTML预览文件', 'xls', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f39fe016a4f5873390030', '20190424203557', '20190424203557', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816a4f7e90016a4f807ef60001', '1', 'xlsx:OFFICE原始文件-To-HTML预览文件', 'xlsx', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f39fe016a4f5e8a890039', '20190424204237', '20190424204237', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '0', '4028b8816a4f21d9016a4f2721760002', '1', 'ppt:OFFICE原始文件-To-PDF预览文件', 'ppt', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f39fe016a4f5e8a96003a', '20190424204237', '20190424204237', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '0', '4028b8816a4f21d9016a4f2721760002', '1', 'pptx:OFFICE原始文件-To-PDF预览文件', 'pptx', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a4f39fe016a4f635be50043', '20190424204752', '20190424204752', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1', '100000000', '1', '4028b8816a4f21d9016a4f2721760002', '1', 'txt:OFFICE原始文件-To-PDF预览文件', 'txt', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a5265da016a527196d0003d', '20190425110217', '20190425110217', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1', '100000000', '0', '4028b8816a4f7e90016a4f807ef60001', '0', 'doc:OFFICE原始文件-To-HTML预览文件', 'doc', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a5265da016a527196e4003e', '20190425110217', '20190425110217', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1', '100000000', '0', '4028b8816a4f7e90016a4f807ef60001', '0', 'docx:OFFICE原始文件-To-HTML预览文件', 'docx', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a52edb2016a52eef3760001', '20190425131912', '20190425131912', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', 'RAR5目前不能支持', '1', '500000000', '1', '4028b8816a52ebc0016a52eca3fb0001', '0', 'rar:ZIP|RAR原始文件-To-txt预览文件', 'rar', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a52edb2016a52ef40710002', '20190425131932', '20190425131932', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '500000000', '1', '4028b8816a52ebc0016a52eca3fb0001', '1', 'zip:ZIP|RAR原始文件-To-txt预览文件', 'zip', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a52edb2016a531d87b2005d', '20190425141005', '20190425141005', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1', '500000000', '0', '4028b8816a52edb2016a531d61d3005c', '2', 'zip:TXT预览文件-To-HTML预览文件', 'zip', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816a549e9b016a54c51ba30030', '20190425215244', '20190425215244', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1', '100000000', '1', '4028b8816a549e9b016a54c46c8e002f', '0', 'txt:TXT原始文件-To-原始txt文件', 'txt', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c14e8c9016c155db82c00c0', '20190722002919', '20190722002919', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1', '1000000000', '0', '4028b8816c14e8c9016c155c1ceb00bf', '2', 'zip:TXT预览文件-To-PDF预览文件', 'zip', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c14e8c9016c155eee9500c1', '20190722003038', '20190722003038', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1', '100000000', '0', '4028b8816a4f39fe016a4f4225140006', '3', 'zip:PDF预览文件-To-图片册', 'zip', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c1d0166016c1d9aeeb00020', '20190723145308', '20190723145308', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '0', '', '1', '1111111111', '1', '4028b8816a4f7e90016a4f807ef60001', '1', 'txt:OFFICE原始文件-To-HTML预览文件', 'txt', '60000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c29a4ad016c29ae6a940028', '20190725230952', '20190725230952', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816a549e9b016a54c46c8e002f', '0', 'log:PDF原始文件-To-原始PDF文件', 'log', '100000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c32dd68016c32e87c680026', '20190727180952', '20190727180952', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816a4f39fe016a4f4225140006', '1', 'ppt:PDF预览文件-To-图片册', 'ppt', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c32dd68016c32e941ee0028', '20190727181043', '20190727181043', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816a4f39fe016a4f4225140006', '1', 'pptx:PDF预览文件-To-图片册', 'pptx', '180000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c9ac891016c9ad68af30015', '20190816223047', '20190816223047', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816c9ac891016c9ad56a280014', '0', 'jpg:图片原始文件-To-原始图片', 'jpg', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c9ac891016c9ad9ec920041', '20190816223428', '20190816223428', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '20000000', '1', '4028b8816c9ac891016c9ad56a280014', '0', 'png:原始文件-To-原始文件', 'png', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c9ac891016c9ada03b40042', '20190816223434', '20190816223434', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '20000000', '1', '4028b8816c9ac891016c9ad56a280014', '0', 'jpeg:原始文件-To-原始文件', 'jpeg', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816c9ac891016c9ada1b490043', '20190816223440', '20190816223440', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '20000000', '1', '4028b8816c9ac891016c9ad56a280014', '0', 'gif:原始文件-To-原始文件', 'gif', '5000');
INSERT INTO `wdap_view_regular` VALUES ('4028b8816d06bfb7016d06c320c10002', '20190906212834', '20190906212834', '系统管理员', '40288b854a329988014a329a12f30002', '系统管理员', '40288b854a329988014a329a12f30002', '1', '', '1', '100000000', '1', '4028b8816d06bfb7016d06c0b8460001', '0', 'txt:原始txt文件-To-HTML预览文件(按编码读取)', 'txt', '120000');

-- ----------------------------
-- Table structure for `wdap_view_task`
-- ----------------------------
DROP TABLE IF EXISTS `wdap_view_task`;
CREATE TABLE `wdap_view_task` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL COMMENT '0等待1执行2完成3错误',
  `PCONTENT` varchar(128) DEFAULT NULL,
  `VIEWDOCID` varchar(32) DEFAULT NULL,
  `CONVERTORID` varchar(32) DEFAULT NULL,
  `STIME` varchar(16) DEFAULT NULL,
  `ETIME` varchar(16) DEFAULT NULL,
  `FILEID` varchar(32) NOT NULL,
  `BEFORETASK` varchar(32) DEFAULT NULL,
  `VIEWIS` varchar(1) NOT NULL,
  `OUTTIMENUM` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RE_WDAP_TASK_CONVERTOR` (`CONVERTORID`),
  KEY `FK_RE_WDAP_VIEWTASK_DOC` (`VIEWDOCID`),
  CONSTRAINT `FK_RE_WDAP_TASK_CONVERTOR` FOREIGN KEY (`CONVERTORID`) REFERENCES `wdap_view_convertor` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wdap_view_task
-- ----------------------------

-- ----------------------------
-- Table structure for `wfs_dirtype`
-- ----------------------------
DROP TABLE IF EXISTS `wfs_dirtype`;
CREATE TABLE `wfs_dirtype` (
  `ID` varchar(32) NOT NULL,
  `CTIME` varchar(16) NOT NULL,
  `ETIME` varchar(16) NOT NULL,
  `CUSERNAME` varchar(64) NOT NULL,
  `CUSER` varchar(32) NOT NULL,
  `PSTATE` varchar(2) NOT NULL,
  `PCONTENT` varchar(128) DEFAULT NULL,
  `SORT` int(11) NOT NULL,
  `PARENTID` varchar(32) NOT NULL,
  `NAME` varchar(128) NOT NULL,
  `TREECODE` varchar(256) NOT NULL,
  `DIRSIZE` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wfs_dirtype
-- ----------------------------

-- ----------------------------
-- Table structure for `wfs_file`
-- ----------------------------
DROP TABLE IF EXISTS `wfs_file`;
CREATE TABLE `wfs_file` (
  `ID` varchar(32) NOT NULL,
  `WDAPFILEID` varchar(32) NOT NULL,
  `TITLE` varchar(256) NOT NULL,
  `FSIZE` bigint(20) NOT NULL,
  `FTYPE` varchar(16) NOT NULL,
  `CTIME` varchar(14) NOT NULL,
  `UTIME` varchar(14) NOT NULL,
  `TYPEID` varchar(32) DEFAULT NULL,
  `USERID` varchar(32) NOT NULL,
  `USERNAME` varchar(64) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_WFS_FILE_REFERENCE_WFS_DIRT` (`TYPEID`),
  CONSTRAINT `FK_WFS_FILE_REFERENCE_WFS_DIRT` FOREIGN KEY (`TYPEID`) REFERENCES `wfs_dirtype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wfs_file
-- ----------------------------
