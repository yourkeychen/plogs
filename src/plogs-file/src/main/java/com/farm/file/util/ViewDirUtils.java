package com.farm.file.util;

import java.io.File;

/**
 * 获得文件的预览根文件夹
 * 
 * @author macpl
 *
 */
public class ViewDirUtils {

	/**
	 * 获得文件的预览根文件夹
	 * 
	 * @param realfile
	 *            文件本体
	 * @return
	 */
	public static String getPath(File realfile) {
		String path = realfile.getParentFile().getPath();
		path = path + File.separator + realfile.getName().replaceAll("\\.", "_") + "_view";
		return path;
	}

}
