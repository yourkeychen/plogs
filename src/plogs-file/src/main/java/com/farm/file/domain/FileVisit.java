package com.farm.file.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/* *
 *功能：文件统计类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "FileVisit")
@Table(name = "wdap_file_visit")
public class FileVisit implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "FILEID", length = 32, nullable = false)
	private String fileid;
	@Column(name = "VIEWNUM", length = 10, nullable = false)
	private Integer viewnum;
	@Column(name = "DOWNUM", length = 10, nullable = false)
	private Integer downum;

	public String getFileid() {
		return this.fileid;
	}

	public void setFileid(String fileid) {
		this.fileid = fileid;
	}

	public Integer getViewnum() {
		return this.viewnum;
	}

	public void setViewnum(Integer viewnum) {
		this.viewnum = viewnum;
	}

	public Integer getDownum() {
		return this.downum;
	}

	public void setDownum(Integer downum) {
		this.downum = downum;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}