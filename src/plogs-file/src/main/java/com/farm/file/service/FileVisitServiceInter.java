package com.farm.file.service;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileVisit;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：文件统计服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface FileVisitServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public FileVisit insertFilevisitEntity(FileVisit entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public FileVisit editFilevisitEntity(FileVisit entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteFilevisitEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public FileVisit getFilevisitEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createFilevisitSimpleQuery(DataQuery query);

	/**
	 * 通过知识创建一条默认统计记录
	 * 
	 * @param entity
	 * @param user
	 */
	public void insertFilevisitEntity(FileBase entity, LoginUser user);

	/**
	 * 通過附件id获得访问统计对象
	 * 
	 * @param ids
	 * @return
	 */
	public FileVisit getFilevisitByFileId(String fileid);

	/**
	 * 通过附件id删除对象
	 * 
	 * @param fileid
	 * @param user
	 */
	public void deleteFilevisitByFileid(String fileid, LoginUser user);

	/**
	 * 一个文件被下载
	 * 
	 * @param fileid
	 */
	public void downloadFileHandle(String fileid);

	/**
	 * 一个文件被预览
	 * 
	 * @param fileid
	 */
	public void viewFileHandle(String fileid);
}