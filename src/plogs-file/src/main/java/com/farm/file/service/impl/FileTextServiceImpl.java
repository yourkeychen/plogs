package com.farm.file.service.impl;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileText;
import org.apache.log4j.Logger;
import com.farm.file.dao.FileTextDaoInter;
import com.farm.file.service.FileTextServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.inter.impl.FileEventHandle;

/* *
 *功能：文件文本服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class FileTextServiceImpl implements FileTextServiceInter {
	@Resource
	private FileTextDaoInter filetextDaoImpl;

	private static final Logger log = Logger.getLogger(FileTextServiceImpl.class);

	@Override
	@Transactional
	public FileText insertFiletextEntity(FileText entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		entity.setCompleteis("0");
		return filetextDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public FileText editFiletextEntity(FileText entity, LoginUser user) {
		FileText entity2 = filetextDaoImpl.getEntity(entity.getId());
		entity2.setFileid(entity.getFileid());
		int MAXLENGTH = 2000000;
		if (entity.getFiletext().length() >= MAXLENGTH) {
			entity.setFiletext(entity.getFiletext().substring(0, MAXLENGTH - 1));
		}
		entity2.setFiletext(entity.getFiletext());
		entity2.setPcontent(entity.getPcontent());
		entity2.setPstate(entity.getPstate());
		entity2.setCtime(entity.getCtime());
		entity2.setId(entity.getId());
		entity2.setCompleteis(entity.getCompleteis());
		filetextDaoImpl.editEntity(entity2);
		FileEventHandle.getInstance().fileTextOnChange(entity.getFileid());
		return entity2;
	}

	@Override
	@Transactional
	public void deleteFiletextEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		filetextDaoImpl.deleteEntity(filetextDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public FileText getFiletextEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return filetextDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFiletextSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WDAP_FILE_TEXT", "ID,FILEID,FILETEXT,PCONTENT,PSTATE,CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public void insertFiletextEntity(FileBase filebase, LoginUser user) {
		FileText text = new FileText();
		text.setFileid(filebase.getId());
		text.setCtime(filebase.getCtime());
		text.setFiletext(filebase.getTitle());
		text.setPstate("1");
		insertFiletextEntity(text, user);
	}

	@Override
	@Transactional
	public FileText getFiletextByFileId(String fileid) {
		List<FileText> texts = filetextDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("FILEID", fileid, "=")).toList());
		if (texts.size() <= 0) {
			return null;
		}
		return texts.get(0);
	}

	@Override
	@Transactional
	public void deleteFiletextByFileid(String fileid, LoginUser user) {
		filetextDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("FILEID", fileid, "=")).toList());
	}

}
