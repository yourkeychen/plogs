package com.farm.file.domain.ex;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileResource;
import com.farm.file.domain.FileVisit;

/**
 * 用来封装文件展示对象
 * 
 * @author macpl
 *
 */
public class FileView {
	// 文件对象
	private FileBase file;
	// 下载地址
	private String downloadUrl;
	// 图标地址
	private String iconUrl;
	// 预览地址
	private String viewUrl;
	// 状态标签
	private String statelabel;
	// 模型标签
	private String modellabel;
	// 资源库
	private FileResource resource;
	// 真实地址
	private String realpath;
	/**
	 * 是否物理存在
	 */
	private boolean physicsExist;
	// 文件大小标签
	private String sizelabel;
	// 统计对象
	private FileVisit visit;
	private Integer viewNum;
	private Integer dowNum;

	public Integer getDowNum() {
		return dowNum;
	}

	public void setDowNum(Integer dowNum) {
		this.dowNum = dowNum;
	}

	public Integer getViewNum() {
		return viewNum;
	}

	public String getSizelabel() {
		return sizelabel;
	}
	
	public boolean isPhysicsExist() {
		return physicsExist;
	}

	public void setPhysicsExist(boolean physicsExist) {
		this.physicsExist = physicsExist;
	}

	public void setSizelabel(String sizelabel) {
		this.sizelabel = sizelabel;
	}

	public String getRealpath() {
		return realpath;
	}

	public void setRealpath(String realpath) {
		this.realpath = realpath;
	}

	public FileResource getResource() {
		return resource;
	}

	public void setResource(FileResource resource) {
		this.resource = resource;
	}

	public String getStatelabel() {
		return statelabel;
	}

	public void setStatelabel(String statelabel) {
		this.statelabel = statelabel;
	}

	public String getModellabel() {
		return modellabel;
	}

	public void setModellabel(String modellabel) {
		this.modellabel = modellabel;
	}

	public FileBase getFile() {
		return file;
	}

	public void setFile(FileBase file) {
		this.file = file;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getViewUrl() {
		return viewUrl;
	}

	public FileVisit getVisit() {
		return visit;
	}

	public void setVisit(FileVisit visit) {
		this.visit = visit;
	}

	public void setViewUrl(String viewUrl) {
		this.viewUrl = viewUrl;
	}

	public void setViewNum(Integer viewnum) {
		this.viewNum = viewnum;
	}

	public void setDownum(Integer downum) {
		this.dowNum = downum;
	}
}
