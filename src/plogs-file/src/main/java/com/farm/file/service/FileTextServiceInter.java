package com.farm.file.service;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileText;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：文件文本服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface FileTextServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public FileText insertFiletextEntity(FileText entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public FileText editFiletextEntity(FileText entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteFiletextEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public FileText getFiletextEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createFiletextSimpleQuery(DataQuery query);

	/**
	 * 通过知识创建文件文本描述
	 * 
	 * @param entity
	 * @param user
	 */
	public void insertFiletextEntity(FileBase entity, LoginUser user);

	/**
	 * 通过附件id查询文本对象
	 * 
	 * @param fileid
	 * @return
	 */
	public FileText getFiletextByFileId(String fileid);

	/**通过附件id删除对象
	 * @param fileid
	 * @param user
	 */
	public void deleteFiletextByFileid(String fileid, LoginUser user);
}