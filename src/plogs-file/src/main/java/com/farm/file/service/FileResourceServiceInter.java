package com.farm.file.service;

import com.farm.file.domain.FileResource;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：资源库服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface FileResourceServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public FileResource insertFileresourceEntity(FileResource entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public FileResource editFileresourceEntity(FileResource entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteFileresourceEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public FileResource getFileresourceEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createFileresourceSimpleQuery(DataQuery query);

	/**
	 * 獲得所有的資源列表
	 * 
	 * @return
	 */
	public List<FileResource> getResources();

	/**
	 * 随机获得可用资源库地址
	 * 
	 * @return
	 */
	public String getRandomResourceid();
}