package com.farm.file.service.impl;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileText;
import com.farm.file.domain.FileVisit;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.file.dao.FileVisitDaoInter;
import com.farm.file.service.FileVisitServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：文件统计服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class FileVisitServiceImpl implements FileVisitServiceInter {
	@Resource
	private FileVisitDaoInter filevisitDaoImpl;

	private static final Logger log = Logger.getLogger(FileVisitServiceImpl.class);

	@Override
	@Transactional
	public FileVisit insertFilevisitEntity(FileVisit entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return filevisitDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public FileVisit editFilevisitEntity(FileVisit entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		FileVisit entity2 = filevisitDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setFileid(entity.getFileid());
		entity2.setViewnum(entity.getViewnum());
		entity2.setDownum(entity.getDownum());
		entity2.setId(entity.getId());
		filevisitDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteFilevisitEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		filevisitDaoImpl.deleteEntity(filevisitDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public FileVisit getFilevisitEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return filevisitDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFilevisitSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WDAP_FILE_VISIT", ",FILEID,VIEWNUM,DOWNUM,ID");
		return dbQuery;
	}

	@Override
	@Transactional
	public void insertFilevisitEntity(FileBase filebase, LoginUser user) {
		FileVisit visit = new FileVisit();
		visit.setDownum(0);
		visit.setFileid(filebase.getId());
		visit.setViewnum(0);
		insertFilevisitEntity(visit, user);
	}

	@Override
	@Transactional
	public FileVisit getFilevisitByFileId(String fileid) {
		List<FileVisit> texts = filevisitDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("FILEID", fileid, "=")).toList());
		return texts.get(0);
	}

	@Override
	@Transactional
	public void deleteFilevisitByFileid(String fileid, LoginUser user) {
		filevisitDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("FILEID", fileid, "=")).toList());
	}

	@Override
	@Transactional
	public void downloadFileHandle(String fileid) {
		FileVisit filevisit = getFilevisitByFileId(fileid);
		filevisit.setDownum(filevisit.getDownum() + 1);
		filevisitDaoImpl.editEntity(filevisit);
	}

	@Override
	@Transactional
	public void viewFileHandle(String fileid) {
		FileVisit filevisit = getFilevisitByFileId(fileid);
		filevisit.setViewnum(filevisit.getViewnum() + 1);
		filevisitDaoImpl.editEntity(filevisit);
	}
}
