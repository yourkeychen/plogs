package com.farm.file.impl;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.FileText;
import com.farm.file.domain.ex.FileView;
import com.farm.file.domain.ex.PersistFile;
import com.farm.file.enums.FileModel;
import com.farm.file.exception.FileExNameException;
import com.farm.file.exception.FileSizeException;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.farm.file.WdapFileServiceInter;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.file.service.FileResourceServiceInter;
import com.farm.file.service.FileTextServiceInter;
import com.farm.file.service.FileVisitServiceInter;
import com.farm.file.util.FarmDocFiles;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.io.FileNotFoundException;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：文件服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class WdapFileServiceImpl implements WdapFileServiceInter {
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	@Resource
	private FileResourceServiceInter fileResourceServiceImpl;
	@Resource
	private FileTextServiceInter fileTextServiceImpl;
	@Resource
	private FileVisitServiceInter fileVisitServiceImpl;

	private static final Logger log = Logger.getLogger(WdapFileServiceImpl.class);

	@Override
	public FileBase saveLocalFile(MultipartFile file, FileModel model, LoginUser currentUser, String fileProcesskey)
			throws FileSizeException, FileExNameException {
		// 校验文件长度
		if (!fileBaseServiceImpl.validateFileSize(file.getSize(), model)) {
			throw new FileSizeException("the file max size is " + model.getMaxsize());
		}
		// 校驗文件類型
		if (!fileBaseServiceImpl.validateFileExname(file.getOriginalFilename(), model)) {
			throw new FileExNameException("the file type is not exist " + StringUtils.join(model.getExnames(), ","));
		}
		CommonsMultipartFile cmFile = (CommonsMultipartFile) file;
		DiskFileItem item = (DiskFileItem) cmFile.getFileItem();
		{// 小于8k不生成到临时文件，临时解决办法。zhanghc20150919
			if (!item.getStoreLocation().exists()) {
				try {
					item.write(item.getStoreLocation());
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		FileBase filebase = fileBaseServiceImpl.saveFile(item.getStoreLocation(), file.getOriginalFilename(),
				currentUser, fileProcesskey);
		return filebase;
	}

	@Override
	public FileBase saveLocalFile(byte[] data, FileModel fileModel, String filename, LoginUser currentUser)
			throws FileSizeException, FileExNameException {
		// 校验文件长度
		if (!fileBaseServiceImpl.validateFileSize(data.length, fileModel)) {
			throw new FileSizeException("the file max size is " + fileModel.getMaxsize());
		}
		// 校驗文件類型
		if (!fileBaseServiceImpl.validateFileExname(filename, fileModel)) {
			throw new FileExNameException(
					"the file type is not exist " + StringUtils.join(fileModel.getExnames(), ","));
		}
		FileBase filebase = fileBaseServiceImpl.saveFile(data, filename, currentUser);
		return filebase;
	}

	@Override
	public String getDownloadUrl(String fileid, FileModel model) {
		return fileBaseServiceImpl.getDownloadUrl(fileid, model);
	}

	@Override
	public String getExName(String filename) {
		return FarmDocFiles.getExName(filename);
	}

	@Override
	public PersistFile getFileIconFile(String fileid) {
		String exname = fileBaseServiceImpl.getFilebaseEntity(fileid).getExname();
		PersistFile appIcon = new PersistFile();
		appIcon.setName("icon." + exname);
		try {
			if (FileModel.getModelByFileExName(exname).equals(FileModel.IMG)) {
				// 图片就直接读取图片
				appIcon.setFile(new File(fileBaseServiceImpl.getFileRealPath(fileid)));
				appIcon.setName("icon." + exname);
			} else {
				// 不是图片就读取系统图标
				appIcon.setFile(FarmDocFiles.getSysIcon(exname.toUpperCase()));
				appIcon.setName("icon.png");
			}
		} catch (FileExNameException e) {
			throw new RuntimeException(e);
		}
		return appIcon;
	}

	@Override
	public void delFileByLogic(String fileid) {
		fileBaseServiceImpl.deleteFileByLogic(fileid, null);
	}

	@Override
	public void submitFile(String fileid) throws FileNotFoundException {
		fileBaseServiceImpl.submitFile(fileid);
	}

	@Override
	public void freeFile(String fileid) {
		fileBaseServiceImpl.freeFile(fileid);
	}

	@Override
	public PersistFile getPersistFile(String fileid) {
		PersistFile appIcon = new PersistFile();
		FileBase filebase = fileBaseServiceImpl.getFilebaseEntity(fileid);
		appIcon.setName(filebase.getTitle());
		appIcon.setFile(new File(fileBaseServiceImpl.getFileRealPath(fileid)));
		appIcon.setSecret(filebase.getSecret());
		return appIcon;
	}

	@Override
	public String getFileRealPath(String fileid) {
		return fileBaseServiceImpl.getFileRealPath(fileid);
	}

	@Override
	public FileBase initFile(LoginUser user, String filename, long length, String appid, String sysname) {
		return fileBaseServiceImpl.initFileBase(user, filename, length, appid, sysname);
	}

	@Override
	public String getIconUrl(String fileid) {
		try {
			return fileBaseServiceImpl.getIconUrl(fileid,
					FileModel.getModelByFileExName(fileBaseServiceImpl.getFilebaseEntity(fileid).getExname()));
		} catch (FileExNameException e) {
			log.error(e);
			return e.getMessage();
		}
	}

	@Override
	public FileModel getFileModel(String fileid) throws FileExNameException {
		return FileModel.getModelByFileExName(fileBaseServiceImpl.getFilebaseEntity(fileid).getExname());
	}

	@Override
	public FileView getFileInfo(String fileid) throws FileNotFoundException {
		return fileBaseServiceImpl.getFileView(fileid);
	}

	@Override
	public void downloadFileHandle(String fileid) {
		fileVisitServiceImpl.downloadFileHandle(fileid);
	}

	@Override
	public void viewFileHandle(String fileid) {
		fileVisitServiceImpl.viewFileHandle(fileid);
	}

	@Override
	public String getFiletext(String fileid) {
		FileText txt = fileTextServiceImpl.getFiletextByFileId(fileid);
		if (txt != null && txt.getCompleteis().equals("1")) {
			return txt.getFiletext();
		} else {
			return "";
		}
	}

	@Override
	public String getFileIdByAppId(String appid) {
		return fileBaseServiceImpl.getFileIdByAppId(appid);
	}

	@Override
	public FileBase getFileBase(String fileid) {
		return fileBaseServiceImpl.getFilebaseEntity(fileid);
	}

	@Override
	public void updateFilesize(String fileId, int length) {
		fileBaseServiceImpl.editFileSize(fileId, length);
	}

	@Override
	public void WriteTextFile(String fileId, String text, LoginUser currentUser) {
		fileBaseServiceImpl.WriteTextFile(fileId, text, currentUser);
	}
}
