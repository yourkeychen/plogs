package com.farm.view.service.impl;

import com.farm.view.domain.Viewtask;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.view.dao.ViewtaskDaoInter;
import com.farm.view.service.ViewtaskServiceInter;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;
/* *
 *功能：转换任务服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class ViewtaskServiceImpl implements ViewtaskServiceInter{
  @Resource
  private ViewtaskDaoInter  viewtaskDaoImpl;

  private static final Logger log = Logger.getLogger(ViewtaskServiceImpl.class);
  @Override
  @Transactional
  public Viewtask insertViewtaskEntity(Viewtask entity,LoginUser user) {
    // TODO 自动生成代码,修改后请去除本注释
    //entity.setCuser(user.getId());
    //entity.setCtime(TimeTool.getTimeDate14());
    //entity.setCusername(user.getName());
    //entity.setEuser(user.getId()); 
    //entity.setEusername(user.getName());
    //entity.setEtime(TimeTool.getTimeDate14());
    //entity.setPstate("1");
    return viewtaskDaoImpl.insertEntity(entity);
  }
  @Override
  @Transactional
  public Viewtask editViewtaskEntity(Viewtask entity,LoginUser user) {
    // TODO 自动生成代码,修改后请去除本注释
    Viewtask entity2 = viewtaskDaoImpl.getEntity(entity.getId());
    //entity2.setEuser(user.getId());
    //entity2.setEusername(user.getName());
    //entity2.setEtime(TimeTool.getTimeDate14()); 
    entity2.setFileid(entity.getFileid());
    entity2.setStime(entity.getStime());
    entity2.setEtime(entity.getEtime());
    entity2.setConvertorid(entity.getConvertorid());
    entity2.setViewdocid(entity.getViewdocid());
    entity2.setPcontent(entity.getPcontent());
    entity2.setPstate(entity.getPstate());
    entity2.setCuser(entity.getCuser());
    entity2.setCusername(entity.getCusername());
    entity2.setCtime(entity.getCtime());
    entity2.setId(entity.getId());
    viewtaskDaoImpl.editEntity(entity2);
    return entity2;
  }
  @Override
  @Transactional
  public void deleteViewtaskEntity(String id,LoginUser user) {
    // TODO 自动生成代码,修改后请去除本注释
    viewtaskDaoImpl.deleteEntity(viewtaskDaoImpl.getEntity(id));
  }
  @Override
  @Transactional
  public Viewtask getViewtaskEntity(String id) {
    // TODO 自动生成代码,修改后请去除本注释
    if (id == null){return null;}
    return viewtaskDaoImpl.getEntity(id);
  }
  @Override
  @Transactional
  public DataQuery createViewtaskSimpleQuery(DataQuery query) {
    // TODO 自动生成代码,修改后请去除本注释
    DataQuery dbQuery = DataQuery
        .init(
            query,
            "WDAP_VIEW_TASK",
            "ID,FILEID,STIME,ETIME,CONVERTORID,VIEWDOCID,PCONTENT,PSTATE,CUSER,CUSERNAME,CTIME");
    return dbQuery;
  }

}
