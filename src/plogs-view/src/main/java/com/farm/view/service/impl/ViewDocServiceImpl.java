package com.farm.view.service.impl;

import com.farm.view.domain.ViewDoc;
import com.farm.view.domain.ViewFileModel;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.ViewFileModels;
import com.farm.core.time.TimeTool;
import com.farm.file.domain.FileBase;
import com.farm.file.domain.ex.PersistFile;
import com.farm.file.enums.FileModel;
import com.farm.file.exception.FileExNameException;
import com.farm.file.impl.WdapFileServiceImpl;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.file.util.FarmDocFiles;

import org.apache.log4j.Logger;
import com.farm.view.dao.ViewDocDaoInter;
import com.farm.view.service.ViewDocServiceInter;
import com.farm.view.service.ViewFileModelServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：预览文件服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class ViewDocServiceImpl implements ViewDocServiceInter {
	@Resource
	private ViewDocDaoInter viewdocDaoImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	@Resource
	private ViewFileModelServiceInter viewModelServiceImpl;
	private static final Logger log = Logger.getLogger(ViewDocServiceImpl.class);

	@Override
	@Transactional
	public ViewDoc insertViewdocEntity(ViewDoc entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return viewdocDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public ViewDoc editViewdocEntity(ViewDoc entity, LoginUser user) {
		ViewDoc entity2 = viewdocDaoImpl.getEntity(entity.getId());
		entity2.setViewis(entity.getViewis());
		entity2.setPstate(entity.getPstate());
		viewdocDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteViewdocEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		viewdocDaoImpl.deleteEntity(viewdocDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public ViewDoc getViewdocEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return viewdocDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createViewdocSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"WDAP_VIEW_DOC A LEFT JOIN WDAP_FILE B ON A.FILEID=B.ID LEFT JOIN WDAP_VIEW_FILEMODEL C ON A.MODELID=C.ID ",
				"A.CTIME AS CTIME,A.ID as ID,B.TITLE AS FILETITLE,C.TITLE AS MODELTITLE,A.VIEWIS AS VIEWIS,A.PSTATE AS PSTATE");
		dbQuery.addDefaultSort(new DBSort("A.CTIME", "desc"));
		return dbQuery;
	}

	@Override
	@Transactional
	public ViewDoc insertViewdoc(ViewDoc viewDoc) {
		viewDoc.setCuser("NONE");
		viewDoc.setCtime(TimeTool.getTimeDate14());
		viewDoc.setCusername("NONE");
		viewDoc.setEuser("NONE");
		viewDoc.setEusername("NONE");
		viewDoc.setEtime(TimeTool.getTimeDate14());
		return viewdocDaoImpl.insertEntity(viewDoc);
	}

	@Override
	@Transactional
	public void deleteViewdocsByFileId(String fileid, String userId, String userName) {
		viewdocDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("FILEID", fileid, "=")).toList());
	}

	@Override
	@Transactional
	public List<ViewDoc> getViewdocsByFileId(String fileid) {
		return viewdocDaoImpl.selectEntitys(DBRuleList.getInstance().add(new DBRule("FILEID", fileid, "=")).toList());
	}

	@Override
	@Transactional
	public void deleteViewdocEntity(ViewDoc viewdoc) {
		viewdocDaoImpl.deleteEntity(viewdoc);
	}

	@Override
	@Transactional
	public List<Map<String, Object>> getFileViewdocs(String fileid) {
		DataQuery dbQuery = DataQuery.getInstance(1,
				"A.CTIME AS CTIME,A.ID as ID,B.TITLE AS FILETITLE,B.ID AS FILEID,C.MODELKEY as MODELKEY,C.TITLE AS MODELTITLE,A.VIEWIS AS VIEWIS,A.PSTATE AS PSTATE",
				"WDAP_VIEW_DOC A LEFT JOIN WDAP_FILE B ON A.FILEID=B.ID LEFT JOIN WDAP_VIEW_FILEMODEL C ON A.MODELID=C.ID ");
		dbQuery.addDefaultSort(new DBSort("A.CTIME", "desc"));
		dbQuery.addRule(new DBRule("A.VIEWIS", "1", "="));
		dbQuery.addRule(new DBRule("FILEID", fileid, "=")).setPagesize(20);
		dbQuery.setNoCount();
		DataResult result;
		try {
			result = dbQuery.search();
			result.runHandle(new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					row.put("ICONURL", "downview/Pubicon.do?id=" + (String) row.get("ID"));
					row.put("DOWNURL",
							ViewFileModels.getModel((String) row.get("MODELKEY")).getDownUrl((String) row.get("ID")));
					row.put("VIEWURL",
							ViewFileModels.getModel((String) row.get("MODELKEY")).getViewUrl((String) row.get("ID")));
				}
			});
			return result.getResultList();
		} catch (SQLException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
	}

	@Override
	@Transactional
	public int getModelFileSize(String viewdocId) {
		ViewFileModelInter medol = getViewFileModelByViewDocId(viewdocId);
		ViewDoc doc = getViewdocEntity(viewdocId);
		FileBase filebase = fileBaseServiceImpl.getFilebaseEntity(doc.getFileid());
		File file = medol.getModelFile(fileBaseServiceImpl.getFile(filebase), filebase.getExname());
		if (file.isDirectory()) {
			// 是文件夾
			return file.listFiles().length;
		} else {
			// 是文件
			return 1;
		}
	}

	@Override
	@Transactional
	public ViewFileModelInter getViewFileModelByViewDocId(String viewdocId) {
		ViewDoc doc = getViewdocEntity(viewdocId);
		ViewFileModel model = viewModelServiceImpl.getViewmodelEntity(doc.getModelid());
		return ViewFileModels.getModel(model.getModelkey());
	}
}
