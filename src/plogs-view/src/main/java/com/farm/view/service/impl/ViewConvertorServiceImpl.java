package com.farm.view.service.impl;

import com.farm.view.domain.ViewConvertor;
import com.farm.view.domain.ViewRegular;
import com.farm.core.time.TimeTool;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import com.farm.view.dao.ViewConvertorDaoInter;
import com.farm.view.service.ViewConvertorServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：文件转换器服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class ViewConvertorServiceImpl implements ViewConvertorServiceInter {
	@Resource
	private ViewConvertorDaoInter viewconvertorDaoImpl;

	private static final Logger log = Logger.getLogger(ViewConvertorServiceImpl.class);

	@Override
	@Transactional
	public ViewConvertor insertViewconvertorEntity(ViewConvertor entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(user.getName());
		entity.setEuser(user.getId());
		entity.setEusername(user.getName());
		entity.setEtime(TimeTool.getTimeDate14());
		return viewconvertorDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public ViewConvertor editViewconvertorEntity(ViewConvertor entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		ViewConvertor entity2 = viewconvertorDaoImpl.getEntity(entity.getId());
		entity2.setTitle(entity.getTitle());
		entity2.setTmodelid(entity.getTmodelid());
		entity2.setOmodelid(entity.getOmodelid());
		entity2.setPcontent(entity.getPcontent());
		entity2.setClasskey(entity.getClasskey());
		entity2.setPstate(entity.getPstate());
		viewconvertorDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteViewconvertorEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		viewconvertorDaoImpl.deleteEntity(viewconvertorDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public ViewConvertor getViewconvertorEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return viewconvertorDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createViewconvertorSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"WDAP_VIEW_CONVERTOR a left join WDAP_VIEW_FILEMODEL T on T.id=a.TMODELID left join WDAP_VIEW_FILEMODEL O on O.id=a.OMODELID",
				"a.ID as ID,a.CTIME as CTIME,a.TITLE as TITLE,TMODELID,OMODELID,a.PSTATE as PSTATE,a.PCONTENT as PCONTENT,CLASSKEY,T.TITLE as TTITLE,O.TITLE as OTITLE");
		return dbQuery;
	}

	@Override
	@Transactional
	public List<ViewConvertor> getAllConvertor() {
		return viewconvertorDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("PSTATE", "1", "=")).toList());
	}

	@Override
	@Transactional
	public void copyViewConvertor(String convertorid, LoginUser currentUser) {
		ViewConvertor convertor = new ViewConvertor();
		ViewConvertor oConvertor = getViewconvertorEntity(convertorid);
		try {
			BeanUtils.copyProperties(convertor, oConvertor);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		convertor.setId(null);
		convertor.setTitle("复制-" + convertor.getTitle());
		insertViewconvertorEntity(convertor, currentUser);
	}
}
