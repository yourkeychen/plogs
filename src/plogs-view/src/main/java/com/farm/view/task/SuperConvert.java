package com.farm.view.task;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.farm.view.convertor.FileConvertorInter;
import com.farm.view.task.domain.ConvertAbleTask;

/**
 * 转换任务的壳子，超时可以报异常
 * 
 * @author wangdong
 *
 */
public class SuperConvert implements Callable<Boolean> {
	private static final Logger log = Logger.getLogger(SuperConvert.class);
	private ConvertAbleTask task;

	public SuperConvert(ConvertAbleTask task) {
		this.task = task;
	}

	@Override
	public Boolean call() throws Exception {
		FileConvertorInter convertor = task.getFileConvertor();
		convertor.doConvert(task.getFileInfo(), task.getOviewFileModel(), task.getTviewFileModel());
		return true; 
	}

}
