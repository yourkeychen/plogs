package com.farm.view.model.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.farm.file.domain.FileBase;
import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.farm.view.model.utils.ModelFileUtils;

/**
 * 图片集文件夹
 * 
 * @author macpl
 *
 */
public class ImgDirModel implements ViewFileModelInter {

	@Override
	public String getTitle() {
		return "图片册";
	}

	@Override
	public String getKey() {
		return "IMGS";
	}

	@Override
	public ModelParas getParamegers() {
		ModelParas paras = new ModelParas();
		return paras;
	}

	@Override
	public File getModelFile(File file, String exname) {
		File modelFile = ModelFileUtils.getModelPath(file, "IMGS");
		return modelFile;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return FarmDocFiles.getSysIcon("IMGS");
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return null;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "show/Pubimgs.do?viewdocId=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		return "imgs";
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return false;
	}

	@Override
	public String getText(File file) {
		return null;
	}
}
