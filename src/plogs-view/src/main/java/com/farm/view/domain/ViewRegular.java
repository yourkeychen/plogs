package com.farm.view.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：转换规则类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "ViewRegular")
@Table(name = "wdap_view_regular")
public class ViewRegular implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "SORT", length = 10, nullable = false)
        private Integer sort;
        @Column(name = "TITLE", length = 64, nullable = false)
        private String title;
        @Column(name = "FILEEXNAME", length = 32, nullable = false)
        private String fileexname;
        @Column(name = "CONVERTORID", length = 32, nullable = false)
        private String convertorid;
        @Column(name = "PCONTENT", length = 128)
        private String pcontent;
        @Column(name = "SIZEMIN", length = 10, nullable = false)
        private Integer sizemin;
        @Column(name = "SIZEMAX", length = 10, nullable = false)
        private Integer sizemax;
        @Column(name = "VIEWIS", length = 1, nullable = false)
        private String viewis;
        @Column(name = "PSTATE", length = 2, nullable = false)
        private String pstate;
        @Column(name = "EUSER", length = 32, nullable = false)
        private String euser;
        @Column(name = "EUSERNAME", length = 64, nullable = false)
        private String eusername;
        @Column(name = "CUSER", length = 32, nullable = false)
        private String cuser;
        @Column(name = "CUSERNAME", length = 64, nullable = false)
        private String cusername;
        @Column(name = "ETIME", length = 16, nullable = false)
        private String etime;
        @Column(name = "CTIME", length = 16, nullable = false)
        private String ctime;
        @Column(name = "OUTTIMENUM", length = 10, nullable = false)
        private Integer outtimenum; 
        
        public Integer getOuttimenum() {
			return outtimenum;
		}
		public void setOuttimenum(Integer outtimenum) {
			this.outtimenum = outtimenum;
		}
		public Integer  getSort() {
          return this.sort;
        }
        public void setSort(Integer sort) {
          this.sort = sort;
        }
        public String  getTitle() {
          return this.title;
        }
        public void setTitle(String title) {
          this.title = title;
        }
        public String  getFileexname() {
          return this.fileexname;
        }
        public void setFileexname(String fileexname) {
          this.fileexname = fileexname;
        }
        public String  getConvertorid() {
          return this.convertorid;
        }
        public void setConvertorid(String convertorid) {
          this.convertorid = convertorid;
        }
        public String  getPcontent() {
          return this.pcontent;
        }
        public void setPcontent(String pcontent) {
          this.pcontent = pcontent;
        }
        public Integer  getSizemin() {
          return this.sizemin;
        }
        public void setSizemin(Integer sizemin) {
          this.sizemin = sizemin;
        }
        public Integer  getSizemax() {
          return this.sizemax;
        }
        public void setSizemax(Integer sizemax) {
          this.sizemax = sizemax;
        }
        public String  getViewis() {
          return this.viewis;
        }
        public void setViewis(String viewis) {
          this.viewis = viewis;
        }
        public String  getPstate() {
          return this.pstate;
        }
        public void setPstate(String pstate) {
          this.pstate = pstate;
        }
        public String  getEuser() {
          return this.euser;
        }
        public void setEuser(String euser) {
          this.euser = euser;
        }
        public String  getEusername() {
          return this.eusername;
        }
        public void setEusername(String eusername) {
          this.eusername = eusername;
        }
        public String  getCuser() {
          return this.cuser;
        }
        public void setCuser(String cuser) {
          this.cuser = cuser;
        }
        public String  getCusername() {
          return this.cusername;
        }
        public void setCusername(String cusername) {
          this.cusername = cusername;
        }
        public String  getEtime() {
          return this.etime;
        }
        public void setEtime(String etime) {
          this.etime = etime;
        }
        public String  getCtime() {
          return this.ctime;
        }
        public void setCtime(String ctime) {
          this.ctime = ctime;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}