package com.farm.view.task.domain;

import com.farm.view.convertor.FileConvertorInter;
import com.farm.view.model.ViewFileModelInter;

/**
 * 转换任务(由数据库数据组装而成，用于任务调度器直接使用)
 * 
 * @author macpl
 *
 */
public class ConvertAbleTask {
	// 任务超时时间限制
	private long outTime;
	//任务id
	private String taskId;
	// 原始文件模型
	private String oModelTitle;
	// 目标文件模型
	private String tModelTitle;
	// 文件转换器
	private FileConvertorInter fileConvertor;
	// 原始文件模型
	private ViewFileModelInter oviewFileModel;
	// 目标文件模型
	private ViewFileModelInter tviewFileModel;
	// 原始文件的封装
	private ConvertTaskFile fileInfo;

	public ConvertTaskFile getFileInfo() {
		return fileInfo;
	}

	public void setFileInfo(ConvertTaskFile fileInfo) {
		this.fileInfo = fileInfo;
	}

	public long getOutTime() {
		return outTime;
	}

	public void setOutTime(long outTime) {
		this.outTime = outTime;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getoModelTitle() {
		return oModelTitle;
	}

	public void setoModelTitle(String oModelTitle) {
		this.oModelTitle = oModelTitle;
	}

	public String gettModelTitle() {
		return tModelTitle;
	}

	public void settModelTitle(String tModelTitle) {
		this.tModelTitle = tModelTitle;
	}

	public FileConvertorInter getFileConvertor() {
		return fileConvertor;
	}

	public void setFileConvertor(FileConvertorInter fileConvertor) {
		this.fileConvertor = fileConvertor;
	}

	public ViewFileModelInter getOviewFileModel() {
		return oviewFileModel;
	}

	public void setOviewFileModel(ViewFileModelInter oviewFileModel) {
		this.oviewFileModel = oviewFileModel;
	}

	public ViewFileModelInter getTviewFileModel() {
		return tviewFileModel;
	}

	public void setTviewFileModel(ViewFileModelInter tviewFileModel) {
		this.tviewFileModel = tviewFileModel;
	}
}
