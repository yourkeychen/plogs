package com.farm.view.model.impl;

import java.io.File;

import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.farm.view.model.utils.HtmlFileHandles;
import com.farm.view.model.utils.ModelFileUtils;

/**
 * pdf预览文件
 * 
 * @author macpl
 *
 */
public class ViewHtmlModel implements ViewFileModelInter {

	@Override
	public String getTitle() {
		return "HTML预览文件";
	}

	@Override
	public String getKey() {
		return "VIEW_HTML";
	}

	@Override
	public ModelParas getParamegers() {
		return new ModelParas();
	}

	@Override
	public File getModelFile(File file, String exname) {
		File modelFile = ModelFileUtils.getModelPath(file, "html", "index.html");
		return modelFile;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return FarmDocFiles.getSysIcon("html");
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "downview/Pubload.do?id=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		return "html";
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return true;
	}

	@Override
	public String getText(File modelfile) {
		return HtmlFileHandles.getHtmlFileText(modelfile);
	}
}
