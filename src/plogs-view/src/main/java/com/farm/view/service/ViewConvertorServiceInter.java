package com.farm.view.service;

import com.farm.view.domain.ViewConvertor;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：文件转换器服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ViewConvertorServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ViewConvertor insertViewconvertorEntity(ViewConvertor entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ViewConvertor editViewconvertorEntity(ViewConvertor entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteViewconvertorEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ViewConvertor getViewconvertorEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createViewconvertorSimpleQuery(DataQuery query);

	/**
	 * 查找所有可用轉換器
	 * 
	 * @return
	 */
	public List<ViewConvertor> getAllConvertor();

	/**
	 * 复制一个转换器
	 * 
	 * @param id
	 * @param currentUser
	 */
	public void copyViewConvertor(String id, LoginUser currentUser);
}