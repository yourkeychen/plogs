package com.farm.view.dao.impl;

import java.math.BigInteger;
import java.sql.SQLException;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.farm.view.domain.ViewTask;
import com.farm.view.dao.ViewTaskDaoInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.utils.HibernateSQLTools;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;

/* *
 *功能：转换任务持久层实现
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Repository
public class ViewTaskDaoImpl extends HibernateSQLTools<ViewTask>implements ViewTaskDaoInter {
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFatory;

	@Override
	public void deleteEntity(ViewTask viewtask) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.delete(viewtask);
	}

	@Override
	public int getAllListNum() {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery("select count(*) from farm_code_field");
		BigInteger num = (BigInteger) sqlquery.list().get(0);
		return num.intValue();
	}

	@Override
	public ViewTask getEntity(String viewtaskid) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		return (ViewTask) session.get(ViewTask.class, viewtaskid);
	}

	@Override
	public ViewTask insertEntity(ViewTask viewtask) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.save(viewtask);
		return viewtask;
	}

	@Override
	public void editEntity(ViewTask viewtask) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.update(viewtask);
	}

	@Override
	public Session getSession() {
		// TODO 自动生成代码,修改后请去除本注释
		return sessionFatory.getCurrentSession();
	}

	@Override
	public DataResult runSqlQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			return query.search(sessionFatory.getCurrentSession());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void deleteEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		deleteSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public List<ViewTask> selectEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		return selectSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		updataSqlFromFunction(sessionFatory.getCurrentSession(), values, rules);
	}

	@Override
	public int countEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		return countSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	public SessionFactory getSessionFatory() {
		return sessionFatory;
	}

	public void setSessionFatory(SessionFactory sessionFatory) {
		this.sessionFatory = sessionFatory;
	}

	@Override
	protected Class<?> getTypeClass() {
		return ViewTask.class;
	}

	@Override
	protected SessionFactory getSessionFactory() {
		return sessionFatory;
	}

	@Override
	public List<Map<String, Object>> getNewRunableTask() throws SQLException {
		DataQuery query = DataQuery.getInstance(1,
				"A.ID AS TASKID,F.TITLE as FILETITLE,A.FILEID as FILEID,A.BEFORETASK as BEFORETASK,"
						+ "A.OUTTIMENUM AS OUTTIMENUM,B.CLASSKEY AS CLASSKEY,CO.TITLE AS OTITLE,"
						+ "CO.MODELKEY AS OKEY,CT.TITLE AS TTITLE,CT.MODELKEY AS TKEY ",
				"WDAP_VIEW_TASK A LEFT JOIN WDAP_VIEW_CONVERTOR B ON A.CONVERTORID = B.ID "
						+ "LEFT JOIN WDAP_VIEW_FILEMODEL CO ON B.OMODELID = CO.ID "
						+ "LEFT JOIN WDAP_VIEW_FILEMODEL CT ON B.TMODELID = CT.ID "
						+ "LEFT JOIN WDAP_FILE F on a.FILEID=F.ID");
		query.setPagesize(100);
		query.setNoCount();
		query.addSort(new DBSort("a.ctime", "asc"));
		query.addSqlRule(" and a.PSTATE='0'");
		DataResult result = query.search();
		return result.getResultList();
	}
}
