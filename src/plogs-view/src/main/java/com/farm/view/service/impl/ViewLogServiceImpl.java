package com.farm.view.service.impl;

import com.farm.view.domain.ViewLog;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.view.dao.ViewLogDaoInter;
import com.farm.view.service.ViewLogServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：预览日志服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class ViewLogServiceImpl implements ViewLogServiceInter {
	@Resource
	private ViewLogDaoInter viewlogDaoImpl;

	private static final Logger log = Logger.getLogger(ViewLogServiceImpl.class);

	@Override
	@Transactional
	public ViewLog insertViewlogEntity(ViewLog entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return viewlogDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public ViewLog editViewlogEntity(ViewLog entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		ViewLog entity2 = viewlogDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setPcontent(entity.getPcontent());
		entity2.setCtime(entity.getCtime());
		entity2.setTaskid(entity.getTaskid());
		entity2.setId(entity.getId());
		viewlogDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteViewlogEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		viewlogDaoImpl.deleteEntity(viewlogDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public ViewLog getViewlogEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return viewlogDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createViewlogSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WDAP_VIEW_LOG", "ID,PCONTENT,CTIME,TASKID");
		return dbQuery;
	}

	@Override
	public ViewLog insertViewlog(String taskid, String note) {
		ViewLog log = new ViewLog();
		log.setTaskid(taskid);
		log.setPcontent(note);
		log.setCtime(TimeTool.getTimeDate14());
		return viewlogDaoImpl.insertEntity(log);
	}

	@Override
	@Transactional
	public void deleteLogsByTaskId(String taskid) {
		viewlogDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("TASKID", taskid, "=")).toList());
	}

	@Override
	@Transactional
	public List<ViewLog> getlogsByTaskid(String taskid) {
		List<ViewLog> logs = viewlogDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("TASKID", taskid, "=")).toList());
		Collections.sort(logs, new Comparator<ViewLog>() {
			@Override
			public int compare(ViewLog o1, ViewLog o2) {
				return o1.getCtime().compareTo(o2.getCtime());
			}
		});
		return logs;
	}

}
