package com.farm.view.model.impl;

import java.io.File;

import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.sun.star.uno.RuntimeException;

/**
 * 单个文件模型：文件系统中的单个文件
 * 
 * @author macpl
 *
 */
public class FileMp4Model implements ViewFileModelInter {

	@Override
	public String getTitle() {
		return "原始MP4文件";
	}

	@Override
	public String getKey() {
		return "MP4FILE";
	}

	@Override
	public ModelParas getParamegers() {
		return new ModelParas();
	}

	@Override
	public File getModelFile(File file, String exname) {
		return file;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return FarmDocFiles.getSysIcon(exname);
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "show/PubVideos.do?viewdocId=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		if (!exname.toUpperCase().equals("MP4")) {
			throw new RuntimeException("the file is not mp4!");
		}
		return "mp4";
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return false;
	}

	@Override
	public String getText(File modelFile) {
		return null;
	}
}
