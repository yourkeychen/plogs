package com.farm.view.model.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 改写html资源工具类
 * 
 * @author macpl
 *
 */
public class HtmlFileHandles {
	/**
	 * 给html文件绑定一个wdap的标准样式
	 * 
	 * @param file
	 */
	public static void bindHtmlStyle(File file, String title) {
		// 修改htm样式加入
		try {
			String charset = getCharSet(readTxtFile(file));
			Document htmldoc = Jsoup.parse(file, charset);
			Element headTag = htmldoc.getElementsByTag("HEAD").get(0);
			headTag.append(
					"<link rel=\"stylesheet\" href=\"" + HtmlStaticResouceNames.OpenOfficeHtmlStyle.getUrl() + "\">");
			// -------------------------
			Elements titleTag = headTag.getElementsByTag("title");
			if (titleTag.size() > 0) {
				titleTag.get(0).text(title);
			} else {
				headTag.append("<title>" + title + "</title>");
			}
			// ------
			Elements body = htmldoc.getElementsByTag("BODY");
			String obodyHtml = body.html();
			body.html("");
			body.append("<div class='htmlPageDefaultBand'> <img class='htmlPageDefaultLogo' src='"
					+ HtmlStaticResouceNames.wdapLogo.getUrl() + "'/> </div>");
			String contentClass = "wdapContent";
			if (title.toUpperCase().endsWith("XLS") || title.toUpperCase().endsWith("XLSX")) {
				contentClass = "wdapExcelContent";
			}
			body.append("<div class='" + contentClass + "'>" + obodyHtml + "</div>");
			Elements tdTags = htmldoc.getElementsByTag("td");
			for (Element tdTag : tdTags) {
				String tdHtml = tdTag.html();
				tdTag.html("<div>" + tdHtml + "</div>");
			}
			FileUtils.writeStringToFile(file, htmldoc.toString(), charset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获得html中的文本
	 * 
	 * @param file
	 * @return
	 */
	public static String getHtmlFileText(File file) {
		String text = null;
		try {
			String charset = getCharSet(readTxtFile(file));
			Document htmldoc = Jsoup.parse(file, charset);
			Elements elements = htmldoc.getElementsByTag("HTML");
			if (elements.size() > 0) {
				text = elements.get(0).text();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}

	/**
	 * 改写html中的图片地址
	 * 
	 * @param modelFile
	 */
	public static void reBindImgs(File file, String viewDocId) {
		try {
			String charset = getCharSet(readTxtFile(file));
			Document htmldoc = Jsoup.parse(file, charset);
			Elements elements = htmldoc.getElementsByTag("img");
			for (Element img : elements) {
				String src = img.attr("src");
				String url = HtmlStaticResouceNames.getModelFileUrl(src, viewDocId);
				img.attr("src", url);
			}
			FileUtils.writeStringToFile(file, htmldoc.toString(), charset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获得html字符集
	 * 
	 * @param content
	 * @return
	 */
	private static String getCharSet(String html) {
		String charset = matchCharset(html).replaceAll("'", "\"");
		charset = charset.substring(0, charset.indexOf("\"") > 0 ? charset.indexOf("\"") : charset.length());
		return charset.trim();
	}

	/**
	 * 获得页面字符匹配
	 */
	private static String matchCharset(String content) {
		String chs = "gb2312";
		Pattern p = Pattern.compile("(?<=charset=)(.+)(?=\")");
		Matcher m = p.matcher(content);
		if (m.find())
			return m.group();
		return chs;
	}

	/**
	 * 获得文件编码
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static String getFileEncode(File file) throws FileNotFoundException {
		InputStream in = new java.io.FileInputStream(file);
		try {
			byte[] b = new byte[3];
			in.read(b);
			if (b[0] == -17 && b[1] == -69 && b[2] == -65)
				return "UTF-8";
			else
				return "gbk";
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "UTF-8";
	}

	/**
	 * 读取txt文件内容
	 * 
	 * @param file
	 * @return
	 */
	private static String readTxtFile(File file) {
		StringBuffer buffer = new StringBuffer();
		InputStreamReader read = null;
		InputStream inputStream = null;
		BufferedReader bufferedReader = null;
		try {
			String encoding = getFileEncode(file);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				inputStream = new FileInputStream(file);
				read = new InputStreamReader(inputStream, encoding);// 考虑到编码格式
				bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					buffer.append(lineTxt);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bufferedReader.close();
				read.close();
				inputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return buffer.toString();
	}
}
