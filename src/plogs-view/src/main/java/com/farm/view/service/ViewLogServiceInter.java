package com.farm.view.service;

import com.farm.view.domain.ViewLog;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：预览日志服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ViewLogServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ViewLog insertViewlogEntity(ViewLog entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ViewLog editViewlogEntity(ViewLog entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteViewlogEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ViewLog getViewlogEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createViewlogSimpleQuery(DataQuery query);

	/**
	 * 写入转换日志
	 * 
	 * @param taskid
	 * @param note
	 */
	public ViewLog insertViewlog(String id, String string);

	/**
	 * 删除某个转换任务的日志
	 * 
	 * @param taskid
	 */
	public void deleteLogsByTaskId(String taskid);

	/**
	 * 获得转换任务的日志
	 * 
	 * @param taskid
	 * @return
	 */
	public List<ViewLog> getlogsByTaskid(String taskid);
}