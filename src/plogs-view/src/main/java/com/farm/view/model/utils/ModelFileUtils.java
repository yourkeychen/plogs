package com.farm.view.model.utils;

import java.io.File;

import com.farm.file.util.ViewDirUtils;

public class ModelFileUtils {

	/**
	 * 获得模型文件
	 * 
	 * @param file
	 *            原文件
	 * @param modelDir
	 *            模型目录
	 * @param newFileName
	 *            模型文件名称
	 * @return
	 */
	public static File getModelPath(File file, String modelDir, String newFileName) {
		String viewdir = ViewDirUtils.getPath(file);
		String path = viewdir + File.separator + modelDir + File.separator + newFileName;
		return new File(path);
	}

	/**
	 * 获得目录文件
	 * 
	 * @param file
	 * @param modelDir
	 * @return
	 */
	public static File getModelPath(File file, String modelDir) {
		String viewdir = ViewDirUtils.getPath(file);
		String path = viewdir + File.separator + modelDir;
		return new File(path);
	}
}
