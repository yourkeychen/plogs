package com.farm.view.model.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * html静态资源辅助类
 * 
 * @author macpl
 *
 */
public enum HtmlStaticResouceNames {
	/**
	 * openoffice生成html文件的css样式文件
	 */
	OpenOfficeHtmlStyle("text/style/openOfficeHtmlStyle.css"), wdapLogo("view/web-simple/atext/png/icon");
	private String webPath;

	public String getWebPath() {
		return webPath;
	}

	private HtmlStaticResouceNames(String path) {
		this.webPath = path;
	}

	/**
	 * 获得一个静态资源的url编码地址
	 * 
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String getUrl() throws UnsupportedEncodingException {
		return "PubWebSource.do?filepath=" + encodeUrl(this.name()) + "&viewDocId=";
	}

	/**
	 * 两次url编码
	 * 
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private static String encodeUrl(String str) throws UnsupportedEncodingException {
		String urlFileName = URLEncoder.encode(str, "utf-8");
		urlFileName = URLEncoder.encode(urlFileName, "utf-8");
		return urlFileName;
	}

	/**
	 * 编码一个web资源文件的url地址
	 * 
	 * @param path
	 * @param wiewdocId
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String getModelFileUrl(String path, String wiewdocId) throws UnsupportedEncodingException {
		return "PubWebSource.do?filepath=" + encodeUrl(path) + "&viewDocId=" + wiewdocId;
	}
}
