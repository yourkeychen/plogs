package com.farm.view.model.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.farm.file.domain.FileBase;
import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;

/**
 * 单个文件模型：文件系统中的单个文件
 * 
 * @author macpl
 *
 */
public class FileModel implements ViewFileModelInter {

	@Override
	public String getTitle() {
		return "原始文件";
	}

	@Override
	public String getKey() {
		return "WDAPFILE";
	}

	@Override
	public ModelParas getParamegers() {
		return new ModelParas();
	}

	@Override
	public File getModelFile(File file, String exname) {
		return file;
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "downview/Pubload.do?id=" + viewDocId;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return FarmDocFiles.getSysIcon(exname);
	}

	@Override
	public String getExname(String exname) {
		return exname;
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return false;
	}

	@Override
	public String getText(File modelFile) { 
		return null;
	}
}
