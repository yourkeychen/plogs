package com.farm.view.task.domain;

import java.io.File;

/**
 * 任务文件信息
 * 
 * @author macpl
 *
 */
public class ConvertTaskFile {

	private String fileRealPath;
	private String fileTitle;
	private String fileExname;

	public ConvertTaskFile(String fileRealPath, String fileTitle, String fileExname) {
		this.fileRealPath = fileRealPath;
		this.fileTitle = fileTitle;
		this.fileExname = fileExname;
	}

	public String getFileRealPath() {
		return fileRealPath;
	}

	public String getFileTitle() {
		return fileTitle;
	}

	public String getExname() {
		return fileExname;
	}

	public File getFile() {
		return new File(getFileRealPath());
	}
}
