package com.farm.view.task;

import org.apache.log4j.Logger;

import com.farm.util.spring.BeanFactory;
import com.farm.view.service.ViewTaskServiceInter;

/**
 * 任務調度器，負責加載任務
 * 
 * @author macpl
 *
 */
public class WdapTaskDispatcherService {
	private WdapTaskDispatcherService() {
	}

	private static final Logger log = Logger.getLogger(WdapTaskDispatcherService.class);
	private ViewTaskServiceInter viewTaskServiceImpl;
	private Thread dispatcherThread = null;

	private static WdapTaskDispatcherService obj = new WdapTaskDispatcherService();

	/**
	 * 加载调度器的单例对象
	 * 
	 * @return
	 */
	public static WdapTaskDispatcherService getInstance() {
		obj.viewTaskServiceImpl = (ViewTaskServiceInter) BeanFactory.getBean("viewTaskServiceImpl");
		return obj;
	}

	/**
	 * 启动调度任务
	 */
	public void start() {
		if (dispatcherThread != null && dispatcherThread.isAlive()) {
			log.warn("启动失败：文件转换任务调度器已经启动!");
			return;
		} else {
			log.info("启动文件转换任务调度器...");
		}
		dispatcherThread = new Thread(new TaskDispatcher(viewTaskServiceImpl));
		dispatcherThread.start();
	}

	/**
	 * 判断调度器是否启动
	 * 
	 * @return
	 */
	public boolean isAlive() {
		if (dispatcherThread != null && dispatcherThread.isAlive()) {
			return true;
		} else {
			return false;
		}
	}
}
