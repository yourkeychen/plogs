package com.farm.view.model.domain;

import java.util.HashMap;
import java.util.Map;

public class ModelParas {
	private Map<String, Object> paras = new HashMap<>();

	public ModelParas() {
	}

	public void put(String key, Object val) {
		paras.put(key, val);
	}

	public int getInt(String key) {
		return (int) paras.get(key);
	}
}
