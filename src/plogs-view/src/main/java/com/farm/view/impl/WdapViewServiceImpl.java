package com.farm.view.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.farm.core.time.TimeTool;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.FileBase;
import com.farm.file.domain.ex.PersistFile;
import com.farm.file.enums.FileModel;
import com.farm.file.exception.FileExNameException;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.view.WdapViewServiceInter;
import com.farm.view.domain.ViewDoc;
import com.farm.view.domain.ViewFileModel;
import com.farm.view.domain.ViewLog;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.ViewFileModels;
import com.farm.view.model.impl.ThumbnailMaxModel;
import com.farm.view.model.impl.ThumbnailMedModel;
import com.farm.view.model.impl.ThumbnailMinModel;
import com.farm.view.model.utils.FileNameSortUtil;
import com.farm.view.service.ViewConvertorServiceInter;
import com.farm.view.service.ViewDocServiceInter;
import com.farm.view.service.ViewFileModelServiceInter;
import com.farm.view.service.ViewLogServiceInter;
import com.farm.view.service.ViewRegularServiceInter;
import com.farm.view.service.ViewTaskServiceInter;

@Service
public class WdapViewServiceImpl implements WdapViewServiceInter {
	@Resource
	private ViewConvertorServiceInter viewConvertorServiceImpl;
	@Resource
	private ViewFileModelServiceInter viewModelServiceImpl;
	@Resource
	private ViewDocServiceInter viewDocServiceImpl;
	@Resource
	private ViewRegularServiceInter viewRegularServiceImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private ViewTaskServiceInter viewTaskServiceImpl;
	@Resource
	private ViewLogServiceInter viewLogServiceImpl;

	@Override
	public PersistFile getViewDocIcon(String viewDocId) {
		PersistFile pfile = new PersistFile();
		ViewDoc doc = viewDocServiceImpl.getViewdocEntity(viewDocId);
		ViewFileModel model = viewModelServiceImpl.getViewmodelEntity(doc.getModelid());
		ViewFileModelInter modelInter = ViewFileModels.getModel(model.getModelkey());
		FileBase filebase = fileBaseServiceImpl.getFilebaseEntity(doc.getFileid());
		File iconFile = modelInter.getIconFile(new File(fileBaseServiceImpl.getFileRealPath(doc.getFileid())),
				filebase.getExname());
		pfile.setFile(iconFile);
		pfile.setName(iconFile.getName());
		pfile.setSecret(filebase.getSecret());
		return pfile;
	}

	@Override
	public PersistFile getFileIcon(String fileid) {
		String exname = fileBaseServiceImpl.getFilebaseEntity(fileid).getExname();
		PersistFile file = wdapFileServiceImpl.getPersistFile(fileid);
		try {
			if (FileModel.getModelByFileExName(exname).equals(FileModel.IMG)) {
				// 找图片icon
				{
					File minIconfile = new ThumbnailMinModel().getIconFile(file.getFile(), exname);
					if (minIconfile != null && minIconfile.exists()) {
						return new PersistFile(minIconfile, minIconfile.getName(), null);
					}
				}
				{
					File medIconfile = new ThumbnailMedModel().getIconFile(file.getFile(), exname);
					if (medIconfile != null && medIconfile.exists()) {
						return new PersistFile(medIconfile, medIconfile.getName(), null);
					}
				}
				{
					File maxIconfile = new ThumbnailMaxModel().getIconFile(file.getFile(), exname);
					if (maxIconfile != null && maxIconfile.exists()) {
						return new PersistFile(maxIconfile, maxIconfile.getName(), null);
					}
				}
			}
		} catch (FileExNameException e) {
			e.printStackTrace();
		}
		return wdapFileServiceImpl.getFileIconFile(fileid);
	}

	@Override
	public PersistFile getViewFile(String viewDocId) {
		PersistFile pfile = new PersistFile();
		ViewDoc doc = viewDocServiceImpl.getViewdocEntity(viewDocId);
		ViewFileModel model = viewModelServiceImpl.getViewmodelEntity(doc.getModelid());
		ViewFileModelInter modelInter = ViewFileModels.getModel(model.getModelkey());
		FileBase filebase = fileBaseServiceImpl.getFilebaseEntity(doc.getFileid());
		File modelFile = modelInter.getModelFile(new File(fileBaseServiceImpl.getFileRealPath(doc.getFileid())),
				filebase.getExname());
		pfile.setFile(modelFile);
		pfile.setName("file." + modelInter.getExname(filebase.getExname()));
		pfile.setSecret(filebase.getSecret());
		return pfile;
	}

	@Override
	public PersistFile getViewFile(String viewdocid, int num) {
		PersistFile pfile = new PersistFile();
		ViewDoc doc = viewDocServiceImpl.getViewdocEntity(viewdocid);
		ViewFileModel model = viewModelServiceImpl.getViewmodelEntity(doc.getModelid());
		ViewFileModelInter modelInter = ViewFileModels.getModel(model.getModelkey());
		FileBase filebase = fileBaseServiceImpl.getFilebaseEntity(doc.getFileid());
		File modelFile = modelInter.getModelFile(new File(fileBaseServiceImpl.getFileRealPath(doc.getFileid())),
				filebase.getExname());
		List<File> files = Arrays.asList(modelFile.listFiles());
		Collections.sort(files, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				return FileNameSortUtil.compareTo(o1.getName(), o2.getName());
			}
		});
		pfile.setFile(files.get(num));
		pfile.setName(files.get(num).getName());
		pfile.setSecret(filebase.getSecret());
		return pfile;
	}

	@Override
	public void viewDocHandle(String viewDocId) {
		ViewDoc viewDoc = viewDocServiceImpl.getViewdocEntity(viewDocId);
		wdapFileServiceImpl.viewFileHandle(viewDoc.getFileid());
	}

	@Override
	public PersistFile getViewDocHtmlResource(String filepath, String viewDocId) {
		PersistFile pfile = new PersistFile();
		ViewDoc viewDoc = viewDocServiceImpl.getViewdocEntity(viewDocId);
		ViewFileModel model = viewModelServiceImpl.getViewmodelEntity(viewDoc.getModelid());
		ViewFileModelInter modelInter = ViewFileModels.getModel(model.getModelkey());
		FileBase filebase = fileBaseServiceImpl.getFilebaseEntity(viewDoc.getFileid());
		File indexFile = modelInter.getModelFile(fileBaseServiceImpl.getFile(filebase), filebase.getExname());
		pfile.setFile(new File(indexFile.getParent() + File.separator + filepath));
		pfile.setName(filepath);
		pfile.setSecret(filebase.getSecret());
		return pfile;
	}

	@Override
	public void reLoadFileViewTask(String fileid) {
		viewTaskServiceImpl.reLoadFileViewTask(fileid, "NONE", "NONE");
	}

	@Override
	public List<Map<String, Object>> getFileViewdocs(String fileid) {
		return viewDocServiceImpl.getFileViewdocs(fileid);
	}

	@Override
	public List<Map<String, Object>> getTasksByFileid(String fileid) {
		return viewTaskServiceImpl.getTasksByFileid(fileid);
	}

	@Override
	public boolean isView(String fileid) {
		List<Map<String, Object>> docs = viewDocServiceImpl.getFileViewdocs(fileid);
		return docs.size() > 0;
	}

	@Override
	public List<String> getTaskLogs(String taskid) {
		List<ViewLog> logs = viewLogServiceImpl.getlogsByTaskid(taskid);
		List<String> logList = new ArrayList<>();
		for (ViewLog log : logs) {
			logList.add(TimeTool.getFormatTimeDate12(log.getCtime(), "yyyy-MM-dd HH:mm:ss") + "  " + log.getPcontent());
		}
		return logList;
	}

	@Override
	public boolean fileConvertAble(String exname, long lenght) {
		return viewRegularServiceImpl.fileConvertAble(exname, lenght);
	}

	@Override
	public String getViewUrl(String fileid) {
		return "include/Pubview.do?fileid=" + fileid;
	}

}
