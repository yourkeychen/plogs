package com.farm.view.convertor.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.farm.core.config.AppConfig;
import com.farm.view.convertor.FileConvertorInter;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.task.domain.ConvertTaskFile;

/**
 * pdf转图片
 * 
 * @author macpl
 *
 */
public class PdfToImgConvertor implements FileConvertorInter {
	@Override
	public String getKey() {
		return "PDF_TO_IMG";
	}

	@Override
	public String getTitle() {
		return "PDF转图片";
	}

	@Override
	public String getNote() {
		return "pdf转成图片册";
	}

	@Override
	public void doConvert(ConvertTaskFile fileinfo, ViewFileModelInter omodel, ViewFileModelInter tmodel) {
		try {
			File ofile = omodel.getModelFile(fileinfo.getFile(), fileinfo.getExname());
			File tfile = tmodel.getModelFile(fileinfo.getFile(), fileinfo.getExname());
			tfile.mkdirs();
			PDDocument doc = PDDocument.load(ofile);
			Date startDate = new Date();
			try {
				PDFRenderer renderer = new PDFRenderer(doc);
				int pageCount = doc.getNumberOfPages();
				List<String> imgpaths = new ArrayList<>();
				// 最大DPI
				int DPI = Integer.valueOf(AppConfig.getString("config.pdf.to.png.maxdpi"));
				// 最小DPI
				int minDpi = Integer.valueOf(AppConfig.getString("config.pdf.to.png.mindpi"));
				// 转换超时时间，超时后立即返回
				int outTimesecond = Integer.valueOf(AppConfig.getString("config.pdf.to.png.timeout.second"));
				System.out.println("pdf to png conf：max-dpi-" + DPI + "  min-dpi-" + minDpi + "  timeout-"
						+ outTimesecond + " second");
				int dpiStep = (DPI - minDpi) / 8;
				if (pageCount > 10) {
					DPI = DPI - dpiStep;
				}
				if (pageCount > 20) {
					DPI = DPI - dpiStep;
				}
				if (pageCount > 30) {
					DPI = DPI - dpiStep;
				}
				if (pageCount > 40) {
					DPI = DPI - dpiStep;
				}
				// 写图片到文件夹中(图片必须未img0.png命名，必须以0为开始，因为html页面是从0开始得)
				int num = 0;
				for (int i = 0; i < pageCount; i++) {
					int innerDpi = DPI;
					if (i > 20) {
						innerDpi = DPI - 1 * dpiStep;
					}
					if (i > 45) {
						innerDpi = DPI - 2 * dpiStep;
					}
					if (i > 70) {
						innerDpi = DPI - 3 * dpiStep;
					}
					if (i > 90) {
						innerDpi = DPI - 4 * dpiStep;
					}
					BufferedImage image = renderer.renderImageWithDPI(i, innerDpi);
					ImageIO.write(image, "PNG", new File(tfile.getPath() + "\\img" + i + ".png"));
					System.out.println("DPI=" + innerDpi + " by" + num + "/" + pageCount + " file:\\img" + i + ".png");
					imgpaths.add("img" + i + ".png");
					num++;
					if (((new Date().getTime()) - startDate.getTime()) / 1000 > outTimesecond) {
						System.out.println("转换时间大于170秒，只转换部分页面!");
						break;
					}
				}
				System.out.println("creat imgs num is:" + num + "/" + pageCount + " by time "
						+ ((new Date().getTime()) - startDate.getTime()) / 1000 + " s");
			} finally {
				doc.close();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
