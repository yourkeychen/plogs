package com.farm.view.service;

import com.farm.view.domain.ViewDoc;
import com.farm.view.model.ViewFileModelInter;
import com.farm.core.sql.query.DataQuery;

import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：预览文件服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ViewDocServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ViewDoc insertViewdocEntity(ViewDoc entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ViewDoc editViewdocEntity(ViewDoc entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteViewdocEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ViewDoc getViewdocEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createViewdocSimpleQuery(DataQuery query);

	/**
	 * 插入一个预览文件
	 * 
	 * @param viewDoc
	 * @return
	 */
	public ViewDoc insertViewdoc(ViewDoc viewDoc);

	/**
	 * 删除预览文件通过文件id
	 * 
	 * @param fileid
	 * @param userId
	 * @param userName
	 */
	public void deleteViewdocsByFileId(String fileid, String userId, String userName);

	/**
	 * 通過文件id獲得預覽文件
	 * 
	 * @param fileid
	 * @return
	 */
	public List<ViewDoc> getViewdocsByFileId(String fileid);

	/**
	 * 刪除預覽文件
	 * 
	 * @param viewdoc
	 */
	public void deleteViewdocEntity(ViewDoc viewdoc);

	/**
	 * 获得文件的预览文件
	 * 
	 * @param setPagesize
	 * @return CTIME,ID,FILETITLE,FILEID,MODELKEY,MODELTITLE,VIEWIS,PSTATE
	 */
	public List<Map<String, Object>> getFileViewdocs(String fileid);

	/**
	 * 獲得文件模型，由预览文件id
	 * 
	 * @param viewdocId
	 * @return
	 */
	public ViewFileModelInter getViewFileModelByViewDocId(String viewdocId);

	/**
	 * 获得预览文件模型对应的文件数量（如图片册模型中的图片数量）
	 * 
	 * @param viewdocId
	 * @return
	 */
	public int getModelFileSize(String viewdocId);
}