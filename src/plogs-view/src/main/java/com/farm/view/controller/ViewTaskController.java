package com.farm.view.controller;

import com.farm.view.domain.ViewLog;
import com.farm.view.domain.ViewTask;
import com.farm.view.service.ViewLogServiceInter;
import com.farm.view.service.ViewTaskServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：转换任务控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/viewtask")
@Controller
public class ViewTaskController extends WebUtils {
	private final static Logger log = Logger.getLogger(ViewTaskController.class);
	@Resource
	private ViewTaskServiceInter viewTaskServiceImpl;
	@Resource
	private ViewLogServiceInter viewLogServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DataResult result = viewTaskServiceImpl.createViewtaskSimpleQuery(query).search();
			result.runDictionary("0:<span class='lableDefault'>等待</span>,1:<span class='labelWarn'>执行</span>,2:<span class='labelSuccess'>完成</span>,3:<span class='labelError'>错误</span>,4:<span class='labelError'>禁用</span>", "PSTATE");
			result.runformatTime("CTIME", "yyyy-MM-dd HH:mm:ss");
			result.runformatTime("STIME", "yyyy-MM-dd HH:mm:ss");
			result.runformatTime("ETIME", "yyyy-MM-dd HH:mm:ss");
			result.runHandle(new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					row.put("SHORTID", row.get("ID").toString().substring(22));
					if (row.get("BEFORETASK") != null) {
						row.put("BEFORETASK", row.get("BEFORETASK").toString().substring(22));
					}
				}
			});
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(ViewTask entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = viewTaskServiceImpl.editViewtaskEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(ViewTask entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = viewTaskServiceImpl.insertViewtaskEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				viewTaskServiceImpl.deleteViewtaskEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("viewer/ViewTaskResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				List<ViewLog> logs = viewLogServiceImpl.getlogsByTaskid(ids);
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("logs", logs)
						.putAttr("entity", viewTaskServiceImpl.getViewtaskEntity(ids))
						.returnModelAndView("viewer/ViewTaskForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset).returnModelAndView("viewer/ViewTaskForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", viewTaskServiceImpl.getViewtaskEntity(ids))
						.returnModelAndView("viewer/ViewTaskForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("viewer/ViewTaskForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("viewer/ViewTaskForm");
		}
	}
}
