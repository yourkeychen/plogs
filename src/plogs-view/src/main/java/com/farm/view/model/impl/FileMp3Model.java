package com.farm.view.model.impl;

import java.io.File;

import com.farm.file.domain.FileBase;
import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.sun.star.uno.RuntimeException;

/**
 * 单个文件模型：文件系统中的单个文件
 * 
 * @author macpl
 *
 */
public class FileMp3Model implements ViewFileModelInter {

	@Override
	public String getTitle() {
		return "原始MP3文件";
	}

	@Override
	public String getKey() {
		return "MP3FILE";
	}

	@Override
	public ModelParas getParamegers() {
		return new ModelParas();
	}

	@Override
	public File getModelFile(File file, String exname) {
		return file;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return FarmDocFiles.getSysIcon(exname);
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "show/PubAudios.do?viewdocId=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		if (!exname.toUpperCase().equals("MP3")) {
			throw new RuntimeException("the file is not mp3!");
		}
		return "mp3";
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return false;
	}

	@Override
	public String getText(File modelFile) {
		return null;
	}
}
