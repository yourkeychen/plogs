package com.farm.view.service;

import com.farm.view.domain.ViewTask;
import com.farm.view.task.domain.ConvertAbleTask;
import com.farm.core.sql.query.DataQuery;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：转换任务服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ViewTaskServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ViewTask insertViewtaskEntity(ViewTask entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ViewTask editViewtaskEntity(ViewTask entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteViewtaskEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ViewTask getViewtaskEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createViewtaskSimpleQuery(DataQuery query);

	/**
	 * 重新加载预览任务（作废队列中的任务，再重新创建新任务）
	 * 
	 * @param fileid
	 * @return
	 */
	public void reLoadFileViewTask(String fileid, String userId, String userName);

	/**
	 * 获得一个可执行任务
	 * 
	 * @return
	 * @throws SQLException
	 */
	public ConvertAbleTask getNewRunableTask() throws SQLException;

	/**
	 * 一个任务成功执行完毕
	 * 
	 * @param taskId
	 */
	public void submitTaskSuccess(String taskId);

	/**
	 * 一个任务执行失败
	 * 
	 * @param taskId
	 * @param message
	 */
	public void submitTaskFail(String taskId, String message);

	/**
	 * 开始执行任务的回调
	 * 
	 * @param taskId
	 */
	public void doTaskHandle(String taskId);

	/**
	 * 获得一个文件相关的预览任务
	 * 
	 * @param fileid
	 * @return ID,BEFORETASK,BTITLE,CTITLE,DTITLE,FILEID,STIME,ETIME,CONVERTORID,VIEWDOCID,PSTATE,CTIME
	 */
	public List<Map<String, Object>> getTasksByFileid(String fileid);
}