package com.farm.view.service;

import com.farm.view.domain.ViewFileModel;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;
import com.farm.core.page.ViewMode;

/* *
 *功能：预览文件模型服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ViewFileModelServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ViewFileModel insertViewmodelEntity(ViewFileModel entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ViewFileModel editViewmodelEntity(ViewFileModel entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteViewmodelEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ViewFileModel getViewmodelEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createViewmodelSimpleQuery(DataQuery query);

	/**
	 * 刷新模型（从代码中加载到数据库中）
	 * 
	 * @param currentUser
	 */
	public void reFreshModels(LoginUser currentUser);

	/**
	 * 通過key查找模型
	 * 
	 * @param key
	 * @return
	 */
	public ViewFileModel getModelByKey(String key);

	/**
	 * 获得所有的可用模型
	 * 
	 * @return
	 */
	public List<ViewFileModel> getAllModels();
}