package com.farm.view.convertor;

import java.util.ArrayList;
import java.util.List;

import com.farm.view.convertor.impl.NoneConvertor;
import com.farm.view.convertor.impl.OpenOfficeConvertor;
import com.farm.view.convertor.impl.PdfToImgConvertor;
import com.farm.view.convertor.impl.ThumbnailConvertor;
import com.farm.view.convertor.impl.TxtToHtmlConvertor;
import com.farm.view.convertor.impl.ZipRarToTxtConvertor;

public class FileConvertors {
	/**
	 * 获得所有模型
	 * 
	 * @return
	 */
	public static List<FileConvertorInter> getAllConvertors() {
		List<FileConvertorInter> list = new ArrayList<>();
		list.add(new NoneConvertor());
		list.add(new ThumbnailConvertor());
		list.add(new PdfToImgConvertor());
		list.add(new OpenOfficeConvertor());
		list.add(new ZipRarToTxtConvertor());
		list.add(new TxtToHtmlConvertor());
		return list;
	}

	/**
	 * 通过key获得模型
	 * 
	 * @param modelkey
	 * @return
	 */
	public static FileConvertorInter getConvertor(String modelkey) {
		for (FileConvertorInter modelinter : getAllConvertors()) {
			if (modelinter.getKey().equals(modelkey)) {
				return modelinter;
			}
		}
		return null;
	}

	/**
	 * 獲得所有轉換器的key描述
	 * 
	 * @return
	 */
	public static String getAllConvertorKeys() {
		String kes = null;
		for (FileConvertorInter modelinter : getAllConvertors()) {
			if (kes == null) {
				kes=modelinter.getKey();
			} else {
				kes=kes+","+modelinter.getKey();
			}
		}
		return kes;
	}
}
