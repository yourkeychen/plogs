package com.farm.view.model.impl;

import java.io.File;

import com.farm.file.domain.FileBase;
import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.farm.view.model.utils.ModelFileUtils;

/**
 * 縮略圖模型
 * 
 * @author macpl
 *
 */
public class ThumbnailMaxModel implements ViewFileModelInter {
	/** 請在ViewFileModels中注冊 **/
	@Override
	public String getTitle() {
		return "大缩略图";
	}

	@Override
	public String getKey() {
		return "MAX_THUMBNAIL";
	}

	@Override
	public ModelParas getParamegers() {
		ModelParas paras = new ModelParas();
		paras.put("WIDTH", 1200);
		return paras;
	}

	@Override
	public File getModelFile(File file, String exname) {
		File modelFile = ModelFileUtils.getModelPath(file, "thumbnail", "max." + exname);
		return modelFile;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return getModelFile(file, exname);
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "downview/Pubload.do?id=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		return exname;
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return false;
	}

	@Override
	public String getText(File file) {
		return null;
	}
}
