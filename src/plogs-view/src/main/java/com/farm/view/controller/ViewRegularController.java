package com.farm.view.controller;

import com.farm.view.domain.ViewConvertor;
import com.farm.view.domain.ViewRegular;
import com.farm.view.service.ViewConvertorServiceInter;
import com.farm.view.service.ViewRegularServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;
import com.farm.parameter.FarmParameterService;
import com.farm.util.web.FarmFormatUnits;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：转换规则控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/viewregular")
@Controller
public class ViewRegularController extends WebUtils {
	private final static Logger log = Logger.getLogger(ViewRegularController.class);
	@Resource
	private ViewRegularServiceInter viewRegularServiceImpl;
	@Resource
	private ViewConvertorServiceInter viewConvertorServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addDefaultSort(new DBSort("FILEEXNAME", "asc"));
			DataResult result = viewRegularServiceImpl.createViewregularSimpleQuery(query).search();
			result.runDictionary("1:可用,0:禁用", "PSTATE");
			result.runDictionary("1:可以预览,0:禁止预览", "VIEWIS");
			result.runDictionary("0:无序", "SORT");
			result.runHandle(new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					row.put("SIZEMIN",
							FarmFormatUnits.getFileLengthAndUnit(Integer.valueOf(row.get("SIZEMIN").toString())));
					row.put("SIZEMAX",
							FarmFormatUnits.getFileLengthAndUnit(Integer.valueOf(row.get("SIZEMAX").toString())));
					int outtime = Integer.valueOf(row.get("OUTTIMENUM").toString());
					if (outtime > 1000) {
						row.put("OUTTIMENUM", outtime / 1000 + "秒");
					} else {
						row.put("OUTTIMENUM", outtime + "毫秒");
					}
				}
			});
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(ViewRegular entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = viewRegularServiceImpl.editViewregularEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(ViewRegular entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = viewRegularServiceImpl.insertViewregularEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				viewRegularServiceImpl.deleteViewregularEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
	/**
	 * 复制规则
	 * 
	 * @return
	 */
	@RequestMapping("/copy")
	@ResponseBody
	public Map<String, Object> copy(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				viewRegularServiceImpl.copyViewregular(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		List<String> exnames = FarmParameterService.getInstance().getParameterStringList("config.doc.upload.types");
		return ViewMode.getInstance().putAttr("exnames", exnames).returnModelAndView("viewer/ViewRegularResult");
	}

	@RequestMapping("/stateForm")
	public ModelAndView stateForm(String ids, HttpSession session) {
		return ViewMode.getInstance().putAttr("ids", ids).putAttr("size", parseIds(ids).size())
				.returnModelAndView("viewer/ViewRegularStateForm");
	}

	/**
	 * 提交修改状态
	 * 
	 * @return
	 */
	@RequestMapping("/editState")
	@ResponseBody
	public Map<String, Object> editState(String ids, String pstate, String viewis, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				viewRegularServiceImpl.editViewregularState(id, pstate, viewis, getCurrentUser(session));
			}
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			List<String> exnames = FarmParameterService.getInstance().getParameterStringList("config.doc.upload.types");
			List<ViewConvertor> convertors = viewConvertorServiceImpl.getAllConvertor();
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("exnames", exnames)
						.putAttr("convertors", convertors)
						.putAttr("entity", viewRegularServiceImpl.getViewregularEntity(ids))
						.returnModelAndView("viewer/ViewRegularForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("exnames", exnames)
						.putAttr("convertors", convertors).returnModelAndView("viewer/ViewRegularForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("exnames", exnames)
						.putAttr("convertors", convertors)
						.putAttr("entity", viewRegularServiceImpl.getViewregularEntity(ids))
						.returnModelAndView("viewer/ViewRegularForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("viewer/ViewRegularForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("viewer/ViewRegularForm");
		}
	}
}
