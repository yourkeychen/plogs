package com.farm.view.service.impl;

import com.farm.view.domain.ViewFileModel;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.ViewFileModels;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.view.dao.ViewFileModelDaoInter;
import com.farm.view.service.ViewFileModelServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.page.ViewMode;

/* *
 *功能：预览文件模型服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class ViewFileModelServiceImpl implements ViewFileModelServiceInter {
	@Resource
	private ViewFileModelDaoInter viewmodelDaoImpl;

	private static final Logger log = Logger.getLogger(ViewFileModelServiceImpl.class);

	@Override
	@Transactional
	public ViewFileModel insertViewmodelEntity(ViewFileModel entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(user.getName());
		entity.setSortnum(1);
		entity.setEuser(user.getId());
		entity.setEusername(user.getName());
		entity.setEtime(TimeTool.getTimeDate14());
		return viewmodelDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public ViewFileModel editViewmodelEntity(ViewFileModel entity, LoginUser user) {
		ViewFileModel entity2 = viewmodelDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		// entity2.setModelkey(entity.getModelkey());
		// entity2.setPstate(entity.getPstate());
		// entity2.setTitle(entity.getTitle());
		entity2.setPcontent(entity.getPcontent());
		entity2.setSortnum(entity.getSortnum());
		// entity2.setEuser(entity.getEuser());
		// entity2.setEusername(entity.getEusername());
		// entity2.setCuser(entity.getCuser());
		// entity2.setCusername(entity.getCusername());
		// entity2.setEtime(entity.getEtime());
		// entity2.setCtime(entity.getCtime());
		// entity2.setId(entity.getId());
		viewmodelDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteViewmodelEntity(String id, LoginUser user) {
		ViewFileModel model = viewmodelDaoImpl.getEntity(id);
		if (ViewFileModels.getModel(model.getModelkey()) == null) {
			viewmodelDaoImpl.deleteEntity(model);
		} else {
			throw new RuntimeException("该文件未失效!");
		}
	}

	@Override
	@Transactional
	public ViewFileModel getViewmodelEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return viewmodelDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createViewmodelSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WDAP_VIEW_FILEMODEL",
				"ID,MODELKEY,PSTATE,TITLE,PCONTENT,EUSER,EUSERNAME,CUSER,CUSERNAME,ETIME,CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public void reFreshModels(LoginUser currentUser) {
		for (ViewFileModelInter model : ViewFileModels.getAllModels()) {
			ViewFileModel viewmodel = getModelByKey(model.getKey());
			if (viewmodel == null) {
				viewmodel = new ViewFileModel();
				viewmodel.setPstate("1");
				viewmodel.setTitle(model.getTitle());
				viewmodel.setModelkey(model.getKey());
				insertViewmodelEntity(viewmodel, currentUser);
			} else {
				viewmodel.setPstate("1");
				viewmodel.setTitle(model.getTitle());
				viewmodel.setModelkey(model.getKey());
				editViewmodelEntity(viewmodel, currentUser);
			}
		}
	}

	@Override
	@Transactional
	public ViewFileModel getModelByKey(String key) {
		List<ViewFileModel> list = viewmodelDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("MODELKEY", key, "=")).toList());
		if (list.size() > 0) {
			return list.get(0);

		} else {
			return null;
		}
	}

	@Override
	@Transactional
	public List<ViewFileModel> getAllModels() {
		List<ViewFileModel> list = viewmodelDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("PSTATE", "1", "=")).toList());
		Collections.sort(list, new Comparator<ViewFileModel>() {
			@Override
			public int compare(ViewFileModel o1, ViewFileModel o2) {
				return o1.getSortnum() - o2.getSortnum();
			}
		});
		return list;
	}

}
