package com.farm.view.convertor.impl;

import com.farm.view.convertor.FileConvertorInter;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.task.domain.ConvertTaskFile;

/**
 * 什么也不做(比如用於pdf原樣預覽)
 * 
 * @author macpl
 *
 */
public class NoneConvertor implements FileConvertorInter {
	@Override
	public String getKey() {
		return "NONE";
	}

	@Override
	public String getTitle() {
		return "空转换器";
	}

	@Override
	public String getNote() {
		return "不执行任何操作，适用于pdf等直接可以预览的文件";
	}

	@Override
	public void doConvert(ConvertTaskFile fileinfo, ViewFileModelInter omodel, ViewFileModelInter tmodel) {
		// 什么都不需要做
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
