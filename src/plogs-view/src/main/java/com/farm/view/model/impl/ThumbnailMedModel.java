package com.farm.view.model.impl;

import java.io.File;

import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.farm.view.model.utils.ModelFileUtils;

/**
 * @author macpl
 *
 */
public class ThumbnailMedModel implements ViewFileModelInter {
	/** 請在ViewFileModels中注冊 **/
	@Override
	public String getTitle() {
		return "中缩略图";
	}

	@Override
	public String getKey() {
		return "MED_THUMBNAIL";
	}

	@Override
	public ModelParas getParamegers() {
		ModelParas paras = new ModelParas();
		paras.put("WIDTH", 960);
		return paras;
	}

	@Override
	public File getModelFile(File file, String exname) {
		File modelFile = ModelFileUtils.getModelPath(file, "thumbnail", "med." + exname);
		return modelFile;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return getModelFile(file, exname);
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "downview/Pubload.do?id=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		return exname;
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return false;
	}

	@Override
	public String getText(File file) {
		return null;
	}
}
