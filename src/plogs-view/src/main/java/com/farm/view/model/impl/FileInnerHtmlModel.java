package com.farm.view.model.impl;

import java.io.File;

import org.apache.commons.lang.StringUtils;

import com.farm.file.util.FarmDocFiles;
import com.farm.util.web.FarmHtmlUtils;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.sun.star.uno.RuntimeException;

/**
 * ihtml为内容中引用了WDAP内文件的html（所以必须基于WDAP的jsp进行加载，否則地址的相對路徑不可用）
 * 
 * @author macpl
 *
 */
public class FileInnerHtmlModel implements ViewFileModelInter {

	@Override
	public String getTitle() {
		return "原始IHTML文件";
	}

	@Override
	public String getKey() {
		return "IHTML";
	}

	@Override
	public ModelParas getParamegers() {
		return new ModelParas();
	}

	@Override
	public File getModelFile(File file, String exname) {
		return file;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return FarmDocFiles.getSysIcon(exname);
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "show/PubInnerHtml.do?viewdocId=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		if (!exname.toUpperCase().equals("IHTML")) {
			throw new RuntimeException("the file is not ihtml!");
		}
		return "ihtml";
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return true;
	}

	@Override
	public String getText(File modelFile) {
		String html = FarmDocFiles.readFileText(modelFile);
		if (StringUtils.isNotBlank(html)) {
			return FarmHtmlUtils.HtmlRemoveTag(html);
		} else {
			return null;
		}
	}
}
