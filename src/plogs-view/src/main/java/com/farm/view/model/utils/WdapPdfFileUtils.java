package com.farm.view.model.utils;
import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class WdapPdfFileUtils {
	private static final Logger log = Logger.getLogger(WdapPdfFileUtils.class);
	public static void main(String[] args) throws Exception {
		System.out.println(get(new File("D:/test/1.pdf")));
	}

	public static String get(File pdfFile) {
		PDDocument document = null;
		try {
			// 方式二：
			document = PDDocument.load(pdfFile);
			// 获取页码
			int pages = document.getNumberOfPages();
			// 读文本内容
			PDFTextStripper stripper = new PDFTextStripper();
			// 设置按顺序输出
			stripper.setSortByPosition(true);
			stripper.setStartPage(1);
			stripper.setEndPage(pages);
			String content = stripper.getText(document);
			return content;
		} catch (Exception e) {
			log.error(e+e.getMessage(), e);
			return e.getMessage() + e;
		} finally {
			try {
				document.close();
			} catch (IOException e) {
				log.error(e+e.getMessage(), e);
			}
		}
	}
}
