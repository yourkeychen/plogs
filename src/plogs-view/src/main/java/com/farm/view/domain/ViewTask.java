package com.farm.view.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/* *
 *功能：转换任务类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "ViewTask")
@Table(name = "wdap_view_task")
public class ViewTask implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "FILEID", length = 32, nullable = false)
	private String fileid;
	@Column(name = "STIME", length = 16)
	private String stime;
	@Column(name = "ETIME", length = 16)
	private String etime;
	@Column(name = "CONVERTORID", length = 32)
	private String convertorid;
	@Column(name = "VIEWDOCID", length = 32)
	private String viewdocid;
	@Column(name = "PCONTENT", length = 128)
	private String pcontent;
	@Column(name = "PSTATE", length = 2, nullable = false)
	private String pstate;
	@Column(name = "CUSER", length = 32, nullable = false)
	private String cuser;
	@Column(name = "CUSERNAME", length = 64, nullable = false)
	private String cusername;
	@Column(name = "CTIME", length = 16, nullable = false)
	private String ctime;
	@Column(name = "BEFORETASK", length = 16)
	private String beforetask;
	@Column(name = "VIEWIS", length = 1, nullable = false)
	private String viewis;
	@Column(name = "OUTTIMENUM", length = 10, nullable = false)
	private Integer outtimenum;

	public Integer getOuttimenum() {
		return outtimenum;
	}

	public void setOuttimenum(Integer outtimenum) {
		this.outtimenum = outtimenum;
	}

	public String getViewis() {
		return viewis;
	}

	public void setViewis(String viewis) {
		this.viewis = viewis;
	}

	public String getBeforetask() {
		return beforetask;
	}

	public void setBeforetask(String beforetask) {
		this.beforetask = beforetask;
	}

	public String getFileid() {
		return this.fileid;
	}

	public void setFileid(String fileid) {
		this.fileid = fileid;
	}

	public String getStime() {
		return this.stime;
	}

	public void setStime(String stime) {
		this.stime = stime;
	}

	public String getEtime() {
		return this.etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

	public String getConvertorid() {
		return this.convertorid;
	}

	public void setConvertorid(String convertorid) {
		this.convertorid = convertorid;
	}

	public String getViewdocid() {
		return this.viewdocid;
	}

	public void setViewdocid(String viewdocid) {
		this.viewdocid = viewdocid;
	}

	public String getPcontent() {
		return this.pcontent;
	}

	public void setPcontent(String pcontent) {
		this.pcontent = pcontent;
	}

	public String getPstate() {
		return this.pstate;
	}

	public void setPstate(String pstate) {
		this.pstate = pstate;
	}

	public String getCuser() {
		return this.cuser;
	}

	public void setCuser(String cuser) {
		this.cuser = cuser;
	}

	public String getCusername() {
		return this.cusername;
	}

	public void setCusername(String cusername) {
		this.cusername = cusername;
	}

	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}