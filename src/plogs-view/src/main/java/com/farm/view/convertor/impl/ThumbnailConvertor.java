package com.farm.view.convertor.impl;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import com.farm.view.convertor.FileConvertorInter;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.task.domain.ConvertTaskFile;

import net.coobird.thumbnailator.Thumbnails;

/**
 * 縮略圖轉換器
 * 
 * @author macpl
 *
 */
public class ThumbnailConvertor implements FileConvertorInter {
	@Override
	public String getKey() {
		return "THUMBNAIL_IMG";
	}

	@Override
	public String getTitle() {
		return "转换图片为缩略图";
	}

	@Override
	public String getNote() {
		return "获得图片的缩略图";
	}

	@Override
	public void doConvert(ConvertTaskFile fileinfo, ViewFileModelInter omodel, ViewFileModelInter tmodel) {
		try {
			File ofile = omodel.getModelFile(fileinfo.getFile(), fileinfo.getExname());
			File tfile = tmodel.getModelFile(fileinfo.getFile(), fileinfo.getExname());
			if (!tfile.getParentFile().exists()) {
				tfile.getParentFile().mkdirs();
			}
			BufferedImage bufferedImage = ImageIO.read(fileinfo.getFile());
			int width = bufferedImage.getWidth();
			int toWidth = tmodel.getParamegers().getInt("WIDTH");
			if (width < toWidth) {
				toWidth = width;
			}
			Thumbnails.of(ofile).width(toWidth).toFile(tfile);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
