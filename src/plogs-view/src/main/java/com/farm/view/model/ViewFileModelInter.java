package com.farm.view.model;

import java.io.File;

import com.farm.view.model.domain.ModelParas;

/** 請在ViewFileModels中注冊 **/
public interface ViewFileModelInter {
	/**
	 * 获得模型名称
	 * 
	 * @return
	 */
	public String getTitle();

	/**
	 * 获得模型KEY
	 * 
	 * @return
	 */
	public String getKey();

	/**
	 * 模型參數
	 * 
	 * @return
	 */
	public ModelParas getParamegers();

	/**
	 * 模型文件
	 * 
	 * @param file
	 *            原文件(文件系统中的问题FileBase)
	 * @param exname
	 *            原文件的扩展名
	 * @return
	 */
	public File getModelFile(File file, String exname);

	/**
	 * 获得模型图标
	 * 
	 * @param file 原文件
	 * @param exname 原文件的扩展名
	 * @return
	 */
	public File getIconFile(File file, String exname);

	/**
	 * 获得下载链接
	 * 
	 * @param viewDocId
	 * @return
	 */
	public String getDownUrl(String viewDocId);

	/**
	 * 获得预览链接
	 * 
	 * @param viewDocId
	 * @return
	 */
	public String getViewUrl(String viewDocId);

	/**
	 * 获得预览文件扩展名
	 * 
	 * @param exname
	 *            原文件的扩展名
	 * @return
	 */
	public String getExname(String exname);

	/**
	 * 是否可以抓取文本
	 * 
	 * @param exname
	 *            原文件的扩展名
	 * @return
	 */
	public boolean isGetTextAble(String exname);

	/**
	 * 是否可以抓取文本
	 * 
	 * @param modelFile
	 *            模型文件
	 * @param exname
	 *            原文件扩展名
	 * @return
	 */
	public String getText(File modelFile);
}
