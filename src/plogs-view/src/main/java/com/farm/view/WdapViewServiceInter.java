package com.farm.view;

import java.util.List;
import java.util.Map;

import com.farm.file.domain.ex.PersistFile;

public interface WdapViewServiceInter {

	/**
	 * 获得预览文件
	 * 
	 * @param viewDocId
	 * @return
	 */
	public PersistFile getViewFile(String viewDocId);

	/**
	 * 获得预览文件(如果预览文件是个文件夹，此方法可以获得文件夹中的文件)
	 * 
	 * @param viewdocid
	 * @param num
	 * @return
	 */
	public PersistFile getViewFile(String viewdocid, int num);

	/**
	 * 文件预览事件被执行
	 * 
	 * @param viewDocId
	 */
	public void viewDocHandle(String viewDocId);

	/**
	 * 获得预览文件中的html资源（在预览模型文件的文件夹中，查找filepath的相對路径文件）
	 * 
	 * @param filepath
	 * @param viewDocId
	 * @return
	 */
	public PersistFile getViewDocHtmlResource(String filepath, String viewDocId);

	/**
	 * 将预览文件转换任务提交到队列中，等待任务执行
	 * 
	 * @param fileid
	 */
	public void reLoadFileViewTask(String fileid);

	/**
	 * 获得文件的预览文档信息
	 * 
	 * @param fileid
	 * @return
	 */
	public List<Map<String, Object>> getFileViewdocs(String fileid);

	/**
	 * 获得文件的转换任务信息
	 * 
	 * @param fileid
	 * @return
	 */
	public List<Map<String, Object>> getTasksByFileid(String fileid);

	/**
	 * 是否已经有预览文件生成，可以预览
	 * 
	 * @param fileid
	 * @return
	 */
	public boolean isView(String fileid);

	/**
	 * 获得任务日志
	 * 
	 * @param taskid
	 * @return
	 */
	public List<String> getTaskLogs(String taskid);

	/**
	 * 文件是否可以生成预览文件
	 * 
	 * @param exname
	 * @param lenght
	 * @return
	 */
	public boolean fileConvertAble(String exname, long lenght);

	/**
	 * 获得预览地址
	 * 
	 * @param fileid
	 * @return
	 */
	public String getViewUrl(String fileid);

	/**
	 * 获得附件的图标
	 * 
	 * @param fileid
	 * @return
	 */
	public PersistFile getFileIcon(String fileid);

	/**
	 * 获得预览文件的图标
	 * 
	 * @param viewDocId
	 * @return
	 */
	public PersistFile getViewDocIcon(String viewDocId);
}
