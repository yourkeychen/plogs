package com.farm.view.service;

import com.farm.view.domain.ViewRegular;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;

import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：转换规则服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ViewRegularServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ViewRegular insertViewregularEntity(ViewRegular entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ViewRegular editViewregularEntity(ViewRegular entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteViewregularEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ViewRegular getViewregularEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createViewregularSimpleQuery(DataQuery query);

	/**
	 * 获得文件可用的转换规则
	 * 
	 * @param fileid
	 * @return
	 */
	public List<ViewRegular> getFileRegular(String fileid);

	/**
	 * 复制一条规则
	 * 
	 * @param id
	 * @param currentUser
	 */
	public void copyViewregular(String regularid, LoginUser currentUser);

	/**
	 * 获得所有转换规则
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getAllRegular();

	/**
	 * 文件是否可以生成预览文件
	 * 
	 * @param exname 文件扩展名
	 * @param lenght 文件大小
	 * @return
	 */
	public boolean fileConvertAble(String exname, long lenght);

	/**修改预览规则状态
	 * @param regularId
	 * @param state
	 * @param viewis
	 * @param currentUser
	 */
	public void editViewregularState(String regularId, String state, String viewis, LoginUser currentUser);
}