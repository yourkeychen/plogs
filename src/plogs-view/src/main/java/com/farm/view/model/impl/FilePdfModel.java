package com.farm.view.model.impl;

import java.io.File;

import com.farm.file.domain.FileBase;
import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.farm.view.model.utils.WdapPdfFileUtils;
import com.sun.star.uno.RuntimeException;

/**
 * 单个文件模型：文件系统中的单个文件
 * 
 * @author macpl
 *
 */
public class FilePdfModel implements ViewFileModelInter {

	@Override
	public String getTitle() {
		return "原始PDF文件";
	}

	@Override
	public String getKey() {
		return "PDFFILE";
	}

	@Override
	public ModelParas getParamegers() {
		return new ModelParas();
	}

	@Override
	public File getModelFile(File file, String exname) {
		return file;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return FarmDocFiles.getSysIcon(exname);
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "downview/Pubload.do?id=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		if (!exname.toUpperCase().equals("PDF")) {
			throw new RuntimeException("the file is not pdf!");
		}
		return "pdf";
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return true;
	}

	@Override
	public String getText(File modelfile) {
		return WdapPdfFileUtils.get(modelfile);
	}
}
