package com.farm.view.model.impl;

import java.io.File;

import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;

public class ImgModel implements ViewFileModelInter {

	@Override
	public String getTitle() {

		return "原始图片";
	}

	@Override
	public String getKey() {
		return "IMGFILE";
	}

	@Override
	public ModelParas getParamegers() {
		return new ModelParas();
	}

	@Override
	public File getModelFile(File file, String exname) {
		return file;
	}

	@Override
	public File getIconFile(File file, String exname) {
		File iconFile = null;
		iconFile = new ThumbnailMinModel().getIconFile(file, exname);
		if (iconFile.exists()) {
			return iconFile;
		}
		iconFile = new ThumbnailMedModel().getIconFile(file, exname);
		if (iconFile.exists()) {
			return iconFile;
		}
		iconFile = new ThumbnailMaxModel().getIconFile(file, exname);
		if (iconFile.exists()) {
			return iconFile;
		}
		if (file.length() <= 1024 * 1024 * 5) {
			return file;
		}
		return FarmDocFiles.getSysIcon(exname);
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "downview/Pubload.do?id=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		return exname;
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return false;
	}

	@Override
	public String getText(File modelFile) {
		return null;
	}

}
