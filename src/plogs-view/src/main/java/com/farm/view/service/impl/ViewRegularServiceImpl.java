package com.farm.view.service.impl;

import com.farm.view.domain.ViewRegular;
import com.farm.core.time.TimeTool;
import com.farm.file.domain.FileBase;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.util.web.FarmFormatUnits;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.farm.view.dao.ViewRegularDaoInter;
import com.farm.view.service.ViewRegularServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：转换规则服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class ViewRegularServiceImpl implements ViewRegularServiceInter {
	@Resource
	private ViewRegularDaoInter viewregularDaoImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	private static final Logger log = Logger.getLogger(ViewRegularServiceImpl.class);

	@Override
	@Transactional
	public ViewRegular insertViewregularEntity(ViewRegular entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(user.getName());
		entity.setEuser(user.getId());
		entity.setEusername(user.getName());
		entity.setEtime(TimeTool.getTimeDate14());
		entity.setFileexname(entity.getFileexname().toLowerCase());
		// entity.setPstate("1");
		return viewregularDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public ViewRegular editViewregularEntity(ViewRegular entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		ViewRegular entity2 = viewregularDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setSort(entity.getSort());
		entity2.setTitle(entity.getTitle());
		entity2.setFileexname(entity.getFileexname().toLowerCase());
		entity2.setConvertorid(entity.getConvertorid());
		entity2.setPcontent(entity.getPcontent());
		entity2.setSizemin(entity.getSizemin());
		entity2.setSizemax(entity.getSizemax());
		entity2.setOuttimenum(entity.getOuttimenum());
		entity2.setViewis(entity.getViewis());
		entity2.setPstate(entity.getPstate());
		// entity2.setEuser(entity.getEuser());
		// entity2.setEusername(entity.getEusername());
		// entity2.setCuser(entity.getCuser());
		// entity2.setCusername(entity.getCusername());
		// entity2.setEtime(entity.getEtime());
		// entity2.setCtime(entity.getCtime());
		// entity2.setId(entity.getId());
		viewregularDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteViewregularEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		viewregularDaoImpl.deleteEntity(viewregularDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public ViewRegular getViewregularEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return viewregularDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createViewregularSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"WDAP_VIEW_REGULAR a left join WDAP_VIEW_CONVERTOR b on a.CONVERTORID=b.id",
				"a.ID as ID,a.SORT as SORT,a.TITLE as TITLE,a.FILEEXNAME as FILEEXNAME,a.CONVERTORID as CONVERTORID,a.PCONTENT as PCONTENT"
						+ ",SIZEMIN,SIZEMAX,VIEWIS,a.PSTATE as PSTATE,b.TITLE as BTITLE,OUTTIMENUM");
		return dbQuery;
	}

	@Override
	@Transactional
	public List<ViewRegular> getFileRegular(String fileid) {
		FileBase file = fileBaseServiceImpl.getFilebaseEntity(fileid);
		List<ViewRegular> allregulars = viewregularDaoImpl.selectEntitys(
				DBRuleList.getInstance().add(new DBRule("FILEEXNAME", file.getExname().toLowerCase(), "=")).toList());
		List<ViewRegular> FileRegular = new ArrayList<>();
		for (ViewRegular regular : allregulars) {
			if (regular.getSizemin() < file.getFilesize() && file.getFilesize() < regular.getSizemax()
					&& regular.getPstate().equals("1")) {
				FileRegular.add(regular);
			}
		}
		return FileRegular;
	}

	@Override
	@Transactional
	public void copyViewregular(String regularid, LoginUser currentUser) {
		ViewRegular regular = new ViewRegular();
		ViewRegular oRegular = getViewregularEntity(regularid);
		try {
			BeanUtils.copyProperties(regular, oRegular);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		regular.setId(null);
		regular.setTitle("复制-" + regular.getTitle());
		insertViewregularEntity(regular, currentUser);
	}

	@Override
	@Transactional
	public List<Map<String, Object>> getAllRegular() {
		DataResult result;
		try {
			DataQuery query = createViewregularSimpleQuery(DataQuery.getInstance());
			query.setPagesize(1000);
			query.addSort(new DBSort("FILEEXNAME", "asc"));
			query.addRule(new DBRule("a.PSTATE", "1", "="));
			result = query.search();
			result.runDictionary("1:可用,0:禁用", "PSTATE");
			result.runDictionary("1:可以预览,0:禁止预览", "VIEWIS");
			result.runDictionary("0:无序", "SORT");
			result.runHandle(new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					row.put("SIZEMIN",
							FarmFormatUnits.getFileLengthAndUnit(Integer.valueOf(row.get("SIZEMIN").toString())));
					row.put("SIZEMAX",
							FarmFormatUnits.getFileLengthAndUnit(Integer.valueOf(row.get("SIZEMAX").toString())));
					int outtime = Integer.valueOf(row.get("OUTTIMENUM").toString());
					if (outtime > 1000) {
						row.put("OUTTIMENUM", outtime / 1000 + "秒");
					} else {
						row.put("OUTTIMENUM", outtime + "毫秒");
					}
				}
			});
			return result.getResultList();
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	@Override
	@Transactional
	public boolean fileConvertAble(String exname, long lenght) {
		List<ViewRegular> allregulars = viewregularDaoImpl.selectEntitys(
				DBRuleList.getInstance().add(new DBRule("FILEEXNAME", exname.toLowerCase(), "=")).toList());
		List<ViewRegular> FileRegular = new ArrayList<>();
		for (ViewRegular regular : allregulars) {
			if (regular.getSizemin() < lenght && lenght < regular.getSizemax() && regular.getPstate().equals("1")) {
				FileRegular.add(regular);
			}
		}
		return FileRegular.size() > 0;
	}

	@Override
	@Transactional
	public void editViewregularState(String regularId, String state, String viewis, LoginUser currentUser) {
		ViewRegular entity2 = viewregularDaoImpl.getEntity(regularId);
		if (StringUtils.isNotBlank(state)) {
			entity2.setPstate(state);
		}
		if (StringUtils.isNotBlank(viewis)) {
			entity2.setViewis(viewis);
		}
		entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setEuser(currentUser.getId());
		entity2.setEusername(currentUser.getName());
		viewregularDaoImpl.editEntity(entity2);
	}
}
