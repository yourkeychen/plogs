package com.farm.view.model.impl;

import java.io.File;

import com.farm.file.util.FarmDocFiles;
import com.farm.view.model.ViewFileModelInter;
import com.farm.view.model.domain.ModelParas;
import com.farm.view.model.utils.ModelFileUtils;
import com.farm.view.model.utils.WdapPdfFileUtils;

/**
 * pdf预览文件
 * 
 * @author macpl
 *
 */
public class ViewPdfModel implements ViewFileModelInter {

	@Override
	public String getTitle() {
		return "PDF预览文件";
	}

	@Override
	public String getKey() {
		return "VIEW_PDF";
	}

	@Override
	public ModelParas getParamegers() {
		return new ModelParas();
	}

	@Override
	public File getModelFile(File file, String exname) {
		File modelFile = ModelFileUtils.getModelPath(file, "pdf", "file.pdf");
		return modelFile;
	}

	@Override
	public File getIconFile(File file, String exname) {
		return FarmDocFiles.getSysIcon("pdf");
	}

	@Override
	public String getDownUrl(String viewDocId) {
		return "downview/Pubfile.do?id=" + viewDocId;
	}

	@Override
	public String getViewUrl(String viewDocId) {
		return "downview/Pubload.do?id=" + viewDocId;
	}

	@Override
	public String getExname(String exname) {
		return "pdf";
	}

	@Override
	public boolean isGetTextAble(String exname) {
		return true;
	}

	@Override
	public String getText(File modelFile) {
		return WdapPdfFileUtils.get(modelFile);
	}

}
