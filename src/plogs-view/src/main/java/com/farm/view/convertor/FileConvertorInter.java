package com.farm.view.convertor;

import com.farm.view.model.ViewFileModelInter;
import com.farm.view.task.domain.ConvertTaskFile;

public interface FileConvertorInter {
	/**
	 * 获得转换器的key
	 * 
	 * @return
	 */
	public String getKey();

	/**
	 * 获得转换器的名称
	 * 
	 * @return
	 */
	public String getTitle();

	/**
	 * 获得转换器描述
	 * 
	 * @return
	 */
	public String getNote();

	/**
	 * 執行轉換
	 * 
	 * @param fileInfo
	 *            文件信息
	 * @param oviewFileModel
	 *            原文件模型
	 * @param tviewFileModel
	 *            目标文件模型
	 */
	public void doConvert(ConvertTaskFile fileInfo, ViewFileModelInter oviewFileModel,
			ViewFileModelInter tviewFileModel);
}
