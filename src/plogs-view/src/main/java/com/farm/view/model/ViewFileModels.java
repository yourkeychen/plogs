package com.farm.view.model;

import java.util.ArrayList;
import java.util.List;

import com.farm.view.model.impl.FileInnerHtmlModel;
import com.farm.view.model.impl.FileModel;
import com.farm.view.model.impl.FileMp3Model;
import com.farm.view.model.impl.FileMp4Model;
import com.farm.view.model.impl.FilePdfModel;
import com.farm.view.model.impl.FileTxtModel;
import com.farm.view.model.impl.ImgDirModel;
import com.farm.view.model.impl.ImgModel;
import com.farm.view.model.impl.ThumbnailMaxModel;
import com.farm.view.model.impl.ThumbnailMedModel;
import com.farm.view.model.impl.ThumbnailMinModel;
import com.farm.view.model.impl.ViewHtmlModel;
import com.farm.view.model.impl.ViewPdfModel;
import com.farm.view.model.impl.ViewTxtModel;

/**
 * 所有的预览模型
 * 
 * @author macpl
 *
 */
public class ViewFileModels {
	/**
	 * 获得所有模型
	 * 
	 * @return
	 */
	public static List<ViewFileModelInter> getAllModels() {
		List<ViewFileModelInter> list = new ArrayList<>();
		list.add(new FileModel());
		list.add(new FilePdfModel());
		list.add(new FileMp4Model());
		list.add(new FileMp3Model());
		list.add(new FileInnerHtmlModel());
		list.add(new ImgDirModel());
		list.add(new ImgModel());
		list.add(new ThumbnailMaxModel());
		list.add(new ThumbnailMedModel());
		list.add(new ThumbnailMinModel());
		list.add(new ViewPdfModel());
		list.add(new ViewHtmlModel());
		list.add(new FileTxtModel());
		list.add(new ViewTxtModel());
		return list;
	}

	/**
	 * 通过key获得模型
	 * 
	 * @param modelkey
	 * @return
	 */
	public static ViewFileModelInter getModel(String modelkey) {
		for (ViewFileModelInter modelinter : getAllModels()) {
			if (modelinter.getKey().equals(modelkey)) {
				return modelinter;
			}
		}
		return null;
	}
}
