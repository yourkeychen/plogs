package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：项目分类阶段类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "ProjectTypeGradation")
@Table(name = "plogs_re_typegradation")
public class ProjectTypeGradation implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "SORT", length = 10, nullable = false)
        private Integer sort;
        @Column(name = "GRADATIONID", length = 32, nullable = false)
        private String gradationid;
        @Column(name = "PROJECTTYPEID", length = 32, nullable = false)
        private String projecttypeid;

        public Integer  getSort() {
          return this.sort;
        }
        public void setSort(Integer sort) {
          this.sort = sort;
        }
        public String  getGradationid() {
          return this.gradationid;
        }
        public void setGradationid(String gradationid) {
          this.gradationid = gradationid;
        }
        public String  getProjecttypeid() {
          return this.projecttypeid;
        }
        public void setProjecttypeid(String projecttypeid) {
          this.projecttypeid = projecttypeid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}