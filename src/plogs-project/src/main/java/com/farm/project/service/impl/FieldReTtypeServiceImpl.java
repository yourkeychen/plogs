package com.farm.project.service.impl;

import com.farm.project.domain.FieldReTtype;
import org.apache.log4j.Logger;
import com.farm.project.dao.FieldReTtypeDaoInter;
import com.farm.project.service.FieldReTtypeServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：任务类型属性服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class FieldReTtypeServiceImpl implements FieldReTtypeServiceInter {
	@Resource
	private FieldReTtypeDaoInter fieldrettypeDaoImpl;

	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(FieldReTtypeServiceImpl.class);

	@Override
	@Transactional
	public FieldReTtype insertFieldrettypeEntity(FieldReTtype entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return fieldrettypeDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public FieldReTtype editFieldrettypeEntity(FieldReTtype entity, LoginUser user) {
		FieldReTtype entity2 = fieldrettypeDaoImpl.getEntity(entity.getId());
		entity2.setWriteis(entity.getWriteis());
		entity2.setReadis(entity.getReadis());
		fieldrettypeDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteFieldrettypeEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		fieldrettypeDaoImpl.deleteEntity(fieldrettypeDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public FieldReTtype getFieldrettypeEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return fieldrettypeDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFieldrettypeSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"PLOGS_FIELD_RETTYPE a left join PLOGS_FIELD b on a.FIELDID=b.ID left join PLOGS_TASK_TYPE c on a.TASKTYPEID=c.ID",
				"a.ID as ID,WRITEIS,READIS,b.NAME as FIELDNAME,c.NAME as TASKTYPENAME");
		return dbQuery;
	}

	@Override
	@Transactional
	public void bindTaskType(String taskTypeId, String fieldid, LoginUser currentUser) {
		fieldrettypeDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("TASKTYPEID", taskTypeId, "="))
				.add(new DBRule("FIELDID", fieldid, "=")).toList());
		FieldReTtype entity = new FieldReTtype();
		entity.setFieldid(fieldid);
		entity.setTasktypeid(taskTypeId);
		entity.setReadis("1");
		entity.setWriteis("1");
		fieldrettypeDaoImpl.insertEntity(entity);
	}

}
