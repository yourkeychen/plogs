package com.farm.project.service.utils;

import java.util.List;

import org.apache.log4j.Logger;

import com.farm.core.inter.FileEventsInter;
import com.farm.project.service.TaskServiceInter;
import com.farm.util.spring.BeanFactory;

public class FileIndexHandles implements FileEventsInter {
	private final static Logger log = Logger.getLogger(FileIndexHandles.class);

	@Override
	public void fileOnDel(String fileid) {
		// 删除文件
		IndexUtils.deleteIndex(fileid);
	}

	@Override
	public void fileTextOnChange(String fileid) {
		// 更新附件文字描述
		TaskServiceInter taskService = (TaskServiceInter) BeanFactory.getBean("taskServiceImpl");
		List<TaskFileIndexInfo> infos = taskService.getIndexInfos(fileid);
		for (TaskFileIndexInfo info : infos) {
			if (info.getTask().getPstate().equals("5")) {
				IndexUtils.indexTaskFile(info);
			} else {
				log.info("the task not complete! 不做索引");
			}
		}
	}

}
