package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;


/* *
 *功能：项目类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "Project")
@Table(name = "plogs_project")
public class Project implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "TYPEID", length = 32, nullable = false)
	private String typeid;
	@Column(name = "GRADATIONNAME", length = 128, nullable = false)
	private String gradationname;
	@Column(name = "GRADATIONID", length = 32, nullable = false)
	private String gradationid;
	@Column(name = "STATE", length = 1, nullable = false)
	private String state;
	@Column(name = "LIVETIME", length = 16, nullable = false)
	private String livetime;
	@Column(name = "NAME", length = 128, nullable = false)
	private String name;
	@Column(name = "PCONTENT", length = 128)
	private String pcontent;
	@Column(name = "EUSER", length = 32, nullable = false)
	private String euser;
	@Column(name = "CUSER", length = 32, nullable = false)
	private String cuser;
	@Column(name = "ETIME", length = 16, nullable = false)
	private String etime;
	@Column(name = "CTIME", length = 16, nullable = false)
	private String ctime;
	@Transient
	private String companyname;

	

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getTypeid() {
		return this.typeid;
	}

	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}

	public String getGradationname() {
		return this.gradationname;
	}

	public void setGradationname(String gradationname) {
		this.gradationname = gradationname;
	}

	public String getGradationid() {
		return this.gradationid;
	}

	public void setGradationid(String gradationid) {
		this.gradationid = gradationid;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLivetime() {
		return this.livetime;
	}

	public void setLivetime(String livetime) {
		this.livetime = livetime;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPcontent() {
		return this.pcontent;
	}

	public void setPcontent(String pcontent) {
		this.pcontent = pcontent;
	}

	public String getEuser() {
		return this.euser;
	}

	public void setEuser(String euser) {
		this.euser = euser;
	}

	public String getCuser() {
		return this.cuser;
	}

	public void setCuser(String cuser) {
		this.cuser = cuser;
	}

	public String getEtime() {
		return this.etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}