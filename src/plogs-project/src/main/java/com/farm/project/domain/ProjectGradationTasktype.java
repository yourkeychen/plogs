package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：阶段任务类型类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "ProjectGradationTaskType")
@Table(name = "plogs_re_gradationtasktype")
public class ProjectGradationTasktype implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "SORT", length = 10, nullable = false)
        private Integer sort;
        @Column(name = "TASKTYPEID", length = 32, nullable = false)
        private String tasktypeid;
        @Column(name = "GRADATIONID", length = 32, nullable = false)
        private String gradationid;

        public Integer  getSort() {
          return this.sort;
        }
        public void setSort(Integer sort) {
          this.sort = sort;
        }
        public String  getTasktypeid() {
          return this.tasktypeid;
        }
        public void setTasktypeid(String tasktypeid) {
          this.tasktypeid = tasktypeid;
        }
        public String  getGradationid() {
          return this.gradationid;
        }
        public void setGradationid(String gradationid) {
          this.gradationid = gradationid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}