package com.farm.project.service;

import com.farm.project.domain.Taskuser;
import com.farm.core.sql.query.DataQuery;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：任务干系人服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface TaskuserServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Taskuser insertTaskuserEntity(Taskuser entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Taskuser editTaskuserEntity(Taskuser entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteTaskuserEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Taskuser getTaskuserEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createTaskuserSimpleQuery(DataQuery query);

	/**
	 * 重置任务关系人
	 * 
	 * @param taskid
	 * @param currentUser
	 */
	public void resetTasktuser(String taskid, LoginUser currentUser);

	/**
	 * 添加一个任务关系人
	 * 
	 * @param taskid
	 * @param userid
	 * @param poptype
	 * @param currentUser
	 * @param isFreshLivetime 是否刷新项目活跃用户
	 */
	public void adduser(String taskid, String userid, String poptype, LoginUser currentUser, boolean isFreshLivetime);

	/**
	 * 绑定唯一执行关系人
	 * 
	 * @param taskId
	 * @param userId
	 * @param currentUser
	 */
	public void bindDoUser(String taskId, String userId, LoginUser currentUser);

	/**
	 * 获得执行人
	 * 
	 * @param taskId
	 * @param poptype
	 * @return
	 */
	public List<Taskuser> getTaskUser(String taskId, String poptype);

	/**
	 * 是否任务执行人
	 * 
	 * @param taskId
	 * @param currentUser
	 * @return
	 */
	public boolean isDoUser(String taskId, LoginUser currentUser);

	/**
	 * 是否任务管理人
	 * 
	 * @param taskId
	 * @param currentUser
	 * @return
	 */
	public boolean isManagerUser(String taskId, LoginUser currentUser);

	/**
	 * 获得任务关系人
	 * 
	 * @param taskId
	 * @param poptype
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getTaskUsers(String taskId) throws SQLException;

}