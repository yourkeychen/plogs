package com.farm.project.service;

import com.farm.project.domain.Field;
import com.farm.project.domain.ProjectType;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目分类服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ProjectTypeServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ProjectType insertProjecttypeEntity(ProjectType entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ProjectType editProjecttypeEntity(ProjectType entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteProjecttypeEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ProjectType getProjecttypeEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createProjecttypeSimpleQuery(DataQuery query);

	/**
	 * 移动树节点
	 * 
	 * @param ids
	 *            移动节点
	 * @param id
	 *            目标节点
	 * @param currentUser
	 */
	public void moveTreeNode(String ids, String targetId, LoginUser currentUser);

	/**
	 * 查詢關聯项目阶段
	 * 
	 * @param query
	 * @param gradationId
	 * @return
	 */
	public DataQuery createReGradationsQuery(DataQuery query, String projectTypeid);

	/**
	 * 綁定項目階段
	 * 
	 * @param gradationid
	 * @param typeid
	 * @param currentUser
	 */
	public void bindGradation(String gradationid, String typeid, LoginUser currentUser);

	/**
	 * 取消項目階段綁定
	 * 
	 * @param id
	 *            中間表id
	 * @param currentUser
	 */
	public void rebindGradation(String id, LoginUser currentUser);

	/**
	 * 上移排序号
	 * 
	 * @param reids
	 *            階段中間表ids
	 * @param currentUser
	 */
	public void toUpProjectTypeGradationSort(String reids, LoginUser currentUser);

	/**
	 * 获得所有类型
	 * 
	 * @param model
	 *            0分类：可以下挂分类和类型，1类型：必须为叶子节点
	 * @return
	 */
	public List<ProjectType> getAllTypes(String model);

	/**
	 * 获得分类数据的展示属性()
	 * 
	 * @param id
	 * @return
	 */
	public List<Field> getDataSearchFields(String id);
}