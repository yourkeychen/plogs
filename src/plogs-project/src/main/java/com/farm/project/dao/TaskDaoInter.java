package com.farm.project.dao;

import com.farm.project.domain.Task;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;

/* *
 *功能：任务数据库持久层接口
 *详细：
 *
 *版本：v0.1
 *作者：Farm代码工程自动生成
 *日期：20150707114057
 *说明：
 */
public interface TaskDaoInter {
	/**
	 * 删除一个任务实体
	 * 
	 * @param entity 实体
	 */
	public void deleteEntity(Task task);

	/**
	 * 由任务id获得一个任务实体
	 * 
	 * @param id
	 * @return
	 */
	public Task getEntity(String taskid);

	/**
	 * 插入一条任务数据
	 * 
	 * @param entity
	 */
	public Task insertEntity(Task task);

	/**
	 * 获得记录数量
	 * 
	 * @return
	 */
	public int getAllListNum();

	/**
	 * 修改一个任务记录
	 * 
	 * @param entity
	 */
	public void editEntity(Task task);

	/**
	 * 获得一个session
	 */
	public Session getSession();

	/**
	 * 执行一条任务查询语句
	 */
	public DataResult runSqlQuery(DataQuery query);

	/**
	 * 条件删除任务实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param rules 删除条件
	 */
	public void deleteEntitys(List<DBRule> rules);

	/**
	 * 条件查询任务实体，依据对象字段值,当rules为空时查询全部(一般不建议使用该方法)
	 * 
	 * @param rules 查询条件
	 * @return
	 */
	public List<Task> selectEntitys(List<DBRule> rules);

	/**
	 * 条件修改任务实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param values 被修改的键值对
	 * @param rules  修改条件
	 */
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules);

	/**
	 * 条件合计任务:count(*)
	 * 
	 * @param rules 统计条件
	 */
	public int countEntitys(List<DBRule> rules);

	/**
	 * 获得项目的再用阶段id
	 * 
	 * @param projectId
	 * @return
	 */
	public List<String> getGradationIdsByProjectId(String projectId);

	/**
	 * 获得任务数量
	 * 
	 * @param userid
	 * @param state
	 * @return
	 */
	public int getTaskNum(String userid, String state);
}