package com.farm.project.dao.impl;

import java.math.BigInteger;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.farm.project.domain.TaskFile;
import com.farm.project.dao.TaskFileDaoInter;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.query.DataQuerys;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.utils.HibernateSQLTools;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;

/* *
 *功能：任务附件持久层实现
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Repository
public class TaskFileDaoImpl extends HibernateSQLTools<TaskFile>implements TaskFileDaoInter {
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFatory;

	@Override
	public void deleteEntity(TaskFile taskfile) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.delete(taskfile);
	}

	@Override
	public int getAllListNum() {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery("select count(*) from farm_code_field");
		BigInteger num = (BigInteger) sqlquery.list().get(0);
		return num.intValue();
	}

	@Override
	public TaskFile getEntity(String taskfileid) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		return (TaskFile) session.get(TaskFile.class, taskfileid);
	}

	@Override
	public TaskFile insertEntity(TaskFile taskfile) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.save(taskfile);
		return taskfile;
	}

	@Override
	public void editEntity(TaskFile taskfile) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.update(taskfile);
	}

	@Override
	public Session getSession() {
		// TODO 自动生成代码,修改后请去除本注释
		return sessionFatory.getCurrentSession();
	}

	@Override
	public DataResult runSqlQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			return query.search(sessionFatory.getCurrentSession());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void deleteEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		deleteSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public List<TaskFile> selectEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		return selectSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		updataSqlFromFunction(sessionFatory.getCurrentSession(), values, rules);
	}

	@Override
	public int countEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		return countSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	public SessionFactory getSessionFatory() {
		return sessionFatory;
	}

	public void setSessionFatory(SessionFactory sessionFatory) {
		this.sessionFatory = sessionFatory;
	}

	@Override
	protected Class<?> getTypeClass() {
		return TaskFile.class;
	}

	@Override
	protected SessionFactory getSessionFactory() {
		return sessionFatory;
	}

	@Override
	public List<Map<String, Object>> getProjectFiles(String projectId, String stime_yyyymmdd, String etime_yyyymmdd,
			LoginUser currentUser) throws SQLException {
		DataQuery query = DataQuery.getInstance(1,
				"b.ID as ID,b.TASKID as TASKID,b.FILEID as FILEID,b.CTIME as CTIME,b.SOURCETYPE as SOURCETYPE,b.LOGID as LOGID,c.TITLE as FILETITLE,c.EXNAME as EXNAME,SECRET",
				"PLOGS_TASK a left join PLOGS_TASK_FILE b on a.id=b.taskid left join WDAP_FILE c on c.id=b.fileid");
		query.addSqlRule(" and a.pstate!='0'");
		if (StringUtils.isNotBlank(projectId)) {
			query.addSqlRule(" and a.projectid='" + projectId + "'");
		}
		if (currentUser != null) {
			query.addSqlRule(" and a.USERID='" + currentUser.getId() + "'");
		}
		if (StringUtils.isNotBlank(stime_yyyymmdd) && StringUtils.isNotBlank(etime_yyyymmdd)) {
			query.addSqlRule(" and ( ( DOSTIME > '" + stime_yyyymmdd.replaceAll("-", "") + "000000"
					+ "' AND DOSTIME < '" + etime_yyyymmdd.replaceAll("-", "") + "999999" + "' ) OR ( DOETIME > '"
					+ stime_yyyymmdd.replaceAll("-", "") + "000000" + "' AND DOETIME < '"
					+ etime_yyyymmdd.replaceAll("-", "") + "999999" + "' ) OR   ( DOSTIME < '"
					+ stime_yyyymmdd.replaceAll("-", "") + "000000" + "' AND DOETIME > '"
					+ etime_yyyymmdd.replaceAll("-", "") + "999999" + "' ) )");
		}
		query.setPagesize(1000);
		DataQuerys.wipeVirus(projectId);
		return query.search().getResultList();
	}

	@Override
	public List<Map<String, Object>> getLogFiles(String logid) throws SQLException {
		DataQuery query = DataQuery.getInstance(1,
				"b.ID as ID,b.TASKID as TASKID,b.FILEID as FILEID,b.CTIME as CTIME,b.LOGID as LOGID,c.TITLE as FILETITLE,c.EXNAME as EXNAME,SECRET",
				"PLOGS_TASK_FILE b left join WDAP_FILE c on c.id=b.fileid");
		query.addSqlRule(" and b.LOGID='" + logid + "' and SOURCETYPE='4'");
		query.setPagesize(1000);
		DataQuerys.wipeVirus(logid);
		return query.search().getResultList();
	}

	@Override
	public List<Map<String, Object>> getTaskReadFiles(String taskid) throws SQLException {
		DataQuery query = DataQuery.getInstance(1,
				"b.ID as ID,b.TASKID as TASKID,b.FILEID as FILEID,b.CTIME as CTIME,b.LOGID as LOGID,c.TITLE as FILETITLE,c.EXNAME as EXNAME,SECRET",
				"PLOGS_TASK_FILE b left join WDAP_FILE c on c.id=b.fileid");
		query.addSqlRule(" and b.TASKID='" + taskid + "' and SOURCETYPE='2'");
		query.setPagesize(1000);
		DataQuerys.wipeVirus(taskid);
		return query.search().getResultList();
	}
}
