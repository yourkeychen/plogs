package com.farm.project.controller;

import com.farm.project.domain.TaskType;
import com.farm.project.service.TaskTypeServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;

import com.farm.web.easyui.EasyUiTreeNode;
import com.farm.web.easyui.EasyUiTreeNodeHandle;
import com.farm.web.easyui.EasyUiUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：任务类型控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/tasktype")
@Controller
public class TaskTypeController extends WebUtils {
	private final static Logger log = Logger.getLogger(TaskTypeController.class);
	@Resource
	private TaskTypeServiceInter taskTypeServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			if (query.getQueryRule().size() == 0) {
				query.addRule(new DBRule("PARENTID", "NONE", "="));
			}
			query.addRule(new DBRule("PSTATE", "2", "!="));
			DataResult result = taskTypeServiceImpl.createTasktypeSimpleQuery(query).search();
			result.runHandle(new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					if (row.get("ID").equals(row.get("UUID"))) {
						row.put("ISDEMO", "否");
					} else {
						row.put("ISDEMO", "是");
					}
					if (row.get("TEMPLETEFILEID") == null) {
						row.put("TEMPLETEFILEID", "无");
					} else {
						row.put("TEMPLETEFILEID", "设置");
					}
				}
			});
			result.runDictionary("0:树型分类,1:任务类型", "MODEL");
			result.runDictionary("1:可用,0:停用", "PSTATE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/chooseQuery")
	@ResponseBody
	public Map<String, Object> chooseQuery(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("PSTATE", "1", "="));
			query.addRule(new DBRule("MODEL", "1", "="));
			DataResult result = taskTypeServiceImpl.createTasktypeSimpleQuery(query).search();
			result.runHandle(new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					if (row.get("ID").equals(row.get("UUID"))) {
						row.put("ISDEMO", "是");
					} else {
						row.put("ISDEMO", "否");
					}
				}
			});
			result.runDictionary("0:树型分类,1:任务类型", "MODEL");
			result.runDictionary("1:可用,0:停用", "PSTATE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 组织机构节点
	 */
	@RequestMapping("/tasktypeTree")
	@ResponseBody
	public Object tasktypeTree(String id) {
		try {
			List<EasyUiTreeNode> allnodes = null;
			if (id == null) {
				// 如果是未传入id，就是根节点，就构造一个虚拟的上级节点
				id = "NONE";
				List<EasyUiTreeNode> list = new ArrayList<>();
				EasyUiTreeNode nodes = new EasyUiTreeNode("NONE", "任务分类", "open", "icon-blogs");
				nodes.setChildren(EasyUiTreeNode.formatAsyncAjaxTree(
						EasyUiTreeNode.queryTreeNodeOne(id, "SORT", "PLOGS_TASK_TYPE", "ID", "PARENTID", "NAME",
								"CTIME", "and a.PSTATE='1'").getResultList(),
						EasyUiTreeNode.queryTreeNodeTow(id, "SORT", "PLOGS_TASK_TYPE", "ID", "PARENTID", "NAME",
								"CTIME", "and a.PSTATE='1'").getResultList(),
						"PARENTID", "ID", "NAME", "CTIME"));
				list.add(nodes);
				allnodes = list;
			} else {
				allnodes = EasyUiTreeNode.formatAsyncAjaxTree(
						EasyUiTreeNode.queryTreeNodeOne(id, "SORT", "PLOGS_TASK_TYPE", "ID", "PARENTID", "NAME",
								"CTIME", "and a.PSTATE='1'").getResultList(),
						EasyUiTreeNode.queryTreeNodeTow(id, "SORT", "PLOGS_TASK_TYPE", "ID", "PARENTID", "NAME",
								"CTIME", "and a.PSTATE='1'").getResultList(),
						"PARENTID", "ID", "NAME", "CTIME");
			}
			EasyUiUtils.runTreeNodeHandle(allnodes, new EasyUiTreeNodeHandle() {
				@Override
				public void handle(EasyUiTreeNode node) {
					TaskType type = taskTypeServiceImpl.getTasktypeEntity(node.getId());
					if (type != null && type.getModel().equals("1")) {
						node.setIconCls("icon-blog");
					}
				}
			});
			return allnodes;
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(TaskType entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = taskTypeServiceImpl.editTasktypeEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(TaskType entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = taskTypeServiceImpl.insertTasktypeEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				taskTypeServiceImpl.deleteTasktypeEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/formatSort")
	@ResponseBody
	public Map<String, Object> formatSort(String ids, HttpSession session) {
		try {
			int n = 1;
			for (String id : parseIds(ids)) {
				taskTypeServiceImpl.editSort(id, n++, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/TaskTypeResult");
	}

	/**
	 * 模板編輯页面
	 * 
	 * @param taskid
	 * @param session
	 * @return
	 */
	@RequestMapping("/templeteSetting")
	public ModelAndView templeteSetting(String taskTypeid, HttpSession session) {
		return ViewMode.getInstance().putAttr("taskTypeid", taskTypeid)
				.putAttr("text", taskTypeServiceImpl.readTempleteHtml(taskTypeid))
				.returnModelAndView("project/TaskTypeTemplete");
	}

	/**
	 * 提交模板
	 * 
	 * @param taskid
	 * @param text
	 * @param session
	 * @return
	 */
	@RequestMapping("/templeteSubmit")
	@ResponseBody
	public Map<String, Object> templeteSubmit(String taskTypeid, String text, HttpSession session) {
		try {
			taskTypeServiceImpl.saveTemplete(taskTypeid, text, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 清除模板
	 * 
	 * @param taskid
	 * @param session
	 * @return
	 */
	@RequestMapping("/templeteClear")
	@ResponseBody
	public Map<String, Object> templeteClear(String taskTypeid, HttpSession session) {
		try {
			taskTypeServiceImpl.clearTemplete(taskTypeid,getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/chooseWin")
	public ModelAndView chooseWin(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/ChooseTaskTypeWin");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids, String parentId) {
		try {

			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", taskTypeServiceImpl.getTasktypeEntity(ids))
						.returnModelAndView("project/TaskTypeForm");
			}
			case (1): {// 新增
				TaskType parent = null;
				if (StringUtils.isNotBlank(parentId)) {
					parent = taskTypeServiceImpl.getTasktypeEntity(parentId);
				}
				return ViewMode.getInstance().putAttr("parent", parent).putAttr("pageset", pageset)
						.returnModelAndView("project/TaskTypeForm");
			}
			case (2): {// 修改
				TaskType type = taskTypeServiceImpl.getTasktypeEntity(ids);
				TaskType parent = null;
				if (StringUtils.isNotBlank(type.getParentid())) {
					parent = taskTypeServiceImpl.getTasktypeEntity(type.getParentid());
				}
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("entity", type)
						.putAttr("parent", parent).returnModelAndView("project/TaskTypeForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/TaskTypeForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("project/TaskTypeForm");
		}
	}

	/**
	 * 跳转树选择页面
	 *
	 * @return
	 */
	@RequestMapping("/treeNodeTreeView")
	public ModelAndView forSend() {
		return ViewMode.getInstance().returnModelAndView("project/ChooseTaskTypeTree");
	}

	/**
	 * 移动节点
	 *
	 * @return
	 */
	@RequestMapping("/moveTreeNodeSubmit")
	@ResponseBody
	public Map<String, Object> moveTreeNodeSubmit(String ids, String id, HttpSession session) {
		try {
			taskTypeServiceImpl.moveTreeNode(ids, id, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
}
