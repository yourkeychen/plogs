package com.farm.project.domain.ex;

import java.util.List;

import com.farm.project.domain.ProjectFieldins;

/* *
 *功能：项目属性(项目属性可能对应一条或多条，所以统一按照多条计算)
 */
public class ProjectField implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private String htmldemoid;
	private String projectid;
	private String pubread;
	private String enums;
	private String name;
	private String valtype;
	private List<ProjectFieldins> fieldines;
	private Integer sort;
	private String uuid;
	private String singletype;
	private boolean eidtAble;

	public boolean isEidtAble() {
		return eidtAble;
	}

	public void setEidtAble(boolean eidtAble) {
		this.eidtAble = eidtAble;
	}

	public String getSingletype() {
		return singletype;
	}

	public void setSingletype(String singletype) {
		this.singletype = singletype;
	}

	public String getHtmldemoid() {
		return this.htmldemoid;
	}

	public void setHtmldemoid(String htmldemoid) {
		this.htmldemoid = htmldemoid;
	}

	public String getProjectid() {
		return this.projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getPubread() {
		return this.pubread;
	}

	public void setPubread(String pubread) {
		this.pubread = pubread;
	}

	public String getEnums() {
		return this.enums;
	}

	public void setEnums(String enums) {
		this.enums = enums;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValtype() {
		return this.valtype;
	}

	public void setValtype(String valtype) {
		this.valtype = valtype;
	}

	public List<ProjectFieldins> getFieldines() {
		return fieldines;
	}

	public void setFieldines(List<ProjectFieldins> fieldines) {
		this.fieldines = fieldines;
	}

	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getUuid() {
		return this.uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}