package com.farm.project.service;

import com.farm.project.domain.Field;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：属性服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface FieldServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Field insertFieldEntity(Field entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Field editFieldEntity(Field entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteFieldEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Field getFieldEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createFieldSimpleQuery(DataQuery query);

	/**
	 * 批量设置属性分类
	 * 
	 * @param fields
	 * @param typeid
	 * @param currentUser
	 */
	public void moveType(String fields, String typeid, LoginUser currentUser);

	/**
	 * 加载系统默认属性
	 * 
	 * @param currentUser
	 */
	public void loadSysField(String typeid, LoginUser currentUser);

	/**
	 * 查找项目属性
	 * 
	 * @param uuid
	 * @return
	 */
	public Field getFieldByUUID(String uuid);

	/**
	 * 修改排序号
	 * 
	 * @param id
	 * @param i
	 * @param currentUser
	 */
	public void editSort(String fieldid, int sort, LoginUser currentUser);

	/**
	 * 在任务中是否可以编辑一个属性
	 * 
	 * @param uuid
	 * @param taskId
	 * @return
	 */
	public boolean isEditByTask(String uuid, String taskId);

	/**
	 * 在任务中是否可以显示一个属性
	 * 
	 * @param uuid
	 * @param taskId
	 * @return
	 */
	public boolean isReadByTask(String uuid, String taskId);
}