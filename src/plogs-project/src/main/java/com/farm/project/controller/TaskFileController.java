package com.farm.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.FileBase;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.project.domain.TaskFile;
import com.farm.project.service.TaskServiceInter;
import com.farm.view.WdapViewServiceInter;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：任务附件控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/taskfile")
@Controller
public class TaskFileController extends WebUtils {
	private final static Logger log = Logger.getLogger(TaskFileController.class);
	@Resource
	private TaskServiceInter taskServiceImpl;
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(String taskid, DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("a.TASKID", taskid, "="));
			query.addSort(new DBSort("a.ctime", "asc"));
			DataResult result = taskServiceImpl.createTaskfileSimpleQuery(query).search();
			result.runformatTime("CTIME", "yyyy-MM-dd");
			result.runDictionary("1:任务描述,2:描述附件,3:任务日志,4:日志附件", "SOURCETYPE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(TaskFile entity, String filename, HttpSession session) {
		try {
			entity = taskServiceImpl.editTaskfileEntity(entity, getCurrentUser(session));
			FileBase file = fileBaseServiceImpl.getFilebaseEntity(entity.getFileid());
			file.setTitle(filename);
			fileBaseServiceImpl.editFilebaseEntity(file, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				taskServiceImpl.deleteTaskfileEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(String taskid, HttpSession session) {
		return ViewMode.getInstance().putAttr("taskid", taskid).returnModelAndView("project/TaskfileResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			// case (0): {// 查看
			// return ViewMode.getInstance().putAttr("pageset", pageset)
			// .putAttr("entity", taskServiceImpl.getTaskfileEntity(ids))
			// .returnModelAndView("project/TaskfileForm");
			// }
			// case (1): {// 新增
			// return ViewMode.getInstance().putAttr("pageset",
			// pageset).returnModelAndView("project/TaskfileForm");
			// }
			case (2): {// 修改
				TaskFile taskFile = taskServiceImpl.getTaskfileEntity(ids);
				FileBase file = wdapFileServiceImpl.getFileBase(taskFile.getFileid());
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("entity", taskFile)
						.putAttr("file", file).returnModelAndView("project/TaskfileForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/TaskfileForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("project/TaskfileForm");
		}
	}
}
