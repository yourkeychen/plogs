package com.farm.project.service.impl;

import com.farm.project.domain.UserView;
import com.farm.project.domain.Uvtask;
import com.farm.core.time.TimeTool;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.farm.project.dao.UserViewDaoInter;
import com.farm.project.dao.UvtaskDaoInter;
import com.farm.project.service.UserViewServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：用户视图服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class UserViewServiceImpl implements UserViewServiceInter {
	@Resource
	private UserViewDaoInter userviewDaoImpl;
	@Resource
	private UvtaskDaoInter uvtaskDaoImpl;
	private static final Logger log = Logger.getLogger(UserViewServiceImpl.class);

	@Override
	@Transactional
	public void deleteUserviewEntity(String id, LoginUser user) {
		// 先釋放任務
		uvtaskDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("VIEWID", id, "=")).toList());
		userviewDaoImpl.deleteEntity(userviewDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public UserView getUserviewEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return userviewDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createUserviewSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "PLOGS_USERVIEW",
				"ID,TYPE,SORT,USERID,VIEWMODEL,PCONTENT,PSTATE,CUSER,CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public UserView updateView(String viewid, String name, Integer sort, LoginUser currentUser) {
		UserView view = userviewDaoImpl.getEntity(viewid);
		view.setName(name);
		view.setSort(sort);
		userviewDaoImpl.editEntity(view);
		return view;
	}

	@Override
	@Transactional
	public UserView addView(String name, Integer sort, LoginUser currentUser) {
		if (userviewDaoImpl.countEntitys(DBRuleList.getInstance().add(new DBRule("TYPE", "1", "="))
				.add(new DBRule("USERID", currentUser.getId(), "=")).toList()) >= 7) {
			throw new RuntimeException("用戶最多創建7个自定义视图!");
		}
		UserView view = new UserView();
		view.setCtime(TimeTool.getTimeDate14());
		view.setCuser(currentUser.getId());
		view.setName(name);
		view.setPstate("1");
		view.setSort(sort);
		view.setType("1");
		view.setUserid(currentUser.getId());
		userviewDaoImpl.insertEntity(view);
		return view;
	}

	@Override
	@Transactional
	public List<UserView> getUserViews(LoginUser currentUser) {
		return userviewDaoImpl.selectEntitys(DBRuleList.getInstance()
				.add(new DBRule("USERID", currentUser.getId(), "=")).add(new DBRule("PSTATE", "1", "=")).toList());
	}

	@Override
	@Transactional
	public void toUserView(String taskId, String viewId, LoginUser currentUser) {
		// 取消視圖
		uvtaskDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("USERID", currentUser.getId(), "="))
				.add(new DBRule("TASKID", taskId, "=")).toList());
		if (StringUtils.isNotBlank(viewId)) {
			// 設置視圖
			Uvtask uvtask = new Uvtask();
			uvtask.setTaskid(taskId);
			uvtask.setViewid(viewId);
			uvtask.setUserid(currentUser.getId());
			uvtaskDaoImpl.insertEntity(uvtask);
		}
	}

}
