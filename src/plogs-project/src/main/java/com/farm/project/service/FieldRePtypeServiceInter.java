package com.farm.project.service;

import com.farm.project.domain.FieldRePtype;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目类型属性服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface FieldRePtypeServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public FieldRePtype insertFieldreptypeEntity(FieldRePtype entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public FieldRePtype editFieldreptypeEntity(FieldRePtype entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteFieldreptypeEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public FieldRePtype getFieldreptypeEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createFieldreptypeSimpleQuery(DataQuery query);

	/**
	 * 属性绑定到项目
	 * 
	 * @param projectTypeId
	 * @param fieldid
	 * @param currentUser
	 */
	public void bindProjectType(String projectTypeId, String fieldid, LoginUser currentUser);
}