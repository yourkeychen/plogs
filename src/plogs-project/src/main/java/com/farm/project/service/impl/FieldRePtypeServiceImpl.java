package com.farm.project.service.impl;

import com.farm.project.domain.FieldRePtype;
import org.apache.log4j.Logger;
import com.farm.project.dao.FieldRePtypeDaoInter;
import com.farm.project.service.FieldRePtypeServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目类型属性服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class FieldRePtypeServiceImpl implements FieldRePtypeServiceInter {
	@Resource
	private FieldRePtypeDaoInter fieldreptypeDaoImpl;

	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(FieldRePtypeServiceImpl.class);

	@Override
	@Transactional
	public FieldRePtype insertFieldreptypeEntity(FieldRePtype entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		entity.setSort(99);
		return fieldreptypeDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public FieldRePtype editFieldreptypeEntity(FieldRePtype entity, LoginUser user) {
		FieldRePtype entity2 = fieldreptypeDaoImpl.getEntity(entity.getId());
		entity2.setWriteis(entity.getWriteis());
		entity2.setReadis(entity.getReadis());
		entity2.setSort(entity.getSort());
		fieldreptypeDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteFieldreptypeEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		fieldreptypeDaoImpl.deleteEntity(fieldreptypeDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public FieldRePtype getFieldreptypeEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return fieldreptypeDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFieldreptypeSimpleQuery(DataQuery query) {
		DataQuery dbQuery = DataQuery.init(query,
				"PLOGS_FIELD_REPTYPE a left join PLOGS_FIELD b on a.FIELDID=b.ID left join PLOGS_PROJECT_TYPE c on a.PROJECTTYPEID=c.ID",
				"a.ID as ID,WRITEIS,READIS,b.NAME as FIELDNAME,c.NAME as PROJECTTYPENAME,a.SORT as SORT");
		return dbQuery;
	}

	@Override
	@Transactional
	public void bindProjectType(String projectTypeId, String fieldid, LoginUser currentUser) {
		fieldreptypeDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("PROJECTTYPEID", projectTypeId, "="))
				.add(new DBRule("FIELDID", fieldid, "=")).toList());
		FieldRePtype entity = new FieldRePtype();
		entity.setFieldid(fieldid);
		entity.setProjecttypeid(projectTypeId);
		entity.setReadis("1");
		entity.setWriteis("1");
		entity.setSort(99);
		fieldreptypeDaoImpl.insertEntity(entity);
	}

}
