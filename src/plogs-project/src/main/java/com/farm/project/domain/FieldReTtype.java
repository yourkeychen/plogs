package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：任务类型属性类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "FieldReTType")
@Table(name = "plogs_field_rettype")
public class FieldReTtype implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "WRITEIS", length = 1, nullable = false)
        private String writeis;
        @Column(name = "READIS", length = 1, nullable = false)
        private String readis;
        @Column(name = "FIELDID", length = 32, nullable = false)
        private String fieldid;
        @Column(name = "TASKTYPEID", length = 32, nullable = false)
        private String tasktypeid;

        public String  getWriteis() {
          return this.writeis;
        }
        public void setWriteis(String writeis) {
          this.writeis = writeis;
        }
        public String  getReadis() {
          return this.readis;
        }
        public void setReadis(String readis) {
          this.readis = readis;
        }
        public String  getFieldid() {
          return this.fieldid;
        }
        public void setFieldid(String fieldid) {
          this.fieldid = fieldid;
        }
        public String  getTasktypeid() {
          return this.tasktypeid;
        }
        public void setTasktypeid(String tasktypeid) {
          this.tasktypeid = tasktypeid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}