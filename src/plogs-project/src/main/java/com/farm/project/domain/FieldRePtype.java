package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：项目类型属性类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "FieldRePType")
@Table(name = "plogs_field_reptype")
public class FieldRePtype implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "WRITEIS", length = 1, nullable = false)
        private String writeis;
        @Column(name = "READIS", length = 1, nullable = false)
        private String readis;
        @Column(name = "FIELDID", length = 32, nullable = false)
        private String fieldid;
        @Column(name = "PROJECTTYPEID", length = 32, nullable = false)
        private String projecttypeid;
        @Column(name = "SORT", length = 10, nullable = false)
        private Integer sort;
        
        public Integer getSort() {
			return sort;
		}
		public void setSort(Integer sort) {
			this.sort = sort;
		}
		public String  getWriteis() {
          return this.writeis;
        }
        public void setWriteis(String writeis) {
          this.writeis = writeis;
        }
        public String  getReadis() {
          return this.readis;
        }
        public void setReadis(String readis) {
          this.readis = readis;
        }
        public String  getFieldid() {
          return this.fieldid;
        }
        public void setFieldid(String fieldid) {
          this.fieldid = fieldid;
        }
        public String  getProjecttypeid() {
          return this.projecttypeid;
        }
        public void setProjecttypeid(String projecttypeid) {
          this.projecttypeid = projecttypeid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}