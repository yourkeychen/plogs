package com.farm.project.controller;

import com.farm.project.domain.Task;
import com.farm.project.domain.Taskuser;
import com.farm.project.service.ProjectuserServiceInter;
import com.farm.project.service.TaskServiceInter;
import com.farm.project.service.TaskuserServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.authority.domain.User;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：任务干系人控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/taskuser")
@Controller
public class TaskuserController extends WebUtils {
	private final static Logger log = Logger.getLogger(TaskuserController.class);
	@Resource
	private TaskuserServiceInter taskUserServiceImpl;
	@Resource
	private ProjectuserServiceInter projectUserServiceImpl;
	@Resource
	private TaskServiceInter taskServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, String taskid, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("taskid", taskid, "="));
			DataResult result = taskUserServiceImpl.createTaskuserSimpleQuery(query).search();
			result.runDictionary("1:管理人,2:执行人,3:监视人", "POPTYPE");
			result.runformatTime("LIVETIME", "yyyy-MM-dd HH:mm");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 重置管理员
	 * 
	 * @param ids
	 * @param session
	 * @return
	 */
	@RequestMapping("/resetUser")
	@ResponseBody
	public Map<String, Object> resetUser(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				taskUserServiceImpl.resetTasktuser(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Taskuser entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = taskUserServiceImpl.editTaskuserEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Taskuser entity, HttpSession session) {
		try {
			taskUserServiceImpl.adduser(entity.getTaskid(), entity.getUserid(), entity.getPoptype(),
					getCurrentUser(session), true);
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				taskUserServiceImpl.deleteTaskuserEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(String taskid, HttpSession session) {
		return ViewMode.getInstance().putAttr("taskid", taskid).returnModelAndView("project/TaskuserResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String taskid, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", taskUserServiceImpl.getTaskuserEntity(ids))
						.returnModelAndView("project/TaskuserForm");
			}
			case (1): {// 新增
				Task task = taskServiceImpl.getTaskEntity(taskid);
				List<User> users = projectUserServiceImpl.getProjectUsers(task.getProjectid());
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("users", users)
						.putAttr("taskid", taskid).returnModelAndView("project/TaskuserForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", taskUserServiceImpl.getTaskuserEntity(ids))
						.returnModelAndView("project/TaskuserForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/TaskuserForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("project/TaskuserForm");
		}
	}
}
