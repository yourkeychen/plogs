package com.farm.project.dao;

import com.farm.project.domain.ProjectCompany;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;


/* *
 *功能：项目业务机构数据库持久层接口
 *详细：
 *
 *版本：v0.1
 *作者：Farm代码工程自动生成
 *日期：20150707114057
 *说明：
 */
public interface ProjectcompanyDaoInter  {
 /** 删除一个项目业务机构实体
 * @param entity 实体
 */
 public void deleteEntity(ProjectCompany projectcompany) ;
 /** 由项目业务机构id获得一个项目业务机构实体
 * @param id
 * @return
 */
 public ProjectCompany getEntity(String projectcompanyid) ;
 /** 插入一条项目业务机构数据
 * @param entity
 */
 public  ProjectCompany insertEntity(ProjectCompany projectcompany);
 /** 获得记录数量
 * @return
 */
 public int getAllListNum();
 /**修改一个项目业务机构记录
 * @param entity
 */
 public void editEntity(ProjectCompany projectcompany);
 /**获得一个session
 */
 public Session getSession();
 /**执行一条项目业务机构查询语句
 */
 public DataResult runSqlQuery(DataQuery query);
 /**
 * 条件删除项目业务机构实体，依据对象字段值(一般不建议使用该方法)
 * 
 * @param rules
 *            删除条件
 */
 public void deleteEntitys(List<DBRule> rules);

 /**
 * 条件查询项目业务机构实体，依据对象字段值,当rules为空时查询全部(一般不建议使用该方法)
 * 
 * @param rules
 *            查询条件
 * @return
 */
 public List<ProjectCompany> selectEntitys(List<DBRule> rules);

 /**
 * 条件修改项目业务机构实体，依据对象字段值(一般不建议使用该方法)
 * 
 * @param values
 *            被修改的键值对
 * @param rules
 *            修改条件
 */
 public void updataEntitys(Map<String, Object> values, List<DBRule> rules);
 /**
 * 条件合计项目业务机构:count(*)
 * 
 * @param rules
 *            统计条件
 */
 public int countEntitys(List<DBRule> rules);
}