package com.farm.project.service.impl;

import com.farm.project.domain.ProjectCompany;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;
import com.farm.project.dao.ProjectcompanyDaoInter;
import com.farm.project.service.ProjectcompanyServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目业务机构服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class ProjectcompanyServiceImpl implements ProjectcompanyServiceInter {
	@Resource
	private ProjectcompanyDaoInter projectcompanyDaoImpl;

	private static final Logger log = Logger.getLogger(ProjectcompanyServiceImpl.class);

	@Override
	@Transactional
	public ProjectCompany insertProjectcompanyEntity(ProjectCompany entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return projectcompanyDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public ProjectCompany editProjectcompanyEntity(ProjectCompany entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		ProjectCompany entity2 = projectcompanyDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setProjectid(entity.getProjectid());
		entity2.setCompanyid(entity.getCompanyid());
		entity2.setId(entity.getId());
		projectcompanyDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteProjectcompanyEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		projectcompanyDaoImpl.deleteEntity(projectcompanyDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public ProjectCompany getProjectcompanyEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return projectcompanyDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createProjectcompanySimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "PLOGS_PROJECT_RE_COMPANY", "ID,PROJECTID,COMPANYID");
		return dbQuery;
	}

	@Override
	@Transactional
	public void bindProjectCompany(String projectid, String companyid) {
		projectcompanyDaoImpl
				.deleteEntitys(DBRuleList.getInstance().add(new DBRule("PROJECTID", projectid, "=")).toList());
		ProjectCompany pc = new ProjectCompany();
		pc.setCompanyid(companyid);
		pc.setProjectid(projectid);
		projectcompanyDaoImpl.insertEntity(pc);
	}

}
