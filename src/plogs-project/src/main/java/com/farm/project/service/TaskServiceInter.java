package com.farm.project.service;

import com.farm.project.domain.Task;
import com.farm.project.domain.TaskFile;
import com.farm.project.domain.TaskLog;
import com.farm.project.service.utils.TaskFileIndexInfo;
import com.farm.core.sql.query.DataQuery;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：任务服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface TaskServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	@Deprecated
	public Task insertTaskEntity(Task entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Task editTaskEntity(Task entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteTaskEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Task getTaskEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createTaskSimpleQuery(DataQuery query);

	/**
	 * 获得用户当前任务
	 * 
	 * @param currentUser
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getTasks(LoginUser currentUser) throws SQLException;

	/**
	 * 更新任務狀態
	 * 
	 * @param taskId      任務id
	 * @param state       任務狀態
	 * @param currentUser
	 */
	public void editTaskState(String taskId, String state, LoginUser currentUser);

	/**
	 * 关闭任务
	 * 
	 * @param taskId
	 * @param currentUser
	 */
	public void closeTask(String taskId, LoginUser currentUser);

	/**
	 * 创建任务
	 * 
	 * @param title
	 * @param projectid
	 * @param gradationid
	 * @param tasktypeid
	 * @param state
	 * @param text
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Task addTask(String title, String projectid, String gradationid, String tasktypeid, String state,
			String planetime, String text, LoginUser currentUser) throws IOException;

	/**
	 * 读取富文本内容
	 * 
	 * @param readfileid
	 * @return
	 */
	public String readText(String fileid);

	/**
	 * 更新任务
	 * 
	 * @param taskid
	 * @param title
	 * @param projectid
	 * @param gradationid
	 * @param tasktypeid
	 * @param state
	 * @param text
	 * @param currentUser
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public Task updateTask(String taskid, String title, String projectid, String gradationid, String tasktypeid,
			String state, String planetime, String text, LoginUser currentUser) throws IOException, SQLException;

	/**
	 * 保存任务日志
	 * 
	 * @param taskid
	 * @param text
	 * @param currentUser
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public TaskLog saveLog(String taskid, String text, LoginUser currentUser) throws IOException, SQLException;

	/**
	 * 获得任务日志
	 * 
	 * @param id
	 * @return
	 */
	public String getlog(String taskid);

	/**
	 * 获得任务的当日日志
	 * 
	 * @param taskid
	 * @return
	 */
	public TaskLog getTodayLog(String taskid);

	/**
	 * 獲得任务列表
	 * 
	 * @param currentUser
	 * @param state       如果有多種状态用逗号分隔，如"1,2,3,4"
	 * @param title       项目名称(模糊查询) 可以为空
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getTasks(LoginUser currentUser, String state, String projectname)
			throws SQLException;

	/**
	 * 计算任务数量
	 * 
	 * @param currentUser
	 * @param state
	 * @return
	 */
	public int getTaskCount(LoginUser currentUser, String state);

	/**
	 * 获得一个任务的所有日志
	 * 
	 * @param taskId
	 * @return
	 */
	public List<TaskLog> getLogs(String taskId);

	/**
	 * 任务附件查询
	 * 
	 * @param query
	 * @return
	 */
	public DataQuery createTaskfileSimpleQuery(DataQuery query);

	/**
	 * 修改任务附件信息
	 * 
	 * @param entity
	 * @param user
	 * @return
	 */
	public TaskFile editTaskfileEntity(TaskFile entity, LoginUser user);

	/**
	 * 刪除任務附件
	 * 
	 * @param id
	 * @param user
	 */
	public void deleteTaskfileEntity(String id, LoginUser user);

	/**
	 * 获得任务信息
	 * 
	 * @param id
	 * @return
	 */
	public TaskFile getTaskfileEntity(String id);

	/**
	 * 加载任务属性
	 * 
	 * @param taskid
	 * @param currentUser
	 */
	public void loadTaskField(String taskid, LoginUser currentUser);

	/**
	 * 获得任务的附件信息
	 * 
	 * @param taskid
	 * @return
	 */
	public List<TaskFile> getTaskfiles(String taskid);

	/**
	 * 获得用来做索引的封装对象
	 * 
	 * @param fileId
	 * @return
	 */
	public TaskFileIndexInfo getIndexInfo(String taskfileid);

	/**
	 * 获得用来做索引的封装对象(包含某附件的任務附件)
	 * 
	 * @param fileid
	 * @return
	 */
	public List<TaskFileIndexInfo> getIndexInfos(String fileid);

	/**
	 * 任务查询
	 * 
	 * @return
	 */
	public DataQuery getTaskQuery(String stime, String etime);

	public TaskLog getLogById(String tasklogid);
}