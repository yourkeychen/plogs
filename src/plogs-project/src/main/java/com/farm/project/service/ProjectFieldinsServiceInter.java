package com.farm.project.service;

import com.farm.project.domain.Field;
import com.farm.project.domain.ProjectFieldins;
import com.farm.project.domain.ex.ProjectField;
import com.farm.core.sql.query.DataQuery;

import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目属性服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ProjectFieldinsServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ProjectFieldins insertProjectfieldinsEntity(ProjectFieldins entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ProjectFieldins editProjectfieldinsEntity(ProjectFieldins entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteProjectfieldinsEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ProjectFieldins getProjectfieldinsEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createProjectfieldinsSimpleQuery(DataQuery query);

	/**
	 * 加载系统默认项目属性
	 * 
	 * @param projectid
	 * @param currentUser
	 */
	public void loadSysField(String projectid, LoginUser currentUser);

	/**
	 * 获得任务属性(任務中編輯屬性時的展示)
	 * 
	 * @param taskId
	 * @return
	 */
	public List<ProjectField> getTaskFields(String taskId);

	/**
	 * 修改属性值
	 * 
	 * @param fieldinsId
	 * @param value
	 * @param currentUser
	 * @return
	 */
	public ProjectFieldins editVal(String fieldinsId, String value, LoginUser currentUser);

	/**
	 * 獲得項目的自定義屬性
	 * 
	 * @param projectId
	 * @return <UUID, TITLE>
	 */
	public Map<String, Object> getProjectFieldDic(String projectId);

	/**
	 * 獲得項目屬性集(全部屬性)用戶項目信息展示時的屬性展示
	 * 
	 * @param projectId
	 * @return
	 */
	public List<ProjectFieldins> getProjectFields(String projectId);

	/**
	 * 获得格式化的枚举项
	 * 
	 * @param enums
	 * @return map:val/title
	 */
	public List<Map<String, String>> getFormatEnums(String enums);

	/**
	 * 获得项目属性实例(單個)
	 * 
	 * @param uuid
	 * @param projectid
	 * @return
	 */
	public ProjectFieldins getProjectFieldins(String uuid, String projectid);

	/**
	 * 获得项目属性实例(多個)
	 * 
	 * @param uuid
	 * @param projectid
	 * @return
	 */
	public List<ProjectFieldins> getProjectFieldines(String uuid, String projectid);

	/**
	 * 添加一個项目属性实例(taskid和projectid必须有其中一个参数)
	 * 
	 * @param field
	 * @param taskid
	 * @param projectid
	 * @param currentUser
	 * @return
	 */
	public ProjectFieldins addFildins(Field field, String taskid, String projectid, LoginUser currentUser);

	/**
	 * 刪除項目屬性（一個項目中如果有多条，就删除一条，如果就一条就清空值）
	 * 
	 * @param fieldInsId
	 * @param projectid
	 * @param currentUser
	 */
	public void deleteProjectfieldinsFromProject(String fieldInsId, String projectid, LoginUser currentUser);

	/**
	 * 获得项目属性(編輯屬性時的展示)
	 * 
	 * @param projectId
	 * @return
	 */
	public List<ProjectField> getAllProjectFields(String projectId, String taskId);
}