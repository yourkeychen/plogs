package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：任务干系人类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "TaskUser")
@Table(name = "plogs_task_user")
public class Taskuser implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "LIVETIME", length = 16, nullable = false)
        private String livetime;
        @Column(name = "POPTYPE", length = 1, nullable = false)
        private String poptype;
        @Column(name = "TASKID", length = 32, nullable = false)
        private String taskid;
        @Column(name = "USERID", length = 32, nullable = false)
        private String userid;
        @Column(name = "PSTATE", length = 2, nullable = false)
        private String pstate;
        @Column(name = "PROJECTID", length = 32, nullable = false)
        private String projectid;
        @Column(name = "PCONTENT", length = 128)
        private String pcontent;
        @Column(name = "CUSER", length = 32, nullable = false)
        private String cuser;
        @Column(name = "CTIME", length = 16, nullable = false)
        private String ctime;

        public String  getLivetime() {
          return this.livetime;
        }
        public void setLivetime(String livetime) {
          this.livetime = livetime;
        }
        public String  getPoptype() {
          return this.poptype;
        }
        public void setPoptype(String poptype) {
          this.poptype = poptype;
        }
        public String  getTaskid() {
          return this.taskid;
        }
        public void setTaskid(String taskid) {
          this.taskid = taskid;
        }
        public String  getUserid() {
          return this.userid;
        }
        public void setUserid(String userid) {
          this.userid = userid;
        }
        public String  getPstate() {
          return this.pstate;
        }
        public void setPstate(String pstate) {
          this.pstate = pstate;
        }
        public String  getProjectid() {
          return this.projectid;
        }
        public void setProjectid(String projectid) {
          this.projectid = projectid;
        }
        public String  getPcontent() {
          return this.pcontent;
        }
        public void setPcontent(String pcontent) {
          this.pcontent = pcontent;
        }
        public String  getCuser() {
          return this.cuser;
        }
        public void setCuser(String cuser) {
          this.cuser = cuser;
        }
        public String  getCtime() {
          return this.ctime;
        }
        public void setCtime(String ctime) {
          this.ctime = ctime;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}