package com.farm.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.lucene.common.IndexTaskDomain;
import com.farm.project.service.TaskServiceInter;
import com.farm.project.service.utils.IndexUtils;
import com.farm.view.WdapViewServiceInter;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：任务附件索引功能
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/taskfindex")
@Controller
public class TaskFileLuceneController extends WebUtils {
	private final static Logger log = Logger.getLogger(TaskFileLuceneController.class);
	@Resource
	private TaskServiceInter taskServiceImpl;
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	private String taskKey;

	@RequestMapping("/list")
	public ModelAndView list(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/TaskFileIndexResult");
	}

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> query(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DataResult result = getFilesQuery(query).search();
			result.runformatTime("CTIME", "yyyy-MM-dd HH:mm");
			result.runDictionary("1:任务描述,2:描述附件,3:任务日志,4:日志附件", "SOURCETYPE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	private DataQuery getFilesQuery(DataQuery query) {
		query.addSort(new DBSort("ctime", "desc"));
		DataQuery dbQuery = DataQuery.init(query,
				"(select a.ID as ID,EXNAME,LEFT(E.FILETEXT,100) as FILETEXT,a.FILEID as FILEID,c.TITLE AS FILENAME,a.SOURCETYPE as SOURCETYPE, a.CTIME AS CTIME,b.id as TASKID, b.TITLE AS TASKNAME,d.NAME as PROJECTNAME from  PLOGS_TASK_FILE a LEFT JOIN plogs_task b ON b.id = a.TASKID LEFT JOIN WDAP_FILE c ON c.id = a.FILEID left join PLOGS_PROJECT d on b.PROJECTID=d.id left join WDAP_FILE_TEXT e on e.FILEID=c.id) tables",
				"ID,EXNAME,FILETEXT,FILEID,FILENAME,SOURCETYPE,CTIME,TASKID,TASKNAME,PROJECTNAME");
		return dbQuery;
	}

	/**
	 * 重做索引
	 * 
	 * @return
	 */
	@RequestMapping("/reIndex")
	@ResponseBody
	public Map<String, Object> reIndex(DataQuery query, HttpServletRequest request) {
		try {
			IndexTaskDomain state = IndexUtils.getTaskState(taskKey);
			if (state.getState() == 3 || state.getState() == -1) {
				state = IndexUtils.startReKnowIndexTaskBy(new Date().toString(), getFilesQuery(query), taskServiceImpl);
				taskKey = state.getTaskKey();
			}
			return ViewMode.getInstance().putAttr("task", state).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 获取当前索引状态
	 * 
	 * @return
	 */
	@RequestMapping("/indexPersont")
	@ResponseBody
	public Map<String, Object> loadIndexStatr() {
		return ViewMode.getInstance().putAttr("task", IndexUtils.getTaskState(taskKey)).returnObjMode();
	}

}
