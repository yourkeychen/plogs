package com.farm.project.controller;

import com.farm.project.domain.ProjectType;
import com.farm.project.service.ProjectTypeServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;

import com.farm.web.easyui.EasyUiTreeNode;
import com.farm.web.easyui.EasyUiTreeNodeHandle;
import com.farm.web.easyui.EasyUiUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：项目分类控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/projecttype")
@Controller
public class ProjectTypeController extends WebUtils {
	private final static Logger log = Logger.getLogger(ProjectTypeController.class);
	@Resource
	private ProjectTypeServiceInter projectTypeServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		try {
			if (query.getQueryRule().size() == 0) {
				query.addRule(new DBRule("PARENTID", "NONE", "="));
			}
			query = EasyUiUtils.formatGridQuery(request, query);
			query.setDefaultSort(new DBSort("SORT", "ASC"));
			DataResult result = projectTypeServiceImpl.createProjecttypeSimpleQuery(query).search();
			result.runDictionary("0:树型分类,1:项目类型", "MODEL");
			result.runDictionary("0:停用,1:可用", "PSTATE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/queryReGradations")
	@ResponseBody
	public Map<String, Object> queryReGradations(DataQuery query, String typeid, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("a.PROJECTTYPEID", typeid, "="));
			query.addDefaultSort(new DBSort("a.SORT", "ASC"));
			DataResult result = projectTypeServiceImpl.createReGradationsQuery(query, typeid).search();
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 加载树形节点
	 */
	@RequestMapping("/projectTypeTree")
	@ResponseBody
	public Object projectTypeTree(String id) {
		try {
			List<EasyUiTreeNode> allnodes = null;
			if (id == null) {
				// 如果是未传入id，就是根节点，就构造一个虚拟的上级节点
				id = "NONE";
				List<EasyUiTreeNode> list = new ArrayList<>();
				EasyUiTreeNode nodes = new EasyUiTreeNode("NONE", "项目分类", "open", "icon-project");
				nodes.setChildren(
						EasyUiTreeNode.formatAsyncAjaxTree(
								EasyUiTreeNode.queryTreeNodeOne(id, "SORT", "PLOGS_PROJECT_TYPE", "ID", "PARENTID",
										"NAME", "CTIME","and a.PSTATE='1'").getResultList(),
						EasyUiTreeNode
								.queryTreeNodeTow(id, "SORT", "PLOGS_PROJECT_TYPE", "ID", "PARENTID", "NAME", "CTIME","and a.PSTATE='1'")
								.getResultList(), "PARENTID", "ID", "NAME", "CTIME"));
				list.add(nodes);
				allnodes = list;
			} else {
				allnodes = EasyUiTreeNode
						.formatAsyncAjaxTree(
								EasyUiTreeNode.queryTreeNodeOne(id, "SORT", "PLOGS_PROJECT_TYPE", "ID", "PARENTID",
										"NAME", "CTIME","and a.PSTATE='1'").getResultList(),
						EasyUiTreeNode
								.queryTreeNodeTow(id, "SORT", "PLOGS_PROJECT_TYPE", "ID", "PARENTID", "NAME", "CTIME","and a.PSTATE='1'")
								.getResultList(), "PARENTID", "ID", "NAME", "CTIME");
			}
			EasyUiUtils.runTreeNodeHandle(allnodes, new EasyUiTreeNodeHandle() {
				@Override
				public void handle(EasyUiTreeNode node) {
					ProjectType type = projectTypeServiceImpl.getProjecttypeEntity(node.getId());
					if (type != null && type.getModel().equals("1")) {
						node.setIconCls("icon-consulting");
					}
				}
			});
			return allnodes;
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(ProjectType entity, HttpSession session) {
		try {
			entity = projectTypeServiceImpl.editProjecttypeEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(ProjectType entity, HttpSession session) {
		try {
			entity = projectTypeServiceImpl.insertProjecttypeEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 项目阶段绑定页面
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/gradations")
	public ModelAndView tasyTypes(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/ProjectTypeToGradation");
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				projectTypeServiceImpl.deleteProjecttypeEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 上移记录排序
	 * 
	 * @param ids
	 * @param session
	 * @return
	 */
	@RequestMapping("/sortToUp")
	@ResponseBody
	public Map<String, Object> sortToUp(String ids, HttpSession session) {
		try {
			projectTypeServiceImpl.toUpProjectTypeGradationSort(ids, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/ProjectTypeResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids, String parentId) {
		try {

			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", projectTypeServiceImpl.getProjecttypeEntity(ids))
						.returnModelAndView("project/ProjectTypeForm");
			}
			case (1): {// 新增
				ProjectType parent = null;
				if (StringUtils.isNotBlank(parentId)) {
					parent = projectTypeServiceImpl.getProjecttypeEntity(parentId);
				}
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("parent", parent)
						.returnModelAndView("project/ProjectTypeForm");
			}
			case (2): {// 修改
				ProjectType type = projectTypeServiceImpl.getProjecttypeEntity(ids);
				ProjectType parent = null;
				if (StringUtils.isNotBlank(type.getParentid())) {
					parent = projectTypeServiceImpl.getProjecttypeEntity(type.getParentid());
				}
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("parent", parent)
						.putAttr("entity", projectTypeServiceImpl.getProjecttypeEntity(ids))
						.returnModelAndView("project/ProjectTypeForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/ProjectTypeForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("project/ProjectTypeForm");
		}
	}

	/**
	 * 跳转树选择页面
	 * 
	 * @return
	 */
	@RequestMapping("/treeNodeTreeView")
	public ModelAndView forSend() {
		return ViewMode.getInstance().returnModelAndView("project/ChooseProjectTypeTree");
	}

	/**
	 * 移动节点
	 * 
	 * @return
	 */
	@RequestMapping("/moveTreeNodeSubmit")
	@ResponseBody
	public Map<String, Object> moveTreeNodeSubmit(String ids, String id, HttpSession session) {
		try {
			projectTypeServiceImpl.moveTreeNode(ids, id, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 绑定阶段到任务分类
	 * 
	 * @return
	 */
	@RequestMapping("/bindGradation")
	@ResponseBody
	public Map<String, Object> bindTaskType(String gradationids, String typeid, HttpSession session) {
		try {
			for (String gradationid : parseIds(gradationids)) {
				projectTypeServiceImpl.bindGradation(gradationid, typeid, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 解除绑定阶段到任务分类
	 * 
	 * @return
	 */
	@RequestMapping("/rebindGradation")
	@ResponseBody
	public Map<String, Object> rebindTaskType(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				projectTypeServiceImpl.rebindGradation(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
}
