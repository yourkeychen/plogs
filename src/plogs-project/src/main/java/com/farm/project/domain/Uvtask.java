package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：视图任务类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "UvTask")
@Table(name = "plogs_re_viewtask")
public class Uvtask implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "VIEWID", length = 32, nullable = false)
        private String viewid;
        @Column(name = "TASKID", length = 32, nullable = false)
        private String taskid;
        @Column(name = "USERID", length = 32, nullable = false)
        private String userid;
        
        public String getUserid() {
			return userid;
		}
		public void setUserid(String userid) {
			this.userid = userid;
		}
		public String  getViewid() {
          return this.viewid;
        }
        public void setViewid(String viewid) {
          this.viewid = viewid;
        }
        public String  getTaskid() {
          return this.taskid;
        }
        public void setTaskid(String taskid) {
          this.taskid = taskid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}