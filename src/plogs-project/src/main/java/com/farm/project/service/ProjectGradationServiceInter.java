package com.farm.project.service;

import com.farm.project.domain.ProjectGradation;
import com.farm.project.domain.TaskType;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目阶段服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ProjectGradationServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public ProjectGradation insertProjectgradationEntity(ProjectGradation entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public ProjectGradation editProjectgradationEntity(ProjectGradation entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteProjectgradationEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public ProjectGradation getProjectgradationEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createProjectgradationSimpleQuery(DataQuery query);

	/**
	 * 修改排序号
	 * 
	 * @param id
	 * @param currentUser
	 */
	public void editSort(String id, int sort, LoginUser currentUser);

	/**
	 * 查詢關聯任务分类
	 * 
	 * @param query
	 * @param gradationId
	 * @return
	 */
	public DataQuery createReTaskTypeQuery(DataQuery query, String gradationId);

	/**
	 * 綁定階段到任务分类
	 * 
	 * @param tasktypeid
	 * @param gradationid
	 * @param currentUser
	 */
	public void bindTaskType(String tasktypeid, String gradationid, LoginUser currentUser);

	/**
	 * 解除阶段和任务分类得绑定关系
	 * 
	 * @param id
	 * @param currentUser
	 */
	public void rebindTaskType(String id, LoginUser currentUser);

	/**
	 * 选择阶段时得查询语句
	 * 
	 * @param query
	 * @return
	 */
	public DataQuery createGradationChooseQuery(DataQuery query);

	/**
	 * 移动树节点
	 * 
	 * @param ids
	 * @param id
	 * @param currentUser
	 */
	public void moveTreeNode(String ids, String targetId, LoginUser currentUser);

	/**
	 * 獲得項目類型可以用的階段列表
	 * 
	 * @param projectTypeid
	 * @return
	 */
	public List<ProjectGradation> getGradationsByTypeid(String projectTypeid);

	/**获得任务类型
	 * @param id
	 * @return
	 */
	public List<TaskType> getTaskTypes(String gradationid);
}