package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：项目业务机构类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "ProjectCompany")
@Table(name = "plogs_project_re_company")
public class ProjectCompany implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "PROJECTID", length = 32, nullable = false)
        private String projectid;
        @Column(name = "COMPANYID", length = 32, nullable = false)
        private String companyid;

        public String  getProjectid() {
          return this.projectid;
        }
        public void setProjectid(String projectid) {
          this.projectid = projectid;
        }
        public String  getCompanyid() {
          return this.companyid;
        }
        public void setCompanyid(String companyid) {
          this.companyid = companyid;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}