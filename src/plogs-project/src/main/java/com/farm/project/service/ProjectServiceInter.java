package com.farm.project.service;

import com.farm.project.domain.Project;
import com.farm.project.domain.ProjectGradation;
import com.farm.project.domain.Task;
import com.farm.project.domain.TaskFile;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ProjectServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Project insertProjectEntity(Project entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Project editProjectEntity(Project entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteProjectEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Project getProjectEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createProjectSimpleQuery(DataQuery query);

	/**
	 * 修改项目分类
	 * 
	 * @param projectId
	 * @param typeId
	 * @param currentUser
	 */
	public void editType(String projectId, String typeId, LoginUser currentUser);

	/**
	 * 获得项目的当前阶段
	 * 
	 * @param projectId
	 * @return
	 */
	public ProjectGradation getGradation(String projectId);

	/**
	 * 获得活跃状态的常用项目
	 * 
	 * @param currentUser
	 * @param name
	 *            部分项目名称（模糊查询）可为空
	 * @return
	 * @throws SQLException
	 */
	public List<Project> getliveProjects(LoginUser currentUser, String name) throws SQLException;

	/**
	 * 修改项目阶段
	 * 
	 * @param projectid
	 * @param projectid2
	 */
	public void editProjectGradation(String projectid, String gradationId, LoginUser currentUser);

	/**
	 * 更新项目活跃时间
	 * 
	 * @param projectid
	 */
	public void refreshLiveTime(String projectid);

	/**
	 * 获得用于视图展示的项目列表(用于前台项目信息查询栏目)
	 * 
	 * @param page
	 * @param pagesize
	 * @param currentUser
	 * @return
	 * @throws SQLException
	 */
	public DataResult getProjectsForView(DataQuery dbQuery, DBSort fieldSort, Integer page, Integer pagesize,
			LoginUser currentUser) throws SQLException;

	/**
	 * 用于前台项目管理（任务工作台中点击项目管理）
	 * 
	 * @param query
	 * @param currentUser
	 * @return
	 */
	public DataResult getProjectsForManager(DataQuery query, LoginUser currentUser);

	/**
	 * 获得项目已用阶段
	 * 
	 * @param projectId
	 * @return
	 */
	public List<ProjectGradation> getProjectHasGradation(String projectId);

	/**
	 * 獲得項目任務
	 * 
	 * @param projectId
	 * @return
	 */
	public List<Task> getProjectTasks(String projectId);

	/**
	 * 获得项目任务文件
	 * 
	 * @param projectId
	 * @return ID,TASKID,FILEID,CTIME,FIELDID,FIELDNAME,LOGID,FILETITLE，EXNAME,
	 *         SECRET
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getProjectTaskFiles(String projectId) throws SQLException;

	/**
	 * 获得项目的所有任务类型
	 * 
	 * @param projectId
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getProjectTaskTypes(String projectId) throws SQLException;

	/**
	 * 获得活跃项目
	 * 
	 * @param currentUser
	 *            当前用户
	 * @param stime_yyyymmdd
	 *            起始时间范围
	 * @param etime_yyyymmdd
	 *            终止时间范围
	 * @return (借用PCONTENT字段來存儲任务数量)
	 * @throws SQLException
	 */
	public List<Project> getliveProjects(LoginUser currentUser, String stime_yyyymmdd, String etime_yyyymmdd)
			throws SQLException;

	/**
	 * 查询某时间段内的任务（所有参数均可为空）
	 * 
	 * @param projectId
	 * @param stime
	 *            etime 同时存在才有意义
	 * @param etime
	 * @return
	 * @throws SQLException
	 */
	public List<Task> getProjectTasks(String projectId, String stime_yyyymmdd, String etime_yyyymmdd,
			LoginUser currentUser) throws SQLException;

	/**
	 * 查询某时间段内的任务附件（所有参数均可为空）
	 * 
	 * @param projectId
	 * @param stime
	 *            etime 同时存在才有意义
	 * @param etime
	 * @param currentUser
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getProjectTaskFiles(String projectId, String stime_yyyymmdd, String etime_yyyymmdd,
			LoginUser currentUser) throws SQLException;

	/**
	 * 由存在的任务过滤掉没有用到的任务分类
	 * 
	 * @param tasks
	 * @param projectTaskTypes
	 * @return
	 */
	public List<Map<String, Object>> filterTaskTypesByTasks(List<Task> tasks,
			List<Map<String, Object>> projectTaskTypes);

	/**
	 * 由存在的任务过滤掉没有用到的项目阶段
	 * 
	 * @param tasks
	 * @param projectHasGradation
	 * @return
	 */
	public List<ProjectGradation> filterGradationByTasks(List<Task> tasks, List<ProjectGradation> projectHasGradation);

	/**
	 * 获得所查询的用户任务数量信息
	 * 
	 * @param userids
	 * @param string
	 * @param string2
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getUserTaskNums(List<String> userids, String stime_yyyymmdd, String etime_yyyymmdd)
			throws SQLException;

	/**
	 * 项目是否有存活任务（未删除或关闭的任务）
	 * 
	 * @param projectId
	 */
	public boolean hasLiveTask(String projectId);

	/**
	 * 更新项目状态
	 * 
	 * @param projectid
	 * @param state
	 * @param currentUser
	 */
	public void editProjectState(String projectid, String state, LoginUser currentUser);

	/**
	 * 获得业务机构的所有项目
	 * 
	 * @param companyid
	 * @return
	 */
	public List<Project> getCompanyProjects(String companyid);

}