package com.farm.project.service;

import com.farm.project.domain.TaskLog;
import com.farm.project.domain.TaskType;
import com.farm.core.sql.query.DataQuery;

import java.io.IOException;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：任务类型服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface TaskTypeServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public TaskType insertTasktypeEntity(TaskType entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public TaskType editTasktypeEntity(TaskType entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteTasktypeEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public TaskType getTasktypeEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createTasktypeSimpleQuery(DataQuery query);

	/** 修改排序
	 * @param id
	 * @param i
	 * @param currentUser
	 */
	public void editSort(String id, int sort, LoginUser currentUser);

	/**移动节点
	 * @param ids
	 * @param id
	 * @param currentUser
	 */
	public void moveTreeNode(String ids, String targetId, LoginUser currentUser);

	/**保持任務類型模板
	 * @param taskTypeid
	 * @param text
	 * @param currentUser
	 * @return
	 * @throws IOException 
	 */
	public void saveTemplete(String taskTypeid, String text, LoginUser currentUser) throws IOException;

	/**讀取模板
	 * @param taskTypeid
	 * @return
	 */
	public String readTempleteHtml(String taskTypeid);

	/**清空模板
	 * @param taskTypeid
	 * @param currentUser
	 */
	public void clearTemplete(String taskTypeid, LoginUser currentUser);
}