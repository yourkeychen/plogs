package com.farm.project.controller;

import com.farm.project.domain.ProjectFieldins;
import com.farm.project.service.ProjectFieldinsServiceInter;
import com.farm.project.service.ProjectServiceInter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：项目属性控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/projectfieldins")
@Controller
public class ProjectfieldinsController extends WebUtils {
	private final static Logger log = Logger.getLogger(ProjectfieldinsController.class);
	@Resource
	private ProjectFieldinsServiceInter projectFieldinsServiceImpl;
	@Resource
	private ProjectServiceInter projectServiceImpl;
	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query,String projectid, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			if(StringUtils.isNotBlank(projectid)){
				query.addRule(new DBRule("PROJECTID", projectid, "="));
			}
			DataResult result = projectFieldinsServiceImpl.createProjectfieldinsSimpleQuery(query).search();
			result.runDictionary("0:默认隐藏,1:公开信息", "PUBREAD");
			result.runDictionary("1:唯一,2:多条", "SINGLETYPE");
			result.runDictionary("1:可用,0:禁用", "PSTATE");
			result.runDictionary("1:富文本,2:附件,3:多行文本,4:单行文本,5:枚举单选,6:枚举多选,7:公司,8:人员,9:时间,10:日期", "VALTYPE");
			result.runformatTime("ETIME", "yyyy-MM-dd HH:mm:ss");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(ProjectFieldins entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = projectFieldinsServiceImpl.editProjectfieldinsEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(ProjectFieldins entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = projectFieldinsServiceImpl.insertProjectfieldinsEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				projectFieldinsServiceImpl.deleteProjectfieldinsEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 加载系统属性
	 * 
	 * @return
	 */
	@RequestMapping("/loadSysFields")
	@ResponseBody
	public Map<String, Object> loadSysFields(String projectid, HttpSession session) {
		try {
			projectFieldinsServiceImpl.loadSysField(projectid, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(String projectid, HttpSession session) {
		return ViewMode.getInstance().putAttr("projectid", projectid)
				.returnModelAndView("project/ProjectfieldinsResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids, String projectid) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", projectFieldinsServiceImpl.getProjectfieldinsEntity(ids))
						.returnModelAndView("project/ProjectfieldinsForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("projectid", projectid)
						.returnModelAndView("project/ProjectfieldinsForm");
			}
			case (2): {// 修改
				ProjectFieldins fieldins = projectFieldinsServiceImpl.getProjectfieldinsEntity(ids);
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("entity", fieldins)
						.putAttr("projectid", fieldins.getProjectid())
						.returnModelAndView("project/ProjectfieldinsForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/ProjectfieldinsForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e)
					.returnModelAndView("project/ProjectfieldinsForm");
		}
	}
}
