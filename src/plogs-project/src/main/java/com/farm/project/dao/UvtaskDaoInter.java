package com.farm.project.dao;

import com.farm.project.domain.Uvtask;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;


/* *
 *功能：视图任务数据库持久层接口
 *详细：
 *
 *版本：v0.1
 *作者：Farm代码工程自动生成
 *日期：20150707114057
 *说明：
 */
public interface UvtaskDaoInter  {
 /** 删除一个视图任务实体
 * @param entity 实体
 */
 public void deleteEntity(Uvtask uvtask) ;
 /** 由视图任务id获得一个视图任务实体
 * @param id
 * @return
 */
 public Uvtask getEntity(String uvtaskid) ;
 /** 插入一条视图任务数据
 * @param entity
 */
 public  Uvtask insertEntity(Uvtask uvtask);
 /** 获得记录数量
 * @return
 */
 public int getAllListNum();
 /**修改一个视图任务记录
 * @param entity
 */
 public void editEntity(Uvtask uvtask);
 /**获得一个session
 */
 public Session getSession();
 /**执行一条视图任务查询语句
 */
 public DataResult runSqlQuery(DataQuery query);
 /**
 * 条件删除视图任务实体，依据对象字段值(一般不建议使用该方法)
 * 
 * @param rules
 *            删除条件
 */
 public void deleteEntitys(List<DBRule> rules);

 /**
 * 条件查询视图任务实体，依据对象字段值,当rules为空时查询全部(一般不建议使用该方法)
 * 
 * @param rules
 *            查询条件
 * @return
 */
 public List<Uvtask> selectEntitys(List<DBRule> rules);

 /**
 * 条件修改视图任务实体，依据对象字段值(一般不建议使用该方法)
 * 
 * @param values
 *            被修改的键值对
 * @param rules
 *            修改条件
 */
 public void updataEntitys(Map<String, Object> values, List<DBRule> rules);
 /**
 * 条件合计视图任务:count(*)
 * 
 * @param rules
 *            统计条件
 */
 public int countEntitys(List<DBRule> rules);
}