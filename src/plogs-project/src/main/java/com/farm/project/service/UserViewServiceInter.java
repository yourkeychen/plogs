package com.farm.project.service;

import com.farm.project.domain.UserView;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：用户视图服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface UserViewServiceInter {
	public UserView updateView(String viewid, String name, Integer sort, LoginUser currentUser);

	public UserView addView(String name, Integer sort, LoginUser currentUser);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteUserviewEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public UserView getUserviewEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createUserviewSimpleQuery(DataQuery query);

	/**
	 * 获得用户的自定义视图
	 * 
	 * @param currentUser
	 * @return
	 */
	public List<UserView> getUserViews(LoginUser currentUser);

	/**为任务分配視圖
	 * @param taskId
	 * @param viewId
	 */
	public void toUserView(String taskId, String viewId,LoginUser currentUser);

}