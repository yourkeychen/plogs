package com.farm.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
/* *
 *功能：任务类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "Task")
@Table(name = "plogs_task")
public class Task implements java.io.Serializable {
        private static final long serialVersionUID = 1L;

        @Id
        @GenericGenerator(name = "systemUUID", strategy = "uuid")
        @GeneratedValue(generator = "systemUUID")
        @Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
        private String id;
        @Column(name = "GRADATIONNAME", length = 128, nullable = false)
        private String gradationname;
        @Column(name = "TASKTYPENAME", length = 128, nullable = false)
        private String tasktypename;
        @Column(name = "TASKTYPEID", length = 32, nullable = false)
        private String tasktypeid;
        @Column(name = "RELAUSERID", length = 32)
        private String relauserid;
        @Column(name = "GRADATIONID", length = 32, nullable = false)
        private String gradationid;
        @Column(name = "PLANTIMES", length = 10)
        private Integer plantimes;
        @Column(name = "DOTIMES", length = 10)
        private Integer dotimes;
        @Column(name = "SOURCEUSERID", length = 32)
        private String sourceuserid;
        @Column(name = "DOETIME", length = 16)
        private String doetime;
        @Column(name = "PLANETIME", length = 16)
        private String planetime;
        @Column(name = "DOSTIME", length = 16)
        private String dostime;
        @Column(name = "USERID", length = 32, nullable = false)
        private String userid;
        @Column(name = "PLANSTIME", length = 16)
        private String planstime;
        @Column(name = "PROJECTID", length = 32, nullable = false)
        private String projectid;
        @Column(name = "READFILEID", length = 32)
        private String readfileid;
        @Column(name = "TITLE", length = 512, nullable = false)
        private String title;
        @Column(name = "PCONTENT", length = 128)
        private String pcontent;
        @Column(name = "PSTATE", length = 2, nullable = false)
        private String pstate;
        @Column(name = "EUSER", length = 32, nullable = false)
        private String euser;
        @Column(name = "ETIME", length = 16, nullable = false)
        private String etime;
        @Column(name = "CUSER", length = 32, nullable = false)
        private String cuser;
        @Column(name = "CTIME", length = 16, nullable = false)
        private String ctime;
        @Column(name = "COMPLETION", length = 10)
        private Integer completion;
        
        
        
        public Integer getCompletion() {
			return completion;
		}
		public void setCompletion(Integer completion) {
			this.completion = completion;
		}
		public String  getGradationname() {
          return this.gradationname;
        }
        public void setGradationname(String gradationname) {
          this.gradationname = gradationname;
        }
        public String  getTasktypename() {
          return this.tasktypename;
        }
        public void setTasktypename(String tasktypename) {
          this.tasktypename = tasktypename;
        }
        public String  getTasktypeid() {
          return this.tasktypeid;
        }
        public void setTasktypeid(String tasktypeid) {
          this.tasktypeid = tasktypeid;
        }
        public String  getRelauserid() {
          return this.relauserid;
        }
        public void setRelauserid(String relauserid) {
          this.relauserid = relauserid;
        }
        public String  getGradationid() {
          return this.gradationid;
        }
        public void setGradationid(String gradationid) {
          this.gradationid = gradationid;
        }
        public Integer  getPlantimes() {
          return this.plantimes;
        }
        public void setPlantimes(Integer plantimes) {
          this.plantimes = plantimes;
        }
        public Integer  getDotimes() {
          return this.dotimes;
        }
        public void setDotimes(Integer dotimes) {
          this.dotimes = dotimes;
        }
        public String  getSourceuserid() {
          return this.sourceuserid;
        }
        public void setSourceuserid(String sourceuserid) {
          this.sourceuserid = sourceuserid;
        }
        public String  getDoetime() {
          return this.doetime;
        }
        public void setDoetime(String doetime) {
          this.doetime = doetime;
        }
        public String  getPlanetime() {
          return this.planetime;
        }
        public void setPlanetime(String planetime) {
          this.planetime = planetime;
        }
        public String  getDostime() {
          return this.dostime;
        }
        public void setDostime(String dostime) {
          this.dostime = dostime;
        }
        public String  getUserid() {
          return this.userid;
        }
        public void setUserid(String userid) {
          this.userid = userid;
        }
        public String  getPlanstime() {
          return this.planstime;
        }
        public void setPlanstime(String planstime) {
          this.planstime = planstime;
        }
        public String  getProjectid() {
          return this.projectid;
        }
        public void setProjectid(String projectid) {
          this.projectid = projectid;
        }
        public String  getReadfileid() {
          return this.readfileid;
        }
        public void setReadfileid(String readfileid) {
          this.readfileid = readfileid;
        }
        public String  getTitle() {
          return this.title;
        }
        public void setTitle(String title) {
          this.title = title;
        }
        public String  getPcontent() {
          return this.pcontent;
        }
        public void setPcontent(String pcontent) {
          this.pcontent = pcontent;
        }
        public String  getPstate() {
          return this.pstate;
        }
        public void setPstate(String pstate) {
          this.pstate = pstate;
        }
        public String  getEuser() {
          return this.euser;
        }
        public void setEuser(String euser) {
          this.euser = euser;
        }
        public String  getEtime() {
          return this.etime;
        }
        public void setEtime(String etime) {
          this.etime = etime;
        }
        public String  getCuser() {
          return this.cuser;
        }
        public void setCuser(String cuser) {
          this.cuser = cuser;
        }
        public String  getCtime() {
          return this.ctime;
        }
        public void setCtime(String ctime) {
          this.ctime = ctime;
        }
        public String  getId() {
          return this.id;
        }
        public void setId(String id) {
          this.id = id;
        }
}