package com.farm.project.controller;

import com.farm.project.domain.Project;
import com.farm.project.domain.ProjectGradation;
import com.farm.project.domain.Task;
import com.farm.project.domain.TaskType;
import com.farm.project.service.ProjectGradationServiceInter;
import com.farm.project.service.ProjectServiceInter;
import com.farm.project.service.TaskServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：任务控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/task")
@Controller
public class TaskController extends WebUtils {
	private final static Logger log = Logger.getLogger(TaskController.class);
	@Resource
	private TaskServiceInter taskServiceImpl;
	@Resource
	private ProjectServiceInter projectServiceImpl;
	@Resource
	private ProjectGradationServiceInter projectGradationServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DataResult result = taskServiceImpl.createTaskSimpleQuery(query).search();
			result.runDictionary("6:计划,1:等待,2:开始,3:执行,4:完成,5:归档,0:删除","PSTATE");
			result.runformatTime("DOSTIME", "yyyy-MM-dd HH:mm:ss");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Task entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = taskServiceImpl.editTaskEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Task entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = taskServiceImpl.insertTaskEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				taskServiceImpl.deleteTaskEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/TaskResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids, String projectid) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", taskServiceImpl.getTaskEntity(ids)).returnModelAndView("project/TaskForm");
			}
			case (1): {// 新增
				// 项目
				Project project = projectServiceImpl.getProjectEntity(projectid);
				if (project == null || !project.getState().equals("1")) {
					return ViewMode.getInstance().putAttr("pageset", pageset)
							.setError("请选择一个有效的项目", new RuntimeException("请选择一个有效的项目"))
							.returnModelAndView("project/TaskForm");
				}
				// 阶段
				ProjectGradation gradation = projectServiceImpl.getGradation(project.getId());
				// 任务类型
				List<TaskType> taskTypes = projectGradationServiceImpl.getTaskTypes(gradation.getId());
				return ViewMode.getInstance().putAttr("gradation", gradation).putAttr("project", project)
						.putAttr("taskTypes", taskTypes).putAttr("pageset", pageset)
						.returnModelAndView("project/TaskForm");
			}
			case (2): {// 修改
				Task task = taskServiceImpl.getTaskEntity(ids);
				// 项目
				Project project = projectServiceImpl.getProjectEntity(task.getProjectid());
				if (project == null || !project.getState().equals("1")) {
					return ViewMode.getInstance().putAttr("pageset", pageset)
							.setError("请选择一个有效的项目", new RuntimeException("请选择一个有效的项目"))
							.returnModelAndView("project/TaskForm");
				}
				// 阶段
				ProjectGradation gradation = projectGradationServiceImpl
						.getProjectgradationEntity(task.getGradationid());
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("gradation", gradation)
						.putAttr("project", project).putAttr("entity", task).returnModelAndView("project/TaskForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/TaskForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("project/TaskForm");
		}
	}
}
