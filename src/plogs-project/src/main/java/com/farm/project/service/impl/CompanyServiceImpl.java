package com.farm.project.service.impl;

import com.farm.project.domain.Company;
import com.farm.project.domain.ProjectCompany;
import com.farm.core.time.TimeTool;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.farm.project.dao.CompanyDaoInter;
import com.farm.project.dao.ProjectcompanyDaoInter;
import com.farm.project.service.CompanyServiceInter;
import com.farm.project.service.ProjectcompanyServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：业务机构服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class CompanyServiceImpl implements CompanyServiceInter {
	@Resource
	private CompanyDaoInter companyDaoImpl;
	@Resource
	private ProjectcompanyServiceInter projectCompanyServiceImpl;
	@Resource
	private ProjectcompanyDaoInter projectcompanyDaoImpl;
	private static final Logger log = Logger.getLogger(CompanyServiceImpl.class);

	@Override
	@Transactional
	public Company insertCompanyEntity(Company entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setEuser(user.getId());
		entity.setEtime(TimeTool.getTimeDate14());
		entity.setPstate("1");
		entity.setLivetime(TimeTool.getTimeDate14());
		return companyDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Company editCompanyEntity(Company entity, LoginUser user) {
		Company entity2 = companyDaoImpl.getEntity(entity.getId());
		entity2.setEuser(user.getId());
		entity2.setEtime(TimeTool.getTimeDate14());
		// entity2.setTextfileid(entity.getTextfileid());
		entity2.setType(entity.getType());
		entity2.setName(entity.getName());
		entity2.setLivetime(TimeTool.getTimeDate14());
		entity2.setPcontent(entity.getPcontent());
		// entity2.setPstate(entity.getPstate());
		companyDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteCompanyEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		projectcompanyDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("companyid", id, "=")).toList());
		companyDaoImpl.deleteEntity(companyDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Company getCompanyEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return companyDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createCompanySimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "PLOGS_PROJECT_COMPANY a left join ALONE_AUTH_USER b on a.euser=b.id",
				"a.ID as ID,a.TEXTFILEID,a.TYPE as TYPE,a.NAME as NAME,a.LIVETIME as LIVETIME,a.PCONTENT as PCONTENT,a.PSTATE as PSTATE,b.name as EUSER,a.CUSER as CUSER,a.ETIME as ETIME,a.CTIME as CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public Company getProjectCompany(String projectid) {
		List<ProjectCompany> pcompanys = projectcompanyDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("PROJECTID", projectid, "=")).toList());
		if (pcompanys.size() > 0) {
			Company company = companyDaoImpl.getEntity(pcompanys.get(0).getCompanyid());
			return company;
		}
		return null;
	}

	@Override
	@Transactional
	public List<Company> findCompanys(String name) {
		DBRuleList dbrules = DBRuleList.getInstance();
		if (StringUtils.isNotBlank(name)) {
			dbrules.add(new DBRule("name", name, "like"));
		}
		List<Company> companys = companyDaoImpl.selectEntitys(dbrules.toList());
		if (companys.size() > 20) {
			return companys.subList(0, 20);
		} else {
			return companys;
		}
	}
}
