package com.farm.project.service;

import com.farm.project.domain.Company;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：业务机构服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface CompanyServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Company insertCompanyEntity(Company entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Company editCompanyEntity(Company entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteCompanyEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Company getCompanyEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createCompanySimpleQuery(DataQuery query);

	/**
	 * 获得项目的关联业务机构
	 * 
	 * @param projectid
	 * @return
	 */
	public Company getProjectCompany(String projectid);

	/**通过公司名称查询公司
	 * @param name
	 * @return
	 */
	public List<Company> findCompanys(String name);
}