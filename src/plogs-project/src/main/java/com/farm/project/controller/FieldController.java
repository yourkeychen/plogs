package com.farm.project.controller;

import com.farm.project.domain.Field;
import com.farm.project.domain.FieldType;
import com.farm.project.service.FieldServiceInter;
import com.farm.project.service.FieldTypeServiceInter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：属性控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/field")
@Controller
public class FieldController extends WebUtils {
	private final static Logger log = Logger.getLogger(FieldController.class);
	@Resource
	private FieldServiceInter fieldServiceImpl;
	@Resource
	private FieldTypeServiceInter fieldTypeServiceImpl;

	@RequestMapping("/chooseWin")
	public ModelAndView chooseWin(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/ChooseFieldWin");
	}

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DBRule rule = query.getAndRemoveRule("TYPEID");
			if (rule != null && !rule.getValue().equals("NONE")) {
				query.addRule(rule);
			}
			DataResult result = fieldServiceImpl.createFieldSimpleQuery(query).search();

			result.runDictionary("1:单条,2:多条", "SINGLETYPE");
			result.runDictionary("1:富文本,2:附件,3:多行文本,4:单行文本,5:枚举单选,6:枚举多选,7:公司,8:人员,9:时间,10:日期", "VALTYPE");
			result.runDictionary("1:可用,0:禁用", "PSTATE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/chooseQuery")
	@ResponseBody
	public Map<String, Object> chooseQuery(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DBRule rule = query.getAndRemoveRule("TYPEID");
			if (rule != null && !rule.getValue().equals("NONE")) {
				query.addRule(rule);
			}
			query.addRule(new DBRule("A.PSTATE", "1", "="));
			DataResult result = fieldServiceImpl.createFieldSimpleQuery(query).search();
			result.runDictionary("1:单条,2:多条", "SINGLETYPE");
			result.runDictionary("1:富文本,2:附件,3:多行文本,4:单行文本,5:枚举单选,6:枚举多选,7:公司,8:人员,9:时间,10:日期", "VALTYPE");
			result.runDictionary("1:可用,0:禁用", "PSTATE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Field entity, HttpSession session) {
		try {
			entity = fieldServiceImpl.editFieldEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Field entity, HttpSession session) {
		try {
			entity = fieldServiceImpl.insertFieldEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				fieldServiceImpl.deleteFieldEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/FieldResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids, String typeid) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", fieldServiceImpl.getFieldEntity(ids))
						.returnModelAndView("project/FieldForm");
			}
			case (1): {// 新增
				FieldType type = fieldTypeServiceImpl.getFieldtypeEntity(typeid);
				Field field = new Field();
				field.setTypeid(typeid);
				return ViewMode.getInstance().putAttr("entity", field).putAttr("type", type).putAttr("pageset", pageset)
						.returnModelAndView("project/FieldForm");
			}
			case (2): {// 修改
				Field field = fieldServiceImpl.getFieldEntity(ids);
				FieldType type = fieldTypeServiceImpl.getFieldtypeEntity(field.getTypeid());
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("entity", field).putAttr("type", type)
						.returnModelAndView("project/FieldForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/FieldForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("project/FieldForm");
		}
	}

	/**
	 * 移动节点
	 *
	 * @return
	 */
	@RequestMapping("/moveTypes")
	@ResponseBody
	public Map<String, Object> moveTreeNodeSubmit(String fieldIds, String typeid, HttpSession session) {
		try {
			fieldServiceImpl.moveType(fieldIds, typeid, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/loadSysField")
	@ResponseBody
	public Map<String, Object> loadSysField(String typeid, HttpSession session) {
		try {
			fieldServiceImpl.loadSysField(typeid, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/formatSort")
	@ResponseBody
	public Map<String, Object> formatSort(String ids, HttpSession session) {
		try {
			int n = 1;
			for (String id : parseIds(ids)) {
				fieldServiceImpl.editSort(id, n++, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
}
