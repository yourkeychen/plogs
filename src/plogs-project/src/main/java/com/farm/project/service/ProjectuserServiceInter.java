package com.farm.project.service;

import com.farm.project.domain.Projectuser;
import com.farm.core.sql.query.DataQuery;

import java.util.List;
import java.util.Set;

import com.farm.authority.domain.User;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目干系人服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface ProjectuserServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public Projectuser insertProjectuserEntity(Projectuser entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Projectuser editProjectuserEntity(Projectuser entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteProjectuserEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Projectuser getProjectuserEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query 传入的查询条件封装
	 * @return
	 */
	public DataQuery createProjectuserSimpleQuery(DataQuery query);

	/**
	 * 添加一个项目用户
	 * 
	 * @param projectid
	 * @param userid
	 * @param poptype
	 * @param currentUser
	 */
	public void adduser(String projectid, String userid, String poptype, LoginUser currentUser);

	/**
	 * 重置管理员（将项目创建人设置为项目管理员）
	 * 
	 * @param projectid
	 * @param currentUser
	 */
	public void resetProjectuser(String projectid, LoginUser currentUser);

	/**
	 * 获得项目组成员
	 * 
	 * @param projectid
	 * @return
	 */
	public List<User> getProjectUsers(String projectid);

	/**
	 * 刷新用户活跃时间
	 * 
	 * @param projectid
	 * @param userid
	 */
	public void refreshLivetime(String projectid, String userid);

	/**
	 * 查询当前项目人员信息
	 * 
	 * @param projectid
	 * @return
	 */
	public DataQuery getProjectUsersQuery(String projectid);

	/**
	 * 获得用户项目id集合
	 * 
	 * @param userid
	 * @param num
	 * @return
	 */
	public Set<String> getProjectids(String userid, int num);
}