package com.farm.project.service.impl;

import com.farm.project.domain.FieldType;
import com.farm.core.time.TimeTool;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.farm.project.dao.FieldDaoInter;
import com.farm.project.dao.FieldTypeDaoInter;
import com.farm.project.service.FieldTypeServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：属性分类服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class FieldTypeServiceImpl implements FieldTypeServiceInter {
	@Resource
	private FieldTypeDaoInter fieldtypeDaoImpl;
	@Resource
	private FieldDaoInter fieldDaoImpl;
	private static final Logger log = Logger.getLogger(FieldTypeServiceImpl.class);

	@Override
	@Transactional
	public FieldType insertFieldtypeEntity(FieldType entity, LoginUser user) {
		entity.setCuser(user.getId());
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setEuser(user.getId());
		entity.setEtime(TimeTool.getTimeDate14());
		entity.setPstate("1");
		if (StringUtils.isBlank(entity.getParentid())) {
			entity.setParentid("NONE");
		}
		entity.setTreecode("NONE");
		entity = fieldtypeDaoImpl.insertEntity(entity);
		initTreeCode(entity.getId());
		return entity;
	}

	private void initTreeCode(String treeNodeId) {
		FieldType node = getFieldtypeEntity(treeNodeId);
		if (node.getParentid().equals("NONE")) {
			node.setTreecode(node.getId());
		} else {
			node.setTreecode(fieldtypeDaoImpl.getEntity(node.getParentid()).getTreecode() + node.getId());
		}
		fieldtypeDaoImpl.editEntity(node);
	}

	@Override
	@Transactional
	public FieldType editFieldtypeEntity(FieldType entity, LoginUser user) {
		FieldType entity2 = fieldtypeDaoImpl.getEntity(entity.getId());
		entity2.setEuser(user.getId());
		entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setName(entity.getName());
		entity2.setSort(entity.getSort());
		entity2.setPcontent(entity.getPcontent());
		fieldtypeDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteFieldtypeEntity(String id, LoginUser user) {
		if (fieldtypeDaoImpl.selectEntitys(DBRule.addRule(new ArrayList<DBRule>(), "parentid", id, "=")).size() > 0) {
			throw new RuntimeException("不能删除该节点，请先删除其子节点");
		}
		if (fieldDaoImpl.selectEntitys(DBRule.addRule(new ArrayList<DBRule>(), "typeid", id, "=")).size() > 0) {
			throw new RuntimeException("不能删除该节点，分类下存在属性");
		}
		fieldtypeDaoImpl.deleteEntity(fieldtypeDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public FieldType getFieldtypeEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return fieldtypeDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createFieldtypeSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "PLOGS_FIELD_TYPE",
				"ID,TREECODE,NAME,PARENTID,SORT,PSTATE,PCONTENT,EUSER,CUSER,ETIME,CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public void moveTreeNode(String ids, String targetId, LoginUser currentUser) {
		String[] idArray = ids.split(",");
		FieldType target = getFieldtypeEntity(targetId);
		for (int i = 0; i < idArray.length; i++) {
			// 移动节点
			FieldType node = getFieldtypeEntity(idArray[i]);
			if (target != null && target.getTreecode().indexOf(node.getTreecode()) >= 0) {
				throw new RuntimeException("不能够移动到其子节点下!");
			}
			if (target == null) {
				node.setParentid("NONE");
			} else {
				node.setParentid(targetId);
			}
			fieldtypeDaoImpl.editEntity(node);
			// 构造所有树TREECODE
			List<FieldType> list = fieldtypeDaoImpl.getAllSubNodes(idArray[i]);
			for (FieldType org : list) {
				initTreeCode(org.getId());
			}
		}
	}
}
