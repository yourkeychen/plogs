package com.farm.project.dao;

import com.farm.project.domain.TaskFile;
import org.hibernate.Session;

import com.farm.core.auth.domain.LoginUser;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/* *
 *功能：任务附件数据库持久层接口
 *详细：
 *
 *版本：v0.1
 *作者：Farm代码工程自动生成
 *日期：20150707114057
 *说明：
 */
public interface TaskFileDaoInter {
	/**
	 * 删除一个任务附件实体
	 * 
	 * @param entity
	 *            实体
	 */
	public void deleteEntity(TaskFile taskfile);

	/**
	 * 由任务附件id获得一个任务附件实体
	 * 
	 * @param id
	 * @return
	 */
	public TaskFile getEntity(String taskfileid);

	/**
	 * 插入一条任务附件数据
	 * 
	 * @param entity
	 */
	public TaskFile insertEntity(TaskFile taskfile);

	/**
	 * 获得记录数量
	 * 
	 * @return
	 */
	public int getAllListNum();

	/**
	 * 修改一个任务附件记录
	 * 
	 * @param entity
	 */
	public void editEntity(TaskFile taskfile);

	/**
	 * 获得一个session
	 */
	public Session getSession();

	/**
	 * 执行一条任务附件查询语句
	 */
	public DataResult runSqlQuery(DataQuery query);

	/**
	 * 条件删除任务附件实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param rules
	 *            删除条件
	 */
	public void deleteEntitys(List<DBRule> rules);

	/**
	 * 条件查询任务附件实体，依据对象字段值,当rules为空时查询全部(一般不建议使用该方法)
	 * 
	 * @param rules
	 *            查询条件
	 * @return
	 */
	public List<TaskFile> selectEntitys(List<DBRule> rules);

	/**
	 * 条件修改任务附件实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param values
	 *            被修改的键值对
	 * @param rules
	 *            修改条件
	 */
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules);

	/**
	 * 条件合计任务附件:count(*)
	 * 
	 * @param rules
	 *            统计条件
	 */
	public int countEntitys(List<DBRule> rules);

	/**
	 * 获得项目任务中的附件
	 * 
	 * @param projectId
	 * @param currentUser
	 * @param etime
	 * @param stime
	 * @return ID,TASKID,FILEID,CTIME,FIELDID,FIELDNAME,LOGID,FILETITLE,SECRET
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getProjectFiles(String projectId, String stime_yyyymmdd, String etime_yyyymmdd,
			LoginUser currentUser) throws SQLException;

	/**
	 * 获得任务日志的附件
	 * 
	 * @param logid
	 * @return ID,TASKID,FILEID,CTIME,LOGID,FILETITLE,SECRET
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getLogFiles(String logid) throws SQLException;

	/**
	 * 获得任务描述中的附件
	 * 
	 * @param taskid
	 * @return ID,TASKID,FILEID,CTIME,LOGID,FILETITLE,SECRET
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getTaskReadFiles(String taskid) throws SQLException;
}