package com.farm.project.service.impl;

import com.farm.project.domain.Project;
import com.farm.project.domain.Projectuser;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;

import com.farm.project.dao.ProjectDaoInter;
import com.farm.project.dao.ProjectuserDaoInter;
import com.farm.project.service.ProjectuserServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.farm.authority.domain.User;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：项目干系人服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class ProjectuserServiceImpl implements ProjectuserServiceInter {
	@Resource
	private ProjectuserDaoInter projectuserDaoImpl;
	@Resource
	private ProjectDaoInter projectDaoImpl;
	private static final Logger log = Logger.getLogger(ProjectuserServiceImpl.class);

	@Override
	@Transactional
	public Projectuser insertProjectuserEntity(Projectuser entity, LoginUser user) {
		// entity.setCuser(user.getId());
		// entity.setCtime(TimeTool.getTimeDate14());
		// entity.setCusername(user.getName());
		// entity.setEuser(user.getId());
		// entity.setEusername(user.getName());
		// entity.setEtime(TimeTool.getTimeDate14());
		// entity.setPstate("1");
		return projectuserDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Projectuser editProjectuserEntity(Projectuser entity, LoginUser user) {
		Projectuser entity2 = projectuserDaoImpl.getEntity(entity.getId());
		List<Projectuser> admins = projectuserDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("PROJECTID", entity2.getProjectid(), "="))
						.add(new DBRule("POPTYPE", "1", "=")).toList());
		if (admins.size() == 1 && admins.get(0).getUserid().equals(entity2.getUserid())) {
			// 只有一个管理员且当前用户是唯一管理员，则不能修改
			throw new RuntimeException("无法修改唯一管理员");
		}
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		// entity2.setLivetime(entity.getLivetime());
		// entity2.setUserid(entity.getUserid());
		entity2.setPoptype(entity.getPoptype());
		// entity2.setProjectid(entity.getProjectid());
		// entity2.setPstate(entity.getPstate());
		// entity2.setPcontent(entity.getPcontent());
		// entity2.setCuser(entity.getCuser());
		// entity2.setCtime(entity.getCtime());
		// entity2.setId(entity.getId());
		projectuserDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteProjectuserEntity(String id, LoginUser user) {
		Projectuser entity2 = projectuserDaoImpl.getEntity(id);
		List<Projectuser> admins = projectuserDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("PROJECTID", entity2.getProjectid(), "="))
						.add(new DBRule("POPTYPE", "1", "=")).toList());
		if (admins.size() == 1 && admins.get(0).getUserid().equals(entity2.getUserid())) {
			throw new RuntimeException("无法删除唯一管理员");
		}
		projectuserDaoImpl.deleteEntity(projectuserDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Projectuser getProjectuserEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return projectuserDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createProjectuserSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "PLOGS_PROJECT_USER a left join ALONE_AUTH_USER b on a.userid=b.id",
				"A.ID AS ID,A.LIVETIME AS LIVETIME,A.USERID AS USERID,A.POPTYPE AS POPTYPE,A.PROJECTID AS PROJECTID,A.PSTATE AS PSTATE,A.PCONTENT AS PCONTENT,A.CUSER AS CUSER,A.CTIME AS CTIME,B.NAME AS USERNAME,B.LOGINNAME AS LOGINNAME");
		return dbQuery;
	}

	@Override
	@Transactional
	public void adduser(String projectid, String userid, String poptype, LoginUser currentUser) {
		if (projectuserDaoImpl.selectEntitys(DBRuleList.getInstance().add(new DBRule("userid", userid, "="))
				.add(new DBRule("projectid", projectid, "=")).toList()).size() <= 0) {
			Projectuser entity = new Projectuser();
			entity.setCuser(currentUser.getId());
			entity.setCtime(TimeTool.getTimeDate14());
			entity.setLivetime(TimeTool.getTimeDate14());
			entity.setPoptype(poptype);
			entity.setProjectid(projectid);
			entity.setUserid(userid);
			entity.setPstate("1");
			projectuserDaoImpl.insertEntity(entity);
		}
	}

	@Override
	@Transactional
	public void resetProjectuser(String projectid, LoginUser currentUser) {
		Project project = projectDaoImpl.getEntity(projectid);
		projectuserDaoImpl.deleteEntitys(DBRuleList.getInstance().add(new DBRule("userid", project.getCuser(), "="))
				.add(new DBRule("projectid", projectid, "=")).toList());
		adduser(projectid, project.getCuser(), "1", currentUser);
	}

	@Override
	@Transactional
	public List<User> getProjectUsers(String projectid) {
		DataQuery query = DataQuery.getInstance(1, "B.ID AS ID,B.NAME AS NAME,B.TYPE AS TYPE,B.LOGINNAME AS LOGINNAME",
				"PLOGS_PROJECT_USER a left join ALONE_AUTH_USER b on a.userid=b.id");
		query.addRule(new DBRule("a.projectid", projectid, "="));
		query.addSort(new DBSort("LIVETIME", "DESC"));
		query.setPagesize(100);
		try {
			return query.search().getObjectList(User.class);
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	@Override
	@Transactional
	public void refreshLivetime(String projectid, String userid) {
		List<Projectuser> users = projectuserDaoImpl.selectEntitys(DBRuleList.getInstance()
				.add(new DBRule("PROJECTID", projectid, "=")).add(new DBRule("USERID", userid, "=")).toList());
		for (Projectuser user : users) {
			user.setLivetime(TimeTool.getTimeDate14());
			projectuserDaoImpl.editEntity(user);
		}
	}

	@Override
	@Transactional
	public DataQuery getProjectUsersQuery(String projectid) {
		DataQuery query = DataQuery.getInstance(1,
				"A.ID AS ID,B.NAME AS NAME,B.TYPE AS TYPE,B.LOGINNAME AS LOGINNAME,A.POPTYPE as POPTYPE,B.ID as USERID",
				"PLOGS_PROJECT_USER a left join ALONE_AUTH_USER b on a.userid=b.id");
		query.addRule(new DBRule("a.projectid", projectid, "="));
		query.addSort(new DBSort("A.LIVETIME", "DESC"));
		query.setPagesize(100);
		return query;
	}

	@Override
	@Transactional
	public Set<String> getProjectids(String userid, int num) {
		List<Projectuser> users = projectuserDaoImpl
				.selectEntitys(DBRuleList.getInstance().add(new DBRule("USERID", userid, "=")).toList());
		Set<String> sets = new HashSet<String>();
		for (Projectuser user : users) {
			sets.add(user.getProjectid());
		}
		return sets;
	}

}
