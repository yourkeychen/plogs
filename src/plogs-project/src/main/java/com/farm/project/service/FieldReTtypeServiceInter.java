package com.farm.project.service;

import com.farm.project.domain.FieldReTtype;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：任务类型属性服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface FieldReTtypeServiceInter {
	/**
	 * 新增实体管理实体
	 * 
	 * @param entity
	 */
	public FieldReTtype insertFieldrettypeEntity(FieldReTtype entity, LoginUser user);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public FieldReTtype editFieldrettypeEntity(FieldReTtype entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteFieldrettypeEntity(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public FieldReTtype getFieldrettypeEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createFieldrettypeSimpleQuery(DataQuery query);

	/**
	 * 綁定任务属性
	 * 
	 * @param taskTypeId
	 * @param fieldid
	 * @param currentUser
	 */
	public void bindTaskType(String taskTypeId, String fieldid, LoginUser currentUser);
}