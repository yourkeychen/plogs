package com.farm.project.service.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;

import com.farm.file.domain.FileBase;
import com.farm.lucene.adapter.DocMap;
import com.farm.project.domain.Project;
import com.farm.project.domain.Task;
import com.farm.project.domain.TaskFile;
import com.farm.util.web.FarmHtmlUtils;

public class TaskFileIndexInfo {
	private FileBase file;
	private Task task;
	private Project project;
	private String text;
	private TaskFile taskfile;

	public TaskFile getTaskfile() {
		return taskfile;
	}

	public void setTaskfile(TaskFile taskfile) {
		this.taskfile = taskfile;
	}

	public FileBase getFile() {
		return file;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setFile(FileBase file) {
		this.file = file;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getId() {
		return file.getId();
	}

	public DocMap getDocMap() {
		if (StringUtils.isBlank(text)) {
			// 拼接分类自定义属性
			text = file.getTitle() + ":" + task.getTitle();
		}
		text = FarmHtmlUtils.HtmlRemoveTag(text);
		DocMap map = new DocMap(getId());
		map.put("FILEID", file.getId(), Store.YES, Index.ANALYZED);
		map.put("SECRET", file.getSecret(), Store.YES, Index.ANALYZED);
		map.put("TEXT", text, Store.YES, Index.ANALYZED, 1.3f);
		map.put("TASKID", task.getId(), Store.YES, Index.ANALYZED);
		map.put("PROJECTID", project.getId(), Store.YES, Index.ANALYZED);
		map.put("FILENAME", file.getTitle(), Store.YES, Index.ANALYZED, 1.6f);
		map.put("TASKNAME", task.getTitle(), Store.YES, Index.ANALYZED);
		map.put("PROJECTNAME", project.getName(), Store.YES, Index.ANALYZED);
		map.put("FILESIZE", String.valueOf(file.getFilesize()), Store.YES, Index.ANALYZED);
		map.put("SOURCETYPE", taskfile.getSourcetype(), Store.YES, Index.ANALYZED);
		map.put("CTIME", taskfile.getCtime(), Store.YES, Index.ANALYZED);
		map.put("EXNAME", file.getExname(), Store.YES, Index.ANALYZED);
		map.put("TASKTYPEID", task.getTasktypeid(), Store.YES, Index.ANALYZED);
		map.put("TASKTYPENAME", task.getTasktypename(), Store.YES, Index.ANALYZED);
		return map;
	}

}
