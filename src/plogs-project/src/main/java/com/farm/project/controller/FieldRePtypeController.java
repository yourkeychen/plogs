package com.farm.project.controller;

import com.farm.project.domain.FieldRePtype;
import com.farm.project.service.FieldRePtypeServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBSort;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：项目类型属性控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/fieldreptype")
@Controller
public class FieldRePtypeController extends WebUtils {
	private final static Logger log = Logger.getLogger(FieldRePtypeController.class);
	@Resource
	private FieldRePtypeServiceInter fieldRePTypeServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, String projecttypeid, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("a.projecttypeid", projecttypeid, "="));
			query.addDefaultSort(new DBSort("A.SORT","ASC"));
			DataResult result = fieldRePTypeServiceImpl.createFieldreptypeSimpleQuery(query).search();
			result.runDictionary("1:默认公开,0:默认隐藏", "READIS");
			result.runDictionary("1:默认公开,0:默认禁止", "WRITEIS");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(FieldRePtype entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = fieldRePTypeServiceImpl.editFieldreptypeEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(FieldRePtype entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = fieldRePTypeServiceImpl.insertFieldreptypeEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				fieldRePTypeServiceImpl.deleteFieldreptypeEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/bindProjectType")
	@ResponseBody
	public Map<String, Object> bindProjectType(String projectTypeId, String fieldids, HttpSession session) {
		try {
			for (String fieldid : parseIds(fieldids)) {
				fieldRePTypeServiceImpl.bindProjectType(projectTypeId, fieldid, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("project/FieldRePtypeResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", fieldRePTypeServiceImpl.getFieldreptypeEntity(ids))
						.returnModelAndView("project/FieldRePtypeForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.returnModelAndView("project/FieldRePtypeForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", fieldRePTypeServiceImpl.getFieldreptypeEntity(ids))
						.returnModelAndView("project/FieldRePtypeForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/FieldRePtypeForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e)
					.returnModelAndView("project/FieldRePtypeForm");
		}
	}
}
