package com.farm.project.controller;

import com.farm.project.domain.Projectuser;
import com.farm.project.service.ProjectuserServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

/* *
 *功能：项目干系人控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/projectuser")
@Controller
public class ProjectuserController extends WebUtils {
	private final static Logger log = Logger.getLogger(ProjectuserController.class);
	@Resource
	private ProjectuserServiceInter projectUserServiceImpl;

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, String projectid, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addRule(new DBRule("projectid", projectid, "="));
			DataResult result = projectUserServiceImpl.createProjectuserSimpleQuery(query).search();

			result.runDictionary("1:管理员,2:成员", "POPTYPE");
			result.runformatTime("LIVETIME", "yyyy-MM-dd HH:mm");

			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Projectuser entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = projectUserServiceImpl.editProjectuserEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交新增数据
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Projectuser entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = projectUserServiceImpl.insertProjectuserEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/addUser")
	@ResponseBody
	public Map<String, Object> addUser(String userids, String projectid, String poptype, HttpSession session) {
		try {
			for (String userid : parseIds(userids)) {
				if (StringUtils.isBlank(poptype)) {
					poptype = "2";
				}
				projectUserServiceImpl.adduser(projectid, userid, poptype, getCurrentUser(session));
			}
			return ViewMode.getInstance().setOperate(OperateType.ADD).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				projectUserServiceImpl.deleteProjectuserEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 重置管理员
	 * 
	 * @param ids
	 * @param session
	 * @return
	 */
	@RequestMapping("/resetUser")
	@ResponseBody
	public Map<String, Object> resetUser(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) { 
				projectUserServiceImpl.resetProjectuser(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(String projectid, HttpSession session) {
		return ViewMode.getInstance().putAttr("projectid", projectid).returnModelAndView("project/ProjectuserResult");
	}

	/**
	 * 显示详细信息（修改或浏览时）
	 *
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids, String projectid) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", projectUserServiceImpl.getProjectuserEntity(ids))
						.returnModelAndView("project/ProjectuserForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset).returnModelAndView("project/ProjectuserForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", projectUserServiceImpl.getProjectuserEntity(ids))
						.returnModelAndView("project/ProjectuserForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("project/ProjectuserForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("project/ProjectuserForm");
		}
	}
}
