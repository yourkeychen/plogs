package com.farm.parameter.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import com.farm.core.Context;
import com.farm.core.auth.util.DesUtil;
import com.farm.core.time.TimeTool;
import com.farm.parameter.util.InfoUtil;
import com.farm.util.spring.HibernateSessionFactory;
import com.farm.util.web.WaterCodeUtils;
import com.farm.web.constant.FarmConstant;

@Service
public class ConstantVarService {
	private static Map<String, String> constant = new HashMap<String, String>();

	public static void registConstant(String key, String value) {
		constant.put(key, value);
	}

	public static List<Entry<String, String>> getEntrys() {
		List<Entry<String, String>> list = new ArrayList<Entry<String, String>>();
		for (Entry<String, String> node : constant.entrySet()) {
			list.add(node);
		}
		return list;
	}

	public static String getValue(String key) {
		return constant.get(key);
	}

	public static void registConstant(String str, ServletContext context) {
		context.setAttribute(FarmConstant.LCENCE_AUTH_WEB_KEY, getLicenceAuth());
	}

	private static String getLicenceAuth() {
		Session session = HibernateSessionFactory.getSession();
		try {
			String privateKey = FarmConstant.LCENCE_AUTH_DB_KEY;
			String yyyy = TimeTool.getFormatTimeDate12(TimeTool.getTimeDate14(), "yyyyMM");
			yyyy = DesUtil.getInstance(privateKey).encryptString(yyyy);
			Query query = session.createSQLQuery("SELECT ENTITYINDEX FROM ALONE_DICTIONARY_ENTITY WHERE NAME=?")
					.setString(0, privateKey);
			@SuppressWarnings("unchecked")
			List<Object> dbkey2 = query.list();

			if (dbkey2.size() > 0) {
				String dbkey = (String) dbkey2.get(0);
				return WaterCodeUtils.getInctance().decode(dbkey);
			}
			return "";
		} catch (Exception e) {
			return "";
		} finally {
			session.close();
		}
	}
}
