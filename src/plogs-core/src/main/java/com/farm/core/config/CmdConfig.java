package com.farm.core.config;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

/**
 * 获得命令
 * 
 * @author wangdong
 * 
 */
public class CmdConfig {
	private static final String BUNDLE_NAME = "cmd"; //$NON-NLS-1$
	private static final Logger log = Logger.getLogger(CmdConfig.class);
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	private CmdConfig() {
	}

	/**
	 * 从properties文件中获得配置值
	 * 
	 * @param key
	 *            配置文件的key
	 * @return
	 */
	public static String getCmd(String key) {
		try {
			String messager = RESOURCE_BUNDLE.getString(key);
			return messager;
		} catch (MissingResourceException e) {
			String messager = "不能在配置文件" + BUNDLE_NAME + "中发现参数：" + '!' + key + '!';
			log.error(messager);
			throw new RuntimeException(messager);
		}
	}
}
