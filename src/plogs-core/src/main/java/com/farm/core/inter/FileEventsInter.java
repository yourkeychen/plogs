package com.farm.core.inter;

/**
 * 附件预览事件合集
 * 
 * @author macpl
 *
 */
public interface FileEventsInter {
	public void fileOnDel(String fileid);

	public void fileTextOnChange(String fileid);
}
