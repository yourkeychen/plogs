package com.farm.core.inter.impl;

import com.farm.core.inter.BusinessHandleInter;
import com.farm.core.inter.FileEventsInter;
import com.farm.core.inter.domain.BusinessHandler;

/**
 * 消息发送事件
 * 
 * @author wangdong
 *
 */
public class FileEventHandle extends BusinessHandleServer implements FileEventsInter {
	/**
	 * 服务ID，用来获取实现类的关键字
	 */
	private static String serverId = "FileEventHandleInterId";

	/**
	 * 获得实例
	 * 
	 * @param serverId
	 * @return
	 */
	public static FileEventHandle getInstance() {
		FileEventHandle obj = new FileEventHandle();
		return obj;
	}

	@Override
	public void fileOnDel(final String fileid) {
		setSycn(true);
		runAll(serverId, new BusinessHandleInter() {
			@Override
			public void execute(BusinessHandler impl) {
				FileEventsInter hander = (FileEventsInter) impl.getBeanImpl();
				hander.fileOnDel(fileid);
			}
		});
	}

	@Override
	public void fileTextOnChange(final String fileid) {
		setSycn(false);
		runAll(serverId, new BusinessHandleInter() {
			@Override
			public void execute(BusinessHandler impl) {
				FileEventsInter hander = (FileEventsInter) impl.getBeanImpl();
				hander.fileTextOnChange(fileid);
			}
		});
		setSycn(true);
	}
}
