package com.farm.web.easyui;

public interface EasyUiTreeNodeHandle {
	public void handle(EasyUiTreeNode node);
}
