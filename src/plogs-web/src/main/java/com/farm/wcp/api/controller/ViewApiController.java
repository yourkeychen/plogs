package com.farm.wcp.api.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.farm.core.page.ViewMode;
import com.farm.file.WdapFileServiceInter;
import com.farm.view.WdapViewServiceInter;
import com.farm.web.WebUtils;

/**
 * 文件服务接口
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/viewapi")
@Controller
public class ViewApiController extends WebUtils {
	private static final Logger log = Logger.getLogger(ViewApiController.class);
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;

	/**
	 * 一個附件是否支持生成预览文件
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/convert/able")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> convertAble(String operatorLoginname, String operatorPassword,
			String exname, Long lenght, String secret, HttpServletRequest request) {
		try {
			log.info("restful API:一個附件是否支持生成预览文件" + exname);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			return ViewMode.getInstance().putAttr("DATA", wdapViewServiceImpl.fileConvertAble(exname, lenght))
					.returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 生成预览文件
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/generate")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> submit(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:提交预览任务（将转换任务添加到队列中）" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			wdapViewServiceImpl.reLoadFileViewTask(fileid);
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得转换任务信息
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/tasks")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> tasks(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:获得转换任务信息" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			List<Map<String, Object>> viewTasks = wdapViewServiceImpl.getTasksByFileid(fileid);
			return ViewMode.getInstance().putAttr("viewtasks", viewTasks).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得预览文件信息
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/viewdocs")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> viewdocs(String operatorLoginname, String operatorPassword,
			String fileid, String secret, HttpServletRequest request) {
		try {
			log.info("restful API:获得预览文件信息" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			List<Map<String, Object>> viewDocs = wdapViewServiceImpl.getFileViewdocs(fileid);
			return ViewMode.getInstance().putAttr("viewdocs", viewDocs).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得任务日志
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/logs")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> logs(String operatorLoginname, String operatorPassword, String taskid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:获得预览文件信息" + taskid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			List<String> logs = wdapViewServiceImpl.getTaskLogs(taskid);
			return ViewMode.getInstance().putAttr("logs", logs).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 预览文件是否就緒
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/view/able")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> able(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:预览文件是否就緒" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			boolean isView = wdapViewServiceImpl.isView(fileid);
			return ViewMode.getInstance().putAttr("DATA", isView).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}
}
