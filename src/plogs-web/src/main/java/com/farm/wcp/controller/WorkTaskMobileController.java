package com.farm.wcp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.project.domain.Project;
import com.farm.project.domain.ProjectGradation;
import com.farm.project.domain.ProjectType;
import com.farm.project.domain.Task;
import com.farm.project.domain.TaskLog;
import com.farm.project.domain.TaskType;
import com.farm.project.service.ProjectFieldinsServiceInter;
import com.farm.project.service.ProjectGradationServiceInter;
import com.farm.project.service.ProjectServiceInter;
import com.farm.project.service.ProjectTypeServiceInter;
import com.farm.project.service.TaskServiceInter;
import com.farm.project.service.TaskTypeServiceInter;
import com.farm.project.service.TaskuserServiceInter;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;

/**
 * 移動端工作视图
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/mtask")
@Controller
public class WorkTaskMobileController extends WebUtils {
	private static final Logger log = Logger.getLogger(WorkTaskMobileController.class);
	@Resource
	private ProjectServiceInter projectServiceImpl;
	@Resource
	private ProjectTypeServiceInter projectTypeServiceImpl;
	@Resource
	private ProjectGradationServiceInter projectGradationServiceImpl;
	@Resource
	private TaskServiceInter taskServiceImpl;
	@Resource
	private TaskTypeServiceInter taskTypeServiceImpl;
	@Resource
	private ProjectFieldinsServiceInter projectFieldinsServiceImpl;
	@Resource
	private TaskuserServiceInter taskUserServiceImpl;

	/***
	 * 任务列表
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/tasks")
	public ModelAndView tasks(String state, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			List<Map<String, Object>> tasks = taskServiceImpl.getTasks(getCurrentUser(session), state, null);
			view.putAttr("tasks", tasks);
			view.putAttr("state", state);
			return view.returnModelAndView("/web-mobile/worktask/tasks");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	/***
	 * 任务信息
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/taskinfo")
	public ModelAndView taskinfo(String taskid, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			Task task = taskServiceImpl.getTaskEntity(taskid);
			{
				// 处理关系人
				List<Map<String, Object>> taskusers = taskUserServiceImpl.getTaskUsers(taskid);
				List<Map<String, Object>> doUsers = new ArrayList<>();
				List<Map<String, Object>> managerUsers = new ArrayList<>();
				for (Map<String, Object> user : taskusers) {
					String poptype = (String) user.get("POPTYPE");
					if (poptype.equals("1")) {
						managerUsers.add(user);
					}
					if (poptype.equals("2")) {
						doUsers.add(user);
					}
				}
				view.putAttr("doUsers", doUsers.size() > 0 ? doUsers : managerUsers);
				view.putAttr("managerUsers", managerUsers);
				view.putAttr("isDoUser", taskUserServiceImpl.isDoUser(taskid, getCurrentUser(session)));
				view.putAttr("isManagerUser", taskUserServiceImpl.isManagerUser(taskid, getCurrentUser(session)));
			}
			view.putAttr("task", task);
			// 获得任务项目名称
			Project project = projectServiceImpl.getProjectEntity(task.getProjectid());
			view.putAttr("project", project);
			if (StringUtils.isNotBlank(task.getReadfileid())) {
				String text = taskServiceImpl.readText(task.getReadfileid());
				view.putAttr("text", text);
			}
			List<TaskLog> logs = taskServiceImpl.getLogs(taskid);
			view.putAttr("logs", logs);
			return view.returnModelAndView("/web-mobile/worktask/taskinfo");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	/***
	 * 任务信息
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/taskform")
	public ModelAndView taskform(String taskid, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			if (StringUtils.isNotBlank(taskid)) {
				// 修改
				Task task = taskServiceImpl.getTaskEntity(taskid);
				Project project = projectServiceImpl.getProjectEntity(task.getProjectid());
				view.putAttr("task", task);
				view.putAttr("cproject", project);
				if (StringUtils.isNotBlank(task.getReadfileid())) {
					view.putAttr("text", taskServiceImpl.readText(task.getReadfileid()));
				}
				ProjectType type = projectTypeServiceImpl.getProjecttypeEntity(project.getTypeid());
				List<ProjectGradation> gradations = projectGradationServiceImpl.getGradationsByTypeid(type.getId());
				view.putAttr("gradations", gradations);
				List<TaskType> taskTypes = projectGradationServiceImpl.getTaskTypes(task.getGradationid());
				view.putAttr("taskTypes", taskTypes);
			} else {
				// 添加
			}
			List<Project> projects = projectServiceImpl.getliveProjects(getCurrentUser(session), null);
			view.putAttr("projects", projects);
			return view.returnModelAndView("/web-mobile/worktask/taskform");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	/***
	 * 日志信息
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/logform")
	public ModelAndView logform(String taskid, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			String text = taskServiceImpl.getlog(taskid);
			view.putAttr("text", text);
			view.putAttr("task", taskServiceImpl.getTaskEntity(taskid));
			return view.returnModelAndView("/web-mobile/worktask/logform");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}
}