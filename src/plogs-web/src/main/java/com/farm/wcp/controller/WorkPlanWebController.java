package com.farm.wcp.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.core.time.TimeTool;
import com.farm.project.domain.UserView;
import com.farm.project.service.FieldServiceInter;
import com.farm.project.service.ProjectFieldinsServiceInter;
import com.farm.project.service.ProjectGradationServiceInter;
import com.farm.project.service.ProjectServiceInter;
import com.farm.project.service.ProjectTypeServiceInter;
import com.farm.project.service.TaskServiceInter;
import com.farm.project.service.TaskTypeServiceInter;
import com.farm.project.service.UserViewServiceInter;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;

/**
 * 计划任务
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/workplan")
@Controller
public class WorkPlanWebController extends WebUtils {
	private static final Logger log = Logger.getLogger(WorkPlanWebController.class);
	@Resource
	private ProjectServiceInter projectServiceImpl;
	@Resource
	private ProjectTypeServiceInter projectTypeServiceImpl;
	@Resource
	private ProjectGradationServiceInter projectGradationServiceImpl;
	@Resource
	private TaskServiceInter taskServiceImpl;
	@Resource
	private TaskTypeServiceInter taskTypeServiceImpl;
	@Resource
	private ProjectFieldinsServiceInter projectFieldinsServiceImpl;
	@Resource
	private FieldServiceInter fieldServiceImpl;
	@Resource
	private UserViewServiceInter userViewServiceImpl;

	/***
	 * 工作首页
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		view.putAttr("MODEL", "TASTVIEW");
		int num6 = taskServiceImpl.getTaskCount(getCurrentUser(session), "6");
		view.putAttr("num6", num6);
		view.putAttr("today", TimeTool.getTimeDate14());
		List<UserView> views = userViewServiceImpl.getUserViews(getCurrentUser(session));
		Collections.sort(views, new Comparator<UserView>() {
			@Override
			public int compare(UserView o1, UserView o2) {
				return o1.getSort() - o2.getSort();
			}
		});
		view.putAttr("views", views);
		return view.returnModelAndView(ThemesUtil.getThemePage("plogs-plans", request));
	}

	/***
	 * 添加|修改任务表单
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadViewFormWin")
	public ModelAndView loadTaskFormWin(String viewId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			if (StringUtils.isNotBlank(viewId)) {
				// 修改
				UserView uview = userViewServiceImpl.getUserviewEntity(viewId);
				view.putAttr("view", uview);
			}
			return view.returnModelAndView("/web-simple/workptask/commons/includeAddViewWinLoader");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/workptask/commons/includeAddViewWinLoader");
		}
	}

	/**
	 * 提交一个视图（修改过创建）
	 * 
	 * @param taskid
	 * @param title
	 * @param projectid
	 * @param gradationid
	 * @param tasktypeid
	 * @param state
	 * @param text
	 * @param session
	 * @return
	 */
	@RequestMapping("/viewSubmit")
	@ResponseBody
	public Map<String, Object> viewSubmit(String viewid, String name, Integer sort, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			UserView uview = null;
			if (StringUtils.isNotBlank(viewid)) {
				// 修改
				uview = userViewServiceImpl.updateView(viewid, name, sort, getCurrentUser(session));
			} else {
				// 添加
				uview = userViewServiceImpl.addView(name, sort, getCurrentUser(session));
			}
			view.putAttr("view", uview);
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
	
	
	@RequestMapping("/toUserView")
	@ResponseBody
	public Map<String, Object> toUserView(String taskId, String viewId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			userViewServiceImpl.toUserView(taskId,viewId,getCurrentUser(session));
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
	
}