package com.farm.wcp.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.core.time.TimeTool;
import com.farm.file.WdapFileServiceInter;
import com.farm.view.WdapViewServiceInter;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;
import com.farm.wfs.domain.DirType;
import com.farm.wfs.domain.Wfile;
import com.farm.wfs.service.DirTypeServiceInter;
import com.farm.wfs.service.WfileServiceInter;

/**
 * 文件夹操作
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/dir")
@Controller
public class WfsDirController extends WebUtils {
	@Resource
	private DirTypeServiceInter dirTypeServiceImpl;
	@Resource
	private WfileServiceInter wFileServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;

	/***
	 * 
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/dir")
	public ModelAndView index(String dirid, HttpServletRequest request, HttpSession session) {
		if (StringUtils.isBlank(dirid)) {
			dirid = "NONE";
		}
		List<DirType> dirs = dirTypeServiceImpl.getDirs(dirid, getCurrentUser(session).getId());
		List<Wfile> wfiles = wFileServiceImpl.getFiles(dirid, getCurrentUser(session).getId());
		List<DirType> dirpath = dirTypeServiceImpl.getDirPath(dirid);
		ViewMode view = ViewMode.getInstance();
		view.putAttr("iconUrlBase", "download/Pubicon.do?id=");
		return view.putAttr("dirs", dirs).putAttr("wfiles", wfiles).putAttr("cdir", dirpath.get(dirpath.size() - 1))
				.putAttr("path", dirpath).returnModelAndView(ThemesUtil.getThemePage("files-dir", request));
	}

	@RequestMapping("/form")
	public ModelAndView create(String dirid, String parentid, HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		view.putAttr("parentid", parentid);
		if (StringUtils.isBlank(dirid)) {
			// 新建文件夹
			DirType dir = new DirType();
			dir.setName("新建文件夹" + TimeTool.getTimeDate14());
			view.putAttr("dir", dir);
		} else {
			// 修改文件夹
			DirType dir = dirTypeServiceImpl.getDir(getCurrentUser(session).getId(), dirid);
			view.putAttr("dir", dir);
		}
		return view.returnModelAndView(ThemesUtil.getThemePage("files-dir-form", request));
	}

	@RequestMapping("/formSubmit")
	@ResponseBody
	public Map<String, Object> formSubmit(String id, String name, String parentid, HttpServletRequest request,
			HttpSession session) {
		try {
			if (StringUtils.isBlank(id)) {
				// 空创建文件夹
				dirTypeServiceImpl.createDir(name, 1, parentid, getCurrentUser(session));
			} else {
				// 不为空修改文件夹
				dirTypeServiceImpl.editDir(id, name, 1, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();

		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/delDir")
	@ResponseBody
	public Map<String, Object> delDir(String dirid, HttpServletRequest request, HttpSession session) {
		try {
			for (String id : parseIds(dirid)) {
				if (id.toUpperCase().startsWith("FILE-")) {
					id = id.replace("FILE-", "");
					wFileServiceImpl.deleteWfile(id, getCurrentUser(session));
				} else {
					id = id.replace("DIR-", "");
					dirTypeServiceImpl.delDir(id, getCurrentUser(session));
				}
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/dirInfo")
	public ModelAndView dirInfo(String dirid, String cdirid, HttpServletRequest request, HttpSession session) {
		if (StringUtils.isBlank(dirid)) {
			dirid = "NONE";
		}
		ViewMode view = ViewMode.getInstance();
		DirType dir = dirTypeServiceImpl.getDir(getCurrentUser(session).getId(), dirid);
		if (dir == null) {
			if (dirid.equals("SEARCH")) {
				dir = new DirType();
				dir.setId("SEARCH");
				dir.setName("文件检索");
			} else {
				dir = new DirType();
				dir.setId("NONE");
				dir.setName("我的网盘");
			}
		}
		view.putAttr("dir", dir);
		view.putAttr("cdirid", cdirid);
		return view.returnModelAndView(ThemesUtil.getThemePage("files-dir-info", request));
	}

	@RequestMapping("/manyFileInfo")
	public ModelAndView manyFileInfo(String fileids, HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		int dirnum = 0;
		int filenum = 0;
		for (String id : parseIds(fileids)) {
			if (id.toUpperCase().startsWith("FILE-")) {
				filenum++;
			}
			if (id.toUpperCase().startsWith("DIR-")) {
				dirnum++;
			}
		}
		view.putAttr("fileids", fileids);
		view.putAttr("dirNum", dirnum);
		view.putAttr("fileNum", filenum);
		return view.returnModelAndView(ThemesUtil.getThemePage("files-many-info", request));
	}

	/**
	 * ajax获取用户的全部分类
	 * 
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("/types")
	@ResponseBody
	public Map<String, Object> types(HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		view.putAttr("types", dirTypeServiceImpl.getAllDirs(getCurrentUser(session).getId()));
		return view.returnObjMode();
	}

	/**
	 * ajax获取用户的全部分类
	 * 
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("/move")
	@ResponseBody
	public Map<String, Object> move(String dirid, String toDirid, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			for (String id : parseIds(dirid)) {
				if (id.toUpperCase().startsWith("FILE-")) {
					id = id.replace("FILE-", "");
					wFileServiceImpl.moveToDir(id, toDirid);
				} else {
					id = id.replace("DIR-", "");
					dirTypeServiceImpl.moveType(getCurrentUser(session).getId(), id, toDirid);
				}
			}
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

}
