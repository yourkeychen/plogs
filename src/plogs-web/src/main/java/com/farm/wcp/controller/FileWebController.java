package com.farm.wcp.controller;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.ex.FileView;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.view.service.ViewTaskServiceInter;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;

/**
 * 文件
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/document")
@Controller
public class FileWebController extends WebUtils {
	private static final Logger log = Logger.getLogger(FileWebController.class);
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	@Resource
	private ViewTaskServiceInter viewTaskServiceImpl;

	/***
	 * 首页
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/uppage")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		return view.returnModelAndView(ThemesUtil.getThemePage("file-upload"));
	}

	/***
	 * 文件信息頁面
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/view")
	public ModelAndView view(String fileid, HttpServletRequest request, HttpSession session) {
		try {
			// fileid 为id或AppId
			ViewMode view = ViewMode.getInstance();
			String FileidByAppId = fileBaseServiceImpl.getFileIdByAppId(fileid);
			if (FileidByAppId == null) {
				FileView fileview = fileBaseServiceImpl.getFileView(fileid);
				view.putAttr("fileview", fileview);
			} else {
				FileView fileview = fileBaseServiceImpl.getFileView(FileidByAppId);
				view.putAttr("fileview", fileview);
			}
			return view.returnModelAndView(ThemesUtil.getThemePage("file-view"));
		} catch (FileNotFoundException e) {
			return ViewMode.getInstance().setError("id为" + fileid + "的文件不存在!", e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	/***
	 * 获得所有临时文件
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/temps")
	@ResponseBody
	public Map<String, Object> temps(int cpage, int size, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			List<FileView> fileViews = fileBaseServiceImpl.getTempFiles(cpage, size);
			view.putAttr("fileViews", fileViews);
			return view.returnObjMode();
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/***
	 * 将一个文件逻辑删除
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> del(String id, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			wdapFileServiceImpl.delFileByLogic(id);
			return view.returnObjMode();
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/***
	 * 将一个文件持久化
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/submit")
	@ResponseBody
	public Map<String, Object> submit(String id, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			wdapFileServiceImpl.submitFile(id);
			viewTaskServiceImpl.reLoadFileViewTask(id, getCurrentUser(session).getId(),
					getCurrentUser(session).getName());
			return view.returnObjMode();
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 获得持久化文件
	 * 
	 * @param id
	 * @param session
	 * @return
	 */
	@RequestMapping("/persists")
	@ResponseBody
	public Map<String, Object> persists(int cpage, int size, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			List<FileView> fileViews = fileBaseServiceImpl.getPresistFiles(cpage, size);
			view.putAttr("fileViews", fileViews);
			return view.returnObjMode();
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
}
