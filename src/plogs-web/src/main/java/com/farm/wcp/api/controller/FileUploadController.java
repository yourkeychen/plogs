package com.farm.wcp.api.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.farm.core.page.ViewMode;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.FileBase;
import com.farm.file.enums.FileModel;
import com.farm.file.exception.FileExNameException;
import com.farm.file.exception.FileSizeException;
import com.farm.file.util.FarmDocFiles;
import com.farm.file.util.FileCopyProcessCache;
import com.farm.view.WdapViewServiceInter;
import com.farm.web.WebUtils;

/**
 * 文件上传
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/upload")
@Controller
public class FileUploadController extends WebUtils {
	private static final Logger log = Logger.getLogger(FileUploadController.class);
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;

	@RequestMapping(value = "/PubUploadProcess.do")
	@ResponseBody
	public Map<String, Object> PubUploadProcess(String processkey, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		Integer process = 0;
		try {
			String filekey = session.getId() + processkey;
			process = FileCopyProcessCache.getProcess(filekey);
		} catch (Exception e) {
			view.setError(e.getMessage(), e);
		}
		return view.putAttr("process", process).returnObjMode();
	}

	@RequestMapping(value = "/logicDel.do")
	@ResponseBody
	public Map<String, Object> logicDel(String id, HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		wdapFileServiceImpl.delFileByLogic(id);
		return view.returnObjMode();
	}

	/**
	 * 上传附件文件
	 * 
	 * @param file
	 * @param limittypes
	 *            如果该参数不为空，则按照该参数验证文件类型，如果为空则按照默认配置验证参数
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/general.do")
	@ResponseBody
	public Map<String, Object> upload(@RequestParam(value = "file", required = false) MultipartFile file,
			String processkey, String type, HttpServletRequest request, HttpSession session) {
		FileBase filebase = null;
		String url = null;
		ViewMode view = ViewMode.getInstance();
		try {
			FileModel fileModel = getFileMOdel(type, file.getOriginalFilename());
			String filekey = session.getId() + processkey;
			filebase = wdapFileServiceImpl.saveLocalFile(file, fileModel, getCurrentUser(session), filekey);
			url = wdapFileServiceImpl.getIconUrl(filebase.getId());
			String filename = getUrlEncodeFileName(filebase.getTitle());
			String exName = FarmDocFiles.getExName(filename);
			if (wdapViewServiceImpl.fileConvertAble(exName, filebase.getFilesize())) {
				// 可以预览的话传出预览地址
				view.putAttr("viewurl", wdapViewServiceImpl.getViewUrl(filebase.getId()));
			} else {
				view.putAttr("viewurl", "none");
			}
			view.putAttr("downurl", wdapFileServiceImpl.getDownloadUrl(filebase.getId(), fileModel));
			view.putAttr("fileName", filename);
			view.putAttr("id", filebase.getId());
			view.putAttr("url", url);
		} catch (FileSizeException e) {
			view.setError("文件大小错误:" + e.getMessage(), e);
		} catch (FileExNameException e) {
			view.setError("文件类型错误:" + e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			view.setError(e.getMessage(), e);
		}
		return view.returnObjMode();
	}

	/**
	 * KindEditor上传附件文件
	 * 
	 * @param file
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/generalKindEditor.do")
	@ResponseBody
	public Map<String, Object> uploadKindeditor(
			@RequestParam(value = "imgFile", required = false) MultipartFile imgFile, String processkey,
			HttpServletRequest request, HttpSession session) {
		FileBase filebase = null;
		String url = null;
		ViewMode view = ViewMode.getInstance();
		try {
			FileModel fileModel = getFileMOdel(null, imgFile.getOriginalFilename());
			String filekey = session.getId() + processkey;
			filebase = wdapFileServiceImpl.saveLocalFile(imgFile, fileModel, getCurrentUser(session), filekey);
			url = wdapFileServiceImpl.getDownloadUrl(filebase.getId(), fileModel);
			String filename = getUrlEncodeFileName(filebase.getTitle());
			String exName = FarmDocFiles.getExName(filename);
			if (wdapViewServiceImpl.fileConvertAble(exName, filebase.getFilesize())) {
				// 可以预览的话传出预览地址
				view.putAttr("viewurl", wdapViewServiceImpl.getViewUrl(filebase.getId()));
			} else {
				view.putAttr("viewurl", "none");
			}
			view.putAttr("fileName", filename);
			view.putAttr("id", filebase.getId());
			view.putAttr("url", url);
			view.putAttr("error", 0);
		} catch (FileSizeException e) {
			view.putAttr("error", 1);
			view.putAttr("message", "文件大小错误 ：" + e.getMessage());
		} catch (FileExNameException e) {
			view.putAttr("error", 1);
			view.putAttr("message", "文件类型错误:" + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			view.putAttr("error", 1);
			view.putAttr("message", e.getMessage());
		}
		return view.returnObjMode();
	}

	/**
	 * KindEditor上传图片
	 * 
	 * @param imgFile
	 * @param processkey
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/generalKindEditorImg.do")
	@ResponseBody
	public Map<String, Object> generalKindEditorImg(
			@RequestParam(value = "imgFile", required = false) MultipartFile imgFile, String processkey,
			HttpServletRequest request, HttpSession session) {
		FileBase filebase = null;
		String url = null;
		ViewMode view = ViewMode.getInstance();
		try {
			FileModel fileModel = getFileMOdel("IMG", imgFile.getOriginalFilename());
			String filekey = session.getId() + processkey;
			filebase = wdapFileServiceImpl.saveLocalFile(imgFile, fileModel, getCurrentUser(session), filekey);
			url = wdapFileServiceImpl.getDownloadUrl(filebase.getId(), fileModel);
			String filename = getUrlEncodeFileName(filebase.getTitle());
			String exName = FarmDocFiles.getExName(filename);
			if (wdapViewServiceImpl.fileConvertAble(exName, filebase.getFilesize())) {
				// 可以预览的话传出预览地址
				view.putAttr("viewurl", wdapViewServiceImpl.getViewUrl(filebase.getId()));
			} else {
				view.putAttr("viewurl", "none");
			}
			view.putAttr("fileName", filename);
			view.putAttr("id", filebase.getId());
			view.putAttr("url", url);
			view.putAttr("error", 0);
		} catch (FileSizeException e) {
			view.putAttr("error", 1);
			view.putAttr("message", "文件大小错误 ：" + e.getMessage());
		} catch (FileExNameException e) {
			view.putAttr("error", 1);
			view.putAttr("message", "文件类型错误:" + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			view.putAttr("error", 1);
			view.putAttr("message", e.getMessage());
		}
		return view.returnObjMode();
	}

	/**
	 * 上传base64位图片
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/base64img")
	@ResponseBody
	public Map<String, Object> base64up(String base64, String filename, HttpSession session) {
		FileBase filebase = null;
		String url = null;
		ViewMode view = ViewMode.getInstance();
		try {
			base64 = base64.trim();
			if (base64.indexOf(",") > 0) {
				// data:image/png;base64,
				if (base64.indexOf("data:image/") == 0) {
					filename = filename.substring(0, filename.indexOf(".")) + "."
							+ base64.substring(base64.indexOf("/") + 1, base64.indexOf(";"));
				}
				base64 = base64.substring(base64.indexOf(",") + 1);
			}
			FileModel fileModel = getFileMOdel(null, filename);
			byte[] data = Base64.decodeBase64(base64);
			filebase = wdapFileServiceImpl.saveLocalFile(data, fileModel, filename, getCurrentUser(session));
			url = wdapFileServiceImpl.getDownloadUrl(filebase.getId(), fileModel);
			filename = getUrlEncodeFileName(filebase.getTitle());
			view.putAttr("fileName", filename);
			view.putAttr("id", filebase.getId());
			view.putAttr("url", url);
		} catch (FileSizeException e) {
			view.setError("文件大小错误:" + e.getMessage(), e);
		} catch (FileExNameException e) {
			view.setError("文件类型错误:" + e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			view.setError(e.getMessage(), e);
		}
		return view.returnObjMode();
	}

	// ---------------------------------------------------------------------------------------
	/**
	 * 获得url编码的中文文件名
	 * 
	 * @param fileName
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String getUrlEncodeFileName(String fileName) throws UnsupportedEncodingException {
		try {
			return URLEncoder.encode(fileName, "utf-8").replaceAll("\\+", "%20");
		} catch (Exception e) {
			return URLEncoder.encode(fileName, "utf-8");
		}
	}

	/**
	 * 獲得文件模型
	 * 
	 * @param fileModelName
	 *            文件模型的key
	 * @param fileName
	 *            文件名稱（有意義的那個名稱）
	 * @return
	 * @throws FileExNameException
	 */
	private FileModel getFileMOdel(String fileModelName, String fileName) throws FileExNameException {
		if (StringUtils.isBlank(fileModelName)) {
			return FileModel.getModelByFileExName(wdapFileServiceImpl.getExName(fileName));
		} else {
			return FileModel.getModel(fileModelName);
		}
	}

}
