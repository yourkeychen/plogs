package com.farm.wcp.controller;

import com.farm.wfs.domain.DirType;
import com.farm.wfs.domain.Wfile;
import com.farm.wfs.service.DirTypeServiceInter;
import com.farm.wfs.service.WfileServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;

import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;

import javax.servlet.http.HttpSession;
import com.farm.core.page.OperateType;
import com.farm.wcp.util.ThemesUtil;
import com.farm.core.page.ViewMode;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.enums.FileModel;
import com.farm.view.WdapViewServiceInter;
import com.farm.view.impl.WdapViewServiceImpl;
import com.farm.web.WebUtils;

/* *
 *功能：网盘文件控制层
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@RequestMapping("/wfile")
@Controller
public class WfsFileController extends WebUtils {
	private final static Logger log = Logger.getLogger(WfsFileController.class);
	@Resource
	private WfileServiceInter wFileServiceImpl;
	@Resource
	private DirTypeServiceInter dirTypeServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;

	/***
	 * 
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/search")
	public ModelAndView index(String word, HttpServletRequest request, HttpSession session) {
		List<DirType> dirs = null;
		List<Wfile> wfiles = null;
		if (StringUtils.isNotBlank(word)) {
			dirs = dirTypeServiceImpl.searchDirs(word, getCurrentUser(session).getId());
			wfiles = wFileServiceImpl.searchFiles(word, getCurrentUser(session).getId());
		}
		List<DirType> path = new ArrayList<>();
		DirType base = new DirType();
		base.setId("SEARCH");
		base.setName("检索文件: ' " + word + " '");
		path.add(base);
		ViewMode view = ViewMode.getInstance();
		view.putAttr("iconUrlBase", "download/Pubicon.do?id=");
		return view.putAttr("dirs", dirs).putAttr("word", word).putAttr("wfiles", wfiles).putAttr("cdir", base)
				.putAttr("path", path).returnModelAndView(ThemesUtil.getThemePage("files-dir", request));
	}

	/**
	 * 移动文件到文件夹
	 * 
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("/move")
	@ResponseBody
	public Map<String, Object> move(String wfileid, String toDirid, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			wFileServiceImpl.moveToDir(wfileid, toDirid);
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 查询结果集合
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addFile(String wdapfileids, String dirid, HttpServletRequest request,
			HttpSession session) {
		try {
			for (String wdapfileid : parseIds(wdapfileids)) {
				wFileServiceImpl.addFile(wdapfileid, dirid, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/reView")
	@ResponseBody
	public Map<String, Object> reView(String wdapfileid, HttpServletRequest request, HttpSession session) {
		try {
			wdapViewServiceImpl.reLoadFileViewTask(wdapfileid);
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/form")
	public ModelAndView edit(String wfileid, HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		Wfile wfile = wFileServiceImpl.getWfileEntity(wfileid);
		view.putAttr("wfile", wfile);
		return view.returnModelAndView(ThemesUtil.getThemePage("files-file-form", request));
	}

	/**
	 * 提交修改数据
	 * 
	 * @return
	 */
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Wfile entity, HttpSession session) {
		try {
			Wfile wfils = wFileServiceImpl.getWfileEntity(entity.getId());
			wfils.setTitle(entity.getTitle());
			entity = wFileServiceImpl.editWfileEntity(wfils, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 删除数据
	 * 
	 * @return
	 */
	@RequestMapping("/delWFile")
	@ResponseBody
	public Map<String, Object> delSubmit(String wfileids, HttpSession session) {
		try {
			for (String id : parseIds(wfileids)) {
				wFileServiceImpl.deleteWfile(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("wfs/WfileResult");
	}

	@RequestMapping("/fileInfo")
	public ModelAndView dirInfo(String wfileid, HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		Wfile wfile = wFileServiceImpl.getWfileEntity(wfileid);
		DecimalFormat fnum = new DecimalFormat("##0.00 ");
		long size1 = (Long) wfile.getFsize();
		float size1plus = ((float) size1) / (1025 * 1024);
		view.putAttr("sizeLable", fnum.format(size1plus) + "m");
		view.putAttr("wfile", wfile);
		view.putAttr("iconUrlBase", "download/Pubicon.do?id=");
		boolean isView = wdapViewServiceImpl.isView(wfile.getWdapfileid());
		view.putAttr("isView", isView);
		view.putAttr("viewUrl", wdapViewServiceImpl.getViewUrl(wfile.getWdapfileid()));
		try {
			view.putAttr("downUrl", wdapFileServiceImpl.getDownloadUrl(wfile.getWdapfileid(), FileModel.FILE));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return view.returnModelAndView(ThemesUtil.getThemePage("files-file-info", request));
	}

	@RequestMapping("/intoFile")
	public ModelAndView intoFile(String wfileid, HttpServletRequest request, HttpSession session)
			throws RemoteException, JSONException {
		ViewMode view = ViewMode.getInstance();
		Wfile wfile = wFileServiceImpl.getWfileEntity(wfileid);
		if (wdapViewServiceImpl.isView(wfile.getWdapfileid())) {
			// 預覽
			return view.returnRedirectOnlyUrl("/" + wdapViewServiceImpl.getViewUrl(wfile.getWdapfileid()));
		} else {
			// 下載
			return view.returnRedirectOnlyUrl(
					"/" + wdapFileServiceImpl.getDownloadUrl(wfile.getWdapfileid(), FileModel.FILE));
		}
	}
}
