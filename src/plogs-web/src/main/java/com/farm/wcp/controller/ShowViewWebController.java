package com.farm.wcp.controller;

import java.io.File;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.file.util.FarmDocFiles;
import com.farm.view.WdapViewServiceInter;
import com.farm.view.domain.ViewDoc;
import com.farm.view.service.ViewDocServiceInter;
import com.farm.view.service.ViewTaskServiceInter;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;

/**
 * 展示各种文件预览的页面
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/show")
@Controller
public class ShowViewWebController extends WebUtils {
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(ShowViewWebController.class);
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	@Resource
	private ViewTaskServiceInter viewTaskServiceImpl;
	@Resource
	private ViewDocServiceInter viewDocServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;

	/***
	 * 展示圖片冊
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/Pubimgs")
	public ModelAndView retask(String viewdocId, HttpServletRequest request, HttpSession session) {
		try {
			wdapViewServiceImpl.viewDocHandle(viewdocId);
			ViewMode view = ViewMode.getInstance();
			int fileSize = viewDocServiceImpl.getModelFileSize(viewdocId);
			view.putAttr("filesize", fileSize);
			view.putAttr("viewdocId", viewdocId);
			return view.returnModelAndView(ThemesUtil.getThemePage("show-imgs"));
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	/***
	 * 展示视频
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/PubVideos")
	public ModelAndView PubVideos(String viewdocId, HttpServletRequest request, HttpSession session) {
		try {
			wdapViewServiceImpl.viewDocHandle(viewdocId);
			ViewMode view = ViewMode.getInstance();
			int fileSize = viewDocServiceImpl.getModelFileSize(viewdocId);
			view.putAttr("filesize", fileSize);
			view.putAttr("viewdocId", viewdocId);
			return view.returnModelAndView(ThemesUtil.getThemePage("show-videos"));
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	/***
	 * 展示内部html
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/PubInnerHtml")
	public ModelAndView PubInnerHtml(String viewdocId, HttpServletRequest request, HttpSession session) {
		try {
			wdapViewServiceImpl.viewDocHandle(viewdocId);
			ViewMode view = ViewMode.getInstance();
			ViewDoc viewdoc = viewDocServiceImpl.getViewdocEntity(viewdocId);
			String path = fileBaseServiceImpl.getFileRealPath(viewdoc.getFileid());
			String ihtml = FarmDocFiles.readFileText(new File(path));
			view.putAttr("ihtml", ihtml);
			return view.returnModelAndView(ThemesUtil.getThemePage("show-innerHtml"));
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	/***
	 * 展示mp3
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/PubAudios")
	public ModelAndView PubAudios(String viewdocId, HttpServletRequest request, HttpSession session) {
		try {
			wdapViewServiceImpl.viewDocHandle(viewdocId);
			ViewMode view = ViewMode.getInstance();
			int fileSize = viewDocServiceImpl.getModelFileSize(viewdocId);
			view.putAttr("filesize", fileSize);
			view.putAttr("viewdocId", viewdocId);
			return view.returnModelAndView(ThemesUtil.getThemePage("show-audios"));
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}
}
