package com.farm.wcp.api.controller;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.farm.authority.FarmAuthorityService;
import com.farm.authority.domain.Organization;
import com.farm.authority.domain.User;
import com.farm.authority.password.PasswordProviderService;
import com.farm.authority.service.OrganizationServiceInter;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.auth.exception.LoginUserNoAuditException;
import com.farm.core.auth.exception.LoginUserNoExistException;
import com.farm.core.page.ViewMode;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.Results;
import com.farm.parameter.FarmParameterService;
import com.farm.web.WebUtils;

/**
 * 组织机构、 [创建、查询、更新、删除] 用户、 [创建、查询、更新、删除] ---------------------------- 知识接口[查询]、
 * 分类接口[查询]、 问答接口[查询]、
 * 
 * @author wangdong
 *
 */
@RequestMapping("/api")
@Controller
public class ApiController extends WebUtils {
	private final static Logger log = Logger.getLogger(ApiController.class);
	@Resource
	private OrganizationServiceInter organizationServiceImpl;
	@Resource
	private UserServiceInter userServiceImpl;

	/**
	 * 验证秘钥和用户信息
	 * 
	 * @param secret
	 */
	public static void checkAuth(String loginname, String plaintextPassword) {
		if (FarmParameterService.getInstance().getParameter("config.restful.debug").equals("true")) {
			return;
		}
		if (FarmParameterService.getInstance().getParameter("config.restful.secret.type").equals("complex")) {
			try {
				if (StringUtils.isBlank(loginname) || StringUtils.isBlank(plaintextPassword)) {
					throw new RuntimeException("loginname or password is null!");
				}
				if (!FarmAuthorityService.getInstance().isLegalityByInterUser(loginname,
						PasswordProviderService.getInstanceProvider().getClientPassword(plaintextPassword))) {
					throw new RuntimeException("user authentication is wrong! by verification error!");
				}
			} catch (LoginUserNoExistException e) {
				throw new RuntimeException("user authentication is wrong! by LoginUserNoExistException!");
			} catch (LoginUserNoAuditException e) {
				throw new RuntimeException("user authentication is wrong! by LoginUserNoAuditException!");
			}
		}
	}

	/**
	 * 验证秘钥
	 * 
	 * @param secret
	 */
	public static void checkSecret(String secret, HttpServletRequest request) {
		String ip = getCurrentIp(request);
		String ips = FarmParameterService.getInstance().getParameter("config.restful.whitelist.ips");
		if (StringUtils.isNotBlank(ips)) {
			List<String> ipWhiteList = WebUtils.parseIds(ips.replaceAll("，", ","));
			if (StringUtils.isBlank(ip) || !ipWhiteList.contains(ip)) {
				throw new RuntimeException("the ip is not exist white list:" + ip);
			}
		} else {
			throw new RuntimeException("the ip white list is blank!");
		}

		String state = FarmParameterService.getInstance().getParameter("config.restful.state");
		if (!state.toLowerCase().equals("true")) {
			throw new RuntimeException("the api is unable,check config file please!");
		}
		if (secret == null) {
			if (FarmParameterService.getInstance().getParameter("config.restful.debug").equals("true")) {
				return;
			} else {
				throw new RuntimeException("secret not exist!");
			}
		}
		if (!FarmParameterService.getInstance().getParameter("config.restful.secret.type").equals("none")) {
			log.info("have received remoteSecret:" + secret);
			// 从配置文件中读取秘钥
			String sysKey = FarmParameterService.getInstance().getParameter("config.restful.secret.key").trim();
			if (!secret.trim().equals(sysKey)) {
				if (!FarmParameterService.getInstance().getParameter("config.restful.debug").equals("true")) {
					throw new RuntimeException("secret error!");
				}
			}
		}
	}

	/**
	 * 获得当前用户
	 * 
	 * @return
	 */
	private LoginUser getUser() {
		return new LoginUser() {
			@Override
			public String getName() {
				return "restful";
			}

			@Override
			public String getLoginname() {
				return "restful";
			}

			@Override
			public String getId() {
				return "restful";
			}

			@Override
			public String getType() {
				return "NONE";
			}

			@Override
			public String getIp() {
				return "NONE";
			}
		};
	}

	/**
	 * 获得当前用户
	 * 
	 * @return
	 */
	private LoginUser getUser(String loginname) {
		if (StringUtils.isBlank(loginname)) {
			return getUser();
		}
		return FarmAuthorityService.getInstance().getUserByLoginName(loginname);
	}

	/**
	 * 查询所有组织机构
	 * 
	 * @param ID
	 * @param NAME
	 * @param PARENTID
	 * @param APPID
	 * @param session
	 * @return
	 */
	@RequestMapping("/get/organization")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getOrganization(String id, String name, String parentid, String appid,
			String secret, String operatorLoginname, String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:查询组织机构");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			DataQuery query = DataQuery.getInstance();
			// --------------------------------------------
			// ------------------------------------------
			if (StringUtils.isNotBlank(id)) {
				query.addRule(new DBRule("id", id, "="));
			}
			if (StringUtils.isNotBlank(name)) {
				query.addRule(new DBRule("name", name, "="));
			}
			if (StringUtils.isNotBlank(parentid)) {
				query.addRule(new DBRule("parentid", parentid, "="));
			}
			if (StringUtils.isNotBlank(appid)) {
				query.addRule(new DBRule("appid", appid, "="));
			}
			query.setPagesize(10000);
			query.addRule(new DBRule("state", "1", "="));
			DataQuery dbQuery = DataQuery.init(query, "ALONE_AUTH_ORGANIZATION",
					"ID,TYPE,SORT,PARENTID,MUSER,CUSER,STATE,UTIME,CTIME,COMMENTS,NAME,TREECODE,APPID");
			DataResult result = dbQuery.search();
			// ------------------------------------------
			// ------------------------------------------
			Results resultObj = Results.getResults(result.getResultList(), result.getTotalSize(),
					result.getCurrentPage(), result.getPageSize());
			return ViewMode.getInstance().putAttr("DATA", resultObj).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得UTC时间
	 *
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping(value = "/secret/utc")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> utc(HttpSession session) {
		log.info("restful API:获得UTC时间，用于制作权限码");
		try {
			Calendar cal = Calendar.getInstance();
			return ViewMode.getInstance().putAttr("UTC", cal.getTimeInMillis()).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 创建组织机构
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/post/organization")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> postOrganization(String parentid, String sort, String name,
			String comments, String secret, String operatorLoginname, String operatorPassword,
			HttpServletRequest request) {
		try {
			log.info("restful API:创建组织机构");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			Organization entity = new Organization();
			if (StringUtils.isNotBlank(parentid)) {
				entity.setParentid(parentid);
			}
			entity.setSort(Integer.valueOf(sort));
			entity.setName(name);
			entity.setComments(comments);
			entity.setType("1");
			entity = organizationServiceImpl.insertOrganizationEntity(entity, getUser(operatorLoginname));
			return ViewMode.getInstance().putAttr("ID", entity.getId()).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 校验用户密码
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/post/isLegality")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> isLegality(String loginname, String password, String secret,
			String operatorLoginname, String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:验证用户的密码");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			boolean isTrue1 = FarmAuthorityService.getInstance().isLegalityByInterUser(loginname, password);
			boolean isTrue2 = FarmAuthorityService.getInstance().isLegalityByInterUser(loginname,
					PasswordProviderService.getInstanceProvider().getClientPassword(password));
			return ViewMode.getInstance().putAttr("DATA", isTrue1 || isTrue2).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得用户信息
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/get/uinfo")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> uinfo(String loginname, String secret, String operatorLoginname,
			String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:获得用户信息");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			LoginUser userinfo = FarmAuthorityService.getInstance().getUserByLoginName(loginname);
			return ViewMode.getInstance().putAttr("DATA", userinfo).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 更新组织机构
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/put/organization")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> putOrganization(String id, String sort, String name, String comments,
			String secret, String operatorLoginname, String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:修改组织机构");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			Organization entity = organizationServiceImpl.getOrganizationEntity(id);
			if (StringUtils.isNotBlank(sort)) {
				entity.setSort(Integer.valueOf(sort));
			}
			if (StringUtils.isNotBlank(name)) {
				entity.setName(name);
			}
			if (StringUtils.isNotBlank(comments)) {
				entity.setComments(comments);
			}
			entity = organizationServiceImpl.editOrganizationEntity(entity, getUser(operatorLoginname));
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 删除组织机构
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/delete/organization")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> delOrganization(String id, String secret, String operatorLoginname,
			String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:删除组织机构");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			organizationServiceImpl.deleteOrganizationEntity(id, getUser(operatorLoginname));
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 查询所有用户
	 * 
	 * @param ID
	 * @param NAME
	 * @param PARENTID
	 * @param APPID
	 * @param session
	 * @return
	 */
	@RequestMapping("/get/user")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getUser(String id, String loginname, String type, String state,
			String orgid, String secret, String operatorLoginname, String operatorPassword,
			HttpServletRequest request) {
		try {
			log.info("restful API:查询用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			DataQuery query = DataQuery.getInstance();
			// --------------------------------------------
			// ------------------------------------------
			if (StringUtils.isNotBlank(id)) {
				query.addRule(new DBRule("USER.ID", id, "="));
			}
			if (StringUtils.isNotBlank(loginname)) {
				query.addRule(new DBRule("USER.LOGINNAME", loginname, "="));
			}
			if (StringUtils.isNotBlank(type)) {
				query.addRule(new DBRule("USER.TYPE", type, "="));
			}
			if (StringUtils.isNotBlank(state)) {
				query.addRule(new DBRule("USER.STATE", state, "="));
			} else {
				query.addRule(new DBRule("USER.STATE", "1", "="));
			}
			if (StringUtils.isNotBlank(orgid)) {
				query.addRule(new DBRule("RFORG.ORGANIZATIONID", orgid, "="));
			}
			query.setPagesize(10000);
			DataQuery dbQuery = DataQuery.init(query,
					"ALONE_AUTH_USER USER left join ALONE_AUTH_USERORG RFORG on USER.ID=RFORG.USERID",
					"USER.ID as ID,USER.NAME as NAME,RFORG.ORGANIZATIONID as ORGANIZATIONID,USER.COMMENTS as COMMENTS,USER.TYPE as TYPE,USER.LOGINNAME as LOGINNAME,USER.IMGID as IMGID,USER.STATE as STATE");
			DataResult result = dbQuery.search();
			// ------------------------------------------
			// ------------------------------------------
			Results resultObj = Results.getResults(result.getResultList(), result.getTotalSize(),
					result.getCurrentPage(), result.getPageSize());
			return ViewMode.getInstance().putAttr("DATA", resultObj).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 创建用户
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/post/user")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> postUser(String name, String loginname, String comments, String orgid,
			String secret, String operatorLoginname, String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:创建用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			User entity = new User();
			entity.setLoginname(loginname);
			entity.setName(name);
			if (StringUtils.isNotBlank(comments)) {
				entity.setComments(comments);
			}
			entity.setType("1");
			entity.setState("1");
			entity = userServiceImpl.insertUserEntity(entity, getUser(operatorLoginname));
			if (StringUtils.isNotBlank(orgid)) {
				userServiceImpl.setUserOrganization(entity.getId(), orgid, getUser());
			}
			return ViewMode.getInstance().putAttr("ID", entity.getId()).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 更新用户
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/put/user")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> putUser(String id, String name, String loginname, String comments,
			String orgid, String secret, String operatorLoginname, String operatorPassword,
			HttpServletRequest request) {
		try {
			log.info("restful API:修改用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			User entity = userServiceImpl.getUserEntity(id);
			if (StringUtils.isNotBlank(name)) {
				entity.setName(name);
			}
			if (StringUtils.isNotBlank(loginname)) {
				entity.setLoginname(loginname);
			}
			if (StringUtils.isNotBlank(comments)) {
				entity.setComments(comments);
			}
			entity = userServiceImpl.editUserEntity(entity, getUser(operatorLoginname));
			if (StringUtils.isNotBlank(orgid)) {
				userServiceImpl.setUserOrganization(id, orgid, getUser(operatorLoginname));
			}
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 删除用户
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/delete/user")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> delUser(String id, String secret, String operatorLoginname,
			String operatorPassword, HttpServletRequest request) {
		try {
			log.info("restful API:删除用户");
			checkSecret(secret, request);
			checkAuth(operatorLoginname, operatorPassword);
			userServiceImpl.deleteUserEntity(id, getUser(operatorLoginname));
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}
}
