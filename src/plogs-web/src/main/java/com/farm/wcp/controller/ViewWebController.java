package com.farm.wcp.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.FileText;
import com.farm.file.domain.ex.FileView;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.file.service.FileTextServiceInter;
import com.farm.view.domain.ViewConvertor;
import com.farm.view.domain.ViewFileModel;
import com.farm.view.domain.ViewLog;
import com.farm.view.domain.ViewTask;
import com.farm.view.service.ViewConvertorServiceInter;
import com.farm.view.service.ViewDocServiceInter;
import com.farm.view.service.ViewFileModelServiceInter;
import com.farm.view.service.ViewLogServiceInter;
import com.farm.view.service.ViewRegularServiceInter;
import com.farm.view.service.ViewTaskServiceInter;
import com.farm.view.task.WdapTaskDispatcherService;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;

/**
 * 预览相关
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/docview")
@Controller
public class ViewWebController extends WebUtils {
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(ViewWebController.class);
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	@Resource
	private ViewTaskServiceInter viewTaskServiceImpl;
	@Resource
	private ViewDocServiceInter viewDocServiceImpl;
	@Resource
	private ViewLogServiceInter viewLogServiceImpl;
	@Resource
	private ViewFileModelServiceInter viewModelServiceImpl;
	@Resource
	private ViewConvertorServiceInter viewConvertorServiceImpl;
	@Resource
	private FileTextServiceInter fileTextServiceImpl;
	@Resource
	private ViewRegularServiceInter viewRegularServiceImpl;

	/***
	 * 文件系統首頁
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/Pubindex")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		boolean isDispatcherLive = WdapTaskDispatcherService.getInstance().isAlive();
		view.putAttr("dispatcherlive", isDispatcherLive);
		List<Map<String, Object>> regulars = viewRegularServiceImpl.getAllRegular();
		view.putAttr("regulars", regulars);
		return view.returnModelAndView("/web-simple/viewer/index");
	}

	/**
	 * 启动调度器
	 * 
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("/dispatcherStart")
	public ModelAndView dispatcherStart(HttpServletRequest request, HttpSession session) {
		WdapTaskDispatcherService.getInstance().start();
		return ViewMode.getInstance().returnRedirectOnlyUrl("/docview/Pubindex.do");
	}

	/***
	 * 生成预览任务
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/retask")
	public ModelAndView retask(String fileid, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			viewTaskServiceImpl.reLoadFileViewTask(fileid, getCurrentUser(session).getId(),
					getCurrentUser(session).getName());
			view.putAttr("fileid", fileid);
			return view.returnRedirectUrl("/docview/info.do");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	@RequestMapping("/info")
	public ModelAndView info(String fileid, HttpServletRequest request, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			FileView fileview = fileBaseServiceImpl.getFileView(fileid);
			view.putAttr("fileview", fileview);
			List<Map<String, Object>> viewDocs = viewDocServiceImpl.getFileViewdocs(fileid);
			view.putAttr("viewdocs", viewDocs);
			List<Map<String, Object>> viewTasks = viewTaskServiceImpl.getTasksByFileid(fileid);
			view.putAttr("viewtasks", viewTasks);
			FileText text = fileTextServiceImpl.getFiletextByFileId(fileid);
			view.putAttr("filetext", text);
			return view.returnModelAndView(ThemesUtil.getThemePage("view-fileinfo"));
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	@RequestMapping("/task")
	public ModelAndView task(String taskid, HttpServletRequest request, HttpSession session) {
		try {
			////////// -!!!!!!!!!!!!!!!!未完成，要实现在界面点击查看任务信息时展示，任务信息以及任务的日志信息.
			ViewMode view = ViewMode.getInstance();
			ViewTask task = viewTaskServiceImpl.getViewtaskEntity(taskid);
			List<ViewLog> logs = viewLogServiceImpl.getlogsByTaskid(task.getId());
			FileView fileview = fileBaseServiceImpl.getFileView(task.getFileid());
			ViewConvertor convertor = viewConvertorServiceImpl.getViewconvertorEntity(task.getConvertorid());
			ViewFileModel tmodel = viewModelServiceImpl.getViewmodelEntity(convertor.getTmodelid());
			ViewFileModel omodel = viewModelServiceImpl.getViewmodelEntity(convertor.getOmodelid());
			view.putAttr("tmodel", tmodel);
			view.putAttr("omodel", omodel);
			view.putAttr("convertor", convertor);
			view.putAttr("fileview", fileview);
			view.putAttr("task", task);
			view.putAttr("logs", logs);
			return view.returnModelAndView(ThemesUtil.getThemePage("view-taskinfo"));
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

}
