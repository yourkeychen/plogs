package com.farm.wcp.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.farm.authority.service.UserServiceInter;
import com.farm.core.page.ViewMode;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.FileBase;
import com.farm.parameter.FarmParameterService;
import com.farm.web.WebUtils;

/**
 * 文件服务接口
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/fileapi")
@Controller
public class FileApiController extends WebUtils {
	private static final Logger log = Logger.getLogger(FileApiController.class);
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private UserServiceInter userServiceImpl;

	@RequestMapping("/file/property")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> getProperty(String operatorLoginname, String operatorPassword,
			String secret, HttpServletRequest request) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("FILESIZE", FarmParameterService.getInstance().getParameterLong("config.doc.upload.length.max"));
			map.put("IMGSIZE", FarmParameterService.getInstance().getParameterLong("config.doc.img.upload.length.max"));
			map.put("FILETYPE", FarmParameterService.getInstance().getParameter("config.doc.upload.types"));
			map.put("IMGTYPE",FarmParameterService.getInstance().getParameter("config.doc.img.upload.types"));
			map.put("MEDIATYPE", FarmParameterService.getInstance().getParameter("config.doc.media.upload.types"));
			return ViewMode.getInstance().putAttr("DATA", map).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得一个附件空间
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/file/init")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> initFileSpace(String operatorLoginname, String operatorPassword,
			String filename, Long length, String appid, String secret, HttpServletRequest request) {
		try {
			log.info("restful API:获得一个附件空间" + length);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			FileBase file = wdapFileServiceImpl.initFile(userServiceImpl.getUserByLoginName(operatorLoginname),
					filename, length, appid, operatorLoginname);
			String realpath = wdapFileServiceImpl.getFileRealPath(file.getId());
			return ViewMode.getInstance().putAttr("realpath", realpath).putAttr("fileid", file.getId())
					.returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 兑换一个AppId为fileId
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/appid/tofileid")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> byAppid(String operatorLoginname, String operatorPassword, String appid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:兑换一个AppId为fileId,appid=" + appid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			return ViewMode.getInstance().putAttr("DATA", wdapFileServiceImpl.getFileIdByAppId(appid)).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 提交一個臨時文件為持久化文件
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/file/submit")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> submit(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:提交持久化一個文件" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			wdapFileServiceImpl.submitFile(fileid);
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 將一個持久化文件轉爲臨時文件
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/file/free")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> free(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:釋放一個文件為臨時文件" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			wdapFileServiceImpl.freeFile(fileid);
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 逻辑删除臨時文件
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/file/del")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> del(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:邏輯刪除一個文件" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			wdapFileServiceImpl.delFileByLogic(fileid);
			return ViewMode.getInstance().returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 獲得文件信息
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/file/info")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> info(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:獲得文件信息" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			return ViewMode.getInstance().putAttr("info", wdapFileServiceImpl.getFileInfo(fileid)).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得图标下载地址
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/url/icon")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> icon(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:获得图标下载地址" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			return ViewMode.getInstance().putAttr("url", wdapFileServiceImpl.getIconUrl(fileid)).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得文件下载地址
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/url/download")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> download(String operatorLoginname, String operatorPassword,
			String fileid, String secret, HttpServletRequest request) {
		try {
			log.info("restful API:获得文件下载地址" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			return ViewMode.getInstance()
					.putAttr("url",
							wdapFileServiceImpl.getDownloadUrl(fileid, wdapFileServiceImpl.getFileModel(fileid)))
					.returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}

	/**
	 * 获得文件的文本信息
	 * 
	 * @param session
	 * @return Map<String,Object>
	 */
	@RequestMapping("/text")
	@ResponseBody
	public ResponseEntity<Map<String, Object>> text(String operatorLoginname, String operatorPassword, String fileid,
			String secret, HttpServletRequest request) {
		try {
			log.info("restful API:获得文件的文本信息" + fileid);
			ApiController.checkSecret(secret, request);
			ApiController.checkAuth(operatorLoginname, operatorPassword);
			return ViewMode.getInstance().putAttr("text", wdapFileServiceImpl.getFiletext(fileid)).returnJsonMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnJsonMode();
		}
	}
}
