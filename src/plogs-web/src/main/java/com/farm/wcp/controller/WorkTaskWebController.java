package com.farm.wcp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.FarmAuthorityService;
import com.farm.authority.domain.User;
import com.farm.core.page.ViewMode;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.time.TimeTool;
import com.farm.project.domain.Company;
import com.farm.project.domain.Field;
import com.farm.project.domain.Project;
import com.farm.project.domain.ProjectFieldins;
import com.farm.project.domain.ProjectGradation;
import com.farm.project.domain.ProjectType;
import com.farm.project.domain.Task;
import com.farm.project.domain.TaskLog;
import com.farm.project.domain.TaskType;
import com.farm.project.domain.Taskuser;
import com.farm.project.domain.UserView;
import com.farm.project.domain.ex.ProjectField;
import com.farm.project.service.CompanyServiceInter;
import com.farm.project.service.FieldServiceInter;
import com.farm.project.service.ProjectFieldinsServiceInter;
import com.farm.project.service.ProjectGradationServiceInter;
import com.farm.project.service.ProjectServiceInter;
import com.farm.project.service.ProjectTypeServiceInter;
import com.farm.project.service.ProjectuserServiceInter;
import com.farm.project.service.TaskServiceInter;
import com.farm.project.service.TaskTypeServiceInter;
import com.farm.project.service.TaskuserServiceInter;
import com.farm.project.service.UserViewServiceInter;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;

/**
 * 工作视图(单视图集成模式)
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/worktask")
@Controller
public class WorkTaskWebController extends WebUtils {
	private static final Logger log = Logger.getLogger(WorkTaskWebController.class);
	@Resource
	private ProjectServiceInter projectServiceImpl;
	@Resource
	private ProjectTypeServiceInter projectTypeServiceImpl;
	@Resource
	private ProjectGradationServiceInter projectGradationServiceImpl;
	@Resource
	private TaskServiceInter taskServiceImpl;
	@Resource
	private TaskTypeServiceInter taskTypeServiceImpl;
	@Resource
	private ProjectFieldinsServiceInter projectFieldinsServiceImpl;
	@Resource
	private FieldServiceInter fieldServiceImpl;
	@Resource
	private UserViewServiceInter userViewServiceImpl;
	@Resource
	private CompanyServiceInter companyServiceImpl;
	@Resource
	private ProjectuserServiceInter projectUserServiceImpl;
	@Resource
	private TaskuserServiceInter taskUserServiceImpl;

	/***
	 * 工作首页
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		view.putAttr("MODEL", "TASTVIEW");
		int num1 = taskServiceImpl.getTaskCount(getCurrentUser(session), "1");
		int num2 = taskServiceImpl.getTaskCount(getCurrentUser(session), "2");
		int num3 = taskServiceImpl.getTaskCount(getCurrentUser(session), "3");
		int num4 = taskServiceImpl.getTaskCount(getCurrentUser(session), "4");
		int num6 = taskServiceImpl.getTaskCount(getCurrentUser(session), "6");
		view.putAttr("num1", num1);
		view.putAttr("num2", num2);
		view.putAttr("num3", num3);
		view.putAttr("num4", num4);
		view.putAttr("num6", num6);
		view.putAttr("today", TimeTool.getTimeDate14());
		return view.returnModelAndView(ThemesUtil.getThemePage("plogs-index", request));
	}

	/***
	 * 获取用户所有任务
	 * 
	 * @param title   项目名称（模糊查询）
	 * @param states  （任务状态，支持多个逗号分隔）
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadTask")
	@ResponseBody
	public Map<String, Object> loadTask(String title, String states, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			if (StringUtils.isBlank(states)) {
				states = "1,2,3,4";
			}
			List<Map<String, Object>> tasks = taskServiceImpl.getTasks(getCurrentUser(session), states, title);
			view.putAttr("tasks", tasks);
			return view.returnObjMode();
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/***
	 * 更新任務狀態
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/updateTaskState")
	@ResponseBody
	public Map<String, Object> updateTaskState(String taskId, String state, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			taskServiceImpl.editTaskState(taskId, state, getCurrentUser(session));
			return view.returnObjMode();
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/***
	 * 提交任務（工作狀態）
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/submitTask")
	@ResponseBody
	public Map<String, Object> submitTask(String taskId, Integer completion, Integer dotimes, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			Task task = taskServiceImpl.getTaskEntity(taskId);
			task.setCompletion(completion);
			task.setDotimes(dotimes);
			task.setUserid(getCurrentUser(session).getId());
			taskServiceImpl.editTaskEntity(task, getCurrentUser(session));
			if (completion != null && completion >= 100) {
				taskServiceImpl.editTaskState(taskId, "4", getCurrentUser(session));
			}
			return view.returnObjMode();
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/***
	 * 关闭任务
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/closeTask")
	@ResponseBody
	public Map<String, Object> closeTask(String taskId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			taskServiceImpl.closeTask(taskId, getCurrentUser(session));
			return view.returnObjMode();
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/***
	 * 打开任务窗口(任务菜单)
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/openTaskWin")
	public ModelAndView openTaskWin(String taskId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			{
				// 处理关系人
				List<Map<String, Object>> taskusers = taskUserServiceImpl.getTaskUsers(taskId);
				List<Map<String, Object>> doUsers = new ArrayList<>();
				List<Map<String, Object>> managerUsers = new ArrayList<>();
				for (Map<String, Object> user : taskusers) {
					String poptype = (String) user.get("POPTYPE");
					if (poptype.equals("1")) {
						managerUsers.add(user);
					}
					if (poptype.equals("2")) {
						doUsers.add(user);
					}
				}
				view.putAttr("doUsers", doUsers.size() > 0 ? doUsers : managerUsers);
				view.putAttr("managerUsers", managerUsers);
				view.putAttr("isDoUser", taskUserServiceImpl.isDoUser(taskId, getCurrentUser(session)));
				view.putAttr("isManagerUser", taskUserServiceImpl.isManagerUser(taskId, getCurrentUser(session)));
			}

			Task task = taskServiceImpl.getTaskEntity(taskId);
			view.putAttr("task", task);
			// 获得任务项目名称
			Project project = projectServiceImpl.getProjectEntity(task.getProjectid());
			view.putAttr("project", project);
			Company company = companyServiceImpl.getProjectCompany(project.getId());
			view.putAttr("company", company);
			if (StringUtils.isNotBlank(task.getReadfileid())) {
				String text = taskServiceImpl.readText(task.getReadfileid());
				view.putAttr("text", text);
			}
			List<TaskLog> logs = taskServiceImpl.getLogs(taskId);
			view.putAttr("logs", logs);
			view.putAttr("user", FarmAuthorityService.getInstance().getUserById(task.getUserid()));
			if (task.getPstate().equals("6")) {
				List<UserView> views = userViewServiceImpl.getUserViews(getCurrentUser(session));
				Collections.sort(views, new Comparator<UserView>() {
					@Override
					public int compare(UserView o1, UserView o2) {
						return o1.getSort() - o2.getSort();
					}
				});
				view.putAttr("views", views);
			}
			return view.returnModelAndView("/web-simple/worktask/commons/includeTaskMenuWinLoader");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeTaskMenuWinLoader");
		}
	}

	/***
	 * 打开任务窗口
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/taskDoPage")
	public ModelAndView taskDoPage(String taskId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			taskServiceImpl.editTaskState(taskId, "3", getCurrentUser(session));
			Task task = taskServiceImpl.getTaskEntity(taskId);
			view.putAttr("task", task);
			// 获得任务项目名称
			Project project = projectServiceImpl.getProjectEntity(task.getProjectid());
			view.putAttr("project", project);
			Company company = companyServiceImpl.getProjectCompany(project.getId());
			view.putAttr("company", company);
			if (StringUtils.isNotBlank(task.getReadfileid())) {
				String text = taskServiceImpl.readText(task.getReadfileid());
				view.putAttr("text", text);
			}
			List<TaskLog> logs = taskServiceImpl.getLogs(taskId);
			TaskLog clog = taskServiceImpl.getTodayLog(taskId);
			view.putAttr("clog", clog);
			if (clog != null) {
				view.putAttr("clogtext", taskServiceImpl.readText(clog.getFileid()));
			}
			view.putAttr("today", TimeTool.getTimeDate14());
			view.putAttr("logs", logs);
			view.putAttr("user", FarmAuthorityService.getInstance().getUserById(task.getUserid()));
			return view.returnModelAndView("/web-simple/worktask/taskIndex");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	/**
	 * 加载当前任务信息界面
	 * 
	 * @param taskId
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadCurrentTaskWin")
	public ModelAndView loadCurrentTaskWin(String taskId, HttpSession session) {
		try {
			if (StringUtils.isBlank(taskId)) {
				return ViewMode.getInstance()
						.returnModelAndView("/web-simple/worktask/commons/includeCurrentTaskInfoBoxLoader");
			}
			ViewMode view = ViewMode.getInstance();
			Task task = taskServiceImpl.getTaskEntity(taskId);
			String text = taskServiceImpl.getlog(task.getId());
			Project project = projectServiceImpl.getProjectEntity(task.getProjectid());
			view.putAttr("text", text);
			view.putAttr("task", task);
			view.putAttr("project", project);
			return view.returnModelAndView("/web-simple/worktask/commons/includeCurrentTaskInfoBoxLoader");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeCurrentTaskInfoBoxLoader");
		}
	}

	/**
	 * 加载屬性編輯窗口
	 * 
	 * @param taskId
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadFieldFormWin")
	public ModelAndView loadFieldFormWin(String uuid, String taskid, String projectid, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			Field field = fieldServiceImpl.getFieldByUUID(uuid);
			if (StringUtils.isNotBlank(taskid)) {
				projectid = taskServiceImpl.getTaskEntity(taskid).getProjectid();
			}
			if (field.getSingletype().endsWith("1")) {
				// 单条
				ProjectFieldins fieldins = projectFieldinsServiceImpl.getProjectFieldins(uuid, projectid);
				return view.returnRedirectUrl("/worktask/loadFieldInsFormWin.do?fieldinsId=" + fieldins.getId()
						+ "&taskid=" + taskid + "&projectid=" + projectid);
			} else {
				// 多条
				view.putAttr("field", field);
				view.putAttr("fieldines", projectFieldinsServiceImpl.getProjectFieldines(uuid, projectid));
				view.putAttr("taskid", taskid);
				view.putAttr("projectid", projectid);
				if (StringUtils.isNotBlank(taskid)) {
					view.putAttr("isEdit", fieldServiceImpl.isEditByTask(uuid, taskid));
				} else {
					view.putAttr("isEdit", true);
				}
				return view.returnModelAndView("/web-simple/worktask/commons/includeManyFieldEditWinLoader");
			}
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeOneFieldEditWinLoader");
		}
	}

	@RequestMapping("/addFieldIns")
	public ModelAndView addFieldIns(String fieldUuid, String taskid, String projectid, HttpSession session) {
		try {
			Field field = fieldServiceImpl.getFieldByUUID(fieldUuid);
			projectFieldinsServiceImpl.addFildins(field, taskid, projectid, getCurrentUser(session));
			return ViewMode.getInstance().returnRedirectUrl("/worktask/loadFieldFormWin.do?uuid=" + field.getUuid()
					+ "&projectid=" + projectid + "&taskid=" + (StringUtils.isBlank(taskid) ? "" : taskid));
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeOneFieldEditWinLoader");
		}
	}

	@RequestMapping("/delFieldIns")
	public ModelAndView delFieldIns(String fieldinsId, String taskid, String projectid, HttpSession session) {
		try {
			ProjectFieldins projectfieldins = projectFieldinsServiceImpl.getProjectfieldinsEntity(fieldinsId);
			projectFieldinsServiceImpl.deleteProjectfieldinsFromProject(projectfieldins.getId(),
					projectfieldins.getProjectid(), getCurrentUser(session));
			Field field = fieldServiceImpl.getFieldByUUID(projectfieldins.getUuid());
			return ViewMode.getInstance().returnRedirectUrl("/worktask/loadFieldFormWin.do?uuid=" + field.getUuid()
					+ "&projectid=" + projectid + "&taskid=" + (StringUtils.isBlank(taskid) ? "" : taskid));
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeOneFieldEditWinLoader");
		}
	}

	@RequestMapping("/loadFieldInsFormWin")
	public ModelAndView loadFieldInsFormWin(String fieldinsId, String projectid, String taskid, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			ProjectFieldins fieldins = projectFieldinsServiceImpl.getProjectfieldinsEntity(fieldinsId);
			view.putAttr("fieldins", fieldins);
			view.putAttr("taskid", taskid);
			view.putAttr("projectid", projectid);
			if (StringUtils.isNotBlank(fieldins.getEnums())) {
				view.putAttr("enums", projectFieldinsServiceImpl.getFormatEnums(fieldins.getEnums()));
			}
			return view.returnModelAndView("/web-simple/worktask/commons/includeOneFieldEditWinLoader");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeOneFieldEditWinLoader");
		}
	}

	/**
	 * 加载当前任务属性
	 * 
	 * @param taskId
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadTaskFieldWin")
	public ModelAndView loadTaskFieldWin(String taskId, HttpSession session) {
		try {
			if (StringUtils.isBlank(taskId)) {
				return ViewMode.getInstance()
						.returnModelAndView("/web-simple/worktask/commons/includeTaskFieldLoadBoxLoader");
			}
			List<ProjectField> fields = projectFieldinsServiceImpl.getTaskFields(taskId);
			ViewMode view = ViewMode.getInstance();
			view.putAttr("fields", fields);
			view.putAttr("taskId", taskId);
			return view.returnModelAndView("/web-simple/worktask/commons/includeTaskFieldLoadBoxLoader");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeTaskFieldLoadBoxLoader");
		}
	}

	/***
	 * 添加|修改任务表单
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadTaskFormWin")
	public ModelAndView loadTaskFormWin(String name, String taskId, String states, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			if (StringUtils.isNotBlank(taskId)) {
				// 修改
				Task task = taskServiceImpl.getTaskEntity(taskId);
				Project project = projectServiceImpl.getProjectEntity(task.getProjectid());
				view.putAttr("task", task);
				view.putAttr("cproject", project);
				if (StringUtils.isNotBlank(task.getReadfileid())) {
					view.putAttr("text", taskServiceImpl.readText(task.getReadfileid()));
				}
				ProjectType type = projectTypeServiceImpl.getProjecttypeEntity(project.getTypeid());
				List<ProjectGradation> gradations = projectGradationServiceImpl.getGradationsByTypeid(type.getId());
				view.putAttr("gradations", gradations);
				List<TaskType> taskTypes = projectGradationServiceImpl.getTaskTypes(task.getGradationid());
				view.putAttr("taskTypes", taskTypes);
			} else {
				// 添加
			}
			List<Project> projects = projectServiceImpl.getliveProjects(getCurrentUser(session), name);
			view.putAttr("states", states);
			view.putAttr("projects", projects);
			return view.returnModelAndView("/web-simple/worktask/commons/includeAddTaskWinLoader");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeAddTaskWinLoader");
		}
	}

	/***
	 * 任务信息中的任务修改-只修改部分屬性（進入修改表單）
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadEditTaskFormWin")
	public ModelAndView loadEditTaskFormWin(String taskId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			// 修改
			Task task = taskServiceImpl.getTaskEntity(taskId);
			Project project = projectServiceImpl.getProjectEntity(task.getProjectid());
			view.putAttr("task", task);
			view.putAttr("cproject", project);
			if (StringUtils.isNotBlank(task.getReadfileid())) {
				view.putAttr("text", taskServiceImpl.readText(task.getReadfileid()));
			}
			ProjectType type = projectTypeServiceImpl.getProjecttypeEntity(project.getTypeid());
			List<ProjectGradation> gradations = projectGradationServiceImpl.getGradationsByTypeid(type.getId());
			view.putAttr("gradations", gradations);
			List<TaskType> taskTypes = projectGradationServiceImpl.getTaskTypes(task.getGradationid());
			view.putAttr("taskTypes", taskTypes);
			return view.returnModelAndView("/web-simple/projectview/commons/includeEditTaskWinLoader");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/projectview/commons/includeEditTaskWinLoader");
		}
	}

	/***
	 * 根据项目获取阶段列表
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadGradations")
	@ResponseBody
	public Map<String, Object> loadGradations(String projectId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			Project project = projectServiceImpl.getProjectEntity(projectId);
			ProjectType type = projectTypeServiceImpl.getProjecttypeEntity(project.getTypeid());
			List<ProjectGradation> gradations = projectGradationServiceImpl.getGradationsByTypeid(type.getId());
			view.putAttr("gradations", gradations);
			view.putAttr("gradationid", project.getGradationid());
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/***
	 * 根据项目阶段获取任务类型列表
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadTaskType")
	@ResponseBody
	public Map<String, Object> loadTaskType(String gradationId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			List<TaskType> taskTypes = projectGradationServiceImpl.getTaskTypes(gradationId);
			view.putAttr("taskTypes", taskTypes);
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 修改一个任务（任务信息中的任务修改-只修改部分属性）
	 * 
	 * @param taskid
	 * @param title
	 * @param projectid
	 * @param gradationid
	 * @param tasktypeid
	 * @param state
	 * @param text
	 * @param session
	 * @return
	 */
	@RequestMapping("/doTaskEdit")
	@ResponseBody
	public Map<String, Object> doTaskEdit(String taskid, String title, String gradationid, String tasktypeid,
			HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			Task task = taskServiceImpl.getTaskEntity(taskid);
			// 修改
			task = taskServiceImpl.updateTask(taskid, title, task.getProjectid(), gradationid, tasktypeid,
					task.getPstate(), task.getPlanetime(), null, getCurrentUser(session));
			view.putAttr("task", task);
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交一个任务（修改过创建）
	 * 
	 * @param taskid
	 * @param title
	 * @param projectid
	 * @param gradationid
	 * @param tasktypeid
	 * @param state
	 * @param text
	 * @param session
	 * @return
	 */
	@RequestMapping("/taskSubmit")
	@ResponseBody
	public Map<String, Object> taskSubmit(String taskid, String title, String projectid, String gradationid,
			String tasktypeid, String state, String planetime, String text, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			Task task = null;
			if (StringUtils.isNotBlank(taskid)) {
				// 修改
				task = taskServiceImpl.updateTask(taskid, title, projectid, gradationid, tasktypeid, state, planetime,
						text, getCurrentUser(session));
			} else {
				// 添加
				task = taskServiceImpl.addTask(title, projectid, gradationid, tasktypeid, state, planetime, text,
						getCurrentUser(session));
				// 初始化任务关系人
				taskUserServiceImpl.resetTasktuser(task.getId(), getCurrentUser(session));
			}
			if (StringUtils.isNotBlank(projectid) && StringUtils.isNotBlank(projectid)) {
				projectServiceImpl.editProjectGradation(projectid, gradationid, getCurrentUser(session));
				projectServiceImpl.refreshLiveTime(projectid);
			}
			view.putAttr("task", task);
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 提交一個任務日志
	 * 
	 * @param taskid
	 * @param text
	 * @param session
	 * @return
	 */
	@RequestMapping("/ctlogSubmit")
	@ResponseBody
	public Map<String, Object> ctlogSubmit(String taskid, String text, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			TaskLog log = taskServiceImpl.saveLog(taskid, text, getCurrentUser(session));
			view.putAttr("log", log);
			view.putAttr("task", taskServiceImpl.getTaskEntity(taskid));
			projectServiceImpl.refreshLiveTime(taskServiceImpl.getTaskEntity(taskid).getProjectid());
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 修改项目属性
	 * 
	 * @param fieldinsId
	 * @param value
	 * @param session
	 * @return
	 */
	@RequestMapping("/taskFieldSubmit")
	@ResponseBody
	public Map<String, Object> taskFieldSubmit(String fieldinsId, String value, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			ProjectFieldins fieldins = projectFieldinsServiceImpl.editVal(fieldinsId, value, getCurrentUser(session));
			view.putAttr("fieldins", fieldins);
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/**
	 * 加载模板
	 * 
	 * @param taskTypeid
	 * @param session
	 * @return
	 */
	@RequestMapping("/loadTemplete")
	@ResponseBody
	public Map<String, Object> loadTemplete(String tasktypeId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			TaskType ttpey = taskTypeServiceImpl.getTasktypeEntity(tasktypeId);
			if (ttpey.getTempletefileid() != null) {
				view.putAttr("hastemp", true);
				view.putAttr("text", taskTypeServiceImpl.readTempleteHtml(tasktypeId));
			} else {
				view.putAttr("hastemp", false);
			}
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	/***
	 * 派发任务窗口
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/sandTaskWin")
	public ModelAndView sandTaskWin(String taskId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			Task task = taskServiceImpl.getTaskEntity(taskId);
			view.putAttr("task", task);
			DataQuery query = projectUserServiceImpl.getProjectUsersQuery(task.getProjectid());
			List<Map<String, Object>> users = query.search().getResultList();
			List<Taskuser> cuser = taskUserServiceImpl.getTaskUser(taskId, "2");
			if (cuser.size() > 0) {
				view.putAttr("douser", cuser.get(0));
			}
			return view.putAttr("users", users)
					.returnModelAndView("/web-simple/worktask/commons/includeSendTaskWinLoader");
		} catch (Exception e) {
			log.error(e);
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("/web-simple/worktask/commons/includeSendTaskWinLoader");
		}
	}

	/**
	 * 设置任务执行人
	 * 
	 * @param taskId
	 * @param userId
	 * @param session
	 * @return
	 */
	@RequestMapping("/sandTask")
	@ResponseBody
	public Map<String, Object> sandTask(String taskId, String userId, HttpSession session) {
		try {
			ViewMode view = ViewMode.getInstance();
			taskUserServiceImpl.bindDoUser(taskId, userId, getCurrentUser(session));
			return view.returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
}