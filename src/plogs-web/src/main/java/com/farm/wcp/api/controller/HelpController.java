package com.farm.wcp.api.controller;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.service.OrganizationServiceInter;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.page.ViewMode;
import com.farm.core.time.TimeTool;
import com.farm.web.WebUtils;

/**
 * 组织机构、 [创建、查询、更新、删除] 用户、 [创建、查询、更新、删除] ---------------------------- 知识接口[查询]、
 * 分类接口[查询]、 问答接口[查询]、
 * 
 * @author wangdong
 *
 */
@RequestMapping("/helper")
@Controller
public class HelpController extends WebUtils {
	private final static Logger log = Logger.getLogger(HelpController.class);
	@Resource
	private OrganizationServiceInter organizationServiceImpl;
	@Resource
	private UserServiceInter userServiceImpl;

	/**
	 * API说明文档
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/readme")
	public ModelAndView index(HttpSession session, String secret, HttpServletRequest request) {
		String sysSecret = TimeTool.format(new Date(), "ddHHmm");
		if ((session.getAttribute("APISECRET") == null) && !sysSecret.equals(secret)) {
			return ViewMode.getInstance().returnModelAndView("help/apiSecretForm");
		}
		session.setAttribute("APISECRET", true);
		String url = request.getRequestURL().toString().substring(0,
				request.getRequestURL().toString().lastIndexOf("/"));
		url = url.substring(0, url.lastIndexOf("/"));
		return ViewMode.getInstance().putAttr("CURL", url).returnModelAndView("help/restfulApi");
	}
}
