package com.farm.wcp.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.FarmAuthorityService;
import com.farm.authority.service.OutuserServiceInter;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.auth.domain.LoginUser;
import com.farm.core.auth.exception.CheckCodeErrorException;
import com.farm.core.auth.exception.LoginUserNoAuditException;
import com.farm.core.auth.exception.LoginUserNoExistException;
import com.farm.core.page.ViewMode;
import com.farm.parameter.FarmParameterService;
import com.farm.wcp.util.CheckCodeUtil;
import com.farm.wcp.util.ThemesUtil;
import com.farm.wcp.util.VerifyCodeUtils;
import com.farm.web.WebUtils;
import com.farm.web.log.PlogsLog;

@RequestMapping("/login")
@Controller
public class LoginWebController extends WebUtils {
	private final static Logger log = Logger.getLogger(LoginWebController.class);
	@Resource
	private OutuserServiceInter outUserServiceImpl;
	@Resource
	private UserServiceInter userServiceImpl;

	@RequestMapping("/submit")
	public ModelAndView loginCommit(String name, String password, String checkcode, HttpServletRequest request,
			HttpSession session) {
		try {
			// 是否启用登录验证码
			if (!CheckCodeUtil.isCheckCodeAble(checkcode, session)) {
				throw new CheckCodeErrorException("验证码未通过");
			}
			name = findUserLoginName(name, session);
			if (FarmAuthorityService.getInstance().isLegality(name, password)) {
				// 登录成功
				// 注册session
				FarmAuthorityService.loginIntoSession(session, getCurrentIp(request), name, "登录页");
				loginStateHandle(true, session);
				return ViewMode.getInstance().returnRedirectUrl("/frame/index.do");
			} else {
				// 登录失败
				loginStateHandle(false, session);
				return ViewMode.getInstance().putAttr("message", "用户密码错误").returnModelAndView("frame/login");
			}
		} catch (LoginUserNoExistException e) {
			log.info("当前用户不存在");
			loginStateHandle(false, session);
			return ViewMode.getInstance().putAttr("message", "当前用户不存在").returnModelAndView("frame/login");
		} catch (CheckCodeErrorException e) {
			log.info(e.getMessage());
			return ViewMode.getInstance().putAttr("message", e.getMessage()).returnModelAndView("frame/login");
		} catch (LoginUserNoAuditException e) {
			return ViewMode.getInstance().putAttr("MESSAGE", "当前用户已注册，正等待管理员审核!")
					.returnModelAndView(ThemesUtil.getThemePage("message-sys"));
		}
	}

	/**
	 * 下载验证码
	 * 
	 * @return
	 */
	@RequestMapping("/Pubcheckcode")
	public void checkcode(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setContentType("image/jpeg");
		// 生成随机字串
		String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
		// 存入会话session
		session = request.getSession(true);
		// 删除以前的
		session.removeAttribute("verCode");
		session.setAttribute("verCode", verifyCode.toUpperCase());
		// 生成图片
		int w = 100, h = 30;
		try {
			VerifyCodeUtils.outputImage(w, h, response.getOutputStream(), verifyCode);
		} catch (IOException e) {
			log.error("下载验证码", e);
		}
	}

	/**
	 * 进入WCP站内登录页面
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/PubLogin")
	public ModelAndView pubLogin(String loginname,HttpSession session, HttpServletRequest request) {
		CheckCodeUtil.loadCheckAbleNum(session);
		ViewMode mode = ViewMode.getInstance();
		LoginUser user = getCurrentUser(session);
		if (user != null) {
			mode.putAttr("user", user);
		}
		mode.putAttr("loginname", loginname);
		return mode.returnModelAndView(ThemesUtil.getThemePage("home-loginPage"));
	}

	/**
	 * 进入默认登录页面（可在配置文件wcpWebConfig.xml中config.login.default.url配置默认页面为default:
	 * login/PubLogin）
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/webPage")
	public ModelAndView login(HttpSession session, HttpServletRequest request) {
		// 将登录前的页面地址存入session中，为了登录后回到该页面中
		String url = request.getHeader("Referer");
		session.setAttribute(FarmParameterService.getInstance().getParameter("farm.constant.session.key.from.url"),
				url);
		try {
			String defultLoginUrl = FarmParameterService.getInstance().getParameter("config.login.default.url");
			if (!defultLoginUrl.toUpperCase().equals("DEFAULT")
					&& (defultLoginUrl.indexOf(".html") > 0 || defultLoginUrl.indexOf(".do") > 0)) {
				return ViewMode.getInstance().returnRedirectUrl(defultLoginUrl);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return ViewMode.getInstance().returnRedirectUrl("/login/PubLogin.html");
	}

	/**
	 * 处理后台过来的同步请求（经过浏览器跳转）：处理验证码屏蔽，处理中文字符编码
	 * 
	 * @param session
	 * @param outusername
	 * @param loginname
	 * @return
	 */
	@SuppressWarnings("unused")
	private String urlDecode(String outusername, String loginname) {
		// 是否使用验证码(在后台自动绑定时使用：如ldap自动绑定用户,因爲不能 从前台获取验证码，所以通过session参数屏蔽)
		try {
			outusername = URLDecoder.decode(outusername, "utf-8");
			outusername = URLDecoder.decode(outusername, "utf-8");
		} catch (Exception e) {
			outusername = loginname;
			log.warn(e);
		}
		return outusername;
	}

	/**
	 * 验证验证码并且获取用户登录名
	 * 
	 * @param name
	 *            登录名/手机号/邮箱
	 * @param session
	 * @return
	 * @throws CheckCodeErrorException
	 */
	private String findUserLoginName(String name, HttpSession session) {
		if (name == null) {
			// 制作验证
			return null;
		}
		LoginUser user = FarmAuthorityService.getInstance().getUserByLoginName(name);
		if (user != null) {
			name = user.getLoginname();
		}
		return name;
	}

	/**
	 * 处理登录状态结果
	 * 
	 * @param state
	 */
	private void loginStateHandle(boolean state, HttpSession session) {
		if (state) {
			CheckCodeUtil.addLoginSuccess(session);
		} else {
			CheckCodeUtil.addLoginError(session);
		}
	}

	/**
	 * 提交登录请求
	 * 
	 * @param session
	 * @return
	 */
	@SuppressWarnings("unused")
	@RequestMapping("/websubmit")
	public ModelAndView webLoginCommit(String checkcode, String name, String password, HttpServletRequest request,
			HttpSession session) {
		try {
			// 是否启用登录验证码
			if (!CheckCodeUtil.isCheckCodeAble(checkcode, session)) {
				throw new CheckCodeErrorException("验证码未通过");
			}
			name = findUserLoginName(name, session);
			if (FarmAuthorityService.getInstance().isLegality(name, password)) {
				// 登录成功
				// 注册session
				Map<String, String> loginParas = FarmAuthorityService.loginIntoSession(session, getCurrentIp(request),
						name, "登录页");
				String goUrl = getGoUrl(session, FarmParameterService.getInstance());
				loginStateHandle(true, session);
				LoginUser user = FarmAuthorityService.getInstance().getUserByLoginName(name);
				PlogsLog.info("用户登陆系统[" + user.getName() + "/" + getCurrentIp(request) + "]", user.getName(), user.getId());
				return ViewMode.getInstance().returnRedirectUrl(goUrl);
			} else {
				// 登录失败
				loginStateHandle(false, session);
				PlogsLog.warn("登陆密码错误[" + name + "/" + getCurrentIp(request) + "]", name, null);
				return ViewMode.getInstance().putAttr("loginname", name).setError("用户密码错误", null)
						.returnModelAndView(ThemesUtil.getThemePage("home-loginPage"));
			}
		} catch (LoginUserNoExistException e) {
			loginStateHandle(false, session);
			PlogsLog.warn("登陆用户不存在[" + name + "/" + getCurrentIp(request) + "]", name, null);
			return ViewMode.getInstance().putAttr("loginname", name).setError("当前用户不存在", null)
					.returnModelAndView(ThemesUtil.getThemePage("home-loginPage"));
		} catch (CheckCodeErrorException e) {
			PlogsLog.warn("登陆验证码错误[" + name + "/" + getCurrentIp(request) + "]", name, null);
			return ViewMode.getInstance().putAttr("loginname", name).setError("验证码错误", null)
					.returnModelAndView(ThemesUtil.getThemePage("home-loginPage"));
		} catch (LoginUserNoAuditException e) {
			PlogsLog.warn("登陆用户已注册,等待管理员审核[" + name + "/" + getCurrentIp(request) + "]", name, null);
			return ViewMode.getInstance().putAttr("MESSAGE", "当前用户已经注册成功，正等待管理员审核!")
					.returnModelAndView(ThemesUtil.getThemePage("home-loginPage"));
		}
	}

	@RequestMapping("/webout")
	public ModelAndView weblogOut(String name, HttpSession session) {
		try {
			clearCurrentUser(session);
			return ViewMode.getInstance().returnRedirectUrl(
					"/" + FarmParameterService.getInstance().getParameter("config.index.defaultpage"));
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView(ThemesUtil.getThemePage("error-sys"));
		}
	}

	@RequestMapping("/page")
	public ModelAndView login(String name) {
		return ViewMode.getInstance().returnModelAndView("/frame/login");
	}

	@RequestMapping("/out")
	public ModelAndView logOut(String name, HttpSession session) {
		clearCurrentUser(session);
		// return ViewMode.getInstance().returnRedirectUrl("/login/page.do");
		return ViewMode.getInstance().returnRedirectUrl("/login/webPage.html");
	}

	/**
	 * 获得当前用户信息
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/PubCurrent")
	@ResponseBody
	public Map<String, Object> current(HttpSession session) {
		log.debug("检查当前用户信息");
		LoginUser user = getCurrentUser(session);
		boolean islogined = (user != null);
		return ViewMode.getInstance().putAttr("islogin", islogined).putAttr("curentUser", user).returnObjMode();
	}
}
