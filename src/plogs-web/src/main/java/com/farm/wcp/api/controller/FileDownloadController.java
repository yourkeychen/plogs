package com.farm.wcp.api.controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.ex.PersistFile;
import com.farm.view.WdapViewServiceInter;
import com.farm.wcp.util.DownloadUtils;
import com.farm.wcp.util.HttpContentType;
import com.farm.web.WebUtils;

/**
 * 文件下載
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/download")
@Controller
public class FileDownloadController extends WebUtils {
	private static final Logger log = Logger.getLogger(FileDownloadController.class);

	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;

	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;

	/**
	 * 下载文件图标
	 * 
	 * @return
	 */
	@RequestMapping("/Pubicon")
	public void loadimg(String id, HttpServletRequest request, HttpServletResponse response) {
		PersistFile file = wdapViewServiceImpl.getFileIcon(id);
		DownloadUtils.downloadFile(file.getFile(), file.getName(), response);
	}

	/**
	 * 下載文件(普通下载)
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	@RequestMapping("/Pubfile")
	public void loadFileByDown(String id, String secret, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException {
		PersistFile file = wdapFileServiceImpl.getPersistFile(id);
		if (!file.getSecret().trim().equals(secret)) {
			response.sendError(405, "安全码错误!secret：" + secret);
			return;
		}
		wdapFileServiceImpl.downloadFileHandle(id);
		DownloadUtils.downloadFile(file.getFile(), file.getName(), response);
	}

	/**
	 * 下載文件夹中文件
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	@RequestMapping("/PubDirFile")
	public void loadDirFileByDown(String viewdocid, int num, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException {
		PersistFile file = wdapViewServiceImpl.getViewFile(viewdocid, num);
		DownloadUtils.downloadFile(file.getFile(), file.getName(), response);
	}

	/**
	 * 加载文件（在线预览）
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	@RequestMapping("/Pubload")
	public void loadFileByFile(String id, String secret, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException {
		PersistFile file = wdapFileServiceImpl.getPersistFile(id);
		if (!file.getSecret().trim().equals(secret)) {
			response.sendError(405, "安全码错误!secret：" + secret);
			return;
		}
		DownloadUtils.sendVideoFile(request, response, file.getFile(), file.getName(),
				new HttpContentType().getContentType(wdapFileServiceImpl.getExName(file.getName())));
	}

	// ---------------------------------------------------------------
}
