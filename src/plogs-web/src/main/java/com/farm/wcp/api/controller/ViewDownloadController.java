package com.farm.wcp.api.controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.ex.PersistFile;
import com.farm.view.WdapViewServiceInter;
import com.farm.view.model.utils.HtmlStaticResouceNames;
import com.farm.wcp.util.DownloadUtils;
import com.farm.wcp.util.HttpContentType;
import com.farm.web.WebUtils;

/**
 * 预览文件下載
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/downview")
@Controller
public class ViewDownloadController extends WebUtils {
	private static final Logger log = Logger.getLogger(ViewDownloadController.class);
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;

	/**
	 * 下载预览文件图标
	 * 
	 * @return
	 */
	@RequestMapping("/Pubicon")
	public void loadimg(String id, HttpServletRequest request, HttpServletResponse response) {
		PersistFile file = wdapViewServiceImpl.getViewDocIcon(id);
		DownloadUtils.downloadFile(file.getFile(), file.getName(), response);
	}

	/**
	 * 下載预览文件(普通下载)
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	@RequestMapping("/Pubfile")
	public void loadFileByDown(String id, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException {
		PersistFile file = wdapViewServiceImpl.getViewFile(id);
		DownloadUtils.downloadFile(file.getFile(), file.getName(), response);
	}

	/**
	 * 加载预览文件（在线预览）
	 * 
	 * @param id
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	@RequestMapping("/Pubload")
	public void loadFileByFile(String id, HttpServletRequest request, HttpServletResponse response)
			throws FileNotFoundException, IOException {
		PersistFile file = wdapViewServiceImpl.getViewFile(id);
		wdapViewServiceImpl.viewDocHandle(id);
		DownloadUtils.sendVideoFile(request, response, file.getFile(), file.getName(),
				new HttpContentType().getContentType(wdapFileServiceImpl.getExName(file.getName())));
	}

	/**
	 * 获得html模型中的资源文件
	 * 
	 * @param filepath
	 *            文件相对名称
	 * @param viewDocId
	 *            预览文件id
	 * @param session
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "/PubWebSource", method = RequestMethod.GET)
	public void loadWebSource(String filepath, String viewDocId, HttpServletResponse response, HttpSession session,
			HttpServletRequest request) throws Exception {
		for (HtmlStaticResouceNames resourcename : HtmlStaticResouceNames.values()) {
			if (filepath.equals(resourcename.name())) {
				String path = request.getContextPath();
				String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
						+ path + "/";
				response.sendRedirect(basePath + resourcename.getWebPath());
				return;
			}
		}
		PersistFile file = wdapViewServiceImpl.getViewDocHtmlResource(filepath, viewDocId);
		DownloadUtils.sendVideoFile(request, response, file.getFile(), file.getName(),
				new HttpContentType().getContentType(wdapFileServiceImpl.getExName(file.getName())));
	}

}
