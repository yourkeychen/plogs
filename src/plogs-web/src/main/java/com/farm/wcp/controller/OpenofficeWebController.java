package com.farm.wcp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.view.model.utils.OpenOffceUtils;
import com.farm.web.WebUtils;

@RequestMapping("/openoffice")
@Controller
public class OpenofficeWebController extends WebUtils {
	@SuppressWarnings("unused")
	private final static Logger log = Logger.getLogger(OpenofficeWebController.class);

	@RequestMapping("/state")
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		return ViewMode.getInstance().putAttr("oostate", OpenOffceUtils.isStart())
				.returnModelAndView("/web-simple/openoffce/index");
	}

	@RequestMapping("/restart")
	public ModelAndView restart(HttpServletRequest request, HttpSession session) {
		OpenOffceUtils.reStart();
		return ViewMode.getInstance().returnRedirectOnlyUrl("/openoffice/state.do");
	}

	@RequestMapping("/shutdown")
	public ModelAndView shutdown(HttpServletRequest request, HttpSession session) {
		OpenOffceUtils.shutdown();
		return ViewMode.getInstance().returnRedirectOnlyUrl("/openoffice/state.do");
	}
}
