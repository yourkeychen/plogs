package com.farm.wcp.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.file.service.FileBaseServiceInter;
import com.farm.lucene.common.IRResult;
import com.farm.parameter.FarmParameterService;
import com.farm.project.service.FieldServiceInter;
import com.farm.project.service.ProjectFieldinsServiceInter;
import com.farm.project.service.ProjectGradationServiceInter;
import com.farm.project.service.ProjectServiceInter;
import com.farm.project.service.ProjectTypeServiceInter;
import com.farm.project.service.ProjectuserServiceInter;
import com.farm.project.service.TaskServiceInter;
import com.farm.project.service.TaskTypeServiceInter;
import com.farm.project.service.utils.IndexUtils;
import com.farm.wcp.util.ThemesUtil;
import com.farm.web.WebUtils;

/**
 * 附件检索
 * 
 * @author autoCode
 * 
 */
@RequestMapping("/fileSearch")
@Controller
public class TaskFileLuceneWebController extends WebUtils {
	private static final Logger log = Logger.getLogger(TaskFileLuceneWebController.class);
	@Resource
	private ProjectServiceInter projectServiceImpl;
	@Resource
	private ProjectTypeServiceInter projectTypeServiceImpl;
	@Resource
	private ProjectGradationServiceInter projectGradationServiceImpl;
	@Resource
	private TaskServiceInter taskServiceImpl;
	@Resource
	private TaskTypeServiceInter taskTypeServiceImpl;
	@Resource
	private ProjectFieldinsServiceInter projectFieldinsServiceImpl;
	@Resource
	private FieldServiceInter fieldServiceImpl;
	@Resource
	private FileBaseServiceInter fileBaseServiceImpl;
	@Resource
	private ProjectuserServiceInter projectUserServiceImpl;

	/***
	 * 附件检索首页
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/search")
	public ModelAndView index(String name, String taskTypeId, String exname, Integer page, HttpServletRequest request,
			HttpSession session) {
		ViewMode view = ViewMode.getInstance();
		List<String> exnames = FarmParameterService.getInstance().getParameterStringList("config.doc.upload.types");
		view.putAttr("exnames", exnames);
		view.putAttr("taskTypeId", taskTypeId);
		view.putAttr("exname", exname);
		view.putAttr("name", name);
		view.putAttr("MODEL", "FILESEARCH");
		if (StringUtils.isNotBlank(name)) {
			try {
				Set<String> projectids = projectUserServiceImpl.getProjectids(getCurrentUser(session).getId(), 1000);
				IRResult result = IndexUtils.searchFiles(name, exname, taskTypeId, page, projectids);
				for (Map<String, Object> node : result.getResultList()) {
					String fileid = (String) node.get("FILEID");
					if (fileBaseServiceImpl.getFilebaseEntity(fileid) == null) {
						IndexUtils.deleteIndex(fileid);
						node.remove("FILEID");
					}
				}
				view.putAttr("result", result);
			} catch (Exception e) {
				IRResult result = IRResult.getInstance();
				e.printStackTrace();
				view.putAttr("message", e.getMessage());
				view.putAttr("result", result);
			}
		} else {
			view.putAttr("result", IRResult.getInstance());
		}
		return view.returnModelAndView(ThemesUtil.getThemePage("plogs-filesearch-index", request));
	}

}