package com.farm.web.init.commons;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.farm.authority.FarmAuthorityService;
import com.farm.core.config.AppConfig;
import com.farm.core.time.TimeTool;
import com.farm.parameter.FarmParameterService;
import com.farm.util.http.HttpUtils;
import com.farm.util.spring.HibernateSessionFactory;

public class FreeWcpInfoSenders {
	private static String state = AppConfig.getString("config.wcp.update.info.able");
	private static int pv = 0;
	private static String lastSendDay8 = "00000000";

	/**
	 * 訪問頁面時計數
	 */
	public static void visitByPv() {
		try {
			pv = pv + 1;
		} catch (Exception e) {
			pv = 0;
		}
	}

	/**
	 * 获得用户数(禁用/可用/待审核都算)
	 * 
	 * @return
	 */
	private static int getUsernum() {
		return FarmAuthorityService.getInstance().CountAllUser();
	}

	public static void main(String[] args) {
		System.out.println(getUsernum());
	}

	/**
	 * 获得pv数量
	 * 
	 * @return
	 */
	private static int getPv() {
		if ("DEBUG".equals(state.toUpperCase())) {
			return 99999;
		} else {
			return pv;
		}
	}

	/**
	 * 获得知识数量
	 * 
	 * @return
	 */
	private static int getTasknum() {
		Session session = HibernateSessionFactory.getSession();
		try {
			SQLQuery query = session.createSQLQuery("select count(*) from PLOGS_TASK");
			BigInteger num = (BigInteger) query.list().get(0);
			return num.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return 0;
	}

	/**
	 * 获得公司名称
	 * 
	 * @return
	 */
	private static String getCompanyName() {
		return FarmParameterService.getInstance().getParameter("config.sys.title")
				+ FarmParameterService.getInstance().getParameter("config.sys.foot"+"-PLOGS");
	}

	/**
	 * 本日已经发送(必須在獲得pv后調用)
	 */
	public static void runSende() {
		// 访问远程端口，上传数据，获得最新版本信息
		try {
			Map<String, String> para = new HashMap<>();
			para.put("key", FreeWcpInfoSenders.getInfoKey());
			String baseUrl = AppConfig.getString("config.wcp.home.code").replaceAll("\\|\\|", ":").replaceAll("\\|",
					".");
			HttpUtils.httpPost("http://" + baseUrl + "/api/info/version.do", para);
		} catch (Exception e) {
		}
		lastSendDay8 = TimeTool.getFormatTimeDate12(TimeTool.getTimeDate14(), "yyyyMMdd");
		pv = 0;
	}

	/**
	 * 獲得信息編碼
	 * 
	 * @return
	 */
	public static String getInfoKey() {
		return FreeVersionInfoUtils.encode(getPv(), getUsernum(), getTasknum(), getCompanyName());
	}

	/**
	 * 是否可以發送
	 * 
	 * @return
	 */
	public static boolean sendAble() {
		try {
			if ("FALSE".equals(state.toUpperCase())) {
				return false;
			}
			if ("DEBUG".equals(state.toUpperCase())) {
				return true;
			}
			if (TimeTool.getFormatTimeDate12(TimeTool.getTimeDate14(), "HH").equals("03")) {
				// 在发送时间
				if (!lastSendDay8.equals(TimeTool.getFormatTimeDate12(TimeTool.getTimeDate14(), "yyyyMMdd"))) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
}
