package com.farm.web.init.commons;

public class CompanyUser {
	private String id;
	private String overtime;
	private int personnum;
	private int docnum;
	private int daypv;
	private String ename;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOvertime() {
		return overtime;
	}

	public void setOvertime(String overtime) {
		this.overtime = overtime;
	}

	public int getPersonnum() {
		return personnum;
	}

	public void setPersonnum(int personnum) {
		this.personnum = personnum;
	}

	public int getDocnum() {
		return docnum;
	}

	public void setDocnum(int docnum) {
		this.docnum = docnum;
	}

	public int getDaypv() {
		return daypv;
	}

	public void setDaypv(int daypv) {
		this.daypv = daypv;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

}
