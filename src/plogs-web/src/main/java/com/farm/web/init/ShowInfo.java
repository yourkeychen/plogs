package com.farm.web.init;

import javax.servlet.ServletContext;
import com.farm.core.config.AppConfig;
import com.farm.web.init.commons.FreeWcpInfoSenders;
import com.farm.web.task.ServletInitJobInter;

public class ShowInfo implements ServletInitJobInter {

	@Override
	public void execute(ServletContext context) {
		System.out.println("PLOGS base dir is:" + System.getProperty("catalina.base"));
		System.out.println("PLOGS version is:v." + AppConfig.getString("config.sys.version"));
		// 启动线程
		// 实现runnable借口，创建多线程并启动
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {// 检查时间，定时执行
						Thread.sleep(10000);
						if (FreeWcpInfoSenders.sendAble()) {
							FreeWcpInfoSenders.runSende();
						}
					} catch (Exception e) {
						// e.printStackTrace();
					}
				}
			}
		}) {
		}.start();
	}
}
