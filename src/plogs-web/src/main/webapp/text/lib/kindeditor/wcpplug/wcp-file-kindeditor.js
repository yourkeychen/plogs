KindEditor.plugin('wcpfile', function(K) {
	var wcp_files_editor;
	var wcpfilesKnowdialog;
	wcp_files_editor = this, name = 'wcpfile';
	// 点击图标时执行
	wcp_files_editor.clickToolbar(name, function() {
		wcpfilesKnowdialog = K
		.dialog( {
			width : 300,
			title : '批量上传附件',
			body : '<div style="margin:10px;height:350px;text-align: center;overflow:auto;">'+
			       '<div class="wcpMultiUploadButtonBox">'+
			       ' 	<span class="btn btn-info fileinput-button"> '+
			       '  		<span>上传附件</span><span id="html5uploadProsess"></span>'+
			       '  		<input id="fileupload" type="file" name="imgFile" multiple>'+
			       ' 	</span>'+
			       ' 	<span class="btn btn-info insertEditor" style="height:30px;padding-top:6px;font-size:12px;"> '+ 
			       '  		<span>插入编辑器</span>'+  
			       ' 	</span>'+
			       '<br/>1.点击"上传附件"按钮或将文件拖入此框上传。<br/>2.点击"插入编辑器"将附件插入编辑器。</div>'+ 
				   '<div class="panel-body" style="height:250px;overflow:auto;margin-top:12px;" id="wcpimgFileListId"><div id="fileQueue"></div></div>'+
				   '</div>',
			closeBtn : {
				name : '关闭',
				click : function(e) {
					wcpfilesKnowdialog.remove();
				}
			}
		});
		$('.wcpMultiUploadButtonBox .insertEditor').click(function(){
			$('.uploadedFileUnit').each(function(i,obj){
				var downloadUrl=$(obj).children("#durl").val();
				var fileId=$(obj).children("#fileid").val();
				var viewUrl=$(obj).children("#vurl").val();
				var imgUrl=$(obj).children(".stream-item").children(".media").children(".pull-left").children("img").attr("src");
				var fileName=$(obj).children(".stream-item").children(".media").children(".pull-left").children("img").attr("alt");
				var isView=true;
				if(viewUrl=='none'){
					viewUrl=downloadUrl;
					isView=false;
				}
				var tag='<br/><div class="ke-wcp-file"><table><tr><td>'
					//图片
					+'<img src="'+imgUrl+'" width="48" height="48" />'+fileName
					//下载按钮
					+'<a class="kind-file-db" href="'+downloadUrl+'">下载</a>'
					//预览按钮
					+(isView?'<a class="kind-file-db" target="_blank" href="'+viewUrl+'">预览</a>':'')
					+'</td></tr></table></div><br/>';
				try{
					wcp_files_editor.insertHtml(tag);
				}catch(e){} 
			});
			wcpfilesKnowdialog.remove();
		});
		$('.wcpMultiUploadButtonBox #fileupload').fileupload({
			url : "upload/generalKindEditor.do",
			dataType : 'json',
			done : function(e, data) {
				var url=data.result.url;
				var viewurl=data.result.viewurl;
				var error=data.result.error;
				var message=data.result.message;
				var id=data.result.id;
				var fileName=data.result.fileName;
				//alert("url:"+url);
				if(error!=0){
					wcpfilesKnowdialog.remove();
					alert(message);
					return;
				}
				addFileNodeByWcpFile('download/Pubicon.do?id='+id, decodeURIComponent(fileName), id,url,viewurl);
			},
			progressall : function(e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('#html5uploadProsess').text(progress + '%');
			}
		});
		$('body').bind('drop', function(ev) {
			ev.stopPropagation();
			ev.preventDefault();
		});
		initDropBox();
	});
	//初始化拖拽框
	function initDropBox(){
		var uploadDrop = $('.wcpMultiUploadButtonBox');
		uploadDrop = uploadDrop[0];
		uploadDrop.addEventListener('dragover', function(event) {
			$('.wcpMultiUploadButtonBox').addClass("active");
			event.preventDefault();
		});
		uploadDrop.addEventListener('dragenter', function(event) {
			event.preventDefault();
		});
		uploadDrop.addEventListener('dragleave', function(event) {
			$('.wcpMultiUploadButtonBox').removeClass("active");
			event.preventDefault();
		});
		uploadDrop.addEventListener('drop', function(event) {
			$('.wcpMultiUploadButtonBox').removeClass("active");
			event.preventDefault();
			var fileList = Array.from(event.dataTransfer.files);
			var uploadAble = updateFileValidate(fileList);
			$('#wcpimgFileListId').html('');
			if (uploadAble) {
				for (var i = 0; i < fileList.length; i++) {
					sendFileByXHR('upload/general.do', fileList[i]);
				}
			}
		});
	}
	//上传拖拽文件
	function sendFileByXHR(url, fielObj) {
		var xhr = new XMLHttpRequest();
		xhr.upload.onprogress = function(event) {
			var progress = parseInt(event.loaded / event.total * 100, 10);
			$('#html5uploadProsess').text(progress + '%');
		}
		xhr.onreadystatechange = function() {
			if (xhr.status === 200 && xhr.readyState === 4) {
				var jsonObject = JSON.parse(xhr.responseText, null);
				if(jsonObject.STATE==1){
					alert(jsonObject.MESSAGE);
				}else{
					var url=jsonObject.downurl;
					var viewurl=jsonObject.viewurl;
					addFileNodeByWcpFile('download/Pubicon.do?id=' + jsonObject.id
						, decodeURIComponent(jsonObject.fileName)
						, jsonObject.id
						,url,viewurl)
				}
			}
		}
		xhr.onabort = function(event) {
			//终止
			console.log('abort');
		}
		xhr.onerror = function(event) {
			console.log('error');
			alert('xhr.onerror-error');
		}
		var data = new FormData();
		data.append("file", fielObj);
		xhr.open('POST', url, true);
		xhr.send(data);
	}
	//添加一個临时文件元素到上传组件
	function addFileNodeByWcpFile(imgUrl, fileName, fileId,downurl,viewurl) {
		var html = '<div class="col-md-12 file-block-box uploadedFileUnit" id="file_'+fileId+'">';
		html = html + '		<div class="stream-item" >';
		html = html + '				<div class="media">';
		html = html + '					<div class="pull-left">';
		html = html + '						<img  alt="'+fileName+'" src="'+imgUrl+'">';
		html = html + '					</div>';
		html = html + '					<div class="media-body" >';
		html = html + '							<i onclick="removeFile(\'' + fileId
				+ '\');" class="glyphicon glyphicon-remove pull-right" ></i>';
		html = html + '						<div class="file-title" >' + fileName + '</div>';
		html = html + '					</div>';
		html = html + '				</div>';
		html = html + '		</div>';
		html = html
				+ '<input type="hidden" id="fileid" value="'+fileId+'" /><input type="hidden" id="durl" value="'+downurl+'" /><input type="hidden" id="vurl" value="'+viewurl+'" />    </div>';
		$('#wcpimgFileListId').append(html);
	}
	//检查上传文件，返回值为是否允许上传
	function updateFileValidate(files) {
		var maxFileNum=5;
		var maxSize=500000000;//500m
		var maxSizeTitle = (maxSize / 1024 / 1024).toFixed(2);
		if (files.length > maxFileNum) {
			alert("单次上传文件数量不能大于" + maxFileNum + "个!");
			return false;
		}
		var isOK = true;
		$(files).each(function(i, obj) {
			if (obj.size > maxSize) {
				alert("文件" + obj.name + "超大,请检查文件大小不能大于" + maxSizeTitle + "m");
				isOK = false;
			}
		});
		return isOK;
	}
});
function removeFile(fileId) {
	$("#file_" + fileId).remove();
} 
KindEditor.lang({
	wcpfile : '批量上传附件'
});
