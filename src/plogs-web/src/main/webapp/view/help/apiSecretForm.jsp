<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../web-simple/atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="../web-simple/commons/head.jsp"></jsp:include>
	<jsp:include page="../web-simple/commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="logoLeftBoxId">
					<jsp:include page="../web-simple/commons/HomeLogo.jsp"></jsp:include>
				</div>
				<div class="col-md-8" style="border-left: 1px dashed #cccccc;">
					<form>
						<div class="form-group">
							<label for="exampleInputEmail1">核心DHM授權码</label> <input
								type="password" class="form-control" name="secret" >
						</div>
						<button type="submit" class="btn btn-default">提交DHM授權碼</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../web-simple/commons/footServer.jsp"></jsp:include>
	<jsp:include page="../web-simple/commons/foot.jsp"></jsp:include>
</body>
</html>