<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../web-simple/atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
<style>
<!--
.container h1 {
	font-weight: 700;
	margin: 10px;
}

.container h3 {
	margin: 10px;
	color: #CE4844;
}

.container .protocol {
	color: #4fadc2;
	font-size: 24px;
	font-weight: 500px;
}

.container .demo {
	color: #4fadc2;
	max-width: 400px;
}

.apimenu {
	
}

.apimenu li {
	margin: 0px;
	padding: 2px;
	line-height: 14px;
	font-size: 14px;
}

.apimenu li a {
	margin: 0px;
	padding: 4px;
}

.page-header {
	padding-top: 50px;
}

caption {
	text-align: left;
	color: #CE4844;
}
-->
</style>
</head>
<body>
	<jsp:include page="../web-simple/commons/head.jsp"></jsp:include>
	<jsp:include page="../web-simple/commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="border-left: 1px dashed #cccccc;">
					<div class="row">
						<div class="col-sm-3 ">
							<nav
								class="bs-docs-sidebar hidden-print hidden-xs hidden-sm affix"
								style="width: 250px;">
								<ul class="nav bs-docs-sidenav apimenu"
									style="max-height: 500px; overflow: auto;">
									<li><a href="helper/readme.do#fireRead">准备</a></li>
									<!-- <li><a href="helper/readme.do#secretApi">权限码secret----[<b>查询</b>]
					</a></li> -->
									<li><a href="helper/readme.do#orgGetApi">组织机构----[<b>查询</b>]
									</a></li>
									<li><a href="helper/readme.do#orgPostApi">组织机构----[<b>创建</b>]
									</a></li>
									<li><a href="helper/readme.do#orgPutApi">组织机构----[<b>更新</b>]
									</a></li>
									<li><a href="helper/readme.do#orgDeleteApi">组织机构----[<b>删除</b>]
									</a></li>
									<li><hr style="margin: 1px;" /></li>
									<li><a href="helper/readme.do#userGetApi">用户----[<b>查询</b>]
									</a></li>
									<li><a href="helper/readme.do#userPostApi">用户----[<b>创建</b>]
									</a></li>
									<li><a href="helper/readme.do#userPutApi">用户----[<b>更新</b>]
									</a></li>
									<li><a href="helper/readme.do#userDeleteApi">用户----[<b>删除</b>]
									</a></li>
									<li><a href="helper/readme.do#isLegalityApi">用户----[<b>校验密码</b>]
									</a></li>
									<li><a href="helper/readme.do#userInfoApi">用户----[<b>由登录名获得用户</b>]
									</a></li>
									<li><hr style="margin: 1px;" /></li>
									<li><a href="helper/readme.do#ViewIsConvert">[<b>预览</b>]----附件可否生成预览文件
									</a></li>

									<li><a href="helper/readme.do#FileProperty">[<b>文件</b>]----系统属性
									</a></li>

									<li><hr style="margin: 1px;" /></li>
									<li><a href="helper/readme.do#FileInitSpace">[<b>文件</b>]----申请附件空间
									</a></li>
									<li><a href="helper/readme.do#FileFindId">[<b>文件</b>]----通过APPID获得FILEID
									</a></li>
									<li><a href="helper/readme.do#FileToPrisiste">[<b>文件</b>]----临时文件转为持久化文件
									</a></li>
									<li><a href="helper/readme.do#FileToFree">[<b>文件</b>]----持久化文件转为临时文件
									</a></li>
									<li><a href="helper/readme.do#FileDel">[<b>文件</b>]----逻辑删除临时文件
									</a></li>
									<li><a href="helper/readme.do#FileGetInfo">[<b>文件</b>]----獲得文件信息
									</a></li>
									<li><a href="helper/readme.do#FileIcon">[<b>文件</b>]----获得附件图标下载地址
									</a></li>
									<li><a href="helper/readme.do#FileDown">[<b>文件</b>]----获得附件文件下载地址
									</a></li>
									<li><a href="helper/readme.do#FileText">[<b>文件</b>]----获得附件的文本信息
									</a></li>
									<li><hr style="margin: 1px;" /></li>
									<li><a href="helper/readme.do#ViewGenerate">[<b>预览</b>]----生成预览文件
									</a></li>
									<li><a href="helper/readme.do#ViewAble">[<b>预览</b>]----预览文件是否就緒
									</a></li>
									<li><a href="helper/readme.do#ViewTasks">[<b>预览</b>]----获得转换任务信息
									</a></li>
									<li><a href="helper/readme.do#ViewDocs">[<b>预览</b>]----获得预览文件信息
									</a></li>
									<li><a href="helper/readme.do#ViewLogs">[<b>预览</b>]----获得任务日志
									</a></li>
									<li><hr style="margin: 1px;" /></li>
									<li><a href="helper/readme.do#CompageUrl">[<b>web组件</b>]----页面URL
									</a></li>
								</ul>
							</nav>
						</div>
						<div class="col-sm-9">
							<!-- 准备 -->
							<div id="fireRead" class="page-header">
								<jsp:include page="comment/firstRead.jsp"></jsp:include>
							</div>
							<!-- secret--<div id="secretApi" class="page-header">
				<---jsp:----include page="comment/secretApi.jsp"></---jsp---:include>
			</div>>
			
			<!-- 机构查询 -->
							<div id="orgGetApi" class="page-header">
								<jsp:include page="comment/orgGetApi.jsp"></jsp:include>
							</div>
							<!-- 机构创建 -->
							<div id="orgPostApi" class="page-header">
								<jsp:include page="comment/orgPostApi.jsp"></jsp:include>
							</div>
							<!-- 机构更新-->
							<div id="orgPutApi" class="page-header">
								<jsp:include page="comment/orgPutApi.jsp"></jsp:include>
							</div>
							<!-- 机构删除-->
							<div id="orgDeleteApi" class="page-header">
								<jsp:include page="comment/orgDeleteApi.jsp"></jsp:include>
							</div>
							<!-- 用户查询 -->
							<div id="userGetApi" class="page-header">
								<jsp:include page="comment/userGetApi.jsp"></jsp:include>
							</div>
							<!-- 用户创建 -->
							<div id="userPostApi" class="page-header">
								<jsp:include page="comment/userPostApi.jsp"></jsp:include>
							</div>
							<!-- 用户更新-->
							<div id="userPutApi" class="page-header">
								<jsp:include page="comment/userPutApi.jsp"></jsp:include>
							</div>
							<!-- 用户删除-->
							<div id="userDeleteApi" class="page-header">
								<jsp:include page="comment/userDeleteApi.jsp"></jsp:include>
							</div>
							<!-- 校验密码-->
							<div id="isLegalityApi" class="page-header">
								<jsp:include page="comment/isLegalityApi.jsp"></jsp:include>
							</div>
							<!-- 由登錄名獲得用戶信息 -->
							<div id="userInfoApi" class="page-header">
								<jsp:include page="comment/userInfoApi.jsp"></jsp:include>
							</div>
							<!-- 附件可否生成预览文件-->
							<div id="ViewIsConvert" class="page-header">
								<jsp:include page="comment/ViewIsConvert.jsp"></jsp:include>
							</div>
							<!-- 附件配置属性-->
							<div id="FileProperty" class="page-header">
								<jsp:include page="comment/FileProperty.jsp"></jsp:include>
							</div>
							<!-- 申请附件空间-->
							<div id="FileInitSpace" class="page-header">
								<jsp:include page="comment/FileInitSpace.jsp"></jsp:include>
							</div>
							<!-- 通过APPID获得FILEID-->
							<div id="FileFindId" class="page-header">
								<jsp:include page="comment/FileFindId.jsp"></jsp:include>
							</div>
							<!-- 临时文件转为持久化文件-->
							<div id="FileToPrisiste" class="page-header">
								<jsp:include page="comment/FileToPrisiste.jsp"></jsp:include>
							</div>
							<!-- 持久化文件转为临时文件-->
							<div id="FileToFree" class="page-header">
								<jsp:include page="comment/FileToFree.jsp"></jsp:include>
							</div>
							<!-- 逻辑删除临时文件-->
							<div id="FileDel" class="page-header">
								<jsp:include page="comment/FileDel.jsp"></jsp:include>
							</div>
							<!-- 獲得文件信息-->
							<div id="FileGetInfo" class="page-header">
								<jsp:include page="comment/FileGetInfo.jsp"></jsp:include>
							</div>
							<!-- 获得附件图标下载地址-->
							<div id="FileIcon" class="page-header">
								<jsp:include page="comment/FileGetIcon.jsp"></jsp:include>
							</div>
							<!-- 获得附件文件下载地址-->
							<div id="FileDown" class="page-header">
								<jsp:include page="comment/FileGetDown.jsp"></jsp:include>
							</div>
							<!-- 获得附件的文本信息-->
							<div id="FileText" class="page-header">
								<jsp:include page="comment/FileText.jsp"></jsp:include>
							</div>
							<!-- 生成预览文件-->
							<div id="ViewGenerate" class="page-header">
								<jsp:include page="comment/ViewGenerate.jsp"></jsp:include>
							</div>
							<!-- 预览文件是否就緒-->
							<div id="ViewAble" class="page-header">
								<jsp:include page="comment/ViewAble.jsp"></jsp:include>
							</div>
							<!-- 获得转换任务信息-->
							<div id="ViewTasks" class="page-header">
								<jsp:include page="comment/ViewTasks.jsp"></jsp:include>
							</div>
							<!-- 获得预览文件信息-->
							<div id="ViewDocs" class="page-header">
								<jsp:include page="comment/ViewDocs.jsp"></jsp:include>
							</div>
							<!-- 获得任务日志-->
							<div id="ViewLogs" class="page-header">
								<jsp:include page="comment/ViewLogs.jsp"></jsp:include>
							</div>
							<!-- web组件URL-->
							<div id="CompageUrl" class="page-header">
								<jsp:include page="comment/CompageUrl.jsp"></jsp:include>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../web-simple/commons/footServer.jsp"></jsp:include>
	<jsp:include page="../web-simple/commons/foot.jsp"></jsp:include>
</body>
</html>