<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<h1>附件配置属性</h1>
<p class="protocol">${CURL}/fileapi/file/property.do</p>
<h3>参数</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>属性</th>
			<th>描述</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">secret</th>
			<td>权限码</td>
			<td class="demo">必填,通过知识库配置文件预先配置</td>
		</tr>
		<tr>
			<th scope="row">operatorLoginname</th>
			<td>操作用户登陆名称</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">operatorPassword</th>
			<td>操作用户登陆密码</td>
			<td class="demo">必填</td>
		</tr>
	</tbody>
</table>
<h3>返回值</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>参数</th>
			<th>值</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">STATE</th>
			<td>状态</td>
			<td class="demo">0成功,1失败</td>
		</tr>
		<tr>
			<th scope="row">DATA|FILESIZE</th>
			<td>文件上传限制</td>
			<td class="demo">999b</td>
		</tr>
		<tr>
			<th scope="row">DATA|IMGSIZE</th>
			<td>图片上传限制</td>
			<td class="demo">999b</td>
		</tr>
		<tr>
			<th scope="row">DATA|FILETYPE</th>
			<td>文件类型限制</td>
			<td class="demo">png,jpg,jpeg,gif,zip,doc,docx,xls,xlsx,pdf,ppt,pptx,web,rar,txt,flv,mp3,mp4,dcr</td>
		</tr>
		<tr>
			<th scope="row">DATA|IMGTYPE</th>
			<td>图片类型限制</td>
			<td class="demo">png,jpg,jpeg,gif</td>
		</tr>
		<tr>
			<th scope="row">DATA|MEDIATYPE</th>
			<td>多媒体类型限制</td>
			<td class="demo">mp3,mp4</td>
		</tr>
		<tr>
			<th scope="row">MESSAGE</th>
			<td>错误信息</td>
			<td class="demo"></td>
		</tr>
	</tbody>
</table>