<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<h1>获得转换任务信息</h1>
<p class="protocol">${CURL}/viewapi/tasks.do</p>
<h3>参数</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>属性</th>
			<th>描述</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">fileid</th>
			<td>附件ID</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">secret</th>
			<td>权限码</td>
			<td class="demo">必填,通过知识库配置文件预先配置</td>
		</tr>
		<tr>
			<th scope="row">operatorLoginname</th>
			<td>操作用户登陆名称</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">operatorPassword</th>
			<td>操作用户登陆密码</td>
			<td class="demo">必填</td>
		</tr>
	</tbody>
</table>
<h3>返回值</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>参数</th>
			<th>值</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">STATE</th>
			<td>状态</td>
			<td class="demo">0成功,1失败</td>
		</tr>
		<tr>
			<th scope="row">viewtasks</th>
			<td>附件对应的预览任务(多个)</td>
			<td class="demo">[{"BTITLE":"原Office文件-转-预览HTML",<br />"STIME":"20190722211322",<br />"CUSERNAME":"NONE",<br />"DTITLE":"HTML预览文件",<br />"PCONTENT":null,<br />"VIEWDOCID":"4028b8816c19ce18016c19d0b1dc002f",<br />"CONVERTORID":"4028b8816a4f7e90016a4f807ef60001",<br />,"PSTATE":"完成"<span
				style="color: red;">(0:等待,1:执行,2:完成,3:错误,4:禁用)</span>,<br />"PSTATE":"完成",<br />"CTIME":"2019-07-22
				21:13:21",<br />"CUSER":"NONE",<br />"CTITLE":"demo.docx",<br />"ETIME":"20190722211323",<br />"ID":"4028b8816c19ce18016c19d0a9750028",<br />"BEFORETASK":null,<br />"FILEID":"4028b8816c19ce18016c19d0a8c70025"}]
			</td>
		</tr>
		<tr>
			<th scope="row">MESSAGE</th>
			<td>错误信息</td>
			<td class="demo"></td>
		</tr>
	</tbody>
</table>