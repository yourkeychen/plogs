<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<h1>获得预览文件信息</h1>
<p class="protocol">${CURL}/viewapi/viewdocs.do</p>
<h3>参数</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>属性</th>
			<th>描述</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">fileid</th>
			<td>附件ID</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">secret</th>
			<td>权限码</td>
			<td class="demo">必填,通过知识库配置文件预先配置</td>
		</tr>
		<tr>
			<th scope="row">operatorLoginname</th>
			<td>操作用户登陆名称</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">operatorPassword</th>
			<td>操作用户登陆密码</td>
			<td class="demo">必填</td>
		</tr>
	</tbody>
</table>
<h3>返回值</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>参数</th>
			<th>值</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">STATE</th>
			<td>状态</td>
			<td class="demo">0成功,1失败</td>
		</tr>
		<tr>
			<th scope="row">viewdocs</th>
			<td>附件对应的预览文档（多个）</td>
			<td class="demo">[{"DOWNURL":null,<br />"MODELTITLE":"图片册",<br />"ID":"4028b8816c19ce18016c19f95dea008f",<br />"VIEWIS":"1"<span
				style="color: red;">(1可以预览0禁止预览)</span>,<br />"MODELKEY":"IMGS",<br />"VIEWURL":"show/Pubimgs.do?viewdocId=4028b8816c19ce18016c19f95dea008f",<br />"FILEID":"4028b8816c19ce18016c19f953aa007f",<br />"PSTATE":"2"<span
				style="color: red;">(0排队1转换中2完成9错误)</span>,<br />"ICONURL":"downview/Pubicon.do?id=4028b8816c19ce18016c19f95dea008f",<br />"CTIME":"20190722215748",<br />"FILETITLE":"demo.docx"}]
			</td>
		</tr>
		<tr>
			<th scope="row">MESSAGE</th>
			<td>错误信息</td>
			<td class="demo"></td>
		</tr>
	</tbody>
</table>