<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<h1>獲得文件信息</h1>
<p class="protocol">${CURL}/fileapi/file/info.do</p>
<h3>参数</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>属性</th>
			<th>描述</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">fileid</th>
			<td>附件id</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">secret</th>
			<td>权限码</td>
			<td class="demo">必填,通过知识库配置文件预先配置</td>
		</tr>
		<tr>
			<th scope="row">operatorLoginname</th>
			<td>操作用户登陆名称</td>
			<td class="demo">必填</td>
		</tr>
		<tr>
			<th scope="row">operatorPassword</th>
			<td>操作用户登陆密码</td>
			<td class="demo">必填</td>
		</tr>
	</tbody>
</table>
<h3>返回值</h3>
<table class="table table-striped">
	<caption></caption>
	<thead>
		<tr>
			<th>参数</th>
			<th>值</th>
			<th>例子</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">STATE</th>
			<td>状态</td>
			<td class="demo">0成功,1失败</td>
		</tr>
		<tr>
			<th scope="row">info.file</th>
			<td>文件信息對象</td>
			<td class="demo">{"id":"4028b8816add7af8016add7c2d470001", <br />"resourceid":"4028b88169fd1b650169fd331e56000a",
				<br />"sysname":"本地", <br />"pstate":"1", <br />"exname":"pdf", <br />"relativepath":"\\2019\\05\\22\\11\\",
				<br />"secret":"de61267cd3ee425eadd5d31aa6ee000f", <br />"filename":"42f36d55a37b4c8aa132f07c24cec97e.pdf.file",
				<br />"title":"报价单.pdf", <br />"filesize":129467, <br />"pcontent":null,<br />
				"euser":"40288b854a329988014a329a12f30002",<br />"eusername":"系统管理员",
				<br />"cuser":"40288b854a329988014a329a12f30002",<br />"cusername":"系统管理员",
				<br />"etime":"20190522110103",<br />"ctime":"20190522110103"}
			</td>
		</tr>
		<tr>
			<th scope="row">info.downloadUrl</th>
			<td>下載地址</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">info.iconUrl</th>
			<td>附件图标地址</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">info.viewUrl</th>
			<td>预览页面地址</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">info.statelabel</th>
			<td>状态标签</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">info.modellabel</th>
			<td>模型标签</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">info.resource</th>
			<td>资源库信息</td>
			<td class="demo">{"id":"4028b88169fd1b650169fd331e56000a",<br />"path":"D:\\wcp3server\\wdapFile2",<br />"state":"1",<br />"pcontent":"",<br />"euser":"40288b854a329988014a329a12f30002",<br />"eusername":"系统管理员",<br />"cuser":"40288b854a329988014a329a12f30002",<br />"cusername":"系统管理员",<br />"etime":"20190409114720",<br />"ctime":"20190408214619",<br />"title":"资源库"}
			</td>
		</tr>
		<tr>
			<th scope="row">info.realpath</th>
			<td>真实地址</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">info.sizelabel</th>
			<td>文件大小</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">info.visit</th>
			<td>访问信息对象</td>
			<td class="demo">{"id":"4028b8816add7af8016add7c2d480002",<br />"fileid":"4028b8816add7af8016add7c2d470001",<br />"viewnum":3,<br />"downum":0}
			</td>
		</tr>
		<tr>
			<th scope="row">info.viewNum</th>
			<td>预览数量</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">info.dowNum</th>
			<td>下载数量</td>
			<td class="demo"></td>
		</tr>
		<tr>
			<th scope="row">MESSAGE</th>
			<td>错误信息</td>
			<td class="demo"></td>
		</tr>
	</tbody>
</table>