<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- 机构接口 -->
<h1>页面组件</h1>
<h2>附件预览页面URL：</h2>
<p class="protocol">${CURL}/include/Pubview.do?fileid=[fileid]</p>
<h2>附件转换日志页面URL：</h2>
<p class="protocol">${CURL}/include/Pubtasks.do?fileid=[fileid]</p>
<h2>附件下载URL：</h2>
<p class="protocol">${CURL}/include/Pubdown.do?id=[fileid]&secret=[secret]</p>
