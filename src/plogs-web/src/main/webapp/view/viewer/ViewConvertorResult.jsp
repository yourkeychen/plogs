<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>文件转换器数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchViewconvertorForm">
			<table class="editTable">
				<tr>
					<td class="title">转换器名称:</td>
					<td><input name="a.TITLE:like" type="text"></td>
					<td class="title"></td>
					<td></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataViewconvertorGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="TITLE" data-options="sortable:true" width="80">转换器名称</th>
					<th field="CTIME" data-options="sortable:true" width="40">创建时间</th>
					<th field="OTITLE" data-options="sortable:true" width="40">初始模型</th>
					<th field="TTITLE" data-options="sortable:true" width="40">目标模型</th>
					<th field="CLASSKEY" data-options="sortable:true" width="100">转换器实现类</th>
					<th field="PSTATE" data-options="sortable:true" width="40">状态</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionViewconvertor = "viewconvertor/del.do";//删除URL
	var url_formActionViewconvertor = "viewconvertor/form.do";//增加、修改、查看URL
	var url_searchActionViewconvertor = "viewconvertor/query.do";//查询URL
	var title_windowViewconvertor = "文件转换器管理";//功能名称
	var gridViewconvertor;//数据表格对象
	var searchViewconvertor;//条件查询组件对象
	var toolBarViewconvertor = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataViewconvertor
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataViewconvertor
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataViewconvertor
	}, {
		id : 'copy',
		text : '复制',
		iconCls : 'icon-administrative-docs',
		handler : copyDataViewconvertor
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataViewconvertor
	} ];
	$(function() {
		//初始化数据表格
		gridViewconvertor = $('#dataViewconvertorGrid').datagrid({
			url : url_searchActionViewconvertor,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarViewconvertor,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchViewconvertor = $('#searchViewconvertorForm').searchForm({
			gridObj : gridViewconvertor
		});
	});
	//查看
	function viewDataViewconvertor() {
		var selectedArray = $(gridViewconvertor).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewconvertor + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewconvertor',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataViewconvertor() {
		var url = url_formActionViewconvertor + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winViewconvertor',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataViewconvertor() {
		var selectedArray = $(gridViewconvertor).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewconvertor + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewconvertor',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//复制
	function copyDataViewconvertor() {
		var selectedArray = $(gridViewconvertor).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			$.messager.confirm(MESSAGE_PLAT.PROMPT, "确认复制该记录", function(flag) {
				if (flag) {
					$(gridViewconvertor).datagrid('loading');
					$.post('viewconvertor/copy.do' + '?ids='
							+ $.farm.getCheckedIds(gridViewconvertor, 'ID'),
							{}, function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridViewconvertor).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridViewconvertor).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//删除
	function delDataViewconvertor() {
		var selectedArray = $(gridViewconvertor).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridViewconvertor).datagrid('loading');
					$.post(url_delActionViewconvertor + '?ids='
							+ $.farm.getCheckedIds(gridViewconvertor, 'ID'),
							{}, function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridViewconvertor).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridViewconvertor).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>