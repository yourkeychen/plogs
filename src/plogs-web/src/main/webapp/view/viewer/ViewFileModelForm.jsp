<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--预览文件模型表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formViewmodel">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<!-- <tr>
					<td class="title">状态:</td>
					<td colspan="3">
					<select id="entity_pstate" name="pstate" value="${entity.pstate}">
					<option value=""></option>
					
					</select>
					
					</td>
				</tr> -->
				<tr>
					<td class="title">展示序号:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[5]']"
						id="entity_sortnum" name="sortnum" value="${entity.sortnum}"></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea style="width: 360px; height: 100px" 
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityViewmodel" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityViewmodel" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formViewmodel" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionViewmodel = 'viewmodel/add.do';
	var submitEditActionViewmodel = 'viewmodel/edit.do';
	var currentPageTypeViewmodel = '${pageset.operateType}';
	var submitFormViewmodel;
	$(function() {
		//表单组件对象
		submitFormViewmodel = $('#dom_formViewmodel').SubmitForm({
			pageType : currentPageTypeViewmodel,
			grid : gridViewmodel,
			currentWindowId : 'winViewmodel'
		});
		//关闭窗口
		$('#dom_cancle_formViewmodel').bind('click', function() {
			$('#winViewmodel').window('close');
		});
		//提交新增数据
		$('#dom_add_entityViewmodel').bind('click', function() {
			submitFormViewmodel.postSubmit(submitAddActionViewmodel);
		});
		//提交修改数据
		$('#dom_edit_entityViewmodel').bind('click', function() {
			submitFormViewmodel.postSubmit(submitEditActionViewmodel);
		});
	});
//-->
</script>