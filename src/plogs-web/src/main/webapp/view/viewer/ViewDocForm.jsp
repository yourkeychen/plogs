<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--预览文件表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formViewdoc">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">是否可以预览:</td>
					<td colspan="3">
					<select id="entity_viewis" name="viewis" val="${entity.viewis}">
							<option value="1">可以</option>
							<option value="0">禁用</option>
					</select>
					</td>
				</tr>
				<tr>
					<td class="title">状态:</td>
					<td colspan="3"><select id="entity_pstate" name="pstate"
						val="${entity.pstate}">
							<option value="2">完成</option>
							<option value="9">禁用</option>
					</select></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityViewdoc" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityViewdoc" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formViewdoc" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionViewdoc = 'viewdoc/add.do';
	var submitEditActionViewdoc = 'viewdoc/edit.do';
	var currentPageTypeViewdoc = '${pageset.operateType}';
	var submitFormViewdoc;
	$(function() {
		//表单组件对象
		submitFormViewdoc = $('#dom_formViewdoc').SubmitForm({
			pageType : currentPageTypeViewdoc,
			grid : gridViewdoc,
			currentWindowId : 'winViewdoc'
		});
		//关闭窗口
		$('#dom_cancle_formViewdoc').bind('click', function() {
			$('#winViewdoc').window('close');
		});
		//提交新增数据
		$('#dom_add_entityViewdoc').bind('click', function() {
			submitFormViewdoc.postSubmit(submitAddActionViewdoc);
		});
		//提交修改数据
		$('#dom_edit_entityViewdoc').bind('click', function() {
			submitFormViewdoc.postSubmit(submitEditActionViewdoc);
		});
	});
//-->
</script>