<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--文件转换器表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formViewconvertor">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">原始模型:</td>
					<td><select id="entity_omodelid" name="omodelid"
						val="${entity.omodelid}">
							<c:forEach items="${models}" var="node">
								<option value="${node.id}">${node.title}</option>
							</c:forEach>
					</select></td>
					<td class="title">目标模型:</td>
					<td><select id="entity_tmodelid" name="tmodelid"
						val="${entity.tmodelid}">
							<c:forEach items="${models}" var="node">
								<option value="${node.id}">${node.title}</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td class="title">转换器实现类:</td>
					<td><select id="entity_classkey" name="classkey"
						val="${entity.classkey}">
							<c:forEach items="${convertors}" var="node">
								<option value="${node.key}">${node.title}</option>
							</c:forEach>
					</select></td>
					<td class="title">状态:</td>
					<td><select id="entity_pstate" name="pstate"
						value="${entity.pstate}">
							<option value="1">可用</option>
							<option value="0">禁用</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">转换器名称:</td>
					<td colspan="3"><input type="text"
						style="width: 270px; height: 28px;" class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[32]']"
						id="entity_title" name="title" value="${entity.title}">&nbsp;<a
						id="initTitleBtn" class="easyui-linkbutton"
						style="margin-top: -2px;" data-options="iconCls:'icon-cost'">生成名称</a></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea style="width: 360px; height: 50px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityViewconvertor" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityViewconvertor" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formViewconvertor" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionViewconvertor = 'viewconvertor/add.do';
	var submitEditActionViewconvertor = 'viewconvertor/edit.do';
	var currentPageTypeViewconvertor = '${pageset.operateType}';
	var submitFormViewconvertor;
	$(function() {
		//表单组件对象
		submitFormViewconvertor = $('#dom_formViewconvertor').SubmitForm({
			pageType : currentPageTypeViewconvertor,
			grid : gridViewconvertor,
			currentWindowId : 'winViewconvertor'
		});
		//关闭窗口
		$('#dom_cancle_formViewconvertor').bind('click', function() {
			$('#winViewconvertor').window('close');
		});
		//提交新增数据
		$('#dom_add_entityViewconvertor').bind('click', function() {
			submitFormViewconvertor.postSubmit(submitAddActionViewconvertor);
		});
		//提交修改数据
		$('#dom_edit_entityViewconvertor').bind('click', function() {
			submitFormViewconvertor.postSubmit(submitEditActionViewconvertor);
		});
		//生成标题
		$('#initTitleBtn').bind('click',function() {
			$('#entity_title').val("[文件类型]"+ $("#entity_omodelid").find("option:selected").text()+"-To-"+$("#entity_tmodelid").find("option:selected").text()+"");
		});
	});
//-->
</script>