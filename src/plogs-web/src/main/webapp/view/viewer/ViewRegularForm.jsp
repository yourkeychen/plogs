<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--转换规则表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formViewregular">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title" style="width: 80px;"></td>
					<td></td>
					<td class="title" style="width: 70px;"></td>
					<td></td>
				</tr>
				<tr>
					<td class="title">源扩展名:</td>
					<td colspan="3"><select style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_fileexname" name="fileexname"
						val="${entity.fileexname}">
							<c:forEach items="${exnames}" var="node">
								<option value="${node}">${node}</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td class="title">转换器:</td>
					<td colspan="3"><select style="width: 420px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[32]']"
						id="entity_convertorid" name="convertorid"
						val="${entity.convertorid}">
							<c:forEach items="${convertors }" var="node">
								<option value="${node.id}">${node.title}</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td class="title">执行序号:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['intVal','maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}">&nbsp;依赖前置序号</td>
					<td class="title">超时时间:</td>
					<td><input type="text" class="easyui-validatebox"
						style="width: 120px;"
						data-options="required:true,validType:[,'maxLength[32]']"
						id="entity_outtimenum" name="outtimenum"
						value="${entity.outtimenum}">&nbsp;单位:毫秒</td>
				</tr>
				<tr>
					<td class="title"></td>
					<td style="font-size: 12px; color: green;">0为无序</td>
					<td class="title"></td>
					<td style="font-size: 12px; color: green;"
						id="entity_outtimenum_title"></td>
				</tr>
				<tr>
					<td class="title">最小文件:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[10]']"
						id="entity_sizemin" name="sizemin" value="${entity.sizemin}">&nbsp;单位：b
					</td>
					<td class="title">最大文件:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[10]']"
						id="entity_sizemax" name="sizemax" value="${entity.sizemax}">&nbsp;单位：b
					</td>
				</tr>
				<tr>
					<td class="title"></td>
					<td style="font-size: 12px; color: green;"
						id="entity_sizemin_title"></td>
					<td class="title"></td>
					<td style="font-size: 12px; color: green;"
						id="entity_sizemax_title"></td>
				</tr>
				<tr>
					<td class="title">是否预览:</td>
					<td><select style="width: 120px;" id="entity_viewis"
						name="viewis" val="${entity.viewis}">
							<option value="1">可以预览</option>
							<option value="0">禁止预览</option>
					</select></td>
					<td class="title">状态:</td>
					<td><select style="width: 120px;" id="entity_pstate"
						name="pstate" val="${entity.pstate}">
							<option value="1">可用</option>
							<option value="0">禁用</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">规则名称:</td>
					<td colspan="3"><input type="text" style="width: 330px; height: 28px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[32]']"
						id="entity_title" name="title" value="${entity.title}">&nbsp;<a
						id="initTitleBtn" class="easyui-linkbutton"
						style="margin-top: -2px;" data-options="iconCls:'icon-cost'">生成名称</a></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea type="text" style="width: 420px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityViewregular" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityViewregular" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formViewregular" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionViewregular = 'viewregular/add.do';
	var submitEditActionViewregular = 'viewregular/edit.do';
	var currentPageTypeViewregular = '${pageset.operateType}';
	var submitFormViewregular;
	$(function() {
		//生成标题
		$('#initTitleBtn').bind('click',function() {
			$('#entity_title').val($("#entity_fileexname").find("option:selected").text()+":"+$("#entity_convertorid").find("option:selected").text());
		});
		//表单组件对象
		submitFormViewregular = $('#dom_formViewregular').SubmitForm({
			pageType : currentPageTypeViewregular,
			grid : gridViewregular,
			currentWindowId : 'winViewregular'
		});
		//关闭窗口
		$('#dom_cancle_formViewregular').bind('click', function() {
			$('#winViewregular').window('close');
		});
		//提交新增数据
		$('#dom_add_entityViewregular').bind('click', function() {
			submitFormViewregular.postSubmit(submitAddActionViewregular);
		});
		//提交修改数据
		$('#dom_edit_entityViewregular').bind('click', function() {
			submitFormViewregular.postSubmit(submitEditActionViewregular);
		});

		$('#entity_sizemin').change(function() {
			formatMinsize();
		});
		formatMinsize('${entity.sizemin}');
		$('#entity_sizemax').change(function() {
			formatMaxsize();
		});
		formatMaxsize('${entity.sizemax}');
		$('#entity_outtimenum').change(function() {
			formatOutTime();
		});
		formatOutTime('${entity.outtimenum}');

	});
	function formatMinsize(val) {
		if (val) {
			formatSize(val, "entity_sizemin_title");
		} else {
			formatSize($('#entity_sizemin').val(), "entity_sizemin_title");
		}
	}
	function formatMaxsize(val) {
		if (val) {
			formatSize(val, "entity_sizemax_title");
		} else {
			formatSize($('#entity_sizemax').val(), "entity_sizemax_title");
		}

	}
	function formatOutTime(val) {
		if (val) {
			formatTime(val, "entity_outtimenum_title");
		} else {
			formatTime($('#entity_outtimenum').val(), "entity_outtimenum_title");
		}
	}
	function formatSize(size, domid) {
		var resize = size / 1024 / 1024;
		$('#' + domid).text(resize.toFixed(4) + "MB");
	}
	function formatTime(size, domid) {
		var resize = size / 1000;
		$('#' + domid).text(resize.toFixed(2) + "s");
	}
//-->
</script>