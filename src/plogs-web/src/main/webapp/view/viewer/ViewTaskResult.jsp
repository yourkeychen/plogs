<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>转换任务数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchViewtaskForm">
			<table class="editTable">
				<tr>
					<td class="title">文件名称:</td>
					<td><input name="c.TITLE:like" type="text"></td>
					<td class="title">文件名称:</td>
					<td><input name="a.FILEID:like" type="text"></td>
				</tr>
				<tr>
					<td class="title">状态:</td>
					<td><select name="a.pstate:=">
							<option value="">~全部~</option>
							<option value="0">等待中</option>
							<option value="1">執行中</option>
							<option value="2">完成</option>
							<option value="3">错误</option>
							<option value="4">禁用</option>
					</select></td>
					<td class="title">任务id</td>
					<td><input name="a.ID:like" type="text"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataViewtaskGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="SHORTID" data-options="sortable:true" width="20">任务id(后10位)</th>
					<th field="CTITLE" data-options="sortable:true" width="40">原文件</th>
					<th field="BTITLE" data-options="sortable:true" width="30">转化器</th>
					<th field="DTITLE" data-options="sortable:true" width="20">目标模型</th>
					<th field="CTIME" data-options="sortable:true" width="20">入队时间</th>
					<!-- <th field="ETIME" data-options="sortable:true" width="20">结束转换时间</th>  -->
					<th field="STIME" data-options="sortable:true" width="20">开始转换时间</th>
					<th field="BEFORETASK" data-options="sortable:true" width="20">依赖任务id(后10位)</th>
					<th field="PSTATE" data-options="sortable:true" width="10">状态</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionViewtask = "viewtask/del.do";//删除URL
	var url_formActionViewtask = "viewtask/form.do";//增加、修改、查看URL
	var url_searchActionViewtask = "viewtask/query.do";//查询URL
	var title_windowViewtask = "转换任务管理";//功能名称
	var gridViewtask;//数据表格对象
	var searchViewtask;//条件查询组件对象
	var toolBarViewtask = [ {
		id : 'view',
		text : '查看/日志',
		iconCls : 'icon-tip',
		handler : viewDataViewtask
	}/**, {
							id : 'add',
							text : '新增',
							iconCls : 'icon-add',
							handler : addDataViewtask
						}, {
							id : 'edit',
							text : '修改',
							iconCls : 'icon-edit',
							handler : editDataViewtask
						}**/
	, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataViewtask
	} ];
	$(function() {
		//初始化数据表格
		gridViewtask = $('#dataViewtaskGrid').datagrid({
			url : url_searchActionViewtask,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarViewtask,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchViewtask = $('#searchViewtaskForm').searchForm({
			gridObj : gridViewtask
		});
	});
	//查看
	function viewDataViewtask() {
		var selectedArray = $(gridViewtask).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewtask + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewtask',
				width : 700,
				height : 400,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataViewtask() {
		var url = url_formActionViewtask + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winViewtask',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataViewtask() {
		var selectedArray = $(gridViewtask).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewtask + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewtask',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataViewtask() {
		var selectedArray = $(gridViewtask).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridViewtask).datagrid('loading');
					$.post(url_delActionViewtask + '?ids='
							+ $.farm.getCheckedIds(gridViewtask, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridViewtask).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridViewtask).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>