<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>转换规则数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchViewregularForm">
			<table class="editTable">
				<tr>
					<td class="title">原文件扩展名:</td>
					<td><select name="FILEEXNAME:like">
							<option value="">~全部~</option>
							<c:forEach items="${exnames}" var="node">
								<option value="${node}">${node}</option>
							</c:forEach>
					</select></td>
					<td class="title">状态:</td>
					<td><select name="a.PSTATE:like">
							<option value="">~全部~</option>
							<option value="1">可用</option>
							<option value="0">禁用</option>
					</select></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataViewregularGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="FILEEXNAME" data-options="sortable:true" width="20">文件扩展名</th>
					<th field="TITLE" data-options="sortable:true" width="60">规则名称</th>
					<th field="SORT" data-options="sortable:true" width="30">执行序号</th>
					<th field="BTITLE" data-options="sortable:true" width="60">转换器</th>
					<th field="SIZEMIN" data-options="sortable:true" width="20">最小文件</th>
					<th field="SIZEMAX" data-options="sortable:true" width="20">最大文件</th>
					<th field="OUTTIMENUM" data-options="sortable:true" width="30">超时时间(秒)</th>
					<th field="VIEWIS" data-options="sortable:true" width="30">是否可以预览</th>
					<th field="PSTATE" data-options="sortable:true" width="20">状态</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionViewregular = "viewregular/del.do";//删除URL
	var url_formActionViewregular = "viewregular/form.do";//增加、修改、查看URL
	var url_searchActionViewregular = "viewregular/query.do";//查询URL
	var title_windowViewregular = "转换规则管理";//功能名称
	var gridViewregular;//数据表格对象
	var searchViewregular;//条件查询组件对象
	var toolBarViewregular = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataViewregular
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataViewregular
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataViewregular
	}, {
		id : 'editStat',
		text : '批量修改状态',
		iconCls : 'icon-report_edit',
		handler : editStateViewregulars
	}, {
		id : 'copy',
		text : '复制',
		iconCls : 'icon-administrative-docs',
		handler : copyDataViewregular
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataViewregular
	} ];
	$(function() {
		//初始化数据表格
		gridViewregular = $('#dataViewregularGrid').datagrid({
			url : url_searchActionViewregular,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarViewregular,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchViewregular = $('#searchViewregularForm').searchForm({
			gridObj : gridViewregular
		});
	});
	//查看
	function viewDataViewregular() {
		var selectedArray = $(gridViewregular).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewregular + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewregular',
				width : 600,
				height : 450,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataViewregular() {
		var url = url_formActionViewregular + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winViewregular',
			width : 600,
			height : 450,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataViewregular() {
		var selectedArray = $(gridViewregular).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewregular + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewregular',
				width : 600,
				height : 450,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//批量修改状态
	function editStateViewregulars() {
		var selectedArray = $(gridViewregular).datagrid('getSelections');
		if (selectedArray.length >= 1) {
			var url = 'viewregular/stateForm.do?ids='
					+ $.farm.getCheckedIds(gridViewregular, 'ID');
			$.farm.openWindow({
				id : 'winRegularState',
				width : 600,
				height : 200,
				modal : true,
				url : url,
				title : '修改状态'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//复制规则
	function copyDataViewregular() {
		var selectedArray = $(gridViewregular).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			$.messager.confirm(MESSAGE_PLAT.PROMPT, "确认复制该记录", function(flag) {
				if (flag) {
					$(gridViewregular).datagrid('loading');
					$.post('viewregular/copy.do?ids='
							+ $.farm.getCheckedIds(gridViewregular, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridViewregular).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridViewregular).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//删除
	function delDataViewregular() {
		var selectedArray = $(gridViewregular).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridViewregular).datagrid('loading');
					$.post(url_delActionViewregular + '?ids='
							+ $.farm.getCheckedIds(gridViewregular, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridViewregular).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridViewregular).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>