<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>预览文件数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchViewdocForm">
			<table class="editTable">
				<tr>
					<td class="title">文件ID:</td>
					<td><input name="A.FILEID:like" type="text"></td>
					<td class="title">文件名称</td>
					<td><input name="B.TITLE:like" type="text"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataViewdocGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="FILETITLE" data-options="sortable:true" width="60">文件名称</th>
					<th field="MODELTITLE" data-options="sortable:true" width="80">预览文件模型</th>
					<th field="VIEWIS" data-options="sortable:true" width="60">可预览</th>
					<th field="PSTATE" data-options="sortable:true" width="60">状态</th>
					<th field="CTIME" data-options="sortable:true" width="50">创建时间</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionViewdoc = "viewdoc/del.do";//删除URL
	var url_formActionViewdoc = "viewdoc/form.do";//增加、修改、查看URL
	var url_searchActionViewdoc = "viewdoc/query.do";//查询URL
	var title_windowViewdoc = "预览文件管理";//功能名称
	var gridViewdoc;//数据表格对象
	var searchViewdoc;//条件查询组件对象
	var toolBarViewdoc = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataViewdoc
	}/**, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataViewdoc
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataViewdoc
	} **/
	, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataViewdoc
	} ];
	$(function() {
		//初始化数据表格
		gridViewdoc = $('#dataViewdocGrid').datagrid({
			url : url_searchActionViewdoc,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarViewdoc,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchViewdoc = $('#searchViewdocForm').searchForm({
			gridObj : gridViewdoc
		});
	});
	//查看
	function viewDataViewdoc() {
		var selectedArray = $(gridViewdoc).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewdoc + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewdoc',
				width : 400,
				height : 200,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataViewdoc() {
		var url = url_formActionViewdoc + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winViewdoc',
			width : 400,
			height : 200,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataViewdoc() {
		var selectedArray = $(gridViewdoc).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewdoc + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewdoc',
				width : 400,
				height : 200,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataViewdoc() {
		var selectedArray = $(gridViewdoc).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridViewdoc).datagrid('loading');
					$.post(url_delActionViewdoc + '?ids='
							+ $.farm.getCheckedIds(gridViewdoc, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridViewdoc).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridViewdoc).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>