<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--转换任务表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formViewtask">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">文件ID:</td>
					<td><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_fileid" name="fileid" value="${entity.fileid}">
					</td>

					<td class="title">转化器ID:</td>
					<td><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[16]']"
						id="entity_convertorid" name="convertorid"
						value="${entity.convertorid}"></td>
				</tr>
				<tr>
					<td class="title">开始转换时间:</td>
					<td><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[8]']" id="entity_stime"
						name="stime" value="${entity.stime}"></td>
					<td class="title">结束转换时间:</td>
					<td><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[8]']" id="entity_etime"
						name="etime" value="${entity.etime}"></td>
				</tr>
				<tr>
					<td class="title">预览文件ID:</td>
					<td><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_viewdocid" name="viewdocid" value="${entity.viewdocid}">
					</td>
					<td class="title">任务创建时间:</td>
					<td><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[8]']"
						id="entity_ctime" name="ctime" value=""> <PF:FormatTime
							date="${entity.ctime}" yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
				</tr>
				<tr>
					<td class="title">状态:</td>
					<td colspan="3"><select style="width: 360px;"
						id="entity_pstate" name="pstate" val="${entity.pstate}">
							<option value="0">等待</option>
							<option value="1">执行</option>
							<option value="2">完成</option>
							<option value="3">错误</option>
							<option value="4">禁用</option>
					</select></td>
				</tr>

			</table>
		</form>
		<div class="demo-info" style="margin: 4px;">
			<div class="demo-tip icon-tip"></div>
			<div>
				<c:forEach items="${logs}" var="node">
					<div>
						<PF:FormatTime date="${node.ctime}"
							yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" />
						:${node.pcontent}
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityViewtask" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityViewtask" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formViewtask" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionViewtask = 'viewtask/add.do';
	var submitEditActionViewtask = 'viewtask/edit.do';
	var currentPageTypeViewtask = '${pageset.operateType}';
	var submitFormViewtask;
	$(function() {
		//表单组件对象
		submitFormViewtask = $('#dom_formViewtask').SubmitForm({
			pageType : currentPageTypeViewtask,
			grid : gridViewtask,
			currentWindowId : 'winViewtask'
		});
		//关闭窗口
		$('#dom_cancle_formViewtask').bind('click', function() {
			$('#winViewtask').window('close');
		});
		//提交新增数据
		$('#dom_add_entityViewtask').bind('click', function() {
			submitFormViewtask.postSubmit(submitAddActionViewtask);
		});
		//提交修改数据
		$('#dom_edit_entityViewtask').bind('click', function() {
			submitFormViewtask.postSubmit(submitEditActionViewtask);
		});
	});
//-->
</script>