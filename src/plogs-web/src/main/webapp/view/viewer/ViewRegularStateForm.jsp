<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--转换规则表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary">共选择${size }条数据 </span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formRegularState">
			<input type="hidden" id="ids_id" name="ids" value="${ids}">
			<table class="editTable">
				<tr>
					<td class="title" style="width: 80px;"></td>
					<td></td>
					<td class="title" style="width: 70px;"></td>
					<td></td>
				</tr>
				<tr>
					<td class="title">是否预览:</td>
					<td><select style="width: 120px;" id="entity_viewis"
						name="viewis">
							<option value="">不变</option>
							<option value="1">可以预览</option>
							<option value="0">禁止预览</option>
					</select></td>
					<td class="title">状态:</td>
					<td><select style="width: 120px;" id="entity_pstate"
						name="pstate">
							<option value="">不变</option>
							<option value="1">可用</option>
							<option value="0">禁用</option>
					</select></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<a id="dom_edit_entityRegularState" href="javascript:void(0)"
				iconCls="icon-save" class="easyui-linkbutton">修改</a> <a
				id="dom_cancle_formRegularState" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitEditActionRegularState = 'viewregular/editState.do';
	var currentPageTypeRegularState = '2';
	var submitFormRegularState;
	$(function() {
		//表单组件对象
		submitFormRegularState = $('#dom_formRegularState').SubmitForm({
			pageType : currentPageTypeRegularState,
			grid : gridViewregular,
			currentWindowId : 'winRegularState'
		});
		//关闭窗口
		$('#dom_cancle_formRegularState').bind('click', function() {
			$('#winRegularState').window('close');
		});
		//提交修改数据
		$('#dom_edit_entityRegularState').bind('click', function() {
			submitFormRegularState.postSubmit(submitEditActionRegularState);
		});
	});
//-->
</script>