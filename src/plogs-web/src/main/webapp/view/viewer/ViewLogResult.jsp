<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<PF:basePath/>">
    <title>预览日志数据管理</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <jsp:include page="/view/conf/include.jsp"></jsp:include>
  </head>
  <body class="easyui-layout">
    <div data-options="region:'north',border:false">
      <form id="searchViewlogForm">
        <table class="editTable">
          <tr style="text-align: center;">
            <td colspan="4">
              <a id="a_search" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-search">查询</a>
              <a id="a_reset" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-reload">清除条件</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div data-options="region:'center',border:false">
      <table id="dataViewlogGrid">
        <thead>
          <tr>
            <th data-options="field:'ck',checkbox:true"></th>
            <th field="PCONTENT" data-options="sortable:true" width="40">日志内容</th>
            <th field="CTIME" data-options="sortable:true" width="40">记录时间</th>
            <th field="TASKID" data-options="sortable:true" width="60">TASKID</th>
            <th field="ID" data-options="sortable:true" width="20">ID</th>
          </tr>
        </thead>
      </table>
    </div>
  </body>
  <script type="text/javascript">
    var url_delActionViewlog = "viewlog/del.do";//删除URL
    var url_formActionViewlog = "viewlog/form.do";//增加、修改、查看URL
    var url_searchActionViewlog = "viewlog/query.do";//查询URL
    var title_windowViewlog = "预览日志管理";//功能名称
    var gridViewlog;//数据表格对象
    var searchViewlog;//条件查询组件对象
    var toolBarViewlog = [ {
      id : 'view',
      text : '查看',
      iconCls : 'icon-tip',
      handler : viewDataViewlog
    }, {
      id : 'add',
      text : '新增',
      iconCls : 'icon-add',
      handler : addDataViewlog
    }, {
      id : 'edit',
      text : '修改',
      iconCls : 'icon-edit',
      handler : editDataViewlog
    }, {
      id : 'del',
      text : '删除',
      iconCls : 'icon-remove',
      handler : delDataViewlog
    } ];
    $(function() {
      //初始化数据表格
      gridViewlog = $('#dataViewlogGrid').datagrid( {
        url : url_searchActionViewlog,
        fit : true,
        fitColumns : true,
        'toolbar' : toolBarViewlog,
        pagination : true,
        closable : true,
        checkOnSelect : true,
        border:false,
        striped : true,
        rownumbers : true,
        ctrlSelect : true
      });
      //初始化条件查询
      searchViewlog = $('#searchViewlogForm').searchForm( {
        gridObj : gridViewlog
      });
    });
    //查看
    function viewDataViewlog() {
      var selectedArray = $(gridViewlog).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionViewlog + '?pageset.pageType='+PAGETYPE.VIEW+'&ids='
            + selectedArray[0].ID;
        $.farm.openWindow( {
          id : 'winViewlog',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '浏览'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //新增
    function addDataViewlog() {
      var url = url_formActionViewlog + '?operateType='+PAGETYPE.ADD;
      $.farm.openWindow( {
        id : 'winViewlog',
        width : 600,
        height : 300,
        modal : true,
        url : url,
        title : '新增'
      });
    }
    //修改
    function editDataViewlog() {
      var selectedArray = $(gridViewlog).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionViewlog + '?operateType='+PAGETYPE.EDIT+ '&ids=' + selectedArray[0].ID;
        $.farm.openWindow( {
          id : 'winViewlog',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '修改'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //删除
    function delDataViewlog() {
      var selectedArray = $(gridViewlog).datagrid('getSelections');
      if (selectedArray.length > 0) {
        // 有数据执行操作
        var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
        $.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
          if (flag) {
            $(gridViewlog).datagrid('loading');
            $.post(url_delActionViewlog + '?ids=' + $.farm.getCheckedIds(gridViewlog,'ID'), {},
              function(flag) {
                var jsonObject = JSON.parse(flag, null);
                $(gridViewlog).datagrid('loaded');
                if (jsonObject.STATE == 0) {
                  $(gridViewlog).datagrid('reload');
              } else {
                var str = MESSAGE_PLAT.ERROR_SUBMIT
                    + jsonObject.MESSAGE;
                $.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
              }
            });
          }
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
            'info');
      }
    }
  </script>
</html>