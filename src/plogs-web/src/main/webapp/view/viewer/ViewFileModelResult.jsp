<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>预览文件模型数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchViewmodelForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataViewmodelGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="TITLE" data-options="sortable:true" width="40">模型名称</th>
					<th field="MODELKEY" data-options="sortable:true" width="40">模型KEY</th>
					<th field="SORTNUM" data-options="sortable:true" width="40">显示排序</th>
					<!-- <th field="PSTATE" data-options="sortable:true" width="60">状态</th> -->
					<th field="PCONTENT" data-options="sortable:true" width="140">备注</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionViewmodel = "viewmodel/del.do";//删除URL
	var url_formActionViewmodel = "viewmodel/form.do";//增加、修改、查看URL
	var url_searchActionViewmodel = "viewmodel/query.do";//查询URL
	var title_windowViewmodel = "预览文件模型管理";//功能名称
	var gridViewmodel;//数据表格对象
	var searchViewmodel;//条件查询组件对象
	var toolBarViewmodel = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataViewmodel
	}, /** {
			id : 'add',
			text : '新增',
			iconCls : 'icon-add',
			handler : addDataViewmodel
		},**/
	{
		id : 'del',
		text : '删除失效模型',
		iconCls : 'icon-remove',
		handler : delDataViewmodel
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataViewmodel
	}, {
		id : 'fresh',
		text : '重新加载模型',
		iconCls : 'icon-refresh',
		handler : refreshDataViewmodel
	} ];
	$(function() {
		//初始化数据表格
		gridViewmodel = $('#dataViewmodelGrid').datagrid({
			url : url_searchActionViewmodel,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarViewmodel,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchViewmodel = $('#searchViewmodelForm').searchForm({
			gridObj : gridViewmodel
		});
	});
	//查看
	function viewDataViewmodel() {
		var selectedArray = $(gridViewmodel).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewmodel + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewmodel',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataViewmodel() {
		var url = url_formActionViewmodel + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winViewmodel',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataViewmodel() {
		var selectedArray = $(gridViewmodel).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionViewmodel + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winViewmodel',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//刷新模型
	function refreshDataViewmodel() {
		// 有数据执行操作
		$.messager.confirm(MESSAGE_PLAT.PROMPT, "即将刷新预览模型，是否继续?",
				function(flag) {
					if (flag) {
						$(gridViewmodel).datagrid('loading');
						$.post("viewmodel/refresh.do", {}, function(flag) {
							var jsonObject = JSON.parse(flag, null);
							$(gridViewmodel).datagrid('loaded');
							if (jsonObject.STATE == 0) {
								$(gridViewmodel).datagrid('reload');
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ jsonObject.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						});
					}
				});
	}

	//删除
	function delDataViewmodel() {
		var selectedArray = $(gridViewmodel).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridViewmodel).datagrid('loading');
					$.post(url_delActionViewmodel + '?ids='
							+ $.farm.getCheckedIds(gridViewmodel, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridViewmodel).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridViewmodel).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>