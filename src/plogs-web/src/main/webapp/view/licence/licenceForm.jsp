<%@page import="com.farm.parameter.service.impl.ConstantVarService"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@page import="com.farm.core.Context"%>
<%@page import="com.farm.util.spring.BeanFactory"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<div class="panel panel-default">
	<div class="panel-body" style="background-color: #f5f5f5;">
		<div style="text-align: left;">
			<br />
			1.联系客服人员获取专业版licence文件
			<br />
			2.把得到的licence序列号填写到下面表单中，并点击"注册按钮"提交
			<br />
			<br />
		</div>
		<!-- 2.把得到的licence文件覆盖到下面目录中 ,并重新启动服务<br />
<PF:ParameterValue key="farm.constant.webroot.path" /> -->
		<div class="row">
			<div class="col-md-12">
				<form method="post" action="parameter/Publicenceup.do">
					<div class="form-group">
						<textarea rows="" cols="" class="form-control" id="licence" name="key" placeholder="此处录入licence">${key}</textarea>
					</div>
					<button type="submit" class="btn btn-default">注册</button>
				</form>
			</div>
		</div>
	</div>
</div>
