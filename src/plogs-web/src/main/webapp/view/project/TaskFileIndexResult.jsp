<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>任务附件索引数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false">
		<table id="dataTaskfileindexGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="FILENAME" data-options="sortable:true" width="60">文件名称</th>
					<th field="EXNAME" data-options="sortable:true" width="20">文件类型</th>
					<th field="SOURCETYPE" data-options="sortable:true" width="20">文件来源</th>
					<th field="TASKNAME" data-options="sortable:true" width="60">任务名称</th>
					<th field="PROJECTNAME" data-options="sortable:true" width="60">项目名称</th>
					<th field="FILETEXT" data-options="sortable:true" width="60">文本描述</th>
					<th field="TASKID" data-options="sortable:true" width="15">任务ID</th>
					<th field="FILEID" data-options="sortable:true" width="15">文件ID</th>
					<th field="CTIME" data-options="sortable:true" width="30">发布时间</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_searchActionTaskfileindex = "taskfindex/query.do";//查询URL
	var title_windowTaskfileindex = "任务附件索引管理";//功能名称
	var gridTaskfileindex;//数据表格对象
	var searchTaskfileindex;//条件查询组件对象
	var toolBarTaskfileindex = [ {
		id : 'reindex',
		text : '重建全部索引',
		iconCls : 'icon-application_osx_terminal',
		handler : reIndexDoc
	} ];

	$(function() {
		//初始化数据表格
		gridTaskfileindex = $('#dataTaskfileindexGrid').datagrid({
			url : url_searchActionTaskfileindex,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarTaskfileindex,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
	});

	//建立索引
	function reIndexDoc() {
		// 有数据执行操作
		$.messager.confirm(MESSAGE_PLAT.PROMPT, "将对所有文件进行索引,是否继续?", function(
				flag) {
			if (flag) {
				var pro = $.messager.progress({
					msg : '正在后台建立索引，请等待...',
					interval : 300,
					text : ''
				});
				isLoadSta = true;
				$.post("taskfindex/reIndex.do", {}, function(flag) {
					if (flag.STATE == 1) {
						var str = MESSAGE_PLAT.ERROR_SUBMIT + flag.MESSAGE;
						$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
					}
					loadproSta();
				}, 'json');
			}
		});
	}
	function loadproSta() {
		if (isLoadSta) {
			$.post("taskfindex/indexPersont.do", {}, function(flag) {
				//0初始化，1删除原索引，2添加新索引，3完成，-1无任务
				if (flag.task.state == 0) {
					var progress = flag.task.processNum + "/"
							+ flag.task.maxNum;
					$(".messager-p-msg").text(
							"初始化文档中(1/3):[" + progress + "]...:");
				}
				if (flag.task.state == 1) {
					var progress = flag.task.processNum + "/"
							+ flag.task.maxNum;
					$(".messager-p-msg").text(
							"删除历史索引中(2/3):[" + progress + "]...:");
				}
				if (flag.task.state == 2) {
					var progress = flag.task.processNum + "/"
							+ flag.task.maxNum;
					$(".messager-p-msg").text(
							"添加新索引中(3/3):[" + progress + "]...:");
				}
				if (flag.task.state == 3) {
					$(".messager-p-msg").text("重建索引完成，請關閉窗口！");
					$.messager.progress('close');
					isLoadSta = false;
					$.messager.alert(MESSAGE_PLAT.PROMPT, "操作成功!", 'info');
				}
				if (flag.task.state == -2) {
					$(".messager-p-msg").text("重建索引完成，請關閉窗口！");
					$.messager.progress('close');
					isLoadSta = false;
					$.messager.alert(MESSAGE_PLAT.ERROR, flag.task.message,
							'error');
				}
				setTimeout("loadproSta()", "500");
			}, 'json');
		}
	}
</script>
</html>