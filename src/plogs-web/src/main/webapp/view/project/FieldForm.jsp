<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--属性表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'" style="background-color: #fafafa;">
		<form id="dom_formField">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title"></td>
					<td></td>
					<td class="title"></td>
					<td></td>
				</tr>
				<tr>
					<td class="title">属性名称:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[128]']"
						id="entity_name" name="name" value="${entity.name}"></td>
					<td class="title">属性分类:</td>
					<td>${type.name}<input type="hidden" id="entity_typeid"
						name="typeid" value="${type.id}">
					</td>
				</tr>
				<tr>
					<td class="title">排序:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
					<td class="title">状态:</td>
					<td><select name="pstate" style="width: 120px;"
						id="entity_pstate" val="${entity.pstate}"><option
								value="1">可用</option>
							<option value="0">禁用</option></select></td>
				</tr>
				<tr>
					<td class="title">属性值类型:</td>
					<td><select name="valtype" id="entity_valtype"
						class="easyui-validatebox" data-options="required:true"
						style="width: 120px;" val="${entity.valtype}">
							<option value="">~请选择~</option>
							<!--  
							<option value="1">富文本</option> -->
							<option value="2">附件</option>
							<option value="3">多行文本</option>
							<option value="4">单行文本</option>
							<option value="5">枚举单选</option>
							<!-- 
							<option value="6">枚举多选</option>
							<option value="7">公司</option>
							<option value="8">人员</option>
							<option value="9">时间</option> -->
							<option value="10">日期</option>
					</select></td>
					<td class="title">唯一性:</td>
					<td><select name="singletype" id="entity_singletype"
						class="easyui-validatebox" data-options="required:true"
						style="width: 120px;" val="${entity.singletype}">
							<option value="">~请选择~</option>
							<option value="1">单条</option>
							<option value="2">多条</option>
					</select></td>
				</tr>
				<tr id="entity_enums_form">
					<td class="title">枚举类型:</td>
					<td colspan="3"><textarea rows="2" style="width: 410px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_enums"
							name="enums">${entity.enums}</textarea></td>
				</tr>
				<tr>
					<td class="title">属性说明:</td>
					<td colspan="3"><textarea rows="2" style="width: 410px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityField" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityField" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formField" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionField = 'field/add.do';
	var submitEditActionField = 'field/edit.do';
	var currentPageTypeField = '${pageset.operateType}';
	var submitFormField;
	$(function() {
		//表单组件对象
		submitFormField = $('#dom_formField').SubmitForm({
			pageType : currentPageTypeField,
			grid : gridField,
			currentWindowId : 'winField'
		});
		initFormShow($('#entity_valtype').val());
		$('#entity_valtype').change(function() {
			initFormShow($('#entity_valtype').val());
		});
		//关闭窗口
		$('#dom_cancle_formField').bind('click', function() {
			$('#winField').window('close');
		});
		//提交新增数据
		$('#dom_add_entityField').bind('click', function() {
			submitFormField.postSubmit(submitAddActionField);
		});
		//提交修改数据
		$('#dom_edit_entityField').bind('click', function() {
			submitFormField.postSubmit(submitEditActionField);
		});
	});

	function initFormShow(formType) {
		if (formType == '5' || formType == '6') {
			$('#entity_enums_form').show();
		} else {
			$('#entity_enums_form').hide();
		}

	}
//-->
</script>