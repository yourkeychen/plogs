<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>项目数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="projectTypeTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="projectTypeTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="projectTypeTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchProjectForm">
				<table class="editTable">
					<tr>
						<td class="title">项目分类类型:</td>
						<td><input id="TYPETITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="TYPEID_RULE" name="TYPEID:=" type="hidden"></td>
						<td class="title">项目名称:</td>
						<td><input name="a.NAME:like" type="text"></td>
						<td class="title">业务机构名称:</td>
						<td><input name="e.NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="6"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataProjectGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">项目名称</th>
						<th field="COMPANY" data-options="sortable:true" width="40">业务机构</th>
						<th field="GRADATIONNAME" data-options="sortable:true" width="20">当前阶段</th>
						<th field="TYPENAME" data-options="sortable:true" width="20">项目分类</th>
						<th field="STATE" data-options="sortable:true" width="15">项目状态</th>
						<th field="LIVETIME" data-options="sortable:true" width="20">活跃时间</th>
						<th field="CUSERNAME" data-options="sortable:true" width="15">创建人</th>
						<th field="CTIME" data-options="sortable:true" width="20">创建时间</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	var url_delActionProject = "project/del.do";//删除URL
	var url_formActionProject = "project/form.do";//增加、修改、查看URL
	var url_searchActionProject = "project/query.do";//查询URL
	var title_windowProject = "项目管理";//功能名称
	var gridProject;//数据表格对象
	var searchProject;//条件查询组件对象
	var toolBarProject = [ {
		id : 'view',
		text : '前台查看项目',
		iconCls : 'icon-tip',
		handler : viewDataProject
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataProject
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataProject
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataProject
	}, {
		id : 'chooseTYpe',
		text : '移动分类',
		iconCls : 'icon-communication',
		handler : setingTypeProject
	}, {
		id : 'fields',
		text : '项目属性',
		iconCls : 'icon-library',
		handler : editDataProjectFields
	}, {
		id : 'setcompany',
		text : '設置业务机构',
		iconCls : 'icon-featured',
		handler : setProjectCompany
	}, {
		id : 'setUser',
		text : '关系人管理',
		iconCls : 'icon-user_green',
		handler : setProjectUser
	}, {
		id : 'resetUser',
		text : '重置关系人',
		iconCls : '	icon-special-offer',
		handler : resetProjectUser
	} ];
	$(function() {
		//初始化数据表格
		gridProject = $('#dataProjectGrid').datagrid({
			url : url_searchActionProject,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarProject,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProject = $('#searchProjectForm').searchForm({
			gridObj : gridProject
		});
		$('#projectTypeTree').tree({
			url : 'projecttype/projectTypeTree.do',
			onSelect : function(node) {
				$('#TYPEID_RULE').val(node.id);
				$('#TYPETITLE_RULE').val(node.text);
				if (node.id == "NONE") {
					$('#TYPEID_RULE').val('');
					$('#TYPETITLE_RULE').val(node.text);
				}
				searchProject.dosearch({
					'ruleText' : searchProject.arrayStr()
				});
			}
		});
		$('#projectTypeTreeReload').bind('click', function() {
			$('#projectTypeTree').tree('reload');
		});
		$('#projectTypeTreeOpenAll').bind('click', function() {
			$('#projectTypeTree').tree('expandAll');
		});
	});
	//新增
	function addDataProject() {
		var typeid = $('#TYPEID_RULE').val();
		if (!typeid) {
			$.messager.alert(MESSAGE_PLAT.PROMPT, "请选择一个项目类型", 'info');
			return;
		}
		var url = url_formActionProject + '?operateType=' + PAGETYPE.ADD
				+ "&typeid=" + typeid;

		$.farm.openWindow({
			id : 'winProject',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}

	//修改
	function editDataProject() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProject + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProject',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//关系人管理
	function setProjectUser() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = 'projectuser/list.do?operateType=' + PAGETYPE.EDIT
					+ '&projectid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProject',
				width : 600,
				height : 400,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//前台查看项目
	function viewDataProject() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = '<PF:basePath/>projectView/projectInfo.do?projectId='
					+ selectedArray[0].ID;
			window.open(url);
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//项目属性管理
	function editDataProjectFields() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length == 1) {
			$.farm.openWindow({
				id : 'winProjectFields',
				width : 800,
				height : 400,
				modal : true,
				url : "projectfieldins/list.do?projectid="
						+ selectedArray[0].ID,
				title : '项目属性'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//设置分类
	function setingTypeProject() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length > 0) {
			$.farm.openWindow({
				id : 'projectTypeTreeNodeWin',
				width : 250,
				height : 300,
				modal : true,
				url : "projecttype/treeNodeTreeView.do",
				title : '移动分类'
			});
			chooseWindowCallBackHandle = function(node) {
				$.messager.confirm('确认对话框', '确定设置分类么？', function(falg1) {
					if (falg1) {
						$.post('project/setingType.do', {
							projectIds : $.farm
									.getCheckedIds(gridProject, 'ID'),
							typeId : node.id
						}, function(flag) {
							if (flag.STATE == 0) {
								$(gridProject).datagrid('reload');
								$('#projectTypeTreeNodeWin').window('close');
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#projectTypeTree').tree(
														'reload');
											}
										});
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ flag.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						}, 'json');
					}
				});
			};
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//重置关系人
	function resetProjectUser() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + "个项目将被重置关系人，该操作无法恢复，是否继续？";
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProject).datagrid('loading');
					$.post('projectuser/resetUser.do?ids='
							+ $.farm.getCheckedIds(gridProject, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProject).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridProject).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	//删除
	function delDataProject() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProject).datagrid('loading');
					$.post(url_delActionProject + '?ids='
							+ $.farm.getCheckedIds(gridProject, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProject).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridProject).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//设置项目公司
	function setProjectCompany() {
		var selectedArray = $(gridProject).datagrid('getSelections');
		if (selectedArray.length > 0) {
			var url = 'company/chooseQuery.do?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + $.farm.getCheckedIds(gridProject, 'ID');
			chooseWindowCallBackHandle = function(row) {
				$.post('project/bindCompany.do', {
					'companyid' : row[0].ID,
					'projectids' : $.farm.getCheckedIds(gridProject, 'ID')
				}, function(jsonObject) {
					$(gridProject).datagrid('loaded');
					if (jsonObject.STATE == 0) {
						$("#winCompanyChoose").window('close');
						$(gridProject).datagrid('reload');
					} else {
						var str = MESSAGE_PLAT.ERROR_SUBMIT
								+ jsonObject.MESSAGE;
						$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
					}
				}, 'json');
			};
			$.farm.openWindow({
				id : 'winCompanyChoose',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '设置项目业务机构'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>