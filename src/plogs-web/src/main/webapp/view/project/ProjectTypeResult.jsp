<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>项目分类数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="projectTypeTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="projectTypeTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="projectTypeTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchProjecttypeForm">
				<table class="editTable">
					<tr>
						<td class="title">上级节点:</td>
						<td><input id="PARENTTITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_RULE" name="PARENTID:=" type="hidden"></td>
						<td class="title">分类名称:</td>
						<td><input name="NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataProjecttypeGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">名称</th>
						<th field="SORT" data-options="sortable:true" width="40">排序</th>
						<th field="MODEL" data-options="sortable:true" width="50">模型</th>
						<th field="GRADATIONNUM" data-options="sortable:true" width="40">阶段数量</th>
						<th field="PSTATE" data-options="sortable:true" width="60">状态</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	var url_delActionProjecttype = "projecttype/del.do";//删除URL
	var url_formActionProjecttype = "projecttype/form.do";//增加、修改、查看URL
	var url_searchActionProjecttype = "projecttype/query.do";//查询URL
	var title_windowProjecttype = "项目分类管理";//功能名称
	var gridProjecttype;//数据表格对象
	var searchProjecttype;//条件查询组件对象
	var toolBarProjecttype = [ {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataProjecttype
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataProjecttype
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataProjecttype
	}, {
		id : 'move',
		text : '移动',
		iconCls : 'icon-communication',
		handler : moveTree
	} , {
		id : 'bindTasyTYpe',
		text : '绑定项目阶段',
		iconCls : 'icon-issue',
		handler : bindGradation
	} , {
		id : 'bindFields',
		text : '绑定项目属性',
		iconCls : 'icon-invoice',
		handler : bindFields
	}];
	$(function() {
		//初始化数据表格
		gridProjecttype = $('#dataProjecttypeGrid').datagrid({
			url : url_searchActionProjecttype,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarProjecttype,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProjecttype = $('#searchProjecttypeForm').searchForm({
			gridObj : gridProjecttype
		});
		$('#projectTypeTree').tree({
			url : 'projecttype/projectTypeTree.do',
			onSelect : function(node) {
				$('#PARENTID_RULE').val(node.id);
				$('#PARENTTITLE_RULE').val(node.text);
				searchProjecttype.dosearch({
					'ruleText' : searchProjecttype.arrayStr()
				});
			}
		});
		$('#projectTypeTreeReload').bind('click', function() {
			$('#projectTypeTree').tree('reload');
		});
		$('#projectTypeTreeOpenAll').bind('click', function() {
			$('#projectTypeTree').tree('expandAll');
		});
	});
	//查看
	function viewDataProjecttype() {
		var selectedArray = $(gridProjecttype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProjecttype + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjecttype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataProjecttype() {
		var parentID = $("#PARENTID_RULE").val();
		var url = url_formActionProjecttype + '?operateType=' + PAGETYPE.ADD
				+ '&parentId=' + parentID;
		$.farm.openWindow({
			id : 'winProjecttype',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataProjecttype() {
		var selectedArray = $(gridProjecttype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProjecttype + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjecttype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataProjecttype() {
		var selectedArray = $(gridProjecttype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProjecttype).datagrid('loading');
					$.post(url_delActionProjecttype + '?ids='
							+ $.farm.getCheckedIds(gridProjecttype, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProjecttype).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridProjecttype).datagrid('reload');
									$.messager.confirm('确认对话框',
											'数据更新,是否重新加载左侧树？', function(r) {
												if (r) {
													$('#projectTypeTree').tree(
															'reload');
												}
											});
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	//移动节点
	function moveTree() {
		var selectedArray = $(gridProjecttype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			$.farm.openWindow({
				id : 'projectTypeTreeNodeWin',
				width : 250,
				height : 300,
				modal : true,
				url : "projecttype/treeNodeTreeView.do",
				title : '移动分类'
			});
			chooseWindowCallBackHandle = function(node) {
				$.messager.confirm('确认对话框', '确定移动该节点么？', function(falg1) {
					if (falg1) {
						$.post('projecttype/moveTreeNodeSubmit.do', {
							ids : $.farm.getCheckedIds(gridProjecttype, 'ID'),
							id : node.id
						}, function(flag) {
							if (flag.STATE == 0) {
								$(gridProjecttype).datagrid('reload');
								$('#projectTypeTreeNodeWin').window('close');
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#projectTypeTree').tree(
														'reload');
											}
										});
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ flag.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						}, 'json');
					}
				});
			};
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	//绑定任务类型
	function bindGradation() {
		var selectedArray = $(gridProjecttype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			if (selectedArray[0].MODELVAL != '1') {
				$.messager
						.alert(MESSAGE_PLAT.PROMPT, "只有项目类型可以绑定阶段!", 'info');
				return;
			}
			$.farm.openWindow({
				id : 'winProjectgradation',
				width : 800,
				height : 400,
				modal : true,
				url : "projecttype/gradations.do",
				title : selectedArray[0].NAME + "-绑定项目阶段"
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	
	//绑定属性
	function bindFields() {
		var selectedArray = $(gridProjecttype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			if (selectedArray[0].MODELVAL != '1') {
				$.messager
						.alert(MESSAGE_PLAT.PROMPT, "只有项目类型可以绑定属性!", 'info');
				return;
			}
			$.farm.openWindow({
				id : 'winFieldRePtype',
				width : 800,
				height : 400,
				modal : true,
				url : "fieldreptype/list.do",
				title : selectedArray[0].NAME + "-绑定项目属性"
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
</script>
</html>