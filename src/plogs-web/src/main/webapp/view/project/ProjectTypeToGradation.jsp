<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'center',border:false">
		<table id="dataProjecttypegradationGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">阶段名称</th>
					<th field="SORT" data-options="sortable:true" width="20">排序</th>
					<th field="PCONTENT" data-options="sortable:true" width="110">阶段备注</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var gridProjecttypegradation;//数据表格对象
	var searchProjecttypegradation;//条件查询组件对象
	var toolBarProjecttypegradation = [ {
		id : 'add',
		text : '添加',
		iconCls : 'icon-add',
		handler : addDataProjecttypegradation
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataProjecttypegradation
	}, {
		id : 'del',
		text : '上移',
		iconCls : 'icon-upcoming-work',
		handler : sortToUp
	} ];
	$(function() {
		var reQueryUrl = "projecttype/queryReGradations.do?typeid="
				+ $.farm.getCheckedIds(gridProjecttype, 'ID');
		//初始化数据表格
		gridProjecttypegradation = $('#dataProjecttypegradationGrid').datagrid(
				{
					url : reQueryUrl,
					fit : true,
					fitColumns : true,
					'toolbar' : toolBarProjecttypegradation,
					pagination : true,
					closable : true,
					checkOnSelect : true,
					border : false,
					striped : true,
					rownumbers : true,
					ctrlSelect : true
				});
		//初始化条件查询
		searchProjecttypegradation = $('#searchProjecttypegradationForm')
				.searchForm({
					gridObj : gridProjecttypegradation
				});
	});
	//添加
	function addDataProjecttypegradation() {
		$.farm.openWindow({
			id : 'winProjectTypeGradation',
			width : 800,
			height : 500,
			modal : true,
			url : "projectgradation/chooseWin.do",
			title : '添加任务类型'
		});
		chooseWindowCallBackHandle = function(ids) {
			$.post('projecttype/bindGradation.do?gradationids=' + ids
					+ "&typeid=" + $.farm.getCheckedIds(gridProjecttype, 'ID'),
					{}, function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridProjecttypegradation).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$('#winProjectTypeGradation').window('close');
							$(gridProjecttypegradation).datagrid('reload');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
		};
	}
	//删除
	function delDataProjecttypegradation() {
		var selectedArray = $(gridProjecttypegradation).datagrid(
				'getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProjecttypegradation).datagrid('loading');
					$.post("projecttype/rebindGradation.do?ids="
							+ $.farm.getCheckedIds(gridProjecttypegradation,
									'ID'), {}, function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridProjecttypegradation).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$(gridProjecttypegradation).datagrid('reload');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//排序上移
	function sortToUp() {
		var selectedArray = $(gridProjecttypegradation).datagrid(
				'getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			$.messager.confirm(MESSAGE_PLAT.PROMPT, "更新排序是否继续", function(flag) {
				if (flag) {
					$(gridProjecttypegradation).datagrid('loading');
					$.post("projecttype/sortToUp.do?ids="
							+ $.farm.getCheckedIds(gridProjecttypegradation,
									'ID'), {}, function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridProjecttypegradation).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$(gridProjecttypegradation).datagrid('reload');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>