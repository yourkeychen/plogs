<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>

<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchProjectfieldinsForm">
			<table class="editTable">
				<tr>
					<td class="title">属性名称:</td>
					<td><input name="NAME:like" type="text"></td>
					<td class="title">属性值:</td>
					<td><input name="VAL:like" type="text"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataProjectfieldinsGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">属性名称</th>
					<th field="VALTYPE" data-options="sortable:true" width="20">属性类型</th>
					<th field="VALTITLE" data-options="sortable:true" width="40">属性值</th>
					<th field="PUBREAD" data-options="sortable:true" width="20">公开范围</th>
					<th field="SORT" data-options="sortable:true" width="13">排序</th>
					<th field="PSTATE" data-options="sortable:true" width="13">状态</th>
					<th field="ETIME" data-options="sortable:true" width="40">更新时间</th>
					<th field="EUSER" data-options="sortable:true" width="40">更新用户</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionProjectfieldins = "projectfieldins/del.do";//删除URL
	var url_formActionProjectfieldins = "projectfieldins/form.do";//增加、修改、查看URL
	var url_searchActionProjectfieldins = "projectfieldins/query.do?projectid=${projectid}";//查询URL
	var title_windowProjectfieldins = "项目属性管理";//功能名称
	var gridProjectfieldins;//数据表格对象
	var searchProjectfieldins;//条件查询组件对象
	var toolBarProjectfieldins = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataProjectfieldins
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataProjectfieldins
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataProjectfieldins
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataProjectfieldins
	}, {
		id : 'loadField',
		text : '重载项目属性',
		iconCls : 'icon-finished-work',
		handler : loadProjectField
	} ];
	$(function() {
		//初始化数据表格
		gridProjectfieldins = $('#dataProjectfieldinsGrid').datagrid({
			url : url_searchActionProjectfieldins,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarProjectfieldins,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProjectfieldins = $('#searchProjectfieldinsForm').searchForm({
			gridObj : gridProjectfieldins
		});
	});
	//查看
	function viewDataProjectfieldins() {
		var selectedArray = $(gridProjectfieldins).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProjectfieldins + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjectfieldins',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataProjectfieldins() {
		var url = url_formActionProjectfieldins + '?operateType='
				+ PAGETYPE.ADD + '&projectid=${projectid}';
		$.farm.openWindow({
			id : 'winProjectfieldins',
			width : 600,
			height : 350,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataProjectfieldins() {
		var selectedArray = $(gridProjectfieldins).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProjectfieldins + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjectfieldins',
				width : 600,
				height : 350,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//加载系统属性
	function loadProjectField() {
		// 有数据执行操作
		$.messager.confirm(MESSAGE_PLAT.PROMPT, "确定加载?", function(flag) {
			if (flag) {
				$(gridProjectfieldins).datagrid('loading');
				$.post("projectfieldins/loadSysFields.do", {
					'projectid' : '${projectid}'
				}, function(flag) {
					var jsonObject = JSON.parse(flag, null);
					$(gridProjectfieldins).datagrid('loaded');
					if (jsonObject.STATE == 0) {
						$(gridProjectfieldins).datagrid('reload');
					} else {
						var str = MESSAGE_PLAT.ERROR_SUBMIT
								+ jsonObject.MESSAGE;
						$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
					}
				});
			}
		});
	}

	//删除
	function delDataProjectfieldins() {
		var selectedArray = $(gridProjectfieldins).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProjectfieldins).datagrid('loading');
					$.post(url_delActionProjectfieldins + '?ids='
							+ $.farm.getCheckedIds(gridProjectfieldins, 'ID'),
							{}, function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProjectfieldins).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridProjectfieldins).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>