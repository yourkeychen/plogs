<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--任务类型属性表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formFieldrettype">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">编辑权限:</td>
					<td colspan="3"><select style="width: 360px;"
						class="easyui-validatebox" data-options="required:true"
						id="entity_writeis" name="writeis" val="${entity.writeis}">
							<option value="1">默认公开</option>
							<option value="0">默认禁止</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">阅读权限:</td>
					<td colspan="3"><select style="width: 360px;"
						class="easyui-validatebox" data-options="required:true"
						id="entity_readis" name="readis" val="${entity.readis}">
							<option value="1">默认公开</option>
							<!-- <option value="0">默认隐藏</option> -->
					</select></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityFieldrettype" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityFieldrettype" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formFieldrettype" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionFieldrettype = 'fieldrettype/add.do';
	var submitEditActionFieldrettype = 'fieldrettype/edit.do';
	var currentPageTypeFieldrettype = '${pageset.operateType}';
	var submitFormFieldrettype;
	$(function() {
		//表单组件对象
		submitFormFieldrettype = $('#dom_formFieldrettype').SubmitForm({
			pageType : currentPageTypeFieldrettype,
			grid : gridFieldrettype,
			currentWindowId : 'winFieldrettype'
		});
		//关闭窗口
		$('#dom_cancle_formFieldrettype').bind('click', function() {
			$('#winFieldrettype').window('close');
		});
		//提交新增数据
		$('#dom_add_entityFieldrettype').bind('click', function() {
			submitFormFieldrettype.postSubmit(submitAddActionFieldrettype);
		});
		//提交修改数据
		$('#dom_edit_entityFieldrettype').bind('click', function() {
			submitFormFieldrettype.postSubmit(submitEditActionFieldrettype);
		});
	});
//-->
</script>