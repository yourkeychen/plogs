<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>业务机构数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false">
		<form id="searchCompanyForm">
			<table class="editTable">
				<tr>
					<td class="title">机构名称:</td>
					<td><input name="NAME:like" type="text"></td>
					<td class="title"></td>
					<td></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="4"><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataCompanyGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="50">名称</th>
					<th field="TYPE" data-options="sortable:true" width="30">机构类型</th>
					<th field="LIVETIME" data-options="sortable:true" width="30">活跃时间</th>
					<th field="EUSER" data-options="sortable:true" width="30">更新用户</th>
					<th field="ETIME" data-options="sortable:true" width="30">更新时间</th>
					<th field="CTIME" data-options="sortable:true" width="30">创建时间</th>
					<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
				</tr>
			</thead>
		</table>
	</div>
</body>
<script type="text/javascript">
	var url_delActionCompany = "company/del.do";//删除URL
	var url_formActionCompany = "company/form.do";//增加、修改、查看URL
	var url_searchActionCompany = "company/query.do";//查询URL
	var title_windowCompany = "业务机构管理";//功能名称
	var gridCompany;//数据表格对象
	var searchCompany;//条件查询组件对象
	var toolBarCompany = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataCompany
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataCompany
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataCompany
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataCompany
	} ];
	$(function() {
		//初始化数据表格
		gridCompany = $('#dataCompanyGrid').datagrid({
			url : url_searchActionCompany,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarCompany,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchCompany = $('#searchCompanyForm').searchForm({
			gridObj : gridCompany
		});
	});
	//查看
	function viewDataCompany() {
		var selectedArray = $(gridCompany).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionCompany + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winCompany',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataCompany() {
		var url = url_formActionCompany + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winCompany',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataCompany() {
		var selectedArray = $(gridCompany).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionCompany + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winCompany',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataCompany() {
		var selectedArray = $(gridCompany).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridCompany).datagrid('loading');
					$.post(url_delActionCompany + '?ids='
							+ $.farm.getCheckedIds(gridCompany, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridCompany).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridCompany).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>