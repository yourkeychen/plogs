<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--业务机构表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formCompany">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">机构名称:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[256]']"
						id="entity_name" name="name" value="${entity.name}"></td>
				</tr>
				<tr>
					<td class="title">机构类型:</td>
					<td colspan="3"><select name="type" id="entity_type"
						class="easyui-validatebox" data-options="required:true"
						style="width: 360px;" val="${entity.type}">
							<option value="0">未知</option>
							<PF:OptionDictionary index="COMPANY_TYPE_ENUM"
								isTextValue="false" />
					</select></td>
				</tr>
				
				<tr>
					<td class="title"></td>
					<td colspan="3">机构类型-数据字典:COMPANY_TYPE_ENUM</td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea type="text" style="width: 360px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityCompany" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityCompany" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formCompany" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionCompany = 'company/add.do';
	var submitEditActionCompany = 'company/edit.do';
	var currentPageTypeCompany = '${pageset.operateType}';
	var submitFormCompany;
	$(function() {
		//表单组件对象
		submitFormCompany = $('#dom_formCompany').SubmitForm({
			pageType : currentPageTypeCompany,
			grid : gridCompany,
			currentWindowId : 'winCompany'
		});
		//关闭窗口
		$('#dom_cancle_formCompany').bind('click', function() {
			$('#winCompany').window('close');
		});
		//提交新增数据
		$('#dom_add_entityCompany').bind('click', function() {
			submitFormCompany.postSubmit(submitAddActionCompany);
		});
		//提交修改数据
		$('#dom_edit_entityCompany').bind('click', function() {
			submitFormCompany.postSubmit(submitEditActionCompany);
		});
	});
//-->
</script>