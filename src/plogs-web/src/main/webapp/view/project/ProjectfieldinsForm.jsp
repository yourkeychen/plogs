<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--项目属性表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formProjectfieldins">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<input type="hidden" id="entity_projectid" name="projectid"
				value="${projectid}">
			<table class="editTable">
				<tr>
					<td class="title">公开范围:</td>
					<td><select style="width: 120px;" class="easyui-validatebox"
						data-options="validType:[,'maxLength[1]']" id="entity_pubread"
						name="pubread" val="${entity.pubread}">
							<option value="1">公开信息</option>
							<option value="0">默认隐藏</option>
					</select></td>
					<td class="title">属性类型:</td>
					<!-- 1.富文本，2附件，3多行文本，4单行文本，5枚举单选，6枚举多选，7公司，8人员，9时间，10日期 -->
					<td><select style="width: 120px;" class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[1]']"
						id="entity_valtype" name="valtype" val="${entity.valtype}">
							<option value="4">单行文本</option>
							<option value="5">枚举单选</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">属性名称:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[128]']"
						id="entity_name" name="name" value="${entity.name}"></td>
					<td class="title">排序号:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
				</tr>
				<tr>
					<td class="title">值:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[32]']" id="entity_val"
						name="val" value="${entity.val}"></td>
					<td class="title">值描述:</td>
					<td><input type="text" style="width: 120px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[256]']" id="entity_valtitle"
						name="valtitle" value="${entity.valtitle}"></td>
				</tr>
				<tr>
					<td class="title">枚举项:</td>
					<td colspan="3"><input type="text" style="width: 410px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[256]']" id="entity_enums"
						name="enums" value="${entity.enums}"></td>
				</tr>
				<tr>
					<td class="title">归档UUID:</td>
					<td colspan="3"><input type="text" style="width: 410px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[256]']" id="entity_uuid"
						name="uuid" value="${entity.uuid}"></td>
				</tr>
				<tr>
					<td class="title">状态:</td>
					<td><select style="width: 120px;" class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[1]']"
						id="entity_pstate" name="pstate" val="${entity.pstate}">
							<option value="1">可用</option>
							<option value="0">禁用</option>
					</select></td>
					<td class="title">唯一性:</td>
					<td><select style="width: 120px;" class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[1]']"
						id="entity_singletype" name="singletype"
						val="${entity.singletype}">
							<option value="1">唯一</option>
							<option value="2">多条</option>
					</select></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityProjectfieldins" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityProjectfieldins" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formProjectfieldins" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionProjectfieldins = 'projectfieldins/add.do';
	var submitEditActionProjectfieldins = 'projectfieldins/edit.do';
	var currentPageTypeProjectfieldins = '${pageset.operateType}';
	var submitFormProjectfieldins;
	$(function() {
		//表单组件对象
		submitFormProjectfieldins = $('#dom_formProjectfieldins').SubmitForm({
			pageType : currentPageTypeProjectfieldins,
			grid : gridProjectfieldins,
			currentWindowId : 'winProjectfieldins'
		});
		//关闭窗口
		$('#dom_cancle_formProjectfieldins').bind('click', function() {
			$('#winProjectfieldins').window('close');
		});
		//提交新增数据
		$('#dom_add_entityProjectfieldins').bind(
				'click',
				function() {
					submitFormProjectfieldins
							.postSubmit(submitAddActionProjectfieldins);
				});
		//提交修改数据
		$('#dom_edit_entityProjectfieldins').bind(
				'click',
				function() {
					submitFormProjectfieldins
							.postSubmit(submitEditActionProjectfieldins);
				});
	});
//-->
</script>