<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--项目类型属性表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formFieldreptype">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">编辑权限:</td>
					<td colspan="3"><select style="width: 360px;"
						class="easyui-validatebox" data-options="required:true"
						id="entity_writeis" name="writeis" val="${entity.writeis}">
							<option value="1">默认公开</option>
							<option value="0">默认禁止</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">阅读权限:</td>
					<td colspan="3"><select style="width: 360px;"
						class="easyui-validatebox" data-options="required:true"
						id="entity_readis" name="readis" val="${entity.readis}">
							<option value="1">默认公开</option>
							<option value="0">默认隐藏</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">排序号:</td>
					<td colspan="3"><input type="text" style="width: 160px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityFieldreptype" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityFieldreptype" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formFieldreptype" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionFieldreptype = 'fieldreptype/add.do';
	var submitEditActionFieldreptype = 'fieldreptype/edit.do';
	var currentPageTypeFieldreptype = '${pageset.operateType}';
	var submitFormFieldreptype;
	$(function() {
		//表单组件对象
		submitFormFieldreptype = $('#dom_formFieldreptype').SubmitForm({
			pageType : currentPageTypeFieldreptype,
			grid : gridFieldreptype,
			currentWindowId : 'winFieldreptype'
		});
		//关闭窗口
		$('#dom_cancle_formFieldreptype').bind('click', function() {
			$('#winFieldreptype').window('close');
		});
		//提交新增数据
		$('#dom_add_entityFieldreptype').bind('click', function() {
			submitFormFieldreptype.postSubmit(submitAddActionFieldreptype);
		});
		//提交修改数据
		$('#dom_edit_entityFieldreptype').bind('click', function() {
			submitFormFieldreptype.postSubmit(submitEditActionFieldreptype);
		});
	});
//-->
</script>