<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchTaskuserForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td class="title">用户登陆名称:</td>
					<td><input name="b.LOGINNAME:like" type="text"></td>
					<td><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataTaskuserGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="USERNAME" data-options="sortable:true" width="70">姓名</th>
					<th field="LOGINNAME" data-options="sortable:true" width="70">登陆名</th>
					<th field="LIVETIME" data-options="sortable:true" width="80">活跃时间</th>
					<th field="POPTYPE" data-options="sortable:true" width="70">类型</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionTaskuser = "taskuser/del.do";//删除URL
	var url_formActionTaskuser = "taskuser/form.do";//增加、修改、查看URL
	var url_searchActionTaskuser = "taskuser/query.do?taskid=${taskid}";//查询URL
	var title_windowTaskuser = "任务干系人管理";//功能名称
	var gridTaskuser;//数据表格对象
	var searchTaskuser;//条件查询组件对象
	var toolBarTaskuser = [// {
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//	handler : viewDataTaskuser
	//}, 
	{
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataTaskuser
	}
	//, {
	//	id : 'edit',
	//	text : '修改',
	//	iconCls : 'icon-edit',
	//	handler : editDataTaskuser
	//}
	, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataTaskuser
	} ];
	$(function() {
		//初始化数据表格
		gridTaskuser = $('#dataTaskuserGrid').datagrid({
			url : url_searchActionTaskuser,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarTaskuser,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchTaskuser = $('#searchTaskuserForm').searchForm({
			gridObj : gridTaskuser
		});
	});
	//查看
	function viewDataTaskuser() {
		var selectedArray = $(gridTaskuser).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionTaskuser + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTaskuser',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataTaskuser() {
		var url = url_formActionTaskuser + '?taskid=${taskid}&operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winTaskuser',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataTaskuser() {
		var selectedArray = $(gridTaskuser).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionTaskuser + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTaskuser',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataTaskuser() {
		var selectedArray = $(gridTaskuser).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridTaskuser).datagrid('loading');
					$.post(url_delActionTaskuser + '?ids='
							+ $.farm.getCheckedIds(gridTaskuser, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridTaskuser).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridTaskuser).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>