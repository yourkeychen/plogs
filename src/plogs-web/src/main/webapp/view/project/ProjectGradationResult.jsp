<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>项目阶段数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="gradationTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="gradationTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="gradationTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchProjectgradationForm">
				<table class="editTable">
					<tr>
						<td class="title">上级节点:</td>
						<td><input id="PARENTTITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_RULE" name="PARENTID:=" type="hidden"></td>
						<td class="title">阶段名称:</td>
						<td><input name="NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataProjectgradationGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">阶段名称</th>
						<th field="SORT" data-options="sortable:true" width="20">排序</th>
						<th field="MODEL" data-options="sortable:true" width="20">类型</th>
						<th field="TASKTYPENUM" data-options="sortable:true" width="30">任务类型数</th>
						<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
						<th field="PSTATE" data-options="sortable:true" width="20">状态</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	var url_delActionProjectgradation = "projectgradation/del.do";//删除URL
	var url_formActionProjectgradation = "projectgradation/form.do";//增加、修改、查看URL
	var url_searchActionProjectgradation = "projectgradation/query.do";//查询URL
	var title_windowProjectgradation = "项目阶段管理";//功能名称
	var gridProjectgradation;//数据表格对象
	var searchProjectgradation;//条件查询组件对象
	var toolBarProjectgradation = [ {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataProjectgradation
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataProjectgradation
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataProjectgradation
	}, {
		id : 'formatSort',
		text : '格式化排序',
		iconCls : 'icon-arrow_refresh',
		handler : formatSort
	}, {
		id : 'move',
		text : '移动',
		iconCls : 'icon-communication',
		handler : moveTree
	}, {
		id : 'bindTasyTYpe',
		text : '绑定任务类型',
		iconCls : 'icon-issue',
		handler : bindTasyTYpe
	} ];
	$(function() {
		//初始化数据表格
		gridProjectgradation = $('#dataProjectgradationGrid').datagrid({
			url : url_searchActionProjectgradation,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarProjectgradation,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProjectgradation = $('#searchProjectgradationForm').searchForm({
			gridObj : gridProjectgradation
		});
		$('#gradationTree').tree({
			url : 'projectgradation/gradationTree.do',
			onSelect : function(node) {
				$('#PARENTID_RULE').val(node.id);
				$('#PARENTTITLE_RULE').val(node.text);
				searchProjectgradation.dosearch({
					'ruleText' : searchProjectgradation.arrayStr()
				});
			}
		});
		$('#gradationTreeReload').bind('click', function() {
			$('#gradationTree').tree('reload');
		});
		$('#gradationTreeOpenAll').bind('click', function() {
			$('#gradationTree').tree('expandAll');
		});
	});
	//查看
	function viewDataProjectgradation() {
		var selectedArray = $(gridProjectgradation).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProjectgradation + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjectgradation',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataProjectgradation() {
		var parentID = $("#PARENTID_RULE").val();
		var url = url_formActionProjectgradation + '?operateType='
				+ PAGETYPE.ADD + '&parentId=' + parentID;
		$.farm.openWindow({
			id : 'winProjectgradation',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataProjectgradation() {
		var selectedArray = $(gridProjectgradation).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProjectgradation + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjectgradation',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//绑定任务类型
	function bindTasyTYpe() {
		var selectedArray = $(gridProjectgradation).datagrid('getSelections');
		if (selectedArray.length == 1) {
			if (selectedArray[0].MODELVAL != '1') {
				$.messager
						.alert(MESSAGE_PLAT.PROMPT, "只有阶段类型可以绑定任务分类!", 'info');
				return;
			}
			$.farm.openWindow({
				id : 'winProjectgradation',
				width : 800,
				height : 400,
				modal : true,
				url : "projectgradation/tasyTypes.do",
				title : selectedArray[0].NAME + "-绑定任务分类"
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//格式化排序
	function formatSort() {
		var selectedArray = $(gridProjectgradation).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			$.messager.confirm(MESSAGE_PLAT.PROMPT, "确认格式化排序?", function(flag) {
				if (flag) {
					$(gridProjectgradation).datagrid('loading');
					$.post("projectgradation/formatSort.do" + '?ids='
							+ $.farm.getCheckedIds(gridProjectgradation, 'ID'),
							{}, function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProjectgradation).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridProjectgradation).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//删除
	function delDataProjectgradation() {
		var selectedArray = $(gridProjectgradation).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProjectgradation).datagrid('loading');
					$.post(url_delActionProjectgradation + '?ids='
							+ $.farm.getCheckedIds(gridProjectgradation, 'ID'),
							{}, function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProjectgradation).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$.messager.confirm('确认对话框',
											'数据更新,是否重新加载左侧树？', function(r) {
												if (r) {
													$('#gradationTree').tree(
															'reload');
												}
											});
									$(gridProjectgradation).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	//移动节点
	function moveTree() {
		var selectedArray = $(gridProjectgradation).datagrid('getSelections');
		if (selectedArray.length > 0) {

			$.farm.openWindow({
				id : 'ProjectgradationTreeNodeWin',
				width : 250,
				height : 300,
				modal : true,
				url : "projectgradation/treeNodeTreeView.do",
				title : '移动分类'
			});
			chooseWindowCallBackHandle = function(node) {
				$.messager.confirm('确认对话框', '确定移动该节点么？', function(falg1) {
					if (falg1) {
						$.post('projectgradation/moveTreeNodeSubmit.do', {
							ids : $.farm.getCheckedIds(gridProjectgradation,
									'ID'),
							id : node.id
						}, function(flag) {
							if (flag.STATE == 0) {
								$(gridProjectgradation).datagrid('reload');
								$('#ProjectgradationTreeNodeWin').window(
										'close');
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#gradationTree').tree(
														'reload');
											}
										});
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ flag.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						}, 'json');
					}
				});
			};
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>