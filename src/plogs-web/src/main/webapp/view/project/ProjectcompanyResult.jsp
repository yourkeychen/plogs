<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<PF:basePath/>">
    <title>项目业务机构数据管理</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <jsp:include page="/view/conf/include.jsp"></jsp:include>
  </head>
  <body class="easyui-layout">
    <div data-options="region:'north',border:false">
      <form id="searchProjectcompanyForm">
        <table class="editTable">
          <tr style="text-align: center;">
            <td colspan="4">
              <a id="a_search" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-search">查询</a>
              <a id="a_reset" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-reload">清除条件</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div data-options="region:'center',border:false">
      <table id="dataProjectcompanyGrid">
        <thead>
          <tr>
            <th data-options="field:'ck',checkbox:true"></th>
            <th field="PROJECTID" data-options="sortable:true" width="90">PROJECTID</th>
            <th field="COMPANYID" data-options="sortable:true" width="90">COMPANYID</th>
            <th field="ID" data-options="sortable:true" width="20">ID</th>
          </tr>
        </thead>
      </table>
    </div>
  </body>
  <script type="text/javascript">
    var url_delActionProjectcompany = "projectcompany/del.do";//删除URL
    var url_formActionProjectcompany = "projectcompany/form.do";//增加、修改、查看URL
    var url_searchActionProjectcompany = "projectcompany/query.do";//查询URL
    var title_windowProjectcompany = "项目业务机构管理";//功能名称
    var gridProjectcompany;//数据表格对象
    var searchProjectcompany;//条件查询组件对象
    var toolBarProjectcompany = [ {
      id : 'view',
      text : '查看',
      iconCls : 'icon-tip',
      handler : viewDataProjectcompany
    }, {
      id : 'add',
      text : '新增',
      iconCls : 'icon-add',
      handler : addDataProjectcompany
    }, {
      id : 'edit',
      text : '修改',
      iconCls : 'icon-edit',
      handler : editDataProjectcompany
    }, {
      id : 'del',
      text : '删除',
      iconCls : 'icon-remove',
      handler : delDataProjectcompany
    } ];
    $(function() {
      //初始化数据表格
      gridProjectcompany = $('#dataProjectcompanyGrid').datagrid( {
        url : url_searchActionProjectcompany,
        fit : true,
        fitColumns : true,
        'toolbar' : toolBarProjectcompany,
        pagination : true,
        closable : true,
        checkOnSelect : true,
        border:false,
        striped : true,
        rownumbers : true,
        ctrlSelect : true
      });
      //初始化条件查询
      searchProjectcompany = $('#searchProjectcompanyForm').searchForm( {
        gridObj : gridProjectcompany
      });
    });
    //查看
    function viewDataProjectcompany() {
      var selectedArray = $(gridProjectcompany).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionProjectcompany + '?pageset.pageType='+PAGETYPE.VIEW+'&ids='
            + selectedArray[0].ID;
        $.farm.openWindow( {
          id : 'winProjectcompany',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '浏览'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //新增
    function addDataProjectcompany() {
      var url = url_formActionProjectcompany + '?operateType='+PAGETYPE.ADD;
      $.farm.openWindow( {
        id : 'winProjectcompany',
        width : 600,
        height : 300,
        modal : true,
        url : url,
        title : '新增'
      });
    }
    //修改
    function editDataProjectcompany() {
      var selectedArray = $(gridProjectcompany).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionProjectcompany + '?operateType='+PAGETYPE.EDIT+ '&ids=' + selectedArray[0].ID;
        $.farm.openWindow( {
          id : 'winProjectcompany',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '修改'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //删除
    function delDataProjectcompany() {
      var selectedArray = $(gridProjectcompany).datagrid('getSelections');
      if (selectedArray.length > 0) {
        // 有数据执行操作
        var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
        $.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
          if (flag) {
            $(gridProjectcompany).datagrid('loading');
            $.post(url_delActionProjectcompany + '?ids=' + $.farm.getCheckedIds(gridProjectcompany,'ID'), {},
              function(flag) {
                var jsonObject = JSON.parse(flag, null);
                $(gridProjectcompany).datagrid('loaded');
                if (jsonObject.STATE == 0) {
                  $(gridProjectcompany).datagrid('reload');
              } else {
                var str = MESSAGE_PLAT.ERROR_SUBMIT
                    + jsonObject.MESSAGE;
                $.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
              }
            });
          }
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
            'info');
      }
    }
  </script>
</html>