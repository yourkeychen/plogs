<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="fieldTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="fieldTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="fieldTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchProjectfieldForm">
				<table class="editTable">
					<tr>
						<td class="title">属性分类:</td>
						<td><input id="PARENTTITLE_field_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_field_RULE" name="TYPEID:=" type="hidden"></td>
						<td class="title">属性名称:</td>
						<td><input name="a.NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataProjectfieldGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="60">属性名称</th>
						<th field="UUID" data-options="sortable:true" width="60">编码</th>
						<th field="SORT" data-options="sortable:true" width="20">排序</th>
						<th field="SINGLETYPE" data-options="sortable:true" width="20">唯一性</th>
						<th field="VALTYPE" data-options="sortable:true" width="25">属性值类型</th>
						<th field="TYPENAME" data-options="sortable:true" width="30">属性分类</th>
						<th field="PSTATE" data-options="sortable:true" width="20">状态</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	var url_searchActionProjectfield = "field/chooseQuery.do";//查询URL
	var title_windowProjectfield = "项目阶段管理";//功能名称
	var gridProjectfield;//数据表格对象
	var searchProjectfield;//条件查询组件对象
	var toolBarProjectfield = [ {
		id : 'add',
		text : '选择',
		iconCls : 'icon-check',
		handler : function() {
			var selectedArray = $(gridProjectfield).datagrid(
					'getSelections');
			if (selectedArray.length > 0) {
				// 有数据执行操作
				var str = selectedArray.length + "条数据将被添加,是否继续?";
				$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
					if (flag) {
						try {
							chooseWindowCallBackHandle($.farm.getCheckedIds(
									gridProjectfield, 'ID'));
						} catch (e) {
							alert('请实现chooseWindowCallBackHandle(ids)');
						}
					}
				});
			} else {
				$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
						'info');
			}
		}
	} ];
	$(function() {
		//初始化数据表格
		gridProjectfield = $('#dataProjectfieldGrid').datagrid({
			url : url_searchActionProjectfield,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarProjectfield,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProjectfield = $('#searchProjectfieldForm').searchForm({
			gridObj : gridProjectfield
		});
		$('#fieldTree').tree({
			url : 'fieldtype/fieldTypeTree.do',
			onSelect : function(node) {
				$('#PARENTID_field_RULE').val(node.id);
				$('#PARENTTITLE_field_RULE').val(node.text);
				searchProjectfield.dosearch({
					'ruleText' : searchProjectfield.arrayStr()
				});
			}
		});
		$('#fieldTreeReload').bind('click', function() {
			$('#fieldTree').tree('reload');
		});
		$('#fieldTreeOpenAll').bind('click', function() {
			$('#fieldTree').tree('expandAll');
		});
	});
</script>
</div>