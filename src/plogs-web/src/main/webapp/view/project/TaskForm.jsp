<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--任务表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formTask">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title"></td>
					<td></td>
					<td class="title"></td>
					<td></td>
				</tr>
				<tr>
					<td class="title">标题:</td>
					<td colspan="3"><input type="text" style="width: 410px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[256]']"
						id="entity_title" name="title" value="${entity.title}"></td>
				</tr>
				<tr>
					<td class="title">项目名称:</td>
					<td>${project.name}<input type="hidden" id="entity_projectid"
						name="projectid" value="${project.id}">
					</td>
					<td class="title">项目阶段:</td>
					<td>${gradation.name}<input type="hidden"
						id="entity_projectid" name="gradationid" value="${gradation.id}"></td>
				</tr>
				<tr>
					<c:if test="${pageset.operateType==1}">
						<td class="title">任务类型:</td>
						<td><select style="width: 120px;" class="easyui-validatebox"
							data-options="required:true,validType:[,'maxLength[32]']"
							id="entity_tasktypeid" name="tasktypeid"
							val="${entity.tasktypeid}">
								<c:forEach items="${taskTypes}" var="node">
									<option value="${node.id}">${node.name}</option>
								</c:forEach>
						</select></td>
					</c:if>
					<c:if test="${pageset.operateType!=1}">
						<td class="title">任务类型:</td>
						<td>${entity.tasktypename}</td>
					</c:if>
					<td class="title">状态:</td>
					<td><select class="easyui-validatebox"
						data-options="required:true" id="entity_pstate" name="pstate"
						val="${entity.pstate}" style="width: 120px;">
							<option value="6">计划</option>
							<option value="1">等待</option>
							<option value="2">开始</option>
							<option value="3">执行</option>
							<option value="4">完成</option>
							<option value="5">归档</option>
							<!-- 6:计划,1:等待,2:开始,3:执行,4:完成,5:归档,0:删除 -->
					</select></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea rows="3" style="width: 420px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityTask" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityTask" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formTask" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionTask = 'task/add.do';
	var submitEditActionTask = 'task/edit.do';
	var currentPageTypeTask = '${pageset.operateType}';
	var submitFormTask;
	$(function() {
		if ("${STATE}" == "1") {
			$.messager.alert(MESSAGE_PLAT.PROMPT, "${MESSAGE}", 'info');
			$('#winTask').window('close');
		}
		//表单组件对象
		submitFormTask = $('#dom_formTask').SubmitForm({
			pageType : currentPageTypeTask,
			grid : gridTask,
			currentWindowId : 'winTask'
		});
		//关闭窗口
		$('#dom_cancle_formTask').bind('click', function() {
			$('#winTask').window('close');
		});
		//提交新增数据
		$('#dom_add_entityTask').bind('click', function() {
			submitFormTask.postSubmit(submitAddActionTask);
		});
		//提交修改数据
		$('#dom_edit_entityTask').bind('click', function() {
			submitFormTask.postSubmit(submitEditActionTask);
		});
	});
//-->
</script>