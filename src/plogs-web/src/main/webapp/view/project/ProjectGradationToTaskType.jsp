<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'center',border:false">
		<table id="dataProjectgradationtasktypeGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">任务类型名称</th>
					<th field="PSTATE" data-options="sortable:true" width="20">任务状态</th> 
					<th field="PCONTENT" data-options="sortable:true" width="110">任务类型备注</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var gridProjectgradationtasktype;//数据表格对象
	var searchProjectgradationtasktype;//条件查询组件对象
	var toolBarProjectgradationtasktype = [ {
		id : 'add',
		text : '添加',
		iconCls : 'icon-add',
		handler : addDataProjectgradationtasktype
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataProjectgradationtasktype
	} ];
	$(function() {
		var reQueryUrl = "projectgradation/queryReTaskType.do?gradationId="
				+ $.farm.getCheckedIds(gridProjectgradation, 'ID');
		//初始化数据表格
		gridProjectgradationtasktype = $('#dataProjectgradationtasktypeGrid')
				.datagrid({
					url : reQueryUrl,
					fit : true,
					fitColumns : true,
					'toolbar' : toolBarProjectgradationtasktype,
					pagination : true,
					closable : true,
					checkOnSelect : true,
					border : false,
					striped : true,
					rownumbers : true,
					ctrlSelect : true
				});
	});
	//添加
	function addDataProjectgradationtasktype() {
		$.farm.openWindow({
			id : 'winProjectgradationtasktype',
			width : 800,
			height : 500,
			modal : true,
			url : "tasktype/chooseWin.do",
			title : '添加任务类型'
		});
		chooseWindowCallBackHandle = function(ids) {
			$.post('projectgradation/bindTaskType.do?tasktypeids=' + ids
					+ "&gradationid="
					+ $.farm.getCheckedIds(gridProjectgradation, 'ID'), {},
					function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridProjectgradationtasktype).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$('#winProjectgradationtasktype').window('close');
							$(gridProjectgradationtasktype).datagrid('reload');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
		};
	}
	//删除
	function delDataProjectgradationtasktype() {
		var selectedArray = $(gridProjectgradationtasktype).datagrid(
				'getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProjectgradationtasktype).datagrid('loading');
					$.post('projectgradation/rebindTaskType.do?ids='
							+ $.farm.getCheckedIds(
									gridProjectgradationtasktype, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProjectgradationtasktype).datagrid(
										'loaded');
								if (jsonObject.STATE == 0) {
									$(gridProjectgradationtasktype).datagrid(
											'reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>