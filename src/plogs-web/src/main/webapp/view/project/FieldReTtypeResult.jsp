<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'center',border:false">
		<table id="dataFieldrettypeGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="FIELDNAME" data-options="sortable:true" width="70">属性名称</th>
					<th field="TASKTYPENAME" data-options="sortable:true" width="100">任务类型</th>
					<th field="WRITEIS" data-options="sortable:true" width="70">编辑权限</th>
					<th field="READIS" data-options="sortable:true" width="60">阅读权限</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionFieldrettype = "fieldrettype/del.do";//删除URL
	var url_formActionFieldrettype = "fieldrettype/form.do";//增加、修改、查看URL
	var url_searchActionFieldrettype = "fieldrettype/query.do";//查询URL
	var title_windowFieldrettype = "任务类型属性管理";//功能名称
	var gridFieldrettype;//数据表格对象
	var toolBarFieldrettype = [ {
		id : 'add',
		text : '添加属性',
		iconCls : 'icon-add',
		handler : addDataFieldrettype
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataFieldrettype
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataFieldrettype
	} ];
	$(function() {
		//初始化数据表格
		gridFieldrettype = $('#dataFieldrettypeGrid').datagrid({
			url : url_searchActionFieldrettype+ "?tasktypeid="
			+ $.farm.getCheckedIds(gridTasktype, 'ID'),
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarFieldrettype,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
	});
	//查看
	function viewDataFieldrettype() {
		var selectedArray = $(gridFieldrettype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionFieldrettype + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winFieldrettype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//添加
	function addDataFieldrettype() {
		$.farm.openWindow({
			id : 'winChooseFieldWin',
			width : 800,
			height : 500,
			modal : true,
			url : "field/chooseWin.do",
			title : '添加属性'
		});
		chooseWindowCallBackHandle = function(ids) {
			$.post('fieldrettype/bindTaskType.do?fieldids=' + ids
					+ "&taskTypeId="
					+ $.farm.getCheckedIds(gridTasktype, 'ID'), {},
					function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridFieldrettype).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$('#winChooseFieldWin').window('close');
							$(gridFieldrettype).datagrid('reload');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
		};
	}
	//修改
	function editDataFieldrettype() {
		var selectedArray = $(gridFieldrettype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionFieldrettype + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winFieldrettype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataFieldrettype() {
		var selectedArray = $(gridFieldrettype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridFieldrettype).datagrid('loading');
					$.post(url_delActionFieldrettype + '?ids='
							+ $.farm.getCheckedIds(gridFieldrettype, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridFieldrettype).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridFieldrettype).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>