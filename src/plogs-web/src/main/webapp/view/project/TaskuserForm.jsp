<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--任务干系人表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formTaskuser">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<c:if test="${pageset.operateType==1}">
				<input type="hidden" id="entity_taskid" name="taskid"
					value="${taskid}">
			</c:if>
			<table class="editTable">
				<tr>
					<td class="title">权限类型:</td>
					<td colspan="3"><select style="width: 360px;"
						class="easyui-validatebox" data-options="required:true"
						id="entity_poptype" name="poptype" val="${entity.poptype}">
							<option value="1">管理人</option>
							<option value="2">执行人</option>
							<option value="3">监视人</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">用户:</td>
					<td colspan="3"><select style="width: 360px;"
						class="easyui-validatebox" data-options="required:true"
						id="entity_userid" name="userid" val="${entity.userid}">
							<c:forEach items="${users }" var="node">
								<option value="${node.id }">${node.name }(${node.loginname })</option>
							</c:forEach>
					</select></td>
				</tr>
				<!-- <tr>
					<td class="title">LIVETIME:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[8]']"
						id="entity_livetime" name="livetime" value="${entity.livetime}">
					</td>
				</tr>
				<tr>
					<td class="title">POPTYPE:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[0.5]']"
						id="entity_poptype" name="poptype" value="${entity.poptype}">
					</td>
				</tr>
				<tr>
					<td class="title">TASKID:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_taskid" name="taskid" value="${entity.taskid}">
					</td>
				</tr>
				<tr>
					<td class="title">USERID:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_userid" name="userid" value="${entity.userid}">
					</td>
				</tr>
				<tr>
					<td class="title">PSTATE:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[1]']"
						id="entity_pstate" name="pstate" value="${entity.pstate}">
					</td>
				</tr>
				<tr>
					<td class="title">PROJECTID:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_projectid" name="projectid" value="${entity.projectid}">
					</td>
				</tr>
				<tr>
					<td class="title">PCONTENT:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
						name="pcontent" value="${entity.pcontent}"></td>
				</tr>
				<tr>
					<td class="title">CUSER:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_cuser" name="cuser" value="${entity.cuser}"></td>
				</tr>
				<tr>
					<td class="title">CTIME:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[8]']"
						id="entity_ctime" name="ctime" value="${entity.ctime}"></td>
				</tr> -->
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityTaskuser" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityTaskuser" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formTaskuser" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionTaskuser = 'taskuser/add.do';
	var submitEditActionTaskuser = 'taskuser/edit.do';
	var currentPageTypeTaskuser = '${pageset.operateType}';
	var submitFormTaskuser;
	$(function() {
		//表单组件对象
		submitFormTaskuser = $('#dom_formTaskuser').SubmitForm({
			pageType : currentPageTypeTaskuser,
			grid : gridTaskuser,
			currentWindowId : 'winTaskuser'
		});
		//关闭窗口
		$('#dom_cancle_formTaskuser').bind('click', function() {
			$('#winTaskuser').window('close');
		});
		//提交新增数据
		$('#dom_add_entityTaskuser').bind('click', function() {
			submitFormTaskuser.postSubmit(submitAddActionTaskuser);
		});
		//提交修改数据
		$('#dom_edit_entityTaskuser').bind('click', function() {
			submitFormTaskuser.postSubmit(submitEditActionTaskuser);
		});
	});
//-->
</script>