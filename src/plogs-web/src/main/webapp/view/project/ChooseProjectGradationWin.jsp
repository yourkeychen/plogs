<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="gradationTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="gradationTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="gradationTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchProjectgradationForm">
				<table class="editTable">
					<tr>
						<td class="title">上级节点:</td>
						<td><input id="PARENTTITLE_GRADATION_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_GRADATION_RULE" name="PARENTID:=" type="hidden"></td>
						<td class="title">阶段名称:</td>
						<td><input name="NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataProjectgradationGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">阶段名称</th>
						<th field="SORT" data-options="sortable:true" width="20">排序</th>
						<th field="MODEL" data-options="sortable:true" width="20">类型</th>
						<th field="TASKTYPENUM" data-options="sortable:true" width="30">任务类型数</th>
						<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
						<th field="PSTATE" data-options="sortable:true" width="20">状态</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	var url_searchActionProjectgradation = "projectgradation/chooseQuery.do";//查询URL
	var title_windowProjectgradation = "项目阶段管理";//功能名称
	var gridProjectgradation;//数据表格对象
	var searchProjectgradation;//条件查询组件对象
	var toolBarProjectgradation = [ {
		id : 'add',
		text : '选择',
		iconCls : 'icon-check',
		handler : function() {
			var selectedArray = $(gridProjectgradation).datagrid(
					'getSelections');
			if (selectedArray.length > 0) {
				// 有数据执行操作
				var str = selectedArray.length + "条数据将被添加,是否继续?";
				$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
					if (flag) {
						try {
							chooseWindowCallBackHandle($.farm.getCheckedIds(
									gridProjectgradation, 'ID'));
						} catch (e) {
							alert('请实现chooseWindowCallBackHandle(ids)');
						}
					}
				});
			} else {
				$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
						'info');
			}
		}
	} ];
	$(function() {
		//初始化数据表格
		gridProjectgradation = $('#dataProjectgradationGrid').datagrid({
			url : url_searchActionProjectgradation,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarProjectgradation,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProjectgradation = $('#searchProjectgradationForm').searchForm({
			gridObj : gridProjectgradation
		});
		$('#gradationTree').tree({
			url : 'projectgradation/gradationTree.do',
			onSelect : function(node) {
				$('#PARENTID_GRADATION_RULE').val(node.id);
				$('#PARENTTITLE_GRADATION_RULE').val(node.text);
				searchProjectgradation.dosearch({
					'ruleText' : searchProjectgradation.arrayStr()
				});
			}
		});
		$('#gradationTreeReload').bind('click', function() {
			$('#gradationTree').tree('reload');
		});
		$('#gradationTreeOpenAll').bind('click', function() {
			$('#gradationTree').tree('expandAll');
		});
	});
</script>
</div>