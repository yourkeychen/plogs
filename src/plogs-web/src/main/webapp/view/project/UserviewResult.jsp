<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<PF:basePath/>">
    <title>用户视图数据管理</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <jsp:include page="/view/conf/include.jsp"></jsp:include>
  </head>
  <body class="easyui-layout">
    <div data-options="region:'north',border:false">
      <form id="searchUserviewForm">
        <table class="editTable">
          <tr style="text-align: center;">
            <td colspan="4">
              <a id="a_search" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-search">查询</a>
              <a id="a_reset" href="javascript:void(0)"
                class="easyui-linkbutton" iconCls="icon-reload">清除条件</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div data-options="region:'center',border:false">
      <table id="dataUserviewGrid">
        <thead>
          <tr>
            <th data-options="field:'ck',checkbox:true"></th>
            <th field="TYPE" data-options="sortable:true" width="40">TYPE</th>
            <th field="SORT" data-options="sortable:true" width="40">SORT</th>
            <th field="USERID" data-options="sortable:true" width="60">USERID</th>
            <th field="VIEWMODEL" data-options="sortable:true" width="90">VIEWMODEL</th>
            <th field="PCONTENT" data-options="sortable:true" width="80">PCONTENT</th>
            <th field="PSTATE" data-options="sortable:true" width="60">PSTATE</th>
            <th field="CUSER" data-options="sortable:true" width="50">CUSER</th>
            <th field="CTIME" data-options="sortable:true" width="50">CTIME</th>
            <th field="ID" data-options="sortable:true" width="20">ID</th>
          </tr>
        </thead>
      </table>
    </div>
  </body>
  <script type="text/javascript">
    var url_delActionUserview = "userview/del.do";//删除URL
    var url_formActionUserview = "userview/form.do";//增加、修改、查看URL
    var url_searchActionUserview = "userview/query.do";//查询URL
    var title_windowUserview = "用户视图管理";//功能名称
    var gridUserview;//数据表格对象
    var searchUserview;//条件查询组件对象
    var toolBarUserview = [ {
      id : 'view',
      text : '查看',
      iconCls : 'icon-tip',
      handler : viewDataUserview
    }, {
      id : 'add',
      text : '新增',
      iconCls : 'icon-add',
      handler : addDataUserview
    }, {
      id : 'edit',
      text : '修改',
      iconCls : 'icon-edit',
      handler : editDataUserview
    }, {
      id : 'del',
      text : '删除',
      iconCls : 'icon-remove',
      handler : delDataUserview
    } ];
    $(function() {
      //初始化数据表格
      gridUserview = $('#dataUserviewGrid').datagrid( {
        url : url_searchActionUserview,
        fit : true,
        fitColumns : true,
        'toolbar' : toolBarUserview,
        pagination : true,
        closable : true,
        checkOnSelect : true,
        border:false,
        striped : true,
        rownumbers : true,
        ctrlSelect : true
      });
      //初始化条件查询
      searchUserview = $('#searchUserviewForm').searchForm( {
        gridObj : gridUserview
      });
    });
    //查看
    function viewDataUserview() {
      var selectedArray = $(gridUserview).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionUserview + '?pageset.pageType='+PAGETYPE.VIEW+'&ids='
            + selectedArray[0].ID;
        $.farm.openWindow( {
          id : 'winUserview',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '浏览'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //新增
    function addDataUserview() {
      var url = url_formActionUserview + '?operateType='+PAGETYPE.ADD;
      $.farm.openWindow( {
        id : 'winUserview',
        width : 600,
        height : 300,
        modal : true,
        url : url,
        title : '新增'
      });
    }
    //修改
    function editDataUserview() {
      var selectedArray = $(gridUserview).datagrid('getSelections');
      if (selectedArray.length == 1) {
        var url = url_formActionUserview + '?operateType='+PAGETYPE.EDIT+ '&ids=' + selectedArray[0].ID;
        $.farm.openWindow( {
          id : 'winUserview',
          width : 600,
          height : 300,
          modal : true,
          url : url,
          title : '修改'
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
            'info');
      }
    }
    //删除
    function delDataUserview() {
      var selectedArray = $(gridUserview).datagrid('getSelections');
      if (selectedArray.length > 0) {
        // 有数据执行操作
        var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
        $.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
          if (flag) {
            $(gridUserview).datagrid('loading');
            $.post(url_delActionUserview + '?ids=' + $.farm.getCheckedIds(gridUserview,'ID'), {},
              function(flag) {
                var jsonObject = JSON.parse(flag, null);
                $(gridUserview).datagrid('loaded');
                if (jsonObject.STATE == 0) {
                  $(gridUserview).datagrid('reload');
              } else {
                var str = MESSAGE_PLAT.ERROR_SUBMIT
                    + jsonObject.MESSAGE;
                $.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
              }
            });
          }
        });
      } else {
        $.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
            'info');
      }
    }
  </script>
</html>