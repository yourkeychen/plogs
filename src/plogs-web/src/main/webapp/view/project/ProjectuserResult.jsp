<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchProjectuserForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td class="title">用户登陆名称:</td>
					<td><input name="b.LOGINNAME:like" type="text"></td>
					<td><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataProjectuserGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="USERNAME" data-options="sortable:true" width="70">姓名</th>
					<th field="LOGINNAME" data-options="sortable:true" width="70">登陆名</th>
					<th field="LIVETIME" data-options="sortable:true" width="80">活跃时间</th>
					<th field="POPTYPE" data-options="sortable:true" width="70">类型</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionProjectuser = "projectuser/del.do";//删除URL
	var url_formActionProjectuser = "projectuser/form.do";//增加、修改、查看URL
	var url_searchActionProjectuser = "projectuser/query.do?projectid=${projectid}";//查询URL
	var title_windowProjectuser = "项目干系人管理";//功能名称
	var gridProjectuser;//数据表格对象
	var searchProjectuser;//条件查询组件对象
	var toolBarProjectuser = [// {
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//	handler : viewDataProjectuser
	//},
	//{
	//	id : 'add',
	//	text : '新增',
	//	iconCls : 'icon-add',
	//	handler : addDataProjectuser
	//}, 
	{
		id : 'adduser',
		text : '导入用户',
		iconCls : 'icon-customers',
		handler : addUser
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataProjectuser
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataProjectuser
	} ];
	$(function() {
		//初始化数据表格
		gridProjectuser = $('#dataProjectuserGrid').datagrid({
			url : url_searchActionProjectuser,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarProjectuser,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProjectuser = $('#searchProjectuserForm').searchForm({
			gridObj : gridProjectuser
		});
	});//导入用户
	function addUser() {
		var url = "user/chooseUser.do";
		$.farm.openWindow({
			id : 'chooseUserWin',
			width : 600,
			height : 400,
			modal : true,
			url : url,
			title : '导入用户'
		});
		chooseWindowCallBackHandle = function(row) {
			var userids;
			$(row).each(function(i, obj) {
				if (userids) {
					userids = userids + ',' + obj.ID;
				} else {
					userids = obj.ID;
				}
			});
			$(gridProjectuser).datagrid('loading');
			$.post("projectuser/addUser.do", {
				'userids' : userids,
				projectid : '${projectid}'
			},
					function(flag) {
						if (flag.STATE == 0) {
							$('#chooseUserWin').window('close');
							$(gridProjectuser).datagrid('reload');
						} else {
							$.messager.alert(MESSAGE_PLAT.ERROR, flag.MESSAGE,
									'error');
						}
					}, 'json');
		};
	}
	//查看
	function viewDataProjectuser() {
		var selectedArray = $(gridProjectuser).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProjectuser + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjectuser',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataProjectuser() {
		var url = url_formActionProjectuser + '?operateType=' + PAGETYPE.ADD;
		$.farm.openWindow({
			id : 'winProjectuser',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataProjectuser() {
		var selectedArray = $(gridProjectuser).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionProjectuser + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProjectuser',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataProjectuser() {
		var selectedArray = $(gridProjectuser).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridProjectuser).datagrid('loading');
					$.post(url_delActionProjectuser + '?ids='
							+ $.farm.getCheckedIds(gridProjectuser, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridProjectuser).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridProjectuser).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>