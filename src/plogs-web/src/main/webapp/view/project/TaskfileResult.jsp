<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'center',border:false">
		<table id="dataTaskfileGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="FILENAME" data-options="sortable:true" width="80">附件名称</th>
					<th field="SOURCETYPE" data-options="sortable:true" width="25">附件来源</th>
					<th field="CTIME" data-options="sortable:true" width="25">创建时间</th>
					<th field="TASKNAME" data-options="sortable:true" width="40">任务名称</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionTaskfile = "taskfile/del.do";//删除URL
	var url_formActionTaskfile = "taskfile/form.do";//增加、修改、查看URL
	var url_searchActionTaskfile = "taskfile/query.do?taskid=${taskid}";//查询URL
	var title_windowTaskfile = "任务附件管理";//功能名称
	var gridTaskfile;//数据表格对象
	var searchTaskfile;//条件查询组件对象
	var toolBarTaskfile = [ {
		id : 'view',
		text : '查看附件',
		iconCls : 'icon-tip',
		handler : viewDataTaskfile
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataTaskfile
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataTaskfile
	} ];
	$(function() {
		//初始化数据表格
		gridTaskfile = $('#dataTaskfileGrid').datagrid({
			url : url_searchActionTaskfile,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarTaskfile,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
	});
	//查看
	function viewDataTaskfile() {
		var selectedArray = $(gridTaskfile).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var fileurl = "<PF:basePath/>/document/view.do?fileid="
					+ selectedArray[0].FILEID;
			window.open(fileurl, "blank");
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//修改
	function editDataTaskfile() {
		var selectedArray = $(gridTaskfile).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionTaskfile + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTaskfile',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataTaskfile() {
		var selectedArray = $(gridTaskfile).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridTaskfile).datagrid('loading');
					$.post(url_delActionTaskfile + '?ids='
							+ $.farm.getCheckedIds(gridTaskfile, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridTaskfile).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridTaskfile).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
