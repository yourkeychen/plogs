<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'center',border:false">
		<table id="dataFieldreptypeGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="FIELDNAME" data-options="sortable:true" width="70">属性名称</th>
					<th field="SORT" data-options="sortable:true" width="70">排序</th>
					<th field="PROJECTTYPENAME" data-options="sortable:true"
						width="130">项目类型</th>
					<th field="WRITEIS" data-options="sortable:true" width="70">编辑权限</th>
					<th field="READIS" data-options="sortable:true" width="60">阅读权限</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionFieldreptype = "fieldreptype/del.do";//删除URL
	var url_formActionFieldreptype = "fieldreptype/form.do";//增加、修改、查看URL
	var url_searchActionFieldreptype = "fieldreptype/query.do";//查询URL
	var title_windowFieldreptype = "项目类型属性管理";//功能名称
	var gridFieldreptype;//数据表格对象
	var toolBarFieldreptype = [
	//{
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//	handler : viewDataFieldreptype
	//}, 
	{
		id : 'add',
		text : '添加属性',
		iconCls : 'icon-add',
		handler : addDataFieldreptype
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataFieldreptype
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataFieldreptype
	} ];
	$(function() {
		//初始化数据表格
		gridFieldreptype = $('#dataFieldreptypeGrid').datagrid(
				{
					url : url_searchActionFieldreptype + "?projecttypeid="
							+ $.farm.getCheckedIds(gridProjecttype, 'ID'),
					fit : true,
					fitColumns : true,
					'toolbar' : toolBarFieldreptype,
					pagination : true,
					closable : true,
					checkOnSelect : true,
					border : false,
					striped : true,
					rownumbers : true,
					ctrlSelect : true
				});
	});
	//查看
	function viewDataFieldreptype() {
		var selectedArray = $(gridFieldreptype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionFieldreptype + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winFieldreptype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//添加
	function addDataFieldreptype() {
		$.farm.openWindow({
			id : 'winProjectTypeGradation',
			width : 800,
			height : 500,
			modal : true,
			url : "field/chooseWin.do",
			title : '添加属性'
		});
		chooseWindowCallBackHandle = function(ids) {
			$.post('fieldreptype/bindProjectType.do?fieldids=' + ids
					+ "&projectTypeId="
					+ $.farm.getCheckedIds(gridProjecttype, 'ID'), {},
					function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridFieldreptype).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$('#winProjectTypeGradation').window('close');
							$(gridFieldreptype).datagrid('reload');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
		};
	}
	//修改
	function editDataFieldreptype() {
		var selectedArray = $(gridFieldreptype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionFieldreptype + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winFieldreptype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataFieldreptype() {
		var selectedArray = $(gridFieldreptype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridFieldreptype).datagrid('loading');
					$.post(url_delActionFieldreptype + '?ids='
							+ $.farm.getCheckedIds(gridFieldreptype, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridFieldreptype).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridFieldreptype).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>