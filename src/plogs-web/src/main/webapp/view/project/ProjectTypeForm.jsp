<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--项目分类表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formProjecttype">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">上級分类:</td>
					<td><c:if test="${empty parent}">无</c:if>${parent.name}<input
						type="hidden" name="parentid" value="${parent.id}"></td>
					<td class="title"></td>
					<td></td>
				</tr>
				<tr>
					<td class="title">分类名称:</td>
					<td><input type="text" style="width: 160px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_name" name="name" value="${entity.name}"></td>
					<td class="title">分类模型:</td>
					<td><select id="entity_model" style="width: 120px;"
						name="model" val="${entity.model}">
							<option value="0">树型分类</option>
							<option value="1">项目类型</option>
					</select>
				</tr>
				<tr>
					<td class="title">排序号:</td>
					<td><input type="text" style="width: 160px;"
						class="easyui-validatebox"
						data-options="required:true,validType:['integer','maxLength[5]']"
						id="entity_sort" name="sort" value="${entity.sort}"></td>
					<td class="title">状态:</td>
					<td><select id="entity_model" style="width: 120px;"
						name="pstate" val="${entity.pstate}">
							<option value="1">可用</option>
							<option value="0">停用</option>
					</select></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityProjecttype" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityProjecttype" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formProjecttype" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionProjecttype = 'projecttype/add.do';
	var submitEditActionProjecttype = 'projecttype/edit.do';
	var currentPageTypeProjecttype = '${pageset.operateType}';
	var submitFormProjecttype;
	$(function() {
		//表单组件对象
		submitFormProjecttype = $('#dom_formProjecttype').SubmitForm({
			pageType : currentPageTypeProjecttype,
			grid : gridProjecttype,
			currentWindowId : 'winProjecttype'
		});
		//关闭窗口
		$('#dom_cancle_formProjecttype').bind('click', function() {
			$('#winProjecttype').window('close');
		});
		//提交新增数据
		$('#dom_add_entityProjecttype').bind(
				'click',
				function() {
					submitFormProjecttype.postSubmit(
							submitAddActionProjecttype, function() {
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#projectTypeTree').tree(
														'reload');
											}
										});
								return true;
							});
				});
		//提交修改数据
		$('#dom_edit_entityProjecttype').bind(
				'click',
				function() {
					submitFormProjecttype.postSubmit(
							submitEditActionProjecttype, function() {
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#projectTypeTree').tree(
														'reload');
											}
										});
								return true;
							});
				});
	});
//-->
</script>