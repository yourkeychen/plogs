<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchCompanyForm">
			<table class="editTable">
				<tr>
					<td class="title">机构名称:</td>
					<td><input name="NAME:like" type="text"></td>
					<td><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataCompanyGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="50">名称</th>
					<th field="TYPE" data-options="sortable:true" width="30">机构类型</th>
					<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionCompany = "company/del.do";//删除URL
	var url_formActionCompany = "company/form.do";//增加、修改、查看URL
	var url_searchActionCompany = "company/query.do";//查询URL
	var title_windowCompany = "业务机构管理";//功能名称
	var gridCompany;//数据表格对象
	var searchCompany;//条件查询组件对象
	var toolBarCompany = [ {
		id : 'view',
		text : '选中',
		iconCls : 'icon-check',
		handler : doChooseCompany
	} ];
	$(function() {
		//初始化数据表格
		gridCompany = $('#dataCompanyGrid').datagrid({
			url : url_searchActionCompany,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarCompany,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchCompany = $('#searchCompanyForm').searchForm({
			gridObj : gridCompany
		});
	});
	//查看
	function doChooseCompany() {
		var selectedArray = $(gridCompany).datagrid('getSelections');
		if (selectedArray.length == 1) {
			chooseWindowCallBackHandle(selectedArray);
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
</script>
