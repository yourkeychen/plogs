<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>属性分类数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="fieldTypeTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="fieldTypeTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="fieldTypeTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchFieldtypeForm">
				<table class="editTable">
					<tr>
						<td class="title">上级节点:</td>
						<td><input id="PARENTTITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_RULE" name="PARENTID:=" type="hidden"></td>
						<td class="title"></td>
						<td></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataFieldtypeGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">名称</th>
						<th field="SORT" data-options="sortable:true" width="40">排序</th>
						<th field="PSTATE" data-options="sortable:true" width="60">状态</th>
						<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	var url_delActionFieldtype = "fieldtype/del.do";//删除URL
	var url_formActionFieldtype = "fieldtype/form.do";//增加、修改、查看URL
	var url_searchActionFieldtype = "fieldtype/query.do";//查询URL
	var title_windowFieldtype = "属性分类管理";//功能名称
	var gridFieldtype;//数据表格对象
	var searchFieldtype;//条件查询组件对象
	var toolBarFieldtype = [ {
		id : 'view',
		text : '查看',
		iconCls : 'icon-tip',
		handler : viewDataFieldtype
	}, {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataFieldtype
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataFieldtype
	}, {
		id : 'move',
		text : '移动',
		iconCls : 'icon-communication',
		handler : moveTree
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataFieldtype
	} ];
	$(function() {
		$('#fieldTypeTree').tree({
			url : 'fieldtype/fieldTypeTree.do',
			onSelect : function(node) {
				$('#PARENTID_RULE').val(node.id);
				$('#PARENTTITLE_RULE').val(node.text);
				searchFieldtype.dosearch({
					'ruleText' : searchFieldtype.arrayStr()
				});
			}
		});
		$('#fieldTypeTreeReload').bind('click', function() {
			$('#fieldTypeTree').tree('reload');
		});
		$('#fieldTypeTreeOpenAll').bind('click', function() {
			$('#fieldTypeTree').tree('expandAll');
		});

		//初始化数据表格
		gridFieldtype = $('#dataFieldtypeGrid').datagrid({
			url : url_searchActionFieldtype,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarFieldtype,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchFieldtype = $('#searchFieldtypeForm').searchForm({
			gridObj : gridFieldtype
		});
	});
	//查看
	function viewDataFieldtype() {
		var selectedArray = $(gridFieldtype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionFieldtype + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winFieldtype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataFieldtype() {
		var parentID = $("#PARENTID_RULE").val();
		var url = url_formActionFieldtype + '?operateType=' + PAGETYPE.ADD
				+ '&parentId=' + parentID;
		$.farm.openWindow({
			id : 'winFieldtype',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataFieldtype() {
		var selectedArray = $(gridFieldtype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionFieldtype + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winFieldtype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataFieldtype() {
		var selectedArray = $(gridFieldtype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridFieldtype).datagrid('loading');
					$.post(url_delActionFieldtype + '?ids='
							+ $.farm.getCheckedIds(gridFieldtype, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridFieldtype).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridFieldtype).datagrid('reload');
									$.messager.confirm('确认对话框',
											'数据更新,是否重新加载左侧组织机构树？', function(r) {
												if (r) {
													$('#fieldTypeTree')
															.tree('reload');
												}
											});
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//移动节点
	function moveTree() {
		var selectedArray = $(gridFieldtype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			$.farm.openWindow({
				id : 'fieldTypeTreeNodeWin',
				width : 250,
				height : 300,
				modal : true,
				url : "fieldtype/treeNodeTreeView.do",
				title : '移动分类'
			});
			chooseWindowCallBackHandle = function(node) {
				$.messager.confirm('确认对话框', '确定移动该节点么？', function(falg1) {
					if (falg1) {
						$.post('fieldtype/moveTreeNodeSubmit.do', {
							ids : $.farm.getCheckedIds(gridFieldtype, 'ID'),
							id : node.id
						}, function(flag) {
							if (flag.STATE == 0) {
								$(gridFieldtype).datagrid('reload');
								$('#fieldTypeTreeNodeWin').window('close');
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#fieldTypeTree').tree(
														'reload');
											}
										});
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ flag.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						}, 'json');
					}
				});
			};
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>