<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>任务类型数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="TaskTypeTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="TaskTypeTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="TaskTypeTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchTasktypeForm">
				<table class="editTable">
					<tr>
						<td class="title">上级节点:</td>
						<td><input id="PARENTTITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_RULE" name="PARENTID:=" type="hidden"></td>
						<td class="title">类型名称:</td>
						<td><input name="NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataTasktypeGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">任务类型</th>
						<th field="SORT" data-options="sortable:true" width="40">排序</th>
						<th field="MODEL" data-options="sortable:true" width="60">类型</th>
						<th field="ISDEMO" data-options="sortable:true" width="60">是否副本</th>
						<th field="TEMPLETEFILEID" data-options="sortable:true" width="60">模板</th>
						<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
						<th field="PSTATE" data-options="sortable:true" width="60">状态</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	var url_delActionTasktype = "tasktype/del.do";//删除URL
	var url_formActionTasktype = "tasktype/form.do";//增加、修改、查看URL
	var url_searchActionTasktype = "tasktype/query.do";//查询URL
	var title_windowTasktype = "任务类型管理";//功能名称
	var gridTasktype;//数据表格对象
	var searchTasktype;//条件查询组件对象
	var toolBarTasktype = [ {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataTasktype
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataTasktype
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataTasktype
	}, {
		id : 'formatSort',
		text : '格式化排序',
		iconCls : 'icon-arrow_refresh',
		handler : formatSort
	}, {
		id : 'move',
		text : '移动',
		iconCls : 'icon-communication',
		handler : moveTree
	}, {
		id : 'bindFields',
		text : '绑定项目属性',
		iconCls : 'icon-invoice',
		handler : bindFields
	}, {
		id : 'setTemplete',
		text : '设置任务模板',
		iconCls : 'icon-cv',
		handler : templeteSetting
	} ];
	$(function() {
		//初始化数据表格
		gridTasktype = $('#dataTasktypeGrid').datagrid({
			url : url_searchActionTasktype,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarTasktype,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchTasktype = $('#searchTasktypeForm').searchForm({
			gridObj : gridTasktype
		});

		$('#TaskTypeTree').tree({
			url : 'tasktype/tasktypeTree.do',
			onSelect : function(node) {
				$('#PARENTID_RULE').val(node.id);
				$('#PARENTTITLE_RULE').val(node.text);
				searchTasktype.dosearch({
					'ruleText' : searchTasktype.arrayStr()
				});
			}
		});
		$('#TaskTypeTreeReload').bind('click', function() {
			$('#TaskTypeTree').tree('reload');
		});
		$('#TaskTypeTreeOpenAll').bind('click', function() {
			$('#TaskTypeTree').tree('expandAll');
		});
	});
	//查看
	function viewDataTasktype() {
		var selectedArray = $(gridTasktype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionTasktype + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTasktype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//新增
	function addDataTasktype() {
		var parentID = $("#PARENTID_RULE").val();
		var url = url_formActionTasktype + '?operateType=' + PAGETYPE.ADD
				+ '&parentId=' + parentID;
		$.farm.openWindow({
			id : 'winTasktype',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}

	//设置模板
	function templeteSetting() {
		var selectedArray = $(gridTasktype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = 'tasktype/templeteSetting.do?operateType=' + PAGETYPE.EDIT
					+ '&taskTypeid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTempleteSetting',
				width : 600,
				height : 500,
				modal : true,
				url : url,
				title : '任务模板'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}

	//修改
	function editDataTasktype() {
		var selectedArray = $(gridTasktype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionTasktype + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTasktype',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataTasktype() {
		var selectedArray = $(gridTasktype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridTasktype).datagrid('loading');
					$.post(url_delActionTasktype + '?ids='
							+ $.farm.getCheckedIds(gridTasktype, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridTasktype).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridTasktype).datagrid('reload');
									$.messager.confirm('确认对话框',
											'数据更新,是否重新加载左侧组织机构树？', function(r) {
												if (r) {
													$('#TaskTypeTree').tree(
															'reload');
												}
											});
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//格式化排序
	function formatSort() {
		var selectedArray = $(gridTasktype).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			$.messager.confirm(MESSAGE_PLAT.PROMPT, "确认格式化排序?", function(flag) {
				if (flag) {
					$(gridTasktype).datagrid('loading');
					$.post("tasktype/formatSort.do" + '?ids='
							+ $.farm.getCheckedIds(gridTasktype, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridTasktype).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridTasktype).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	//移动节点
	function moveTree() {
		var selectedArray = $(gridTasktype).datagrid('getSelections');
		if (selectedArray.length > 0) {

			$.farm.openWindow({
				id : 'taskTypeTreeNodeWin',
				width : 250,
				height : 300,
				modal : true,
				url : "tasktype/treeNodeTreeView.do",
				title : '移动分类'
			});
			chooseWindowCallBackHandle = function(node) {
				$.messager.confirm('确认对话框', '确定移动该节点么？', function(falg1) {
					if (falg1) {
						$.post('tasktype/moveTreeNodeSubmit.do', {
							ids : $.farm.getCheckedIds(gridTasktype, 'ID'),
							id : node.id
						}, function(flag) {
							if (flag.STATE == 0) {
								$(gridTasktype).datagrid('reload');
								$('#taskTypeTreeNodeWin').window('close');
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#TaskTypeTree').tree(
														'reload');
											}
										});
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ flag.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						}, 'json');
					}
				});
			};
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
	//绑定属性
	function bindFields() {
		var selectedArray = $(gridTasktype).datagrid('getSelections');
		if (selectedArray.length == 1) {
			if (selectedArray[0].MODELVAL != '1') {
				$.messager.alert(MESSAGE_PLAT.PROMPT, "只有任务类型可以绑定属性!", 'info');
				return;
			}
			$.farm.openWindow({
				id : 'winFieldReTtype',
				width : 800,
				height : 400,
				modal : true,
				url : "fieldrettype/list.do",
				title : selectedArray[0].NAME + "-绑定任务属性"
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
</script>
</html>