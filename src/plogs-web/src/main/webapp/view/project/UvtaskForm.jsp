<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--视图任务表单-->
<div class="easyui-layout" data-options="fit:true">
  <div class="TableTitle" data-options="region:'north',border:false">
    <span class="label label-primary"> 
    <c:if test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if>
    <c:if test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if>
    <c:if test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
    </span>
  </div>
  <div data-options="region:'center'">
    <form id="dom_formUvtask">
      <input type="hidden" id="entity_id" name="id" value="${entity.id}">
      <table class="editTable">
      <tr>
        <td class="title">
        VIEWID:
        </td>
        <td colspan="3">
          <input type="text" style="width: 360px;" class="easyui-validatebox" data-options="required:true,validType:[,'maxLength[16]']"
          id="entity_viewid" name="viewid" value="${entity.viewid}">
        </td>
      </tr>
      <tr>
        <td class="title">
        TASKID:
        </td>
        <td colspan="3">
          <input type="text" style="width: 360px;" class="easyui-validatebox" data-options="required:true,validType:[,'maxLength[16]']"
          id="entity_taskid" name="taskid" value="${entity.taskid}">
        </td>
      </tr>
    </table>
    </form>
  </div>
  <div data-options="region:'south',border:false">
    <div class="div_button" style="text-align: center; padding: 4px;">
      <c:if test="${pageset.operateType==1}">
      <a id="dom_add_entityUvtask" href="javascript:void(0)"  iconCls="icon-save" class="easyui-linkbutton">增加</a>
      </c:if>
      <c:if test="${pageset.operateType==2}">
      <a id="dom_edit_entityUvtask" href="javascript:void(0)" iconCls="icon-save" class="easyui-linkbutton">修改</a>
      </c:if>
      <a id="dom_cancle_formUvtask" href="javascript:void(0)" iconCls="icon-cancel" class="easyui-linkbutton"   style="color: #000000;">取消</a>
    </div>
  </div>
</div>
<script type="text/javascript">
  var submitAddActionUvtask = 'uvtask/add.do';
  var submitEditActionUvtask = 'uvtask/edit.do';
  var currentPageTypeUvtask = '${pageset.operateType}';
  var submitFormUvtask;
  $(function() {
    //表单组件对象
    submitFormUvtask = $('#dom_formUvtask').SubmitForm( {
      pageType : currentPageTypeUvtask,
      grid : gridUvtask,
      currentWindowId : 'winUvtask'
    });
    //关闭窗口
    $('#dom_cancle_formUvtask').bind('click', function() {
      $('#winUvtask').window('close');
    });
    //提交新增数据
    $('#dom_add_entityUvtask').bind('click', function() {
      submitFormUvtask.postSubmit(submitAddActionUvtask);
    });
    //提交修改数据
    $('#dom_edit_entityUvtask').bind('click', function() {
      submitFormUvtask.postSubmit(submitEditActionUvtask);
    });
  });
  //-->
</script>