<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh-CN.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-kindeditor.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-file-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-multiimage-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/htmltags.js"></script>
<script src="<PF:basePath/>text/lib/fileUpload/jquery.ui.widget.js"></script>
<script
	src="<PF:basePath/>text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.css"
	rel="stylesheet">

<style>
<!--
.uploadedFileUnit {
	padding: 4px;
	background-color: #dddddd;
	margin: 4px;
}

.uploadedFileUnit img {
	width: 64px;
	height: 64px;
}
-->
</style>
<!--任务类型表单-->
<div data-options="fit:true">
	<div data-options="region:'center'">
		<textarea name="text" id="templete"
			style="width: 100%; height: 400px;">${fn:replace(fn:replace(text,'&lt;', '&amp;lt;'),'&gt;', '&amp;gt;')}</textarea>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<a id="dom_add_entityTemplete" href="javascript:void(0)"
				iconCls="icon-save" class="easyui-linkbutton">保存</a> <a
				id="dom_edit_entityTemplete" href="javascript:void(0)"
				iconCls="icon-remove" class="easyui-linkbutton">删除</a> <a
				id="dom_cancle_formTemplete" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script>
	var templeteeditor;
	var tooltype = 'max';
	var autoHeight = false;
	$(function() {
		var miniMenu = [ 'fontsize', 'forecolor', 'bold', 'underline',
				'removeformat', 'justifyleft', 'justifycenter',
				'insertunorderedlist', 'wcpfile', 'source' ];
		var maxMenu = [ 'fontsize', 'forecolor', 'bold', 'underline',
				'removeformat', 'justifyleft', 'justifycenter',
				'insertunorderedlist', 'formatblock', '|', 'link', 'code',
				'wcpimgs', 'wcpfile', '|', 'source' ];
		var mobileMenu = [ 'justifyleft', 'justifycenter',
				'insertunorderedlist', 'wcpfile', 'source' ];
		if (tooltype == 'max') {
			userMenu = maxMenu;
		}
		if (tooltype == 'min') {
			userMenu = miniMenu;
		}
		if (tooltype == 'mobile') {
			autoHeight = true;
			userMenu = mobileMenu;
		}
		templeteeditor = KindEditor.create('textarea[id="templete"]', {
			resizeType : 1,
			afterChange : function() {
				//内容更新后
			},
			cssPath : '<PF:basePath/>text/lib/kindeditor/editInner.css',
			uploadJson : basePath + 'upload/generalKindEditor.do',
			formatUploadUrl : false,
			allowPreviewEmoticons : false,
			resizeType : 0,
			allowImageRemote : true,
			autoHeightMode : autoHeight,
			allowImageUpload : true,
			items : userMenu,
			afterCreate : function() {
				//粘贴的文件直接上传到后台
				pasteImgHandle(this, basePath + 'upload/base64img.do');
				//禁止粘贴图片等文件:pasteNotAble(this.edit);
			},
			htmlTags : htmlTagsVar
		});
	});

	//刷新超文本内容到表单中
	function syncEditorcontent() {
		templeteeditor.sync();
	}
	//获得潮文本内容
	function getEditorcontent() {
		return templeteeditor.html();
	}
	//获得潮文本内容
	function getEditorcontentText() {
		return templeteeditor.text();
	}
	//设置超文本内容
	function setEditorcontent(text) {
		templeteeditor.html(text);
	}
	//禁止kindeditor中粘贴文件
	function pasteNotAble(edit) {
		var doc = edit.doc;
		var cmd = edit.cmd;
		$(doc.body).bind('paste', function(ev) {
			var $this = $(this);
			var dataItem = ev.originalEvent.clipboardData.items[0];
			if (dataItem) {
				var file = dataItem.getAsFile();
				if (file) {
					//暂时不处理，文件上传
					return false;
				}
			}
		});
	}
</script>
<script type="text/javascript">
	$(function() {
		//关闭窗口
		$('#dom_cancle_formTemplete').bind('click', function() {
			$('#winTempleteSetting').window('close');
		});
		//提交模板数据
		$('#dom_add_entityTemplete').bind(
				'click',
				function() {
					$.messager.confirm(MESSAGE_PLAT.PROMPT, "是否立即保存模板？",
							function(flag) {
								if (flag) {
									$.post('tasktype/templeteSubmit.do', {
										'taskTypeid' : '${taskTypeid}',
										'text' : getEditorcontent()
									}, function(jsonObject) {
										if (jsonObject.STATE == 0) {
											$(gridTasktype).datagrid('reload');
											$('#winTempleteSetting').window(
													'close');
										} else {
											var str = MESSAGE_PLAT.ERROR_SUBMIT
													+ jsonObject.MESSAGE;
											$.messager.alert(
													MESSAGE_PLAT.ERROR, str,
													'error');
										}
									}, 'json');
								}
							});
				});
		//清除模板数据
		$('#dom_edit_entityTemplete').bind(
				'click',
				function() {
					$.messager.confirm(MESSAGE_PLAT.PROMPT, "是否清除模板，清除后不可恢复？",
							function(flag) {
								if (flag) {
									$.post('tasktype/templeteClear.do', {
										'taskTypeid' : '${taskTypeid}'
									}, function(jsonObject) {
										if (jsonObject.STATE == 0) {
											$(gridTasktype).datagrid('reload');
											$('#winTempleteSetting').window(
													'close');
										} else {
											var str = MESSAGE_PLAT.ERROR_SUBMIT
													+ jsonObject.MESSAGE;
											$.messager.alert(
													MESSAGE_PLAT.ERROR, str,
													'error');
										}
									}, 'json');
								}
							});
				});
	});
//-->
</script>