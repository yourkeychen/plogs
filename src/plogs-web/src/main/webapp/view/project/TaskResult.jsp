<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>任务数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div class="easyui-layout"
		data-options="region:'west',split:true,border:false"
		style="width: 360px;">
		<div data-options="region:'north',border:false" style="height: 200px;">
			<div class="TREE_COMMON_BOX_SPLIT_DIV">
				<a id="projectTypeTreeReload" href="javascript:void(0)"
					class="easyui-linkbutton" data-options="plain:true"
					iconCls="icon-reload">刷新</a> <a id="projectTypeTreeOpenAll"
					href="javascript:void(0)" class="easyui-linkbutton"
					data-options="plain:true" iconCls="icon-sitemap">展开</a>
			</div>
			<ul id="projectTypeTree"></ul>
		</div>
		<div data-options="region:'center',border:false"
			style="border-top: 1px solid #dddddd;">
			<table id="dataProjectGrid">
				<thead>
					<tr>
						<th field="NAME" data-options="sortable:true" width="40">项目名称</th>
						<th field="GRADATIONNAME" data-options="sortable:true" width="30">当前阶段</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchTaskForm">
				<table class="editTable">
					<tr>
						<td class="title">项目名称:</td>
						<td><input id="PROJECTTITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8; width: 200px;">
							<input id="PROJECTID_RULE" name="PROJECTID:like" type="hidden"></td>
						<td class="title">任务类型:</td>
						<td><input name="a.TASKTYPENAME:like" type="text"></td>
					</tr>
					<tr>
						<td class="title">任务名称:</td>
						<td><input name="TITLE:like" type="text"
							style="width: 200px;"></td>
						<td class="title">任务状态:</td>
						<td><select name="PSTATE:like">
								<option value="">全部</option>
								<option value="1">计划</option>
								<option value="2">开始</option>
								<option value="3">处理</option>
								<option value="4">结束</option>
								<option value="5">关闭</option>
						</select></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataTaskGrid">
				<thead data-options="frozen:true">
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="TITLE" data-options="sortable:true" width="230">任务名称</th>
						<th field="TASKTYPENAME" data-options="sortable:true" width="130">任务类型</th>
						<th field="USERNAME" data-options="sortable:true" width="80">负责人</th>
						<th field="PSTATE" data-options="sortable:true" width="60">状态</th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="PROJECTNAME" data-options="sortable:true" width="180">项目名称</th>
						<th field="GRADATIONNAME" data-options="sortable:true" width="130">项目阶段</th>
						<th field="PLANSTIME" data-options="sortable:true" width="100">计划开始时间</th>
						<th field="DOSTIME" data-options="sortable:true" width="100">实际开始时间</th>
						<th field="DOTIMES" data-options="sortable:true" width="70">执行时长</th>
						<th field="SOURCEUSERID" data-options="sortable:true" width="70">来源人</th>
						<th field="CUSERNAME" data-options="sortable:true" width="70">创建人</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	var url_delActionTask = "task/del.do";//删除URL
	var url_formActionTask = "task/form.do";//增加、修改、查看URL
	var url_searchActionTask = "task/query.do";//查询URL
	var title_windowTask = "任务管理";//功能名称
	var gridTask;//数据表格对象
	var searchTask;//条件查询组件对象
	var toolBarTask = [ {
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataTask
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataTask
	}, {
		id : 'editfiles',
		text : '附件管理',
		iconCls : 'icon-archives',
		handler : editTaskFiles
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataTask
	}, {
		id : 'setUser',
		text : '关系人管理',
		iconCls : 'icon-user_green',
		handler : setTaskUser
	}, {
		id : 'resetUser',
		text : '重置关系人',
		iconCls : '	icon-special-offer',
		handler : resetTaskUser
	} ];
	$(function() {
		//初始化数据表格
		gridTask = $('#dataTaskGrid').datagrid({
			url : url_searchActionTask,
			fit : true,
			fitColumns : false,
			'toolbar' : toolBarTask,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//項目列表
		var gridProject = $('#dataProjectGrid').datagrid({
			url : "project/query.do?state=1",
			fit : true,
			fitColumns : true,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		$('#dataProjectGrid').datagrid({
			onClickRow : function(rowIndex, rowData) {
				$('#PROJECTID_RULE').val(rowData.ID);
				$('#PROJECTTITLE_RULE').val(rowData.NAME);
				searchTask.dosearch({
					'ruleText' : searchTask.arrayStr()
				});
			}
		});
		//初始化条件查询
		searchTask = $('#searchTaskForm').searchForm({
			gridObj : gridTask
		});
		$('#projectTypeTree').tree({
			url : 'projecttype/projectTypeTree.do',
			onSelect : function(node) {
				//按照項目分類id查詢：node.id
				if (node.id == "NONE") {
					$('#dataProjectGrid').datagrid('load', {});
				} else {
					$('#dataProjectGrid').datagrid('load', {
						ruleText : 'TYPEID:=:' + node.id
					});
				}
			}
		});
		$('#projectTypeTreeReload').bind('click', function() {
			$('#projectTypeTree').tree('reload');
		});
		$('#projectTypeTreeOpenAll').bind('click', function() {
			$('#projectTypeTree').tree('expandAll');
		});
	});

	//关系人管理
	function setTaskUser() {
		var selectedArray = $(gridTask).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = 'taskuser/list.do?operateType=' + PAGETYPE.EDIT
					+ '&taskid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winProject',
				width : 600,
				height : 400,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//重置关系人
	function resetTaskUser() {
		var selectedArray = $(gridTask).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + "个任务将被重置关系人，该操作无法恢复，是否继续？";
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridTask).datagrid('loading');
					$.post('taskuser/resetUser.do?ids='
							+ $.farm.getCheckedIds(gridTask, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridTask).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridTask).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//查看
	function viewDataTask() {
		var selectedArray = $(gridTask).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionTask + '?pageset.pageType=' + PAGETYPE.VIEW
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTask',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataTask() {
		var projectid = $('#PROJECTID_RULE').val();
		if (!projectid) {
			$.messager.alert(MESSAGE_PLAT.PROMPT, "请选择一个项目后添加任务!", 'info');
			return;
		}
		var url = url_formActionTask + '?operateType=' + PAGETYPE.ADD
				+ "&projectid=" + projectid;
		$.farm.openWindow({
			id : 'winTask',
			width : 600,
			height : 300,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataTask() {
		var selectedArray = $(gridTask).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionTask + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTask',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//附件管理
	function editTaskFiles() {
		var selectedArray = $(gridTask).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = 'taskfile/list.do?taskid=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winTaskFile',
				width : 800,
				height : 400,
				modal : true,
				url : url,
				title : '任务附件管理'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataTask() {
		var selectedArray = $(gridTask).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridTask).datagrid('loading');
					$.post(url_delActionTask + '?ids='
							+ $.farm.getCheckedIds(gridTask, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridTask).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridTask).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>