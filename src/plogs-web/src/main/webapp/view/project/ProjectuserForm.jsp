<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--项目干系人表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formProjectuser">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<!-- <tr>
					<td class="title">LIVETIME:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[8]']"
						id="entity_livetime" name="livetime" value="${entity.livetime}">
					</td>
				</tr>
				<tr>
					<td class="title">USERID:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_userid" name="userid" value="${entity.userid}">
					</td>
				</tr><tr>
					<td class="title">PROJECTID:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_projectid" name="projectid" value="${entity.projectid}">
					</td>
				</tr>
				<tr>
					<td class="title">PSTATE:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[1]']"
						id="entity_pstate" name="pstate" value="${entity.pstate}">
					</td>
				</tr>
				<tr>
					<td class="title">PCONTENT:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
						name="pcontent" value="${entity.pcontent}"></td>
				</tr>
				<tr>
					<td class="title">CUSER:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[16]']"
						id="entity_cuser" name="cuser" value="${entity.cuser}"></td>
				</tr>
				<tr>
					<td class="title">CTIME:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[8]']"
						id="entity_ctime" name="ctime" value="${entity.ctime}"></td>
				</tr> -->
				<tr>
					<td class="title">权限类型:</td>
					<td colspan="3"><select style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true"
						id="entity_poptype" name="poptype" val="${entity.poptype}">
							<option value="1">管理员</option>
							<option value="2">成员</option>
					</select></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityProjectuser" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityProjectuser" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formProjectuser" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionProjectuser = 'projectuser/add.do';
	var submitEditActionProjectuser = 'projectuser/edit.do';
	var currentPageTypeProjectuser = '${pageset.operateType}';
	var submitFormProjectuser;
	$(function() {
		//表单组件对象
		submitFormProjectuser = $('#dom_formProjectuser').SubmitForm({
			pageType : currentPageTypeProjectuser,
			grid : gridProjectuser,
			currentWindowId : 'winProjectuser'
		});
		//关闭窗口
		$('#dom_cancle_formProjectuser').bind('click', function() {
			$('#winProjectuser').window('close');
		});
		//提交新增数据
		$('#dom_add_entityProjectuser').bind('click', function() {
			submitFormProjectuser.postSubmit(submitAddActionProjectuser);
		});
		//提交修改数据
		$('#dom_edit_entityProjectuser').bind('click', function() {
			submitFormProjectuser.postSubmit(submitEditActionProjectuser);
		});
	});
//-->
</script>