<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--项目表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formProject">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title"></td>
					<td></td>
					<td class="title"></td>
					<td></td>
				</tr>
				<tr>
					<td class="title" rowspan="2">项目名称:</td>
					<td colspan="3"><input type="text" style="width: 420px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_name" name="name" value="${entity.name}"></td>
				</tr>
				<tr>
					<td colspan="3"
						style="color: green; font-size: 12px; padding: 8px;">参考名称格式:[客户名称]-[产品名称]-[服务名称]</td>
				</tr>
				<tr>
					<td class="title">项目类型:</td>
					<td colspan="3">${type.name}<input type="hidden" name="typeid"
						id="entity_typeid" value="${type.id}">
					</td>
				</tr>
				<tr>
					<td class="title">项目阶段:</td>
					<td><select id="entity_gradationid" style="width: 120px;"
						name="gradationid" val="${entity.gradationid}">
							<c:forEach items="${gradations}" var="node">
								<option value="${node.id}">${node.name}</option>
							</c:forEach>
					</select></td>
					<td class="title">项目状态:</td>
					<td><select id="entity_model" style="width: 120px;"
						name="state" val="${entity.state}">
							<option value="1">激活</option>
							<option value="0">停用</option>
							<option value="2">关闭</option>
							<option value="3">删除</option>
					</select></td>
				</tr>
				<tr>
					<td class="title">项目备注:</td>
					<td colspan="3"><textarea rows="3" style="width: 420px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityProject" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityProject" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formProject" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionProject = 'project/add.do';
	var submitEditActionProject = 'project/edit.do';
	var currentPageTypeProject = '${pageset.operateType}';
	var submitFormProject;
	$(function() {
		if ("${STATE}" == "1") {
			$.messager.alert(MESSAGE_PLAT.PROMPT, "${MESSAGE}", 'info');
			$('#winProject').window('close');
		}
		//表单组件对象
		submitFormProject = $('#dom_formProject').SubmitForm({
			pageType : currentPageTypeProject,
			grid : gridProject,
			currentWindowId : 'winProject'
		});
		//关闭窗口
		$('#dom_cancle_formProject').bind('click', function() {
			$('#winProject').window('close');
		});
		//提交新增数据
		$('#dom_add_entityProject').bind('click', function() {
			if (!$('#entity_typeid').val()) {
				$.messager.alert(MESSAGE_PLAT.PROMPT, "请选择一个项目类型", 'info');
				return;
			}
			submitFormProject.postSubmit(submitAddActionProject);
		});
		//提交修改数据
		$('#dom_edit_entityProject').bind('click', function() {
			submitFormProject.postSubmit(submitEditActionProject);
		});
	});
//-->
</script>