<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="projectTypeTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="projectTypeTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="projectTypeTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchProjecttypeForm">
				<table class="editTable">
					<tr>
						<td class="title">上级节点:</td>
						<td><input id="PARENTTITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_RULE" name="PARENTID:=" type="hidden"></td>
						<td class="title">分类名称:</td>
						<td><input name="NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataProjecttypeGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">名称</th>
						<th field="SORT" data-options="sortable:true" width="40">排序</th>
						<th field="MODEL" data-options="sortable:true" width="50">模型</th>
						<th field="PSTATE" data-options="sortable:true" width="60">状态</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	var gridProjecttype;//数据表格对象
	var searchProjecttype;//条件查询组件对象
	var toolBarProjecttype = [ {
		id : 'add',
		text : '选择',
		iconCls : 'icon-check',
		handler : function() {
			var selectedArray = $(gridProjecttype).datagrid('getSelections');
			if (selectedArray.length > 0) {
				// 有数据执行操作
				var str = selectedArray.length + "条数据将被添加,是否继续?";
				$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
					if (flag) {
						alert($.farm.getCheckedIds(gridProjecttype, 'ID'));
						try {
							chooseWindowCallBackHandle($.farm.getCheckedIds(
									gridProjecttype, 'ID'));
						} catch (e) {
							alert('请实现chooseWindowCallBackHandle(ids)');
						}
					}
				});
			} else {
				$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
						'info');
			}

		}
	} ];
	$(function() {
		//初始化数据表格
		gridProjecttype = $('#dataProjecttypeGrid').datagrid({
			url : "projecttype/query.do",
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarProjecttype,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchProjecttype = $('#searchProjecttypeForm').searchForm({
			gridObj : gridProjecttype
		});
		$('#projectTypeTree').tree({
			url : 'projecttype/projectTypeTree.do',
			onSelect : function(node) {
				$('#PARENTID_RULE').val(node.id);
				$('#PARENTTITLE_RULE').val(node.text);
				searchProjecttype.dosearch({
					'ruleText' : searchProjecttype.arrayStr()
				});
			}
		});
		$('#projectTypeTreeReload').bind('click', function() {
			$('#projectTypeTree').tree('reload');
		});
		$('#projectTypeTreeOpenAll').bind('click', function() {
			$('#projectTypeTree').tree('expandAll');
		});
	});
</script>
</html>