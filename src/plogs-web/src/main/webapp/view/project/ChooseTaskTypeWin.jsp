<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'west',split:true,border:false"
		style="width: 200px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="TaskTypeTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="TaskTypeTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="TaskTypeTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchTasktypeForm">
				<table class="editTable">
					<tr>
						<td class="title">上级节点:</td>
						<td><input id="PARENTTITLE_CHOOSETASKTYPE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_CHOOSETASKTYPE_RULE" name="PARENTID:=" type="hidden"></td>
						<td class="title">类型名称:</td>
						<td><input name="NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="4"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataTasktypeGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="40">任务类型</th>
						<th field="SORT" data-options="sortable:true" width="40">排序</th>
						<th field="PCONTENT" data-options="sortable:true" width="80">备注</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	var gridTasktype;//数据表格对象
	var searchTasktype;//条件查询组件对象
	var toolBarTasktype = [{
		id : 'add',
		text : '选择',
		iconCls : 'icon-check',
		handler : function() {
			var selectedArray = $(gridTasktype).datagrid('getSelections');
			if (selectedArray.length > 0) {
				// 有数据执行操作
				var str = selectedArray.length + "条数据将被添加,是否继续?";
				$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
					if (flag) {
						try {
							chooseWindowCallBackHandle($.farm.getCheckedIds(
									gridTasktype, 'ID'));
						} catch (e) {
							alert('请实现chooseWindowCallBackHandle(ids)');
						}
					}
				});
			} else {
				$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
						'info');
			}

		}
	} ];
	$(function() {
		//初始化数据表格
		gridTasktype = $('#dataTasktypeGrid').datagrid({
			url : "tasktype/chooseQuery.do",
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarTasktype,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchTasktype = $('#searchTasktypeForm').searchForm({
			gridObj : gridTasktype
		});
		$('#TaskTypeTree').tree({
			url : 'tasktype/tasktypeTree.do',
			onSelect : function(node) {
				$('#PARENTID_CHOOSETASKTYPE_RULE').val(node.id);
				$('#PARENTTITLE_CHOOSETASKTYPE_RULE').val(node.text);
				searchTasktype.dosearch({
					'ruleText' : searchTasktype.arrayStr()
				});
			}
		});
		$('#TaskTypeTreeReload').bind('click', function() {
			$('#TaskTypeTree').tree('reload');
		});
		$('#TaskTypeTreeOpenAll').bind('click', function() {
			$('#TaskTypeTree').tree('expandAll');
		});
	});
</script>
