<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--任务附件表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formTaskfile">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">附件名称:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[256]']" id="entity_fileid"
						name="filename" value="${file.title}"></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityTaskfile" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityTaskfile" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formTaskfile" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionTaskfile = 'taskfile/add.do';
	var submitEditActionTaskfile = 'taskfile/edit.do';
	var currentPageTypeTaskfile = '${pageset.operateType}';
	var submitFormTaskfile;
	$(function() {
		//表单组件对象
		submitFormTaskfile = $('#dom_formTaskfile').SubmitForm({
			pageType : currentPageTypeTaskfile,
			grid : gridTaskfile,
			currentWindowId : 'winTaskfile'
		});
		//关闭窗口
		$('#dom_cancle_formTaskfile').bind('click', function() {
			$('#winTaskfile').window('close');
		});
		//提交新增数据
		$('#dom_add_entityTaskfile').bind('click', function() {
			submitFormTaskfile.postSubmit(submitAddActionTaskfile);
		});
		//提交修改数据
		$('#dom_edit_entityTaskfile').bind('click', function() {
			submitFormTaskfile.postSubmit(submitEditActionTaskfile);
		});
	});
//-->
</script>