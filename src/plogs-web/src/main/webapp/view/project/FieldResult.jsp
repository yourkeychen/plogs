<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>属性数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/view/conf/include.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',split:true,border:false"
		style="width: 250px;">
		<div class="TREE_COMMON_BOX_SPLIT_DIV">
			<a id="fieldTypeTreeReload" href="javascript:void(0)"
				class="easyui-linkbutton" data-options="plain:true"
				iconCls="icon-reload">刷新</a> <a id="fieldTypeTreeOpenAll"
				href="javascript:void(0)" class="easyui-linkbutton"
				data-options="plain:true" iconCls="icon-sitemap">展开</a>
		</div>
		<ul id="fieldTypeTree"></ul>
	</div>
	<div class="easyui-layout" data-options="region:'center',border:false">
		<div data-options="region:'north',border:false">
			<form id="searchFieldForm">
				<table class="editTable">
					<tr>
						<td class="title">属性分类:</td>
						<td><input id="PARENTTITLE_RULE" type="text"
							readonly="readonly" style="background: #F3F3E8"> <input
							id="PARENTID_RULE" name="TYPEID:=" type="hidden"></td>
						<td class="title">唯一编码:</td>
						<td><input name="UUID:like" type="text"></td>
						<td class="title">属性名称:</td>
						<td><input name="NAME:like" type="text"></td>
					</tr>
					<tr style="text-align: center;">
						<td colspan="6"><a id="a_search" href="javascript:void(0)"
							class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
							id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload">清除条件</a></td>
					</tr>
				</table>
			</form>
		</div>
		<div data-options="region:'center',border:false">
			<table id="dataFieldGrid">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th field="NAME" data-options="sortable:true" width="60">属性名称</th>
						<th field="UUID" data-options="sortable:true" width="60">编码</th>
						<th field="SORT" data-options="sortable:true" width="20">排序</th>
						<th field="SINGLETYPE" data-options="sortable:true" width="20">唯一性</th>
						<th field="VALTYPE" data-options="sortable:true" width="25">属性值类型</th>
						<th field="TYPENAME" data-options="sortable:true" width="30">属性分类</th>
						<th field="PSTATE" data-options="sortable:true" width="20">状态</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
	var url_delActionField = "field/del.do";//删除URL
	var url_formActionField = "field/form.do";//增加、修改、查看URL
	var url_searchActionField = "field/query.do";//查询URL
	var title_windowField = "属性管理";//功能名称
	var gridField;//数据表格对象
	var searchField;//条件查询组件对象
	var toolBarField = [
	//{
	//	id : 'view',
	//	text : '查看',
	//	iconCls : 'icon-tip',
	//	handler : viewDataField
	//}, 
	{
		id : 'add',
		text : '新增',
		iconCls : 'icon-add',
		handler : addDataField
	}, {
		id : 'edit',
		text : '修改',
		iconCls : 'icon-edit',
		handler : editDataField
	}, {
		id : 'move',
		text : '设置分类',
		iconCls : 'icon-communication',
		handler : moveTree
	}, {
		id : 'formatSort',
		text : '格式化排序',
		iconCls : 'icon-arrow_refresh',
		handler : formatSort
	}, {
		id : 'loadsys',
		text : '加载系统属性',
		iconCls : 'icon-invoice',
		handler : loadsysfield
	}, {
		id : 'del',
		text : '删除',
		iconCls : 'icon-remove',
		handler : delDataField
	} ];
	$(function() {
		$('#fieldTypeTree').tree({
			url : 'fieldtype/fieldTypeTree.do',
			onSelect : function(node) {
				$('#PARENTID_RULE').val(node.id);
				$('#PARENTTITLE_RULE').val(node.text);
				searchField.dosearch({
					'ruleText' : searchField.arrayStr()
				});
			}
		});
		$('#fieldTypeTreeReload').bind('click', function() {
			$('#fieldTypeTree').tree('reload');
		});
		$('#fieldTypeTreeOpenAll').bind('click', function() {
			$('#fieldTypeTree').tree('expandAll');
		});
		//初始化数据表格
		gridField = $('#dataFieldGrid').datagrid({
			url : url_searchActionField,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarField,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true,
			onLoadSuccess : function() {
				$('.datagrid-btable .datagrid-cell').each(function(i, obj) {
					$(obj).attr('title', $(obj).text());
				});
			}
		});

		//初始化条件查询
		searchField = $('#searchFieldForm').searchForm({
			gridObj : gridField
		});
	});
	//查看
	function viewDataField() {
		var selectedArray = $(gridField).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionField + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winField',
				width : 600,
				height : 350,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增
	function addDataField() {
		var parentID = $("#PARENTID_RULE").val();
		if (!parentID || parentID == 'NONE') {
			if (parentID == 'NONE') {
				$.messager.alert(MESSAGE_PLAT.PROMPT,
						"请先选择左侧分类!（属性分类在'属性分类配置'菜单下维护）", 'info');
			} else {
				$.messager.alert(MESSAGE_PLAT.PROMPT,
						"请先选择左侧分类!", 'info');
			}
			return;
		}
		var url = url_formActionField + '?operateType=' + PAGETYPE.ADD
				+ '&typeid=' + parentID;
		$.farm.openWindow({
			id : 'winField',
			width : 600,
			height : 350,
			modal : true,
			url : url,
			title : '新增'
		});
	}
	//修改
	function editDataField() {
		var selectedArray = $(gridField).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionField + '?operateType=' + PAGETYPE.EDIT
					+ '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winField',
				width : 600,
				height : 350,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataField() {
		var selectedArray = $(gridField).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridField).datagrid('loading');
					$.post(url_delActionField + '?ids='
							+ $.farm.getCheckedIds(gridField, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridField).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridField).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//加载系统属性
	function loadsysfield() {
		var parentID = $("#PARENTID_RULE").val();
		if (!parentID || parentID == 'NONE') {
			$.messager.alert(MESSAGE_PLAT.PROMPT, "请先选择左侧分类!", 'info');
			return;
		}
		// 有数据执行操作
		$.messager.confirm(MESSAGE_PLAT.PROMPT, "此操作不会重复创建已有属性,是否继续加载系统属性?",
				function(flag) {
					if (flag) {
						$(gridField).datagrid('loading');
						$.post('field/loadSysField.do?typeid=' + parentID, {},
								function(flag) {
									var jsonObject = JSON.parse(flag, null);
									$(gridField).datagrid('loaded');
									if (jsonObject.STATE == 0) {
										$(gridField).datagrid('reload');
									} else {
										var str = MESSAGE_PLAT.ERROR_SUBMIT
												+ jsonObject.MESSAGE;
										$.messager.alert(MESSAGE_PLAT.ERROR,
												str, 'error');
									}
								});
					}
				});
	}

	//移动节点
	function moveTree() {
		var selectedArray = $(gridField).datagrid('getSelections');
		if (selectedArray.length > 0) {
			$.farm.openWindow({
				id : 'fieldTypeTreeNodeWin',
				width : 250,
				height : 300,
				modal : true,
				url : "fieldtype/treeNodeTreeView.do",
				title : '移动分类'
			});
			chooseWindowCallBackHandle = function(node) {
				$.messager.confirm('确认对话框', '确定移动该节点么？', function(falg1) {
					if (falg1) {
						$.post('field/moveTypes.do', {
							fieldIds : $.farm.getCheckedIds(gridField, 'ID'),
							typeid : node.id
						}, function(flag) {
							if (flag.STATE == 0) {
								$(gridField).datagrid('reload');
								$('#fieldTypeTreeNodeWin').window('close');
								$.messager.confirm('确认对话框', '数据更新,是否重新加载左侧树？',
										function(r) {
											if (r) {
												$('#fieldTypeTree').tree(
														'reload');
											}
										});
							} else {
								var str = MESSAGE_PLAT.ERROR_SUBMIT
										+ flag.MESSAGE;
								$.messager.alert(MESSAGE_PLAT.ERROR, str,
										'error');
							}
						}, 'json');
					}
				});
			};
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//格式化排序
	function formatSort() {
		var selectedArray = $(gridField).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			$.messager.confirm(MESSAGE_PLAT.PROMPT, "确认格式化排序?", function(flag) {
				if (flag) {
					$(gridField).datagrid('loading');
					$.post("field/formatSort.do" + '?ids='
							+ $.farm.getCheckedIds(gridField, 'ID'), {},
							function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridField).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridField).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>
</html>