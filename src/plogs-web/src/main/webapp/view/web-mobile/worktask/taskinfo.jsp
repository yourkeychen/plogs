<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; overflow: hidden;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li><a href="home/Pubindex.html">首页</a></li>
					<li><a href="worktask/index.html">任务类型</a></li>
					<li><a href="mtask/tasks.html?state=${task.pstate}"> <c:if
								test="${task.pstate=='1'}">
								计划任务
							</c:if> <c:if test="${task.pstate=='2'}">
								准备任务
							</c:if> <c:if test="${task.pstate=='3'}">
								执行任务
							</c:if> <c:if test="${task.pstate=='4'}">
								完成任务
							</c:if>
					</a></li>
					<li><a class="active">任务信息</a></li>
				</ol>
				<div style="padding: 8px;">
					<div style="float: left;">
						<!-- 1.计划，2开始，3处理，4结束,5关闭 -->
						<c:if test="${task.pstate=='1'}">
							<img src="text/img/plogs/taskPlan.png"
								style="height: 64px; width: 64px;">
						</c:if>
						<c:if test="${task.pstate=='2'}">
							<img src="text/img/plogs/taskStar.png"
								style="height: 64px; width: 64px;">
						</c:if>
						<c:if test="${task.pstate=='3'}">
							<img src="text/img/plogs/taskJustDo.png"
								style="height: 64px; width: 64px;">
						</c:if>
						<c:if test="${task.pstate=='4'}">
							<img src="text/img/plogs/taskComplete.png"
								style="height: 64px; width: 64px;">
						</c:if>
					</div>
					<div
						style="padding-left: 60px; padding-top: 8px; margin-left: 20px;">
						<div style="font-size: 18px; font-weight: 700;">${task.title}&nbsp;&nbsp;<code>${task.tasktypename}</code>
						</div>
						<div style="font-size: 14px; font-weight: 200; margin-top: 6px;">
							<a target="_blank"
								href="projectView/projectInfo.do?projectId=${project.id}">
								${project.name}</a>&nbsp;&nbsp;
							<code>${task.gradationname}</code>
						</div>
					</div>
					<div style="margin-top: 20px;">
						<table class="table table-striped table-bordered">

							<tbody>
								<tr>
									<th>完成度</th>
									<td><c:if test="${!empty task.completion}">
											<div class="progress" style="margin: 0px;">
												<div
													class="progress-bar progress-bar-success progress-bar-striped"
													role="progressbar" aria-valuenow="60" aria-valuemin="0"
													aria-valuemax="100" style="width: ${task.completion}%;">${task.completion}%</div>
											</div>
										</c:if></td>
								</tr>
								<tr>
									<th>管理人</th>
									<td><c:forEach items="${managerUsers }" var="node">
											<code> ${node.USERNAME } </code>
										</c:forEach></td>
								</tr>
								<tr>
									<th>执行人</th>
									<td><c:forEach items="${doUsers }" var="node">
											<code> ${node.USERNAME } </code>
										</c:forEach></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="btn-group btn-group-justified " role="group"
						style="margin-top: 20px;" aria-label="Justified button group">
						<c:if test="${isManagerUser }">
							<c:if test="${task.pstate!='4'}">
								<!--  -->
								<a onclick="updateTaskState('${task.id}','0')"
									class="btn btn-primary" role="button">作废任务</a>
							</c:if>
							<!--  -->
							<a href="mtask/taskform.do?taskid=${task.id}"
								class="btn btn-primary" role="button">修改任务</a>
						</c:if>
						<c:if test="${task.pstate!='4'}">
							<c:if test="${isDoUser }">
								<!--  -->
								<a href="mtask/logform.do?taskid=${task.id}"
									class="btn btn-primary" role="button">记录日志</a>
								<!--  -->
								<a onclick="updateTaskState('${task.id}','4')"
									class="btn btn-primary" role="button">完成任务</a>
							</c:if>
						</c:if>
					</div>
					<c:if test="${!empty text}">
						<div class="panel panel-default" style="margin-top: 20px;">
							<div class="panel-heading">
								<b>任务说明</b>
							</div>
							<div class="panel-body">
								<div class="ke-content" style="overflow: auto;">${text}</div>
							</div>
						</div>
					</c:if>
					<c:if test="${!empty logs}">
						<c:forEach var="log" items="${logs}">
							<div class="panel panel-default" style="margin-top: 20px;">
								<div class="panel-heading">
									<b>${log.datekey}-日志</b>
								</div>
								<div class="panel-body">
									<div class="ke-content" style="overflow: auto;">${log.text}</div>
								</div>
							</div>
						</c:forEach>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	function updateTaskState(taskId, state) {
		if (confirm('确定变更任务状态?')) {
			$
					.post(
							'worktask/updateTaskState.do',
							{
								'taskId' : taskId,
								'state' : state
							},
							function(flag) {
								if (flag.STATE == '0') {
									window.location = "<PF:basePath/>mtask/tasks.do?state=${task.pstate}";
								} else {
									alert(flag.MESSAGE);
								}
							}, 'json');
		}
	}
</script>
</html>