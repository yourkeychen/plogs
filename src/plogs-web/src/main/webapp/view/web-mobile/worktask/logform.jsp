<%@page import="com.farm.core.time.TimeTool"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script src="text/lib/layout/jquery.layout-latest.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh-CN.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-kindeditor.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-file-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/echarts/echarts.all.2.2.7.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-multiimage-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/htmltags.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/alert/sweetalert.min.js"></script>
<!-- 附件批量上传组件 -->
<script src="<PF:basePath/>text/lib/fileUpload/jquery.ui.widget.js"></script>
<script
	src="<PF:basePath/>text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.css"
	rel="stylesheet">
<style type="text/css">
.ke-toolbar .ke-outline {
	padding: 10px;
}

.ke-toolbar .ke-separator {
	height: 48px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; overflow: hidden;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li><a href="home/Pubindex.html">首页</a></li>
					<li><a href="worktask/index.html">任务类型</a></li>
					<c:if test="${!empty task}">
						<li><a href="mtask/taskinfo.do?taskid=${task.id}">任务信息</a></li>
					</c:if>
					<li><a class="active"><%=TimeTool.getFormatTimeDate12(TimeTool.getTimeDate14(), "MM月dd日")%>任务日志</a></li>
				</ol>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="modal-title">
							<c:if test="${empty task}">添加任务</c:if>
							<c:if test="${!empty task}">更新任务</c:if>
						</h4>
					</div>
					<div class="panel-body">
						<form class="form-horizontal" id="addTaskFormId">
							<input type="hidden" id="taskformTaskId" value="${task.id}">
							<div class="form-group">
								<div class="col-sm-12">
									<textarea name="text" id="taskReadText"
										style="height: 250px; width: 100%;">${fn:replace(fn:replace(text,'&lt;', '&amp;lt;'),'&gt;', '&amp;gt;')}</textarea>
									<jsp:include
										page="/view/web-simple/worktask/kindeditors/includeEditorLoader.jsp">
										<jsp:param name="textareaId" value="taskReadText" />
										<jsp:param name="tooltype" value="mobile" />
									</jsp:include>
								</div>
							</div>
						</form>
						<div>
							<span class="alertMsgClass" id="errormessageShowboxId"></span>
							<button type="button" onclick="submitTaskForm()"
								class="btn btn-primary">更新日志</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script>
	$(function() {
		//項目选定事件
		$('#taskProject').change(function() {
			loadGradations($(this).val());
		});
		//階段选定事件
		$('#taskGradation').change(function() {
			loadTaskTypes($(this).val());
		});
	});
	//提交表单
	function submitTaskForm() {
		if (confirm("是否提交日志?")) {
			var taskid = $('#taskformTaskId').val();
			eval('taskReadTexteditor').sync();
			var taskReadText = $('#taskReadText').val();
			$.post('worktask/ctlogSubmit.do', {
				'text' : taskReadText,
				'taskid' : taskid
			}, function(flag) {
				if (flag.STATE == '0') {
					alert(flag.task.title + ":日志保存成功!");
					window.location = "<PF:basePath/>mtask/taskinfo.do?taskid="+flag.task.id;
				} else {
					alert(flag.MESSAGE);
				}
			}, 'json');
		}
	}
</script>
</html>