<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; overflow: hidden;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li><a href="home/Pubindex.html">首页</a></li>
					<li><a class="active">任务类型</a></li>
				</ol>
				<div class="panel panel-default">
					<div class="panel-body">
						<a href="mtask/tasks.do?state=1"
							class="btn btn-info btn-lg btn-block">等待任务(${num1})</a><a
							href="mtask/tasks.do?state=2"
							class="btn btn-warning btn-lg btn-block">开始任务(${num2})</a>
						<hr style="margin: 20px;" />
						<a href="mtask/tasks.do?state=3"
							class="btn btn-danger btn-lg btn-block">执行任务(${num3})</a>
						<hr style="margin: 20px;" />
						<a href="mtask/tasks.do?state=4"
							class="btn btn-success btn-lg btn-block">完成任务(${num4})</a>
						<hr style="margin: 20px;" />
						<a href="mtask/taskform.do"
							class="btn btn-default btn-lg btn-block">添加任务</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>