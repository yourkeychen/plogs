<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; overflow: hidden;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li><a href="home/Pubindex.html">首页</a></li>
					<li><a href="worktask/index.html">任务类型</a></li>
					<li><a class="active"> <c:if test="${state=='1'}">
								计划任务
							</c:if> <c:if test="${state=='2'}">
								准备任务
							</c:if> <c:if test="${state=='3'}">
								执行任务
							</c:if> <c:if test="${state=='4'}">
								完成任务
							</c:if>
					</a></li>
				</ol>
				<c:forEach items="${tasks}" var="node">
					<div>
						<div class="panel panel-default">
							<div class="panel-heading" style="font-size: 16px;">
								<code>${node.TASKTYPENAME }</code>
								&nbsp;&nbsp;<b>${node.TITLE}</b>
							</div>
							<div class="panel-body">${node.COMPANYNAME}:${node.PROJECTNAME }<!--  <code>${node.GRADATIONNAME }</code>-->
								<a href="mtask/taskinfo.do?taskid=${node.ID}"
									style="float: right;" class="btn btn-primary btn-sm ">查看</a>
							</div>
						</div>
					</div>
				</c:forEach>
				<c:if test="${empty tasks}">
					<p
						style="background-color: #fcf8e3; padding: 20px; text-align: center; margin: 20px; border: 1px dashed #a94442;">暂无此类任务</p>
				</c:if>
				<div style="text-align: center; padding: 20px;">
					<a href="worktask/index.do" class="btn btn-primary"
						style="width: 100%;">&nbsp;&nbsp;&nbsp;返回&nbsp;&nbsp;&nbsp;</a>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>