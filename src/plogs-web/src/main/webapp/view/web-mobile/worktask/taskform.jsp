<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script src="text/lib/layout/jquery.layout-latest.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh-CN.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-kindeditor.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-file-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/echarts/echarts.all.2.2.7.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-multiimage-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/htmltags.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/alert/sweetalert.min.js"></script>
<!-- 附件批量上传组件 -->
<script src="<PF:basePath/>text/lib/fileUpload/jquery.ui.widget.js"></script>
<script
	src="<PF:basePath/>text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.css"
	rel="stylesheet">
<style type="text/css">
.ke-toolbar .ke-outline {
	padding: 10px;
}

.ke-toolbar .ke-separator {
	height: 48px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; overflow: hidden;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li><a href="home/Pubindex.html">首页</a></li>
					<li><a href="worktask/index.html">任务类型</a></li>
					<c:if test="${!empty task}">
						<li><a href="mtask/taskinfo.do?taskid=${task.id}">任务信息</a></li>
					</c:if>
					<li><a class="active">任务表单</a></li>
				</ol>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="modal-title">
							<c:if test="${empty task}">添加任务</c:if>
							<c:if test="${!empty task}">更新任务</c:if>
						</h4>
					</div>
					<div class="panel-body">
						<form class="form-horizontal" id="addTaskFormId">
							<input type="hidden" id="taskformTaskId" value="${task.id}">
							<div class="form-group" id="taskProjectGroupId">
								<label for="taskProject" class="col-sm-2 control-label">项目<span
									class="alertMsgClass">*</span></label>
								<div class="col-sm-10">
									<select id="taskProject" class="form-control"
										val=${task.projectid}>
										<option value="">~选择项目~</option>
										<c:forEach var="node" items="${projects}">
											<option value="${node.id}">${node.companyname}:${node.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group" id="taskGradationGroupID">
								<label for="taskGradation" class="col-sm-2 control-label">阶段<span
									class="alertMsgClass">*</span></label>
								<div class="col-sm-10">
									<select id="taskGradation" class="form-control"
										val='${task.gradationid}'>
										<option value="">~先选择项目~</option>
										<c:forEach var="node" items="${gradations}">
											<option value="${node.id}">${node.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group" id="taskTypeGroupId">
								<label for="taskType" class="col-sm-2 control-label">类型<span
									class="alertMsgClass">*</span></label>
								<div class="col-sm-10">
									<select id="taskType" class="form-control"
										val='${task.tasktypeid}'>
										<option value="">~先选择阶段~</option>
										<c:forEach var="node" items="${taskTypes}">
											<option value="${node.id}">${node.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group" id="titleGroupId">
								<label for="taskTitle" class="col-sm-2 control-label">标题<span
									class="alertMsgClass">*</span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="taskTitle"
										value="${task.title}" placeholder="任务标题">
								</div>
							</div>
							<div class="form-group">
								<label for="taskState" class="col-sm-2 control-label">状态<span
									class="alertMsgClass">*</span></label>
								<div class="col-sm-10">
									<select id="taskState" class="form-control"
										val="${task.pstate}">
										<option value="1">等待</option>
										<option value="2">开始</option>
										<option value="3">执行</option>
										<option value="4">完成</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="taskState" class="col-sm-2 control-label">任务描述</label>
								<div class="col-sm-10">
									<textarea name="text" id="taskReadText"
										style="height: 200px; width: 100%;">${fn:replace(fn:replace(text,'&lt;', '&amp;lt;'),'&gt;', '&amp;gt;')}</textarea>
									<jsp:include
										page="/view/web-simple/worktask/kindeditors/includeEditorLoader.jsp">
										<jsp:param name="textareaId" value="taskReadText" />
										<jsp:param name="tooltype" value="mobile" />
									</jsp:include>
								</div>
							</div>
						</form>
						<div>
							<span class="alertMsgClass" id="errormessageShowboxId"></span>
							<button type="button" onclick="submitTaskForm()"
								class="btn btn-primary">
								<c:if test="${empty task}">添加任务</c:if>
								<c:if test="${!empty task}">更新任务</c:if>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script>
	$(function() {
		//項目选定事件
		$('#taskProject').change(function() {
			loadGradations($(this).val());
		});
		//階段选定事件
		$('#taskGradation').change(function() {
			loadTaskTypes($(this).val());
		});
		$('select', '#addTaskFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
		validateInput('taskTitle', function(id, val, obj) {
			// 标题
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 256)) {
				return {
					valid : false,
					msg : '长度不能大于' + 256
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, null, 'titleGroupId');
		validateInput('taskProject', function(id, val, obj) {
			// 项目
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, null, 'taskProjectGroupId');
		validateInput('taskGradation', function(id, val, obj) {
			// 阶段
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, null, 'taskGradationGroupID');
		validateInput('taskType', function(id, val, obj) {
			// 类型
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, null, 'taskTypeGroupId');
	});
	//提交表单
	function submitTaskForm() {
		if (!validate('addTaskFormId')) {
			$('#errormessageShowboxId').text('信息录入有误，请检查！');
		} else {
			$('#errormessageShowboxId').text('');
			if (confirm("是否提交数据?")) {
				var title = $('#taskTitle').val();
				var projectid = $('#taskProject').val();
				var gradationid = $('#taskGradation').val();
				var tasktypeid = $('#taskType').val();
				var state = $('#taskState').val();
				var taskid = $('#taskformTaskId').val();
				eval('taskReadTexteditor').sync();
				var taskReadText = $('#taskReadText').val();
				$.post('worktask/taskSubmit.do', {
					'title' : title,
					'projectid' : projectid,
					'gradationid' : gradationid,
					'tasktypeid' : tasktypeid,
					'state' : state,
					'text' : taskReadText,
					'taskid' : taskid
				}, function(flag) {
					if (flag.STATE == '0') {
						alert(flag.task.title + ":任务创建成功!");
						window.location = "<PF:basePath/>worktask/index.do";

					} else {
						alert(flag.MESSAGE);
					}
				}, 'json');
			}
		}
	}
	//加载远程阶段类型
	function loadGradations(projectId) {
		$.post('worktask/loadGradations.do', {
			'projectId' : projectId
		}, function(data) {
			if (data.STATE == '0') {
				$('#taskGradation').html('<option value="">~选择项目~</option>');
				$('#taskType').html('<option value="">~选择阶段~</option>');
				$(data.gradations).each(
						function(i, obj) {
							$('#taskGradation').append(
									'<option value="'+obj.id+'">' + obj.name
											+ '</option>');
						});
				$('#taskGradation').val(data.gradationid);
				loadTaskTypes(data.gradationid);
			} else {
				pAlert(data.MESSAGE);
			}

		}, 'json');
	}

	//加载任务类型
	function loadTaskTypes(gradationId) {
		//加载阶段
		$.post('worktask/loadTaskType.do', {
			'gradationId' : gradationId
		}, function(data) {
			if (data.STATE == '0') {
				$('#taskType').html('<option value="">~选择任务类型~</option>');
				$(data.taskTypes).each(
						function(i, obj) {
							$('#taskType').append(
									'<option value="'+obj.id+'">' + obj.name
											+ '</option>');
						});
			} else {
				pAlert(data.MESSAGE);
			}

		}, 'json');
	}
</script>
</html>