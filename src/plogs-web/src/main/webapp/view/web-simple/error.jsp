<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<title>受控异常-<PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="noindex,nofllow">
<jsp:include page="atext/include-web.jsp"></jsp:include>
<%
	System.out.println("errorUrl:" + request.getAttribute("URL"));
%>
</head>
<body>
	<jsp:include page="commons/head.jsp"></jsp:include>
	<jsp:include page="commons/superContent.jsp"></jsp:include>

	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="logoLeftBoxId"
					style="border-right: 1px dashed #cccccc;">
					<div
						style="color: #999999; text-align: center; margin: auto; margin-top: 100px;">
						<div>
							<img style="max-width: 128px; max-height: 128px;"
								src="view/web-simple/atext/png/icon" alt="..."
								class="img-rounded">
						</div>
						<div>
							<h1>
								<PF:ParameterValue key="config.sys.title" />
							</h1>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="alert alert-danger" role="alert" style="margin-top: 120px;">
						<strong>错误信息:</strong> ${MESSAGE}
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="commons/footServer.jsp"></jsp:include>
	<jsp:include page="commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {
		$(window).resize(function() {
			$('#logoLeftBoxId').css('min-height', $(window).height() - 270);
		});
		$('#logoLeftBoxId').css('min-height', $(window).height() - 270);
	});
</script>
</html>