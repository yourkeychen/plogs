<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="logoLeftBoxId">
					<jsp:include page="../commons/HomeLogo.jsp"></jsp:include>
				</div>
				<div class="col-md-8" style="border-left: 1px dashed #cccccc;">
					<div class="row" style="margin-top: 50px;">
						<div class="col-lg-2"></div>
						<div class="col-lg-8">
							<div class="input-group ">
								<input type="text" class="form-control" id="gotoFileId"
									placeholder="请输入文件id或appId..."> <span
									class="input-group-btn">
									<button class="btn btn-default" id="gotoFileButtonId"
										type="button">
										<i class=" glyphicon glyphicon-search "></i>&nbsp;查询文件
									</button> <c:if test="${USEROBJ.type=='3'}">
										<button class="btn btn-warning" id="uploadFileButtonId"
											onclick="OpenUploadWin();" type="button">
											<i class=" glyphicon glyphicon-cloud-upload "></i>&nbsp;上传文件
										</button>
									</c:if>
								</span>
							</div>
						</div>
						<div class="col-lg-2"></div>
					</div>
					<c:if test="${USEROBJ.type=='3'}">
						<!-- /.row -->
						<table class="table table-bordered"
							style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
							<tbody>
								<tr>
									<td style="min-width: 100px;">当前版本</td>
									<td>v<PF:ParameterValue key="config.sys.version" /></td>
								</tr>
								<tr>
									<td>文件上传限制</td>
									<td>
										<%
											DecimalFormat fnum = new DecimalFormat("##0.00 ");
												long size1 = FarmParameterService.getInstance().getParameterLong("config.doc.upload.length.max");
												float size1plus = ((float) size1) / (1025 * 1024);
										%> <%=fnum.format(size1plus)%>m
								</tr>
								<tr>
									<td>图片上传限制</td>
									<td>
										<%
											long size2 = FarmParameterService.getInstance().getParameterLong("config.doc.img.upload.length.max");
												float size2plus = ((float) size2) / (1025 * 1024);
										%> <%=fnum.format(size2plus)%>m
									</td>
								</tr>
								<tr>
									<td>文件类型限制</td>
									<td style="word-break: break-all;"><PF:ParameterValue
											key="config.doc.upload.types" /></td>
								</tr>

								<tr>
									<td>图片类型限制</td>
									<td style="word-break: break-all;"><PF:ParameterValue
											key="config.doc.img.upload.types" /></td>
								</tr>
								<tr>
									<td>多媒体类型限制</td>
									<td style="word-break: break-all;"><PF:ParameterValue
											key="config.doc.media.upload.types" /></td>
								</tr>
								<tr>
									<td>转换任务调度器是否启动</td>
									<td style="word-break: break-all;">${dispatcherlive}<a
										href="<PF:basePath/>docview/dispatcherStart.do">启动</a></td>
								</tr>
								<tr>
									<td>OpenOffice状态</td>
									<td style="word-break: break-all;"><a
										href="<PF:basePath/>openoffice/state.do">查看</a></td>
								</tr>
							</tbody>
						</table>
					</c:if>
					<jsp:include page="../commons/includeRegulars.jsp"></jsp:include>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {
		$('#gotoFileButtonId').click(
				function() {
					if (!isnull($('#gotoFileId').val())) {
						window.location = "document/view.do?fileid="
								+ $('#gotoFileId').val();
					}
				});
	});
	//表单是否为空
	function isnull(val) {
		var str = val.replace(/(^\s*)|(\s*$)/g, '');//去除空格;
		if (str == '' || str == undefined || str == null) {
			return true;
		} else {
			return false;
		}
	}
</script>
</html>