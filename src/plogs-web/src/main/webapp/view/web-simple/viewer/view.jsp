<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>文件预览- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="logoLeftBoxId">
					<ol class="breadcrumb">
						<li><a>wdap</a></li>
						<li><a>${fileview.file.title}</a></li>
						<li class="active">预览</li>
					</ol>
					<jsp:include page="../document/commons/includeFileTitleBox.jsp"></jsp:include>
				</div>
				<div class="col-md-8" style="border-left: 1px dashed #cccccc;">
					<jsp:include page="../operat/includeFileOperatBox.jsp"></jsp:include>
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered  table-striped"
								style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
								<thead>
									<tr>
										<th>模型</th>
										<th>入队时间</th>
										<th>状态</th>
										<th><PF:UserIsAdmin userid="${USEROBJ.id }">
												<a href="docview/retask.do?fileid=${fileview.file.id}"
													class="btn btn-danger btn-xs" role="button"
													title="重新生成预览任务，删除原有预览文件">重新生成</a>
											</PF:UserIsAdmin></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${viewtasks}" var="node">
										<tr>
											<td>${node.DTITLE}</td>
											<td>${node.CTIME}</td>
											<td>${node.STATETITLE}</td>
											<td style="width: 80px;"><a
												href="docview/task.do?taskid=${node.ID}"
												class="btn btn-default btn-xs" role="button">查看信息</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<c:forEach items="${viewdocs}" var="node">
							<div class="col-sm-3">
								<div class="thumbnail">
									<div
										style="height: 64px; margin-top: 20px; text-align: center;">
										<img
											style="max-height: 64px; max-width: 128px; display: block; margin: auto;"
											src="${node.ICONURL}">
									</div>
									<div class="caption">
										<div style="text-align: center;">
											<span>&nbsp;${node.MODELTITLE}</span>
										</div>
										<div class="btn-group btn-group-justified  btn-group-xs"
											role="group" style="margin-top: 4px;"
											aria-label="Justified button group">
											<c:if test="${node.VIEWIS=='1'&&node.VIEWURL!=null}">
												<a href="${node.VIEWURL}" class="btn btn-default"
													role="button">预览</a>
											</c:if>
											<c:if test="${node.DOWNURL!=null}">
												<a href="${node.DOWNURL}" class="btn btn-default"
													role="button">下载</a>
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered  table-striped"
								style="background-color: #ffffff; font-size: 12px; color: #999999;">
								<tr>
									<td style="min-width: 70px;">文本信息</td>
									<td>${filetext.completeis=='1'?'有':'无'}</td>
								</tr>
								<tr>
									<td>信息来源</td>
									<td>${filetext.pcontent}</td>
								</tr>
								<tr>
									<td>内容</td>
									<td><div style="max-height: 200px; overflow: auto;">${filetext.filetext}</div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>