<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<meta http-equiv="imagetoolbar" content="false">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<meta name="viewport" content="width=device-width, initial-scale=1.0">  
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<!-- 禁止鼠标右键  oncontextmenu="return false"-->
<body oncontextmenu="return false">
	<div class="navbar navbar-inverse navbar-fixed-top ">
		<div class="container-fluid">
			<div>
				<div class="navbar-brand"
					style="color: #ffffff; font-weight: bold; padding: 5px;"
					href="<PF:defaultIndexPage/>">
					<img src="<PF:basePath/>view/web-simple/atext/png/icon" height="40"
						alt="WCP" align="middle" />
				</div>
			</div>
			<div class="text-right" style="margin-right: 40px;">
				<div class="btn-group " role="group" aria-label="..."
					style="margin-top: 8px;">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default btn-sm dropdown-toggle"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							第 <span id="currentNumSpan">1</span> 页 <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" id="allPageChooseUl"
							style="min-width: 110px; text-align: center; max-height: 200px; overflow: auto;">
							<c:forEach begin="1" end="${filesize}" var="page">
								<li><a
									href="javascript:scrollToMarkName('pageName${page}','${page}')">第${page}页</a></li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<div class="containerbox "
		style="background-color: #3e3e3e; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container"
			style="margin-top: 50px; margin-bottom: 10px; max-width: 960px;">
			<div class="row">
				<div class="col-md-12 text-center imgContainer" id="imgsBoxDiv">
					<c:forEach begin="1" end="${filesize}" var="page">
						<c:if test="${page<5}">
							<img id="Page1" alt="${page}" class="img-responsive"
								src="download/PubDirFile.do?viewdocid=${viewdocId}&num=${page-1}"
								data-src="download/PubDirFile.do?viewdocid=${viewdocId}&num=${page-1}"
								name="pageName${page}"
								style="margin: auto; min-height: 64px; min-width: 64px;" src="">
							<div
								style="text-align: center; color: #cccccc; padding-bottom: 16px; font-size: 14px;">第${page}页/共${filesize}页
							</div>
						</c:if>
						<c:if test="${page>=5}">
							<img id="Page1" alt="${page}" class="img-responsive"
								data-src="download/PubDirFile.do?viewdocid=${viewdocId}&num=${page-1}"
								name="pageName${page}"
								style="margin: auto; min-height: 64px; min-width: 64px;"
								src="text/img/lasyImg.png">
							<div
								style="text-align: center; color: #cccccc; padding-bottom: 16px; font-size: 14px;">第${page}页/共${filesize}页
							</div>
						</c:if>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	//滾動到指定位置
	function scrollToMarkName(markName, num) {
		$('#currentNumSpan').text(num);
		$('html,body').animate({
			scrollTop : $("img[name='" + markName + "']").offset().top - 50
		}, 0, function() {
			$('html,body').animate({
				scrollTop : $("img[name='" + markName + "']").offset().top - 70
			}, 200);
		});
	}
	$(function() {
		$(window).on('scroll', function() {//当页面滚动的时候绑定事件
			$('.imgContainer img').each(function() {//遍历所有的img标签
				if (checkShow($(this))) {
					$('#currentNumSpan').text($(this).attr("alt"));
					if (!isLoaded($(this))) {
						loadImg($(this));
					}
				}
			})
		})//禁止图片拖动下载
		for (i in document.images) {
			document.images[i].ondragstart = imgdragstart;
		}
	});
	//图片是否可见
	function checkShow($img) { // 传入一个img的jq对象
		var scrollTop = $(window).scrollTop(); //即页面向上滚动的距离
		var windowHeight = $(window).height(); // 浏览器自身的高度
		var offsetTop = $img.offset().top; //目标标签img相对于document顶部的位置
		if (offsetTop < (scrollTop + windowHeight) && offsetTop > scrollTop) { //在2个临界状态之间的就为出现在视野中的
			return true;
		}
		return false;
	}
	//是否已经加载
	function isLoaded(imgObj) {
		return imgObj.attr('data-src') === imgObj.attr('src'); //如果data-src和src相等那么就是已经加载过了
	}
	//加载图片
	function loadImg(imgObj) {
		imgObj.attr('src', imgObj.attr('data-src')); // 加载就是把自定义属性中存放的真实的src地址赋给src属性
	}
	//不允許拖拽
	function imgdragstart() {
		return false;
	};
</script>
</html>