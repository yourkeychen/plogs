<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>音频预览- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<!-- 禁止鼠标右键  oncontextmenu="return false"-->
<body oncontextmenu="return false">
	<div class="navbar navbar-inverse navbar-fixed-top ">
		<div class="container-fluid">
			<div class="navbar-header hidden-xs hidden-sm">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					style="color: #ffffff; font-weight: bold; padding: 5px;"
					href="<PF:defaultIndexPage/>"> <img
					src="<PF:basePath/>view/web-simple/atext/png/icon" height="40"
					alt="WCP" align="middle" />
				</a>
			</div>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<div class="containerbox "
		style="background-color: #494A5F; border-top: 1px solid #eeeeee; padding-bottom: 120px; padding-top: 120px;">
		<div class="container" style="margin-top: 50px; margin-bottom: 10px;">
			<div class="row">
				<div class="col-md-12 text-center">
					<div
						style="background: #333333; border-radius: 8px; padding: 20px; padding-bottom: 16px;">
						<audio src="downview/Pubload.do?id=${viewdocId}"
							style="width: 70%;" controls preload></audio>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	
</script>
</html>