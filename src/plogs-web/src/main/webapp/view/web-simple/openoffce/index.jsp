<%@page import="com.farm.core.config.CmdConfig"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Openofce状态- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>

	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="logoLeftBoxId">
					<jsp:include page="../commons/HomeLogo.jsp"></jsp:include>
				</div>
				<div class="col-md-8" style="border-left: 1px dashed #cccccc;">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-8">
							<div style="text-align: center; margin-top: 50px;">
								<img class="img-thumbnail"
									src="<PF:basePath/>/view/web-simple/openoffce/text/openOfficeLogo.png">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-8">
							<table class="table table-bordered"
								style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
								<tbody>
									<tr>
										<td style="min-width: 100px;">服务地址</td>
										<td><PF:ParameterValue key="config.openoffice.host" /></td>
									</tr>
									<tr>
										<td>服務端口</td>
										<td><PF:ParameterValue key="config.openoffice.port" /></td>
									</tr>
									<tr>
										<td>服务器环境</td>
										<td><PF:IfParameterEquals val="true"
												key="config.server.os.islinux">linux</PF:IfParameterEquals>
											<PF:IfParameterNoEquals key="config.server.os.islinux"
												val="true">windows</PF:IfParameterNoEquals></td>
									</tr>
									<PF:IfParameterEquals key="config.server.os.islinux" val="true">
										<tr>
											<td>启动脚本</td>
											<td><pre
													style="color: #ffffff; background-color: #000000;"><%=CmdConfig.getCmd("config.cmd.openoffice.linux.start")%></pre></td>
										</tr>
										<tr>
											<td>关闭脚本</td>
											<td><pre
													style="color: #ffffff; background-color: #000000;"><%=CmdConfig.getCmd("config.cmd.openoffice.linux.kill")%></pre></td>
										</tr>
									</PF:IfParameterEquals>
									<PF:IfParameterEquals key="config.server.os.islinux"
										val="false">
										<tr>
											<td>启动脚本</td>
											<td><pre
													style="color: #ffffff; background-color: #000000;"><%=CmdConfig.getCmd("config.cmd.openoffice.win.start")%></pre></td>
										</tr>
										<tr>
											<td>关闭脚本</td>
											<td><pre
													style="color: #ffffff; background-color: #000000;"><%=CmdConfig.getCmd("config.cmd.openoffice.win.kill")%></pre></td>
										</tr>
									</PF:IfParameterEquals>
									<tr>
										<td>服务状态</td>
										<td>${oostate}</td>
									</tr>
									<tr>
										<td>操作</td>
										<td><a href="<PF:basePath/>openoffice/shutdown.do">停止</a>&nbsp;&nbsp;
											<a href="<PF:basePath/>openoffice/restart.do">重启</a></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-2"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>