<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<input type="text" class="form-control field_input_day"
	id="fieldValInputId" placeholder="${fieldins.name}"
	value="${fieldins.valtitle}">
<script>
	$(function() {
		$(".field_input_day").datetimepicker({
			format : "yyyy-mm-dd",
			showMeridian : true,
			autoclose : true,
			todayBtn : true,
			minView : 2,
			startView : 3
		});
		$('select', '#editTaskFieldFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
		validateInput('fieldValInputId', function(id, val, obj) {
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, null, 'taskFieldGroupId');
	});
	//提交表单
	function submitInputForm(fieldinsId, func) {
		pConfirm("是否提交数据?", function() {
			$.post('worktask/taskFieldSubmit.do', {
				'value' : $('#fieldValInputId').val(),
				'fieldinsId' : fieldinsId
			}, function(flag) {
				if (flag.STATE == '0') {
					func();
				} else {
					pAlert(flag.MESSAGE);
				}
			}, 'json');
		});
	}
</script>