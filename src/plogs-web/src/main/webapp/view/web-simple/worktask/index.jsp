<%@page import="java.util.Date"%>
<%@page import="com.farm.core.time.TimeTool"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>任务工作台- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script src="text/lib/layout/jquery.layout-latest.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh-CN.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-kindeditor.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-file-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/echarts/echarts.all.2.2.7.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-multiimage-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/htmltags.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/alert/sweetalert.min.js"></script>
<!-- 附件批量上传组件 -->
<script src="<PF:basePath/>text/lib/fileUpload/jquery.ui.widget.js"></script>
<script
	src="<PF:basePath/>text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.css"
	rel="stylesheet">
<script charset="utf-8"
	src="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.min.js"></script>
<link
	href="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.css"
	rel="stylesheet">
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

.plogs-backbox {
	background-color: #f4f4f4;
	height: 100%;
	width: 100%;
}

.plogs-view-title {
	padding: 10px;
	font-weight: 700;
}
/*
* 計劃
*/
.plogs-view-title.type1 {
	background-color: #d9edf7;
	color: #337ab7;
}

.plogs-view-type1box .plogs-task-box {
	border-color: #337ab7;
}

.plogs-view-type1box .plogs-task-box:HOVER {
	background-color: #d9edf7;
}
/*
* 準備
*/
.plogs-view-title.type2 {
	background-color: #fcf8e3;
	color: #f0ad4e;
}

.plogs-view-type2box .plogs-task-box {
	border-color: #f0ad4e;
}

.plogs-view-type2box .plogs-task-box:HOVER {
	background-color: #fcf8e3;
}
/*
* 執行
*/
.plogs-view-title.type3 {
	background-color: #f2dede;
	color: #d9534f;
}

.plogs-view-type3box .plogs-task-box {
	border-color: #d9534f;
}

.plogs-view-type3box .plogs-task-box:HOVER {
	background-color: #f2dede;
}
/*
* 完成
*/
.plogs-view-title.type4 {
	background-color: #dff0d8;
	color: #5cb85c;
}

.plogs-view-type4box .plogs-task-box {
	border-color: #5cb85c;
}

.plogs-view-type4box .plogs-task-box:HOVER {
	background-color: #dff0d8;
}

.plogs-view-tasks {
	padding: 0px;
}

.plogs-task-box {
	height: 45px;
	border: 1px dashed;
	padding: 4px;
	background-color: #ffffff;
	cursor: pointer;
	border-left: 8px solid;
	margin-bottom: 4px;
	line-height: 1.3em;
}

.plogs-task-box .plogs-projec-title {
	font-size: 10px;
	font-weight: 200;
	color: #666;
}

.plogs-task-box .plogs-task-title {
	font-size: 14px;
	font-weight: 600;
	color: #888;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

.plogs-view-type3box .plogs-task-box .plogs-task-title {
	color: #000;
}

.plogs-task-box code {
	font-size: 0.8em;
	font-weight: 200;
	float: right;
	padding: 0px;
	padding-right: 4px;
	padding-left: 4px
}

.plogs-projec-petime {
	font-size: 10px;
	margin-left: 4px;
	color: #c7254e;
}
</style>
</head>
<body>
	<!-- 各种弹出窗口 -->
	<div class="openWin">
		<!-- 操作任务弹出窗口 -->
		<jsp:include page="commons/includeTaskMenuWin.jsp"></jsp:include>
		<!-- 添加任务弹出窗口 -->
		<jsp:include page="commons/includeAddTaskWin.jsp"></jsp:include>
		<!-- 任务派发窗口 -->
		<jsp:include page="commons/includeSendTaskWin.jsp"></jsp:include>
	</div>
	<div class="ui-layout-center" id="plogs-current-task">
		<div
			style="float: left; width: 25%; background-color: #f1f7fa; height: 100%;"><jsp:include
				page="commons/includType1.jsp"></jsp:include></div>
		<div
			style="float: left; width: 25%; background-color: #fefcf4; height: 100%;"><jsp:include
				page="commons/includType2.jsp"></jsp:include></div>
		<div
			style="float: left; width: 25%; background-color: #faf2f2; height: 100%;"><jsp:include
				page="commons/includType3.jsp"></jsp:include></div>
		<div
			style="float: left; width: 25%; background-color: #f2f9ef; height: 100%;"><jsp:include
				page="commons/includType4.jsp"></jsp:include></div>
	</div>
	<div class="ui-layout-north">
		<!-- 顶部头  -->
		<jsp:include page="../commons/head.jsp"></jsp:include>
	</div>
	<div class="ui-layout-west">
		<div style="background-color: #f4f4f4; height: 98%;">
			<div style="text-align: center; padding-top: 20px;">
				<h1 style="margin: 0px; padding: 0px;">任务工作台</h1>
			</div>
			<div style="text-align: center; padding-top: 20px; color: #666666;">
				<span style="font-size: 16px; font-weight: 700;"><PF:FormatTime
						date="${today }" yyyyMMddHHmmss="yyyy年MM月dd日 WW" /> </span>
			</div>
			<div style="padding: 10px;">
				<div class="panel panel-default">
					<div class="panel-body">
						<jsp:include page="commons/includSearchForm.jsp"></jsp:include>
					</div>
				</div>
			</div>
			<div class="list-group" style="margin: 4px;">
				<!--  -->
				<a id="addProjectTaskButtonId" class="list-group-item"><i
					class="glyphicon glyphicon-plus-sign list-group-item-success"></i>&nbsp;添加任务</a>
				<!--  -->
				<a id="loadUserTasksButtonId" class="list-group-item"><i
					class="glyphicon glyphicon-refresh"></i>&nbsp;刷新任务</a>
			</div><!-- 
			<div class="list-group" style="margin: 4px;">
				
				<c:if test="${USEROBJ.type=='3' }">
					<a id="gotoProjectMngButtonId" class="list-group-item"><i
						class="glyphicon glyphicon-edit list-group-item-success"></i>&nbsp;项目管理台</a>
				</c:if>
				<a id="gotoPlanMngButtonId" class="list-group-item"><i
					class="glyphicon glyphicon-screenshot list-group-item-success"></i>&nbsp;任务计划台<code>${num6}</code></a>
			</div> -->
		</div>
	</div>
	<div class="ui-layout-south">
		<div style="background-color: #000000;">
			<!-- 底部权限 -->
			<jsp:include page="../commons/foot.jsp"></jsp:include>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(function() {
		//主体分隔
		$('body').layout({
			applyDefaultStyles : true,
			north__closable : false,//可以被关闭  
			north__resizable : false,//可以改变大小
			north__size : 50,
			north__spacing_open : 1,
			//---------------------------
			south__closable : false,//可以被关闭  
			south__resizable : false,//可以改变大小
			south__size : 36,
			south__spacing_open : 1,
			//---------------------------
			west__closable : true,//可以被关闭  
			west__resizable : false,//可以改变大小
			west__size : 250,
			west__spacing_open : 8
		});
	});
</script>
<script>
	$(function() {
		loadUserTask();
		$('#addProjectTaskMenuId').bind('click', function() {
			addProjectTask();
		});
		$('#addProjectTaskButtonId').bind('click', function() {
			addProjectTask();
		});
		$('#gotoProjectMngButtonId').bind('click', function() {
			gotoProjectMng();
		});
		$('#gotoPlanMngButtonId').bind('click', function() {
			gotoPlanMng();
		});
		$('#loadUserTasksButtonId').bind('click', function() {
			var obj = pAlert("loading...", 500);
			loadUserTask();
		});
		$(".taskAutoHeightBox").height($(window).height() - 130);
		$(window).resize(function() {
			$(".taskAutoHeightBox").height($(window).height() - 130);
		});
	});
	
	//任务派发
	function doSendTask(taskid) {
		pAlert("loading...", 10000);
		$('#taskSendFormBox').load('worktask/sandTaskWin.do', {
			'taskId' : taskid
		}, function() {
			swal.close();
			$('#taskSendFormBox').modal('show');
			$('#taskOpratorMenuBox').modal('hide');
		});
	}
	
	//执行一个任务
	function doTheTask(taskId) {
		window.location = "<PF:basePath/>worktask/taskDoPage.do?taskId="
				+ taskId;
	}
	//进入项目管理界面
	function gotoProjectMng(taskId) {
		window.location = "<PF:basePath/>workproject/index.do";
	}
	//进入任务计划台
	function gotoPlanMng() {
		window.location = "<PF:basePath/>workplan/index.do";
	}
	//创建一个任务
	function addProjectTask() {
		//项目名称条件
		$('#taskAddFormBox').load('worktask/loadTaskFormWin.do', {
			'name' : $('#searchNameInputId').val(),'states':'1234'
		}, function() {
			$('#taskAddFormBox').modal('show');
		});
	}
	//加载视图任务
	function loadUserTask() {
		$.post('worktask/loadTask.do', {
			"title" : $('#searchNameInputId').val()
		}, function(flag) {
			//TITLE,PSTATE,SOURCEUSERID,GRADATIONNAME,GRADATIONID,TASKTYPENAME,TASKTYPEID,PROJECTNAME
			$('.plogs-task-box').remove();
			var willOpenTaskId;
			$(flag.tasks).each(function(i, obj) {
				appendTaskToView(obj, obj.PSTATE);
			});
		}, 'json');
	}
	//添加一个任务
	function appendTaskToView(node, viewtype) {
		$('.plogs-view-type' + viewtype + 'box').append(
				creatTaskDiv(node, viewtype));
	}
	//创建任务元素
	function creatTaskDiv(node, viewtype) {
		if (!node.PLANETIME) {
			node.PLANETIME = "";
		}
		var div = '<div class="plogs-task-box" id="' + node.ID
				+ '" onClick="openTaskMenu(this);" >';
		div = div + '    <div class="plogs-task-title">' + node.TITLE
				+ '</div>';
		div = div + '    <div  class="plogs-task-title">';
		div = div + '        <span class="plogs-projec-title">'
				+node.COMPANYNAME+":"+ node.PROJECTNAME
				+ '</span><span title="计划结束时间" class="plogs-projec-petime">'
				+ node.PLANETIME + '</span>';
		div = div + '        <code>' + node.TASKTYPENAME + '</code>';
		div = div + '    </div>';
		div = div + '</div>';
		return div;
	}
	//打开任务的操作菜单 (任务信息)
	function openTaskMenu(taskBox) {
		var taskid = $(taskBox).attr('id');
		openTaskMenuByTaskId(taskid);
	}
	//打开任务的操作菜单 (任务信息)
	function openTaskMenuByTaskId(taskid) {
		pAlert("loading...", 10000);
		$('#taskOpratorMenuBox').load('worktask/openTaskWin.do', {
			'taskId' : taskid
		}, function() {
			swal.close();
			$('#taskOpratorMenuBox').modal('show');
		});
	}
	//修改任務狀態
	function updateTaskState(taskId, state) {
		if (state == '0') {
			pConfirm('任务作废后将被删除，不可恢复，确认执行？', function() {
				doUpdateTaskState(taskId, state);
			});
			return;
		}
		if (state == '5') {
			pConfirm('任务归档后将从列表隐藏，确认执行？', function() {
				doUpdateTaskState(taskId, state);
			});
			return;
		}
		doUpdateTaskState(taskId, state);
	}
	//执行任务状态改变
	function doUpdateTaskState(taskId, state) {
		pAlert("loading...", 10000);
		$.post('worktask/updateTaskState.do', {
			'taskId' : taskId,
			'state' : state
		}, function(flag) {
			swal.close();
			if (flag.STATE == '0') {
				//('修改成功');重新加載任務
				$('.plogs-task-box').remove();
				loadUserTask();
				$('#taskOpratorMenuBox').modal('hide');
			} else {
				pAlert(flag.MESSAGE);
			}
		}, 'json');
	}

	//关闭任務
	function closeTask(taskId) {
		pAlert("loading...", 10000);
		pConfirm('确认关闭，关闭后任务将存档隐藏!', function() {
			$.post('worktask/closeTask.do', {
				'taskId' : taskId
			}, function(flag) {
				swal.close();
				if (flag.STATE == '0') {
					//重新加載任務
					loadUserTask();
					$('#taskOpratorMenuBox').modal('hide');
				} else {
					pAlert(flag.MESSAGE);
				}
			}, 'json');
		});
	}

	//修改任務
	function editTask(taskId) {
		pAlert("loading...", 10000);
		$('#taskAddFormBox').load('worktask/loadTaskFormWin.do', {
			'taskId' : taskId,'states':'1234'
		}, function() {
			swal.close();
			$('#taskOpratorMenuBox').modal('hide');
			$('#taskAddFormBox').modal('show');
		});
	}
</script>
<script>
	//询问框
	function pConfirm(tip, okFunc, noFunc) {
		swal({
			text : tip,
			buttons : true,
			buttons : {
				'確定' : true,
				cancel : "取消"
			},
		}).then(function(willDelete) {
			if (willDelete) {
				okFunc();
			} else {
				if (noFunc) {
					noFunc();
				}
			}
		});
	}
	//消息框
	function pAlert(tip, timenum) {
		if (timenum) {
			swal({
				text : tip,
				timer : timenum,
				buttons : false
			});
		} else {
			swal(tip);
		}
	}
</script>
</html>