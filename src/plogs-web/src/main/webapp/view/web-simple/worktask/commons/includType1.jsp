<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- 计划中 -->
<div class="ui-layout-north">
	<div class="plogs-view-title type1">1等待</div>
</div>
<div class="ui-layout-center">
	<div class="plogs-backbox plogs-view-tasks">
		<div class="plogs-view-type1box taskAutoHeightBox"
			style="overflow: auto;background-color: #f1f7fa;"></div>
	</div>
</div>
