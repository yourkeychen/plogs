<%@page import="java.util.Date"%>
<%@page import="com.farm.core.time.TimeTool"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="input-group">
	<input type="text" id="searchNameInputId" class="form-control"
		placeholder="项目名称..."> <span class="input-group-btn">
		<button title="清空条件" class="btn btn-default"
			onclick="clearSearchForm()" type="button">
			<i class="glyphicon glyphicon-refresh"></i>
		</button>
		<button title="执行查询" class="btn btn-default"
			onclick="doSearchForm()" type="button">
			<i class="glyphicon glyphicon-search"></i>
		</button>
	</span>
</div>
<!-- /input-group -->
<script type="text/javascript">
	$(function() {
		clearSearchForm();
		$('#searchNameInputId').change(function() {
			doSearchForm();
		});
		$('#searchNameInputId').bind('keyup', function(event) {
			if (event.keyCode == "13") {
				doSearchForm();
			}
		});
	});
	function doSearchForm() {
		loadUserTask();
	}
	function clearSearchForm() {
		$('#searchNameInputId').val('');
		loadUserTask();
	}
</script>


