<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- 当前任务属性 -->
<style>
.wdap-right-fieldtable, table {
	table-layout: fixed;
}

.wdap-right-fieldtable td {
	word-break: break-all;
}
</style>
<table class="table table-bordered table-striped wdap-right-fieldtable"
	style="margin: 0px; padding: 0px;">
	<thead>
		<tr>
			<th style="text-align: center;" width="35">属性名称</th>
			<th style="text-align: center;" width="50">值</th>
			<th style="text-align: center;" width="15">操作</th>
		</tr>
	</thead>
	<tbody>
		<c:if test="${!empty fields }">
			<c:forEach items="${fields}" var="node">
				<tr>
					<td style="width: 40%; text-align: right;">${node.name}</td>
					<td style="width: 40%; font-weight: 200; text-align: left;"><c:if
							test="${fn:length(node.fieldines) ==1 }">
							${node.fieldines[0].valtitle}
						</c:if> <c:if test="${fn:length(node.fieldines) >1 }">
							 ${fn:length(node.fieldines)}条记录
						</c:if></td>
					<td><c:if test="${node.singletype=='1' }">
							<!-- 单条 -->
							<c:if test="${node.eidtAble}">
								<a onclick="editFieldVal('${node.uuid}','${taskId}')">编辑</a>
							</c:if>
						</c:if> <c:if test="${node.singletype=='2' }">
							<!-- 多条 -->
							<c:if test="${node.eidtAble}">
								<a onclick="editFieldVal('${node.uuid}','${taskId}')">编辑</a>
							</c:if>
							<c:if test="${!node.eidtAble}">
								<a onclick="editFieldVal('${node.uuid}','${taskId}')">查看</a>
							</c:if>
						</c:if></td>
				</tr>
			</c:forEach>
		</c:if>
		<c:if test="${empty fields }">
			<tr>
				<td colspan="3" align="center">无属性</td>
			</tr>
		</c:if>
	</tbody>
</table>