<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--点击提交工作后的任务状态表单 -->
<div class="modal fade" id="taskCompeletForm" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">任务状态</h4>
			</div>
			<div class="modal-body" style="padding-bottom: 0px;">
				<form class="form-horizontal">
					<!-- 任务完成百分比 -->
					<div class="form-group">
						<label for="completionVal" class="col-sm-2 control-label">完成度</label>
						<div class="col-sm-10">
							<select class="form-control" id="completionVal" val="${task.completion}">
								<c:forEach begin="0" end="100" var="node" step="5">
									<option value="${100-node }">${100-node }%</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<!-- 任务完成总工时 -->
					<div class="form-group" id="taskdoTimeGroup">
						<label for="taskdoTime" class="col-sm-2 control-label">总工时</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="taskdoTime" value="${task.dotimes}"
								placeholder="单位：小时">
						</div>
					</div>
				</form>
			</div>
			<c:if test="${STATE=='0'}">
				<div class="modal-footer">
					<span class="alertMsgClass" id="taskCompeletErrormessageboxId"></span>
					<button type="button" onclick="submitTaskCompeletForm()"
						class="btn btn-primary">提交</button>
				</div>
			</c:if>
		</div>
	</div>
	<script>
		$(function(){
			$('select', '#taskCompeletForm').each(function(i, obj) {
				var val = $(obj).attr('val');
				$(obj).val(val);
			});
			validateInput('taskdoTime', function(id, val, obj) {
				if (valid_isNull(val)) {
					return {
						valid : false,
						msg : '不能为空'
					};
				}
				if (!valid_isNumber(val)) {
					return {
						valid : false,
						msg : '请录入正整數'
					};
				}
				if (val<0) {
					return {
						valid : false,
						msg : '请录入正整數' 
					};
				}
				return {
					valid : true,
					msg : '正确'
				};
			}, null, 'taskdoTimeGroup');
		});
	
		//提交表单
		function submitTaskCompeletForm() {
			if (!validate('taskCompeletForm')) {
				$('#taskCompeletErrormessageboxId').text('信息录入有误，请检查！');
			} else {
				$.post('worktask/submitTask.do', {
					'taskId' : '${task.id}',
					'completion' :  $('#completionVal').val(),
					'dotimes' : $('#taskdoTime').val()
				}, function(flag) {
					if (flag.STATE == '0') {
						window.location = "<PF:basePath/>worktask/index.do";
					} else {
						pAlert(flag.MESSAGE);
					}
				}, 'json');
			}
		}
	</script>
</div>