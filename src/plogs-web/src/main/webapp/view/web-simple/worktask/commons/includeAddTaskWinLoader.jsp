<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!--点击任务后弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">
				<c:if test="${empty task}">添加任务</c:if>
				<c:if test="${!empty task}">更新任务</c:if>
			</h4>
		</div>
		<div class="modal-body" style="padding-bottom: 0px;">
			<form class="form-horizontal" id="addTaskFormId">
				<input type="hidden" id="taskformTaskId" value="${task.id}">
				<div class="form-group" id="taskProjectGroupId">
					<label for="taskProject" class="col-sm-3 control-label">项目<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="taskProject" class="form-control" val=${task.projectid}>
							<option value="">~选择项目~</option>
							<c:forEach var="node" items="${projects}">
								<option value="${node.id}">${node.companyname}${(!empty node.companyname)?' : ':''}${node.name}
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group" id="taskGradationGroupID">
					<label for="taskGradation" class="col-sm-3 control-label">阶段<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="taskGradation" class="form-control"
							val='${task.gradationid}'>
							<option value="">~先选择项目~</option>
							<c:forEach var="node" items="${gradations}">
								<option value="${node.id}">${node.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group" id="taskTypeGroupId">
					<label for="taskType" class="col-sm-3 control-label">类型<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="taskType" class="form-control"
							val='${task.tasktypeid}'>
							<option value="">~先选择阶段~</option>
							<c:forEach var="node" items="${taskTypes}">
								<option value="${node.id}">${node.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group" id="titleGroupId">
					<label for="taskTitle" class="col-sm-3 control-label">标题<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="taskTitle"
							value="${task.title}" placeholder="任务标题">
					</div>
				</div>
				<div class="form-group">
					<label for="taskState" class="col-sm-3 control-label">状态<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="taskState" class="form-control" val="${task.pstate}">
							<c:if test="${fn:contains(states,'6')==true}">
								<option value="6">计划</option>
							</c:if>
							<c:if test="${fn:contains(states,'1')==true}">
								<option value="1">等待</option>
							</c:if>
							<c:if test="${fn:contains(states,'2')==true}">
								<option value="2">开始</option>
							</c:if>
							<c:if test="${fn:contains(states,'3')==true}">
								<option value="3">执行</option>
							</c:if>
							<c:if test="${fn:contains(states,'4')==true}">
								<option value="4">完成</option>
							</c:if>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="taskPEdate" class="col-sm-3 control-label">计划完成时间</label>
					<div class="col-sm-9">
						<input type="text" class="form-control form_day" id="taskPEdate"
							value="${task.planetime}">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<textarea name="text" id="taskReadText"
							style="height: 200px; width: 100%;">${fn:replace(fn:replace(text,'&lt;', '&amp;lt;'),'&gt;', '&amp;gt;')}</textarea>
						<jsp:include
							page="/view/web-simple/worktask/kindeditors/includeEditorLoader.jsp">
							<jsp:param name="textareaId" value="taskReadText" />
							<jsp:param name="tooltype" value="max" />
						</jsp:include>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<span class="alertMsgClass" id="errormessageShowboxId"></span>
			<button type="button" onclick="submitTaskForm()"
				class="btn btn-primary">
				<c:if test="${empty task}">添加任务</c:if>
				<c:if test="${!empty task}">更新任务</c:if>
			</button>
		</div>
	</div>
</div>
<script>
	
	$(function() {
		//項目选定事件
		$('#taskProject').change(function() {
			loadGradations($(this).val());
		});
		//階段选定事件
		$('#taskGradation').change(function() {
			loadTaskTypes($(this).val());
		});

		//任务类型选定事件
		$('#taskType').change(function() {
			loadTemplete($(this).val());
		});

		$('select', '#addTaskFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
		$(".form_day").datetimepicker({
			format : "yyyy-mm-dd",
			showMeridian : true,
			autoclose : true,
			todayBtn : true,
			minView : 2,
			startView : 3
		});
		validateInput('taskTitle', function(id, val, obj) {
			// 标题
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 256)) {
				return {
					valid : false,
					msg : '长度不能大于' + 256
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'titleGroupId');
		validateInput('taskProject', function(id, val, obj) {
			// 项目
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'taskProjectGroupId');
		validateInput('taskGradation', function(id, val, obj) {
			// 阶段
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'taskGradationGroupID');
		validateInput('taskType', function(id, val, obj) {
			// 类型
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'taskTypeGroupId');
	});
	//提交表单
	function submitTaskForm() {
		if (!validate('addTaskFormId')) {
			$('#errormessageShowboxId').text('信息录入有误，请检查！');
		} else {
			$('#errormessageShowboxId').text('');
			pConfirm("是否提交数据?", function() {
				var title = $('#taskTitle').val();
				var projectid = $('#taskProject').val();
				var gradationid = $('#taskGradation').val();
				var tasktypeid = $('#taskType').val();
				var state = $('#taskState').val();
				var taskid = $('#taskformTaskId').val();
				eval('taskReadTexteditor').sync();
				var taskReadText = $('#taskReadText').val();
				var planetime = $('#taskPEdate').val();
				$.post('worktask/taskSubmit.do', {
					'title' : title,
					'projectid' : projectid,
					'gradationid' : gradationid,
					'tasktypeid' : tasktypeid,
					'state' : state,
					'text' : taskReadText,
					'taskid' : taskid,
					'planetime' : planetime
				}, function(flag) {
					if (flag.STATE == '0') {
						pAlert(flag.task.title + ":任务提交成功!");
						loadUserTask();
						$('#taskAddFormBox').modal('hide');
					} else {
						pAlert(flag.MESSAGE);
					}
				}, 'json');

			});
		}
	}
	//加载远程阶段类型
	function loadGradations(projectId) {
		$.post('worktask/loadGradations.do', {
			'projectId' : projectId
		}, function(data) {
			if (data.STATE == '0') {
				$('#taskGradation').html('<option value="">~选择项目~</option>');
				$('#taskType').html('<option value="">~选择阶段~</option>');
				$(data.gradations).each(
						function(i, obj) {
							$('#taskGradation').append(
									'<option value="'+obj.id+'">' + obj.name
											+ '</option>');
						});
				$('#taskGradation').val(data.gradationid);
				loadTaskTypes(data.gradationid);
			} else {
				pAlert(data.MESSAGE);
			}

		}, 'json');
	}

	//加载任务类型
	function loadTaskTypes(gradationId) {
		//加载阶段
		$.post('worktask/loadTaskType.do', {
			'gradationId' : gradationId
		}, function(data) {
			if (data.STATE == '0') {
				$('#taskType').html('<option value="">~选择任务类型~</option>');
				$(data.taskTypes).each(
						function(i, obj) {
							$('#taskType').append(
									'<option value="'+obj.id+'">' + obj.name
											+ '</option>');
						});
			} else {
				pAlert(data.MESSAGE);
			}

		}, 'json');
	}
	
	//加载模板
	function loadTemplete(taskTypeId) {
		$.post('worktask/loadTemplete.do?tasktypeId=' + taskTypeId, {},
				function(flag) {
					if (flag.hastemp) {
						if (!getEditorcontent()) {
							//加载模板
							setEditorcontent(flag.text);
						} else {
							pConfirm("是否使用任务模板?", function() {
								//加载模板
								setEditorcontent(flag.text);
							});
						}
					}
				}, 'json');
	}
</script>