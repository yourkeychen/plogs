<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--点击編輯屬性弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">编辑属性</h4>
		</div>
		<div class="modal-body" style="padding-bottom: 0px;">
			<c:if test="${STATE=='1'}">
				<span style="color: red;">${MESSAGE}</span>
			</c:if>
			<c:if test="${STATE=='0'}">
				<form class="form-horizontal" id="editTaskFieldFormId">
					<input type="hidden" id="editFieldformTaskId"
						value="${fieldins.id}">
					<div class="form-group" id="taskFieldGroupId">
						<label for="taskTitle" class="col-sm-3 control-label">${fieldins.name}<span
							class="alertMsgClass">*</span></label>
						<div class="col-sm-9">
							<!-- 1.富文本，2附件，3多行文本，4单行文本，5枚举单选，6枚举多选，7公司，8人员，9时间，10日期 -->
							<c:if test="${fieldins.valtype=='2'}">
								<!-- 2附件 -->
								<jsp:include
									page="/view/web-simple/worktask/commons/formEditor/FileInput.jsp"></jsp:include>
							</c:if>
							<c:if test="${fieldins.valtype=='3'}">
								<!-- 3多行文本 -->
								<jsp:include
									page="/view/web-simple/worktask/commons/formEditor/TextareaInput.jsp"></jsp:include>
							</c:if>
							<c:if test="${fieldins.valtype=='4'}">
								<!-- 4单行文本 -->
								<jsp:include
									page="/view/web-simple/worktask/commons/formEditor/TextInput.jsp"></jsp:include>
							</c:if>
							<c:if test="${fieldins.valtype=='5'}">
								<!-- 5枚举单选 -->
								<jsp:include
									page="/view/web-simple/worktask/commons/formEditor/SelectOneInput.jsp"></jsp:include>
							</c:if>
							<c:if test="${fieldins.valtype=='10'}">
								<!-- 9日期 -->
								<jsp:include
									page="/view/web-simple/worktask/commons/formEditor/DateInput.jsp"></jsp:include>
							</c:if>
						</div>
					</div>
				</form>
			</c:if>
		</div>
		<c:if test="${STATE=='0'}">
			<div class="modal-footer">
				<span class="alertMsgClass" id="fieldErrormessageboxId"></span>
				<button type="button" onclick="submitTaskFieldForm()"
					class="btn btn-primary">提交</button>
			</div>
		</c:if>
	</div>
</div>
<script>
	//提交表单
	function submitTaskFieldForm() {
		if (!validate('editTaskFieldFormId')) {
			$('#fieldErrormessageboxId').text('信息录入有误，请检查！');
		} else {
			$('#fieldErrormessageboxId').text('');
			var fieldinsId = $('#editFieldformTaskId').val();
			submitInputForm(fieldinsId, function() {
				loadTaskField('${taskid}');
				$('#fieldEditFormBox').modal('hide');
			});
		}
	}
</script>