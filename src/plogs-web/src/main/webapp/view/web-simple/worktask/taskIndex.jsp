<%@page import="java.util.Date"%>
<%@page import="com.farm.core.time.TimeTool"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${task.title }-<PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script src="text/lib/layout/jquery.layout-latest.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh-CN.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-kindeditor.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-file-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/echarts/echarts.all.2.2.7.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-multiimage-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/htmltags.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/alert/sweetalert.min.js"></script>
<!-- 附件批量上传组件 -->
<script src="<PF:basePath/>text/lib/fileUpload/jquery.ui.widget.js"></script>
<script
	src="<PF:basePath/>text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.css"
	rel="stylesheet">
<script charset="utf-8"
	src="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.min.js"></script>
<link
	href="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.css"
	rel="stylesheet">
<link rel="stylesheet" href="<PF:basePath/>text/lib/viewer/viewer.css" />
<script charset="utf-8" src="<PF:basePath/>text/lib/viewer/viewer.js"></script>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

.plogs-backbox {
	background-color: #f4f4f4;
	height: 100%;
	width: 100%;
}

.plogs-view-title {
	padding: 10px;
	font-weight: 700;
}

.plogs-view-tasks {
	padding: 0px;
}

.plogs-task-box {
	height: 45px;
	border: 1px dashed;
	padding: 4px;
	background-color: #ffffff;
	cursor: pointer;
	border-left: 8px solid;
	margin-bottom: 4px;
	line-height: 1.3em;
}

.plogs-task-box .plogs-projec-title {
	font-size: 10px;
	font-weight: 200;
	color: #666;
}

.plogs-task-box .plogs-task-title {
	font-size: 14px;
	font-weight: 600;
	color: #888;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

.plogs-view-type3box .plogs-task-box .plogs-task-title {
	color: #000;
}

.plogs-task-box code {
	font-size: 0.8em;
	font-weight: 200;
	float: right;
	padding: 0px;
	padding-right: 4px;
	padding-left: 4px
}

.plogs-projec-petime {
	font-size: 10px;
	margin-left: 4px;
	color: #c7254e;
}
</style>
</head>
<body>
	<!-- 各种弹出窗口 -->
	<div class="openWin">
		<!-- 属性编辑弹出窗口 -->
		<jsp:include page="commons/includeFieldEditWin.jsp"></jsp:include>
		<!-- 任务提交窗口 -->
		<jsp:include page="commons/includeTaskSubmitWin.jsp"></jsp:include>
	</div>
	<div class="ui-layout-center" id="plogs-current-task">
		<div class="ui-layout-north">
			<h1 style="font-size: 18px; margin: 10px;">
				<PF:FormatTime date="${today }" yyyyMMddHHmmss="MM月dd日 WW" />
			</h1>
		</div>
		<div class="ui-layout-center">
			<div class="plogs-backbox">
				<textarea name="text" id="currentLogText" style="width: 100%;">${fn:replace(fn:replace(clogtext,'&lt;', '&amp;lt;'),'&gt;', '&amp;gt;')}</textarea>
				<script type="text/javascript">
					$(function() {
						var currentLogWinBoxHeight = $(window).height();
						$('#currentLogText').height(
								currentLogWinBoxHeight - 190);

					});
				</script>
				<jsp:include
					page="/view/web-simple/worktask/kindeditors/includeEditorLoader.jsp">
					<jsp:param name="textareaId" value="currentLogText" />
					<jsp:param name="tooltype" value="max" />
				</jsp:include>
			</div>
		</div>
		<div class="ui-layout-east">
			<div class="ui-layout-east" id="taskFieldloadBoxId">
				<div class="plogs-backbox" style="padding: 20px;">loading...</div>
			</div>
		</div>
		<div class="ui-layout-south">
			<div id="currentBottomMenusBoxId"
				style="padding-left: 60px; background-color: #f4f4f4; height: 50px; padding-top: 8px; text-align: right; padding-right: 20px;">
				<div style="float: left;">
					<code id="currentLogOpLogsId">任務日志编辑中</code>
				</div>
				<div class="btn-group " role="group" aria-label="Large button group">
					<button type="button" onclick="backCurrentTasks()"
						class="btn btn-default">&nbsp;返回任务列表&nbsp;</button>
					<!--<button type="button" onclick="saveCurentTask()"
						class="btn btn-info">&nbsp;保存今日日志&nbsp;</button>  -->
					<button onclick="completeCurrentTask()" type="button"
						class="btn btn-success">&nbsp;提交当前工作&nbsp;</button>
				</div>
			</div>
		</div>
	</div>
	<div class="ui-layout-west">
		<div style="background-color: #f4f4f4; height: 98%;">
			<div class="modal-header"
				style="background-color: #ffffff; padding: 10px;">
				<div class="modal-title" id="myModalLabel">
					<div style="float: left;">
						<!-- 1.计划，2开始，3处理，4结束,5关闭 -->
						<c:if test="${task.pstate=='1'}">
							<img src="text/img/plogs/taskPlan.png"
								style="height: 64px; width: 64px;">
						</c:if>
						<c:if test="${task.pstate=='2'}">
							<img src="text/img/plogs/taskStar.png"
								style="height: 64px; width: 64px;">
						</c:if>
						<c:if test="${task.pstate=='3'}">
							<img src="text/img/plogs/taskJustDo.png"
								style="height: 64px; width: 64px;">
						</c:if>
						<c:if test="${task.pstate=='4'}">
							<img src="text/img/plogs/taskComplete.png"
								style="height: 64px; width: 64px;">
						</c:if>
					</div>
					<div
						style="padding-left: 60px; padding-top: 8px; margin-left: 20px;">
						<div
							style="font-size: 18px; font-weight: 700; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"
							title="${task.title}">${task.title}&nbsp;&nbsp;<code>${task.tasktypename}</code>
						</div>
						<div style="font-size: 14px; font-weight: 200; margin-top: 6px;">
							<a target="_blank"
								href="projectView/projectInfo.do?projectId=${project.id}">
								${company.name}:${project.name}</a>&nbsp;&nbsp;
							<code>${task.gradationname}</code>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-body"
				style="overflow: auto; background-color: #ffffff; margin: 10px; padding: 10px;">
				<table class="table table-striped table-bordered"
					style="margin-bottom: 0px;">
					<tbody>
						<tr>
							<th>执行人</th>
							<td><code>${user.name}</code></td>
							<th>已用时</th>
							<td><code> ${task.dotimes==null?0:task.dotimes}小时 </code></td>
						</tr>
						<c:if test="${!empty task.completion}">
							<tr>
								<th>完成度</th>
								<td colspan="3"><div class="progress" style="margin: 0px;">
										<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
											aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
											style="width: ${task.completion}%;">${task.completion}%</div>
									</div></td> 
							</tr>
						</c:if>
						<tr>
							<th>实际开始时间</th>
							<td><code>
									<PF:FormatTime date="${task.dostime}"
										yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
								</code></td>
							<th>实际结束时间</th>
							<td><code>
									<PF:FormatTime date="${task.doetime}"
										yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
								</code></td>
						</tr>
						<c:if test="${!empty task.planstime||!empty task.planetime}">
							<tr>
								<th>计划开始时间</th>
								<td><code>
										<PF:FormatTime date="${task.planstime}"
											yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
									</code></td>
								<th>计划结束时间</th>
								<td><code>
										<PF:FormatTime date="${task.planetime}"
											yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
									</code></td>
							</tr>
						</c:if>
					</tbody>
				</table>
			</div>
			<c:if test="${!empty text}">
				<div
					style="overflow: auto; background-color: #ffffff; margin: 10px; padding: 10px;">
					<h1 style="font-size: 18px;">任务描述</h1>
					<div class="ke-content" id="taskFormTextBoxId">${text}</div>
				</div>
			</c:if>
			<c:if test="${!empty logs}">
				<c:forEach var="log" items="${logs}">
					<c:if test="${clog.id!=log.id}">
						<div
							style="overflow: auto; background-color: #ffffff; margin: 10px; padding: 10px;">
							<h1 style="font-size: 18px;">
								<PF:FormatTime date="${log.datekey}"
									yyyyMMddHHmmss="yyyy-MM-dd WW" />
								-日志
							</h1>
							<div class="ke-content" id="taskFormTextBoxId"
								style="overflow: auto; background-color: #ffffff; margin: 10px; padding: 10px;">
								${log.text}</div>
						</div>
					</c:if>
				</c:forEach>
			</c:if>
		</div>
	</div>
	<div class="ui-layout-north">
		<!-- 顶部头  -->
		<jsp:include page="../commons/head.jsp"></jsp:include>
	</div>
	<div class="ui-layout-south">
		<div style="background-color: #000000;">
			<!-- 底部权限 -->
			<jsp:include page="../commons/foot.jsp"></jsp:include>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(function() {
		//主体分隔
		$('body').layout({
			applyDefaultStyles : true,
			north__closable : false,//可以被关闭  
			north__resizable : false,//可以改变大小
			north__size : 50,
			north__spacing_open : 1,
			//---------------------------
			south__closable : false,//可以被关闭  
			south__resizable : false,//可以改变大小
			south__size : 36,
			south__spacing_open : 1,
			//---------------------------
			west__closable : true,//可以被关闭  
			west__resizable : false,//可以改变大小
			west__size : 560,
			west__spacing_open : 8,
			//---------------------------
			east__closable : true,//可以被关闭  
			east__resizable : false,//可以改变大小
			east__size : 200,
			east__spacing_open : 8
		});
		//任務操作区
		$('#plogs-current-task').layout({
			applyDefaultStyles : true,
			north__closable : false,//可以被关闭  
			north__resizable : false,//可以改变大小
			north__size : 40,
			north__spacing_open : 1,
			//---------------------------
			south__closable : false,//可以被关闭  
			south__resizable : false,//可以改变大小
			south__size : 50,
			south__spacing_open : 1,
			//----------------------------
			east__closable : true,//可以被关闭  
			east__resizable : false,//可以改变大小
			east__spacing_open : 4,
			east__size : 300
		});
	});
</script>
<script>
	$(function() {
		$(window).resize(function() {
			var editor = eval('currentLogTexteditor');
			editor.edit.setHeight($(window).height() - 240);
		});
		loadTaskField();
		//图片点击
		$('.ke-content img').each(function(i, obj) {
			try {
				$(obj).css("cursor", "pointer");
				var gallery = new Viewer(obj, {
					navbar : false,
					toolbar : {
						zoomIn : 4,
						zoomOut : 4,
						oneToOne : 4,
						reset : 4,
						prev : 0,
						play : {
							show : 0,
							size : 'large',
						},
						next : 0,
						rotateLeft : 4,
						rotateRight : 4,
						flipHorizontal : 4,
						flipVertical : 4
					},
					keyboard : false,
				});
			} catch (e) {
			}
		});
	});
	//返回任务列表
	function backCurrentTasks() {
		saveCurentTask(function() {
			window.location = "<PF:basePath/>worktask/index.do";
		});
	}
	//完成任务
	function completeCurrentTask() {
		saveCurentTask(function() {
			//doUpdateTaskState('${task.id}', "4", function() {
			//	window.location = "<PF:basePath/>worktask/index.do";
			//});
			$('#taskCompeletForm').modal('show');
		});
	}
	//执行任务状态改变
	function doUpdateTaskState(taskId, state, func) {
		$.post('worktask/updateTaskState.do', {
			'taskId' : taskId,
			'state' : state
		}, function(flag) {
			if (flag.STATE == '0') {
				func();
			} else {
				pAlert(flag.MESSAGE);
			}
		}, 'json');
	}
	//保存当前任务
	function saveCurentTask(func) {
		//('保存当前任务:' + currentTaskId);
		//獲得text内容
		eval('currentLogTexteditor').sync();
		var text = $('#currentLogText').val();
		//提交到後臺
		$.post('worktask/ctlogSubmit.do', {
			'text' : text,
			'taskid' : '${task.id}'
		},
				function(flag) {
					if (flag.STATE == '0') {
						if (flag.log != null) {
							$('#currentLogOpLogsId').text(
									"日志保存成功:" + flag.task.title + "-"
											+ flag.log.etime);
						} else {
							$('#currentLogOpLogsId').text("日志无需保存");
						}
						if (func) {
							func();
						}
					} else {
						pAlert(flag.MESSAGE);
					}
				}, 'json');
	}
	//加载任务属性
	function loadTaskField() {
		$('#taskFieldloadBoxId').load('worktask/loadTaskFieldWin.do', {
			'taskId' : '${task.id}'
		}, function() {
		});
	}
	//編輯任务属性
	function editFieldVal(fieldUuid, taskid) {
		$('#fieldEditFormBox').load('worktask/loadFieldFormWin.do', {
			'uuid' : fieldUuid,
			'taskid' : taskid
		}, function() {
			$('#fieldEditFormBox').modal('show');
		});
	}
	//添加任务属性实例
	function addFieldInsVal(fieldUuid, taskid) {
		$('#fieldEditFormBox').load('worktask/addFieldIns.do', {
			'fieldUuid' : fieldUuid,
			'taskid' : taskid
		}, function() {
			$('#fieldEditFormBox').modal('show');
		});
	}
	//删除任务属性实例
	function delFieldInsVal(fieldInsid, taskid) {
		pConfirm("确定删除一条属性?", function() {
			$('#fieldEditFormBox').load('worktask/delFieldIns.do', {
				'fieldinsId' : fieldInsid,
				'taskid' : taskid
			}, function() {
				loadTaskField();
				$('#fieldEditFormBox').modal('show');
			});
		});
	}
	//編輯任务属性实例
	function editFieldInsVal(fieldInsid, taskid) {
		$('#fieldEditFormBox').load('worktask/loadFieldInsFormWin.do', {
			'fieldinsId' : fieldInsid,
			'taskid' : taskid
		}, function() {
			$('#fieldEditFormBox').modal('show');
		});
	}
</script>
<script>
	//询问框
	function pConfirm(tip, okFunc, noFunc) {
		swal({
			text : tip,
			buttons : true,
			buttons : {
				'確定' : true,
				cancel : "取消"
			},
		}).then(function(willDelete) {
			if (willDelete) {
				okFunc();
			} else {
				if (noFunc) {
					noFunc();
				}
			}
		});
	}
	//消息框
	function pAlert(tip, timenum) {
		if (timenum) {
			swal({
				text : tip,
				timer : timenum,
				buttons : false
			});
		} else {
			swal(tip);
		}
	}
</script>
</html>