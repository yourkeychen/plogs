<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<script src="text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="text/lib/fileUpload/jquery.fileupload.css" rel="stylesheet">
<div class="input-group">
	<input type="text" class="form-control" id="fieldValTitleInputId"
		readonly="readonly" value="${fieldins.valtitle}"
		placeholder="请上传文件..."> <span class="input-group-btn">
		<span class="btn btn-info fileinput-button"
		style="height: 34px; padding-top: 6px;" role="button"> <span
			style="font-size: 14px;">上传文件</span> <input id="fileupload"
			type="file" name="file" multiple> <input type="hidden"
			value="${fieldins.val}" id="fieldValInputId">
	</span>
	</span>
</div>
<div style="text-align: center; margin-top: 20px;">
	<div class="progress" id="uploadProgressId" style="display: none;">
		<div class="progress-bar  progress-bar-striped active"
			id="progressBarId" role="progressbar" aria-valuenow="60"
			aria-valuemin="0" aria-valuemax="100" style="width: 60%;">60%</div>
	</div>
	<div class="progress" id="serverStatId" style="display: none;">
		<div class="progress-bar progress-bar-success" role="progressbar"
			id="serverStatLableId" aria-valuenow="100" aria-valuemin="0"
			aria-valuemax="100" style="width: 100%"></div>
	</div>
</div>
<div id="validateFileBoxId"></div>
<script>
	var maxSize = parseInt("${config_doc_upload_length_max}");
	var allUploadFilesize = 0;
	var maxSizeTitle = (maxSize / 1024 / 1024).toFixed(2);
	$(function() {
		$('#fileupload').fileupload({
			url : "upload/general.do",
			dataType : 'json',
			change : function(e, data) {
				return updateFileValidate(data.files);
			},
			done : function(e, data) {
				uploadComplete(data.result);
			},
			progressall : function(e, data) {
				runProgress(data.loaded, data.total);
			}
		});
		validateInput('fieldValInputId', function(id, val, obj) {
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 256)) {
				return {
					valid : false,
					msg : '长度不能大于' + 256
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'NONE', 'taskFieldGroupId');
	});
	//提交表单
	function submitInputForm(fieldinsId, func) {
		pConfirm("是否提交数据?", function() {
			$.post('worktask/taskFieldSubmit.do', {
				'value' : $('#fieldValInputId').val(),
				'fieldinsId' : fieldinsId
			}, function(flag) {
				if (flag.STATE == '0') {
					func();
				} else {
					pAlert(flag.MESSAGE);
				}
			}, 'json');
		});
	} //检查上传文件，返回值为是否允许上传
	function updateFileValidate(files) {
		var maxFileNum = 1;
		if (files.length > maxFileNum) {
			pAlert("单次上传文件数量不能大于" + maxFileNum + "个!");
			return false;
		}
		var isOK = true;
		$(files).each(
				function(i, obj) {
					if (obj.size > maxSize) {
						pAlert("文件" + obj.name + "超大,请检查文件大小不能大于"
								+ maxSizeTitle + "m");
						isOK = false;
					}
				});
		return isOK;
	}
	//加载进度条
	function runProgress(cProgess, totalProgess) {
		var progress = parseInt(cProgess / totalProgess * 100, 10);
		loadProgress(progress);
		if (progress == 100) {
			$('#serverStatId').show();
			loadRemoteProcess(true);
		} else {
			$('#serverStatId').hide();
		}
	}
	//检查远程进度
	function loadRemoteProcess(state) {
		isLoadRemotProecess = state;
		if (state) {
			setTimeout("doloadRemoteProcess()", 1000); //延迟1秒
		}
	}
	//加载远程进度
	function doloadRemoteProcess() {
		$.post("upload/PubUploadProcess.do", {}, function(flag) {
			if (flag.STATE == 0) {
				$('#serverStatLableId')
						.text('等待服务器处理中' + flag.process + '%...');
				if (flag.process > 99) {
					isLoadRemotProecess = false;
				}
				if (isLoadRemotProecess) {
					setTimeout("doloadRemoteProcess()", 1000); //延迟1秒	
				}
			} else {
				pAlert(flag.MESSAGE);
			}
		}, 'json');
	}
	function loadProgress(progress) {
		if (progress > 0 && progress < 99) {
			$('#uploadProgressId').show();
			$('#progressBarId').attr('aria-valuenow', progress);
			$('#progressBarId').text(progress + '%');
			$('#progressBarId').attr('style', "width: " + progress + "%;");
		} else {
			$('#uploadProgressId').hide();
		}
	}
	//上传文件完成
	function uploadComplete(data) {
		var state = data.STATE;
		var message = data.MESSAGE;
		if (state == 1) {
			pAlert(message);
			$('#serverStatId').hide();
			return;
		}
		var url = data.url;
		var message = data.MESSAGE;
		var id = data.id;
		var fileName = data.fileName;
		{
			//处理完成信息
			$('#fieldValTitleInputId').val(fileName);
			$('#fieldValInputId').val(id);
		}
		allUploadFilesize--;
		if (allUploadFilesize <= 0) {
			allUploadFilesize = 0;
			$('#serverStatId').hide();
		}
	}
</script>