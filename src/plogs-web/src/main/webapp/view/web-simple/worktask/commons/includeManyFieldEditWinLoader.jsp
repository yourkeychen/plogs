<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--点击編輯屬性弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">编辑属性</h4>
		</div>
		<div class="modal-body"
			style="padding-bottom: 0px; padding-bottom: 40px;">
			<table class="table table-striped table-bordered"
				style="background-color: #f9f6f1;">
				<thead>
					<tr>
						<th>创建日期</th>
						<th>${field.name}</th>
						<c:if test="${isEdit}">
							<th style="text-align: center; width: 120px;">操作</th>
						</c:if>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="node" varStatus="status" items="${fieldines}">
						<tr>
							<td><PF:FormatTime date="${node.ctime}"
									yyyyMMddHHmmss="yyyy-MM-dd HH:mm" /></td>
							<td>${node.valtitle}</td>
							<c:if test="${isEdit}">
								<td style="text-align: center;">&nbsp;
									<button type="button"
										onclick="editFieldInsVal('${node.id}', '${taskid}')"
										class="btn btn-primary btn-xs">编辑</button>&nbsp;
									<button type="button"
										onclick="delFieldInsVal('${node.id}', '${taskid}')"
										class="btn btn-primary btn-xs">删除</button>&nbsp;
								</td>
							</c:if>
						</tr>
					</c:forEach>
					<c:if test="${isEdit}">
						<tr>
							<td colspan="3" style="text-align: center;">
								<button type="button"
									onclick="addFieldInsVal('${field.uuid}', '${taskid}')"
									class="btn btn-primary btn-xs">&nbsp;&nbsp;添加&nbsp;&nbsp;</button>
							</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
	</div>
</div>