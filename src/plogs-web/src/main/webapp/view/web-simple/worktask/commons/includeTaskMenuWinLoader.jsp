<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<link rel="stylesheet" href="<PF:basePath/>text/lib/viewer/viewer.css" />
<script charset="utf-8" src="<PF:basePath/>text/lib/viewer/viewer.js"></script>
<!--点击任务后弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div class="modal-title" id="myModalLabel" style="height: 64px;">
				<div style="float: left;">
					<!-- 1.计划，2开始，3处理，4结束,5关闭 -->
					<c:if test="${task.pstate=='1'}">
						<img src="text/img/plogs/taskPlan.png"
							style="height: 64px; width: 64px;">
					</c:if>
					<c:if test="${task.pstate=='2'}">
						<img src="text/img/plogs/taskStar.png"
							style="height: 64px; width: 64px;">
					</c:if>
					<c:if test="${task.pstate=='3'}">
						<img src="text/img/plogs/taskJustDo.png"
							style="height: 64px; width: 64px;">
					</c:if>
					<c:if test="${task.pstate=='4'}">
						<img src="text/img/plogs/taskComplete.png"
							style="height: 64px; width: 64px;">
					</c:if>
					<c:if test="${task.pstate=='6'}">
						<img src="text/img/plogs/plan.png"
							style="height: 64px; width: 64px;">
					</c:if>
				</div>
				<div
					style="padding-left: 60px; padding-top: 8px; margin-left: 20px;">
					<div
						style="font-size: 18px; font-weight: 700; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"
						title="${task.title}">${task.title}&nbsp;&nbsp;<code>${task.tasktypename}</code>
					</div>
					<div style="font-size: 14px; font-weight: 200; margin-top: 6px;">
						<a target="_blank"
							href="projectView/projectInfo.do?projectId=${project.id}">
							${company.name}${(!empty company)?' : ':''}${project.name}</a>&nbsp;&nbsp;
						<code>${task.gradationname}</code>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-body"
			style="margin-bottom: 0px; padding-bottom: 0px;">
			<table class="table table-striped table-bordered"
				style="margin-bottom: 0px;">
				<tbody>
					<tr>
						<th>创建时间</th>
						<td><code>
								<PF:FormatTime date="${task.ctime}"
									yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
							</code></td>
						<th>创建人</th>
						<td><code>${user.name}</code></td>
					</tr>
					<tr>
						<th>实际开始时间</th>
						<td><code>
								<PF:FormatTime date="${task.dostime}"
									yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
							</code></td>
						<th>实际结束时间</th>
						<td><code>
								<PF:FormatTime date="${task.doetime}"
									yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
							</code></td>
					</tr>
					<tr>
						<th>管理人</th>
						<td><c:forEach items="${managerUsers }" var="node">
								<code> ${node.USERNAME } </code>
							</c:forEach></td>
						<th>执行人</th>
						<td><c:forEach items="${doUsers }" var="node">
								<code> ${node.USERNAME } </code>
							</c:forEach></td>
					</tr>
					<c:if test="${!empty task.planstime||!empty task.planetime}">
						<tr>
							<th>计划开始时间</th>
							<td><code>
									<PF:FormatTime date="${task.planstime}"
										yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
								</code></td>
							<th>计划结束时间</th>
							<td><code>
									<PF:FormatTime date="${task.planetime}"
										yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
								</code></td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<div class="modal-footer">
			<c:if test="${task.pstate=='6'&& not empty views}">
				<!-- 计划任务可以选择分组 -->
				<div class="btn-group btn-group-justified btn-group-xs" role="group"
					title="设置任务分组" style="margin-top: 12px;"
					aria-label="Justified button group">
					<a onclick="updateTaskView('${task.id}','')"
						class="btn btn-default" role="button">取消分组</a>
					<c:forEach items="${views}" var="node">
						<a onclick="updateTaskView('${task.id}','${node.id}')"
							class="btn btn-success" role="button">${node.name}</a>
					</c:forEach>
				</div>
			</c:if>
			<c:if test="${isManagerUser }">
				<div style="margin-top: 12px;"
					class="btn-group btn-group-justified btn-group-xs" role="group"
					title="设置任务状态" aria-label="Justified button group">
					<!--  -->
					<a onclick="updateTaskState('${task.id}','6')"
						class="btn btn btn-default" role="button">计划</a>
					<!--  -->
					<a onclick="updateTaskState('${task.id}','1')" class="btn btn-info"
						role="button">等待</a>
					<!--  -->
					<a onclick="updateTaskState('${task.id}','2')"
						class="btn btn-warning" role="button">开始</a>
					<!--  -->
					<a onclick="updateTaskState('${task.id}','3')"
						class="btn btn-danger" role="button">执行 </a>
					<!-- 
				<a onclick="updateTaskState('${task.id}','4')"
					class="btn btn-success" role="button">完成结束</a>
				 -->
				</div>
				<div class="btn-group btn-group-justified btn-group-xs" role="group"
					style="margin-top: 12px;" aria-label="Justified button group">
					<!--  -->
					<a onclick="updateTaskState('${task.id}','0')"
						class="btn btn-primary" role="button"><i
						class="glyphicon glyphicon-trash" style="color: #ffffff;"></i>&nbsp;作废任务</a>
					<!--  -->
					<a onclick="editTask('${task.id}')" class="btn btn-primary"
						role="button"><i class="glyphicon glyphicon-pencil"
						style="color: #ffffff;"></i>&nbsp;修改任务</a>
				</div>
				<div style="margin-top: 16px;"></div>
			</c:if>

			<div style="margin: 8px;">
				<c:if test="${task.pstate=='4'}">
					<!-- <a onclick="doTheTask('${task.id}')" class="btn btn-primary"
					role="button">再处理</a> -->
					<a onclick="updateTaskState('${task.id}','3')"
						class="btn btn-primary" role="button">再处理 </a>
					<c:if test="${isManagerUser }">
						<!--  -->
						<a onclick="updateTaskState('${task.id}','5')"
							class="btn btn-primary" role="button">任务归档</a>
					</c:if>
				</c:if>

				<c:if test="${task.pstate!='4'&&task.pstate!='6'}">
					<c:if test="${isDoUser }">
						<a onclick="doTheTask('${task.id}')" class="btn btn-primary"
							role="button">立即处理Go！</a>
					</c:if>
					<c:if test="${isManagerUser }">
						<a onclick="doSendTask('${task.id}')" class="btn btn-primary"
							role="button">任务派发</a>
					</c:if>
				</c:if>
				<a target="_blank" href="projectView/taskinfo.do?taskid=${task.id}"
					class="btn btn-success" role="button">详情</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭窗口</button>
			</div>
		</div>
		<c:if test="${!empty text}">
			<div class="modal-body" id="taskFormTextBoxId"
				style="overflow: auto; border-top: 1px solid #ddd;">
				<h1 style="font-size: 18px;">任务描述</h1>
				<div class="ke-content">${text}</div>
			</div>
		</c:if>
		<c:if test="${!empty logs}">
			<c:forEach var="log" items="${logs}">
				<div class="modal-body" id="taskFormTextBoxId"
					style="overflow: auto; border-top: 1px solid #ddd;">
					<h1 style="font-size: 18px;">
						<PF:FormatTime date="${log.datekey}"
							yyyyMMddHHmmss="yyyy-MM-dd WW" />
						-日志
					</h1>
					<div class="ke-content">${log.text}</div>
				</div>
			</c:forEach>
		</c:if>
	</div>
</div>
<script>
	$(function() {
		$('#taskFormTextBoxId a').each(function(i, obj) {
			//target="_blank"
			$(obj).attr('target', '_blank');
		});
		//图片点击
		$('.ke-content img').each(function(i, obj) {
			try {
				$(obj).css("cursor", "pointer");
				var gallery = new Viewer(obj, {
					navbar : false,
					toolbar : {
						zoomIn : 4,
						zoomOut : 4,
						oneToOne : 4,
						reset : 4,
						prev : 0,
						play : {
							show : 0,
							size : 'large',
						},
						next : 0,
						rotateLeft : 4,
						rotateRight : 4,
						flipHorizontal : 4,
						flipVertical : 4
					},
					keyboard : false,
				});
			} catch (e) {
			}
		});
	});
</script>