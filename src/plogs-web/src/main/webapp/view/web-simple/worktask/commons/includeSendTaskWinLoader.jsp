<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!--点击任务后弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">任务派发</h4>
		</div>
		<div class="modal-body" style="padding-bottom: 0px;">
			<form id="sendTaskFormId">
				<div class="form-group" id="poptype2UserGroupId">
					<label for="poptype2UserId">执行人</label><select id="poptype2UserId"
						val="${douser.userid}" class="form-control">
						<option value=""></option>
						<c:forEach items="${users}" var="node">
							<option value="${node.USERID }">${node.NAME}(${node.LOGINNAME})</option>
						</c:forEach>
					</select>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<span class="alertMsgClass" id="errormessageSendboxId"></span>
			<button type="button" onclick="sendTaskToUser()"
				class="btn btn-primary">派发任务</button>
		</div>
	</div>
</div>
<script>
	$(function() {
		$('select', '#sendTaskFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
		validateInput('poptype2UserId', function(id, val, obj) {
			// 执行人
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageSendboxId', 'poptype2UserGroupId');
	});
	function sendTaskToUser() {
		if (!validate('sendTaskFormId')) {
			//$('#errormessageSendboxId').text('信息录入有误，请检查！');
		} else {
			pConfirm('是否立即派发执行任务？', function() {
				pAlert("loading...", 10000);
				$.post('worktask/sandTask.do', {
					'taskId' : '${task.id}',
					'userId' : $('#poptype2UserId').val()
				}, function(flag) {
					swal.close();
					if (flag.STATE == '0') {
						$('#taskSendFormBox').modal('hide');
						updateTaskState('${task.id}', '3');
					} else {
						pAlert(flag.MESSAGE);
					}
				}, 'json');
			});
		}
	}
</script>