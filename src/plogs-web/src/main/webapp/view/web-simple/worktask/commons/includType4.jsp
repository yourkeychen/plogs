<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- 已经完成 -->
<div class="ui-layout-north">
	<div class="plogs-view-title type4">4完成</div>
</div>
<div class="ui-layout-center">
	<div class="plogs-backbox plogs-view-tasks">
		<div class="plogs-view-type4box taskAutoHeightBox"
			style="overflow: auto; width: 100%; background-color: #f2f9ef;"></div>
	</div>
</div>
