<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<script>
var ${param.textareaId}editor;
var tooltype='${param.tooltype}';
var autoHeight=false;
	$(function() {
		var miniMenu=['fontsize','forecolor','bold','underline','removeformat','justifyleft','justifycenter','insertunorderedlist','wcpfile','source' ];
		var maxMenu=['fontsize','forecolor','bold','underline','removeformat','justifyleft','justifycenter','insertunorderedlist', 'formatblock','|','link','code','wcpimgs','wcpfile','|','source' ];
		var mobileMenu=['justifyleft','justifycenter','insertunorderedlist','wcpfile','source' ];
		if(tooltype=='max'){
			userMenu=maxMenu;
		}
		if(tooltype=='min'){
			userMenu=miniMenu;
		}
		if(tooltype=='mobile'){
			autoHeight=true;
			userMenu=mobileMenu;
		}
		${param.textareaId}editor = KindEditor
				.create(
						'textarea[id="${param.textareaId}"]',
						{
							resizeType : 1,
							afterChange : function() {
								//内容更新后
							},
							cssPath : '<PF:basePath/>text/lib/kindeditor/editInner.css',
							uploadJson : basePath
									+ 'upload/generalKindEditor.do',
							formatUploadUrl : false,
							allowPreviewEmoticons : false,
							allowImageRemote : true,
							autoHeightMode: autoHeight,
							allowImageUpload : true, 
							items : userMenu,
							afterCreate : function() {
								//粘贴的文件直接上传到后台
								pasteImgHandle(this, basePath
										+ 'upload/base64img.do');
								//禁止粘贴图片等文件:pasteNotAble(this.edit);
							},
							htmlTags : htmlTagsVar
						});
	});

	//刷新超文本内容到表单中
	function syncEditorcontent() {
		${param.textareaId}editor.sync();
	}
	//获得潮文本内容
	function getEditorcontent() {
		return ${param.textareaId}editor.html();
	}
	//获得潮文本内容
	function getEditorcontentText() {
		return ${param.textareaId}editor.text();
	}
	//设置超文本内容
	function setEditorcontent(text) {
		${param.textareaId}editor.html(text);
	}
	//禁止kindeditor中粘贴文件
	function pasteNotAble(edit) {
		var doc = edit.doc;
		var cmd = edit.cmd;
		$(doc.body).bind('paste', function(ev) {
			var $this = $(this);
			var dataItem = ev.originalEvent.clipboardData.items[0];
			if (dataItem) {
				var file = dataItem.getAsFile();
				if (file) {
					//暂时不处理，文件上传
					return false;
				}
			}
		});
	}
</script>
