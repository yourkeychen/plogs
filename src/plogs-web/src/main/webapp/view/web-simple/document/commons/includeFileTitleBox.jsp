<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div
	style="color: #999999; text-align: center; margin: auto; margin-top: 80px;">
	<div
		style="height: 128px; display: flex; justify-content: center; align-items: Center;">
		<img style="max-width: 256px; max-height: 128px;"
			src="${fileview.iconUrl}" alt="..." class="img-rounded">
	</div>
	<div>
		<h2 style="margin-left: 0px;">${fileview.file.title}</h2>
	</div>
	<div>
		<span class="label label-default"
			style="font-size: 12px; padding: 4px;">下载(${fileview.dowNum})</span>
		<span class="label label-default"
			style="font-size: 12px; padding: 4px;">预览(${fileview.viewNum})</span>
	</div>
</div>
