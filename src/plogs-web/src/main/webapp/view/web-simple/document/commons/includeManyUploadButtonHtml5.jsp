<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- 批量上传附件 -->
<script src="text/lib/fileUpload/jquery.ui.widget.js"></script>
<script src="text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="text/lib/fileUpload/jquery.fileupload.css" rel="stylesheet">
<style>
<!--
.bar {
	height: 18px;
	background: green;
}
-->
</style>
<div class="btn-group btn-group-sm" role="group" aria-label="...">
	<button type="button" onclick="loadTempFiles()" class="btn btn-default">刷新</button>
</div>
<div class="btn-group btn-group-sm" role="group" aria-label="...">
	<button type="button" data-toggle="modal" data-target="#myModal"
		class="btn btn-info">提交base64编码文件</button>
</div>
<span class="btn btn-info fileinput-button"> <span>批量上传文件</span>
	<span id="html5uploadProsess"></span> <input id="fileupload"
	type="file" name="file" multiple>
</span>
<script type="text/javascript">
	//单个文件大小
	var maxSize = parseInt("${config_doc_upload_length_max}");
	//单次上传文件数量
	var maxFileNum = 5;
	$(function() {
		var maxSizeTitle = (maxSize / 1024 / 1024).toFixed(2);
		$('#fileupload').fileupload(
				{
					url : "upload/general.do",
					dataType : 'json',
					change : function(e, data) {
						if (data.files.length > maxFileNum) {
							alert("单次上传文件数量不能大于" + maxFileNum + "个!");
							return false;
						}
						var isOK = true;
						$(data.files).each(
								function(i, obj) {
									if (obj.size > maxSize) {
										alert("文件" + obj.name
												+ "超大,请检查文件大小不能大于"
												+ maxSizeTitle + "m");
										isOK = false;
									}
								});
						return isOK;
					},
					done : function(e, data) {
						var url = data.result.url;
						var state = data.result.STATE;
						var message = data.result.MESSAGE;
						var id = data.result.id;
						var fileName = data.result.fileName;
						if (state == 1) {
							showErrorMessage(message)
							return;
						}
						addFileNode('download/Pubicon.do?id=' + id,
								decodeURIComponent(fileName), id, "fileListId",
								true);
					},
					progressall : function(e, data) {
						var progress = parseInt(data.loaded / data.total * 100,
								10);
						$('#html5uploadProsess').text(progress + '%');
						if (progress == 100) {
							$('#html5uploadProsess').hide();
						} else {
							$('#html5uploadProsess').show();
						}
					}
				});
		loadTempFiles();
		$('#base64SubmitButId').click(function() {
			submitBase64Imgs();
		});
	});

	//加载10条临时文件
	function loadTempFiles() {
		$("#fileListId").html('');
		$.post('document/temps.do', {
			cpage : 1,
			size : 5
		}, function(data) {
			$(data.fileViews).each(
					function(i, obj) {
						addFileNode(obj.iconUrl, obj.file.title, obj.file.id,
								"fileListId", true)
					});
		}, 'json');
	}
	//添加一个界面文件元素
	function addFileNode(imgUrl, fileName, fileId, domBoxId, isTempFile) {
		if (isTempFile) {
			$('.blocktip-title').hide();
		}
		var html = '<div class="col-sm-12 file-block-box"   id="file_'+fileId+'">';
		html = html + '		<div class="stream-item" >';
		html = html + '				<div class="media">';
		html = html + '					<div class="pull-left">';
		html = html + '						<img  alt="'+fileName+'" src="'+imgUrl+'">';
		html = html + '					</div>';
		html = html + '					<div class="media-body" >';
		if (isTempFile) {
			html = html
					+ '<i onclick="removeFile(\''
					+ fileId
					+ '\');" title="删除临时文件" class="glyphicon glyphicon-remove pull-right" ></i>'
					+ '<i onclick="submitFile(\''
					+ fileId
					+ '\');" title="提交持久化" class="glyphicon glyphicon-ok pull-right" ></i>';
		} else {
			html = html
					+ '<i onclick="viewFile(\''
					+ fileId
					+ '\');" title="查看该文件" class="glyphicon glyphicon-eye-open pull-right" ></i>';
		}
		html = html + '						<div class="file-title" >' + fileName + '</div>';
		html = html + '					</div>';
		html = html + '				</div>';
		html = html + '		</div>';
		html = html
				+ '<input type="hidden" name="fileId" value="'+fileId+'" /></div>';
		$('#' + domBoxId).append(html);
	}
	//提交一个文件为持久状态
	function submitFile(fileId) {
		$.post('document/submit.do', {
			id : fileId
		}, function(data) {
			if (data.STATE == 0) {
				hideFileBox(fileId);
				loadPersistFiles();
			} else {
				showErrorMessage(data.MESSAGE);
			}
		}, 'json');
	}
	//删除一个文件
	function removeFile(fileId) {
		$.post('document/del.do', {
			id : fileId
		}, function(data) {
			if (data.STATE == 0) {
				hideFileBox(fileId);
			} else {
				showErrorMessage(data.MESSAGE);
			}
		}, 'json');
	}
	//刪除一個附件圖標元素
	function hideFileBox(fileid) {
		$("#file_" + fileid).remove();
		//判断附件是否上传
		if ($("input[name='fileId']").length == 0) {
			$('.blocktip-title').show();
		}
	}
	//顯示错误消息弹框
	function showErrorMessage(message) {
		Modal.alert({
			msg : message,
			title : '错误',
			btnok : '确定',
			btncl : '取消'
		});
	}

	//提交base64位编码图片
	function submitBase64Imgs() {
		if (!isnull($('#base64FileName').val())
				&& !isnull($('#base64FileCode').val())) {
			$.post('upload/base64img.do', {
				'base64' : $('#base64FileCode').val(),
				'filename' : $('#base64FileName').val()
			}, function(data) {
				$('#myModal').modal('hide')
				var url = data.url;
				var state = data.STATE;
				var message = data.MESSAGE;
				var id = data.id;
				var fileName = data.fileName;
				if (state == 1) {
					showErrorMessage(message)
					return;
				}
				addFileNode('download/Pubicon.do?id=' + id,
						decodeURIComponent(fileName), id, "fileListId", true);
			}, 'json');
		} else {
			alert("请录入base64文件名称和编码!");
		}
	}
	//表单是否为空
	function isnull(val) {
		var str = val.replace(/(^\s*)|(\s*$)/g, '');//去除空格;
		if (str == '' || str == undefined || str == null) {
			return true;
		} else {
			return false;
		}
	}
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-9999" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">录入base64编码的图片</h4>
			</div>
			<div class="modal-body">
				<input id="base64FileName" type="text" class="form-control"
					placeholder="录入文件名称"> <br />
				<textarea id="base64FileCode" class="form-control"
					placeholder="请录入base64编码文件" style="width: 100%; height: 200px;"></textarea>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button id="base64SubmitButId" type="button" class="btn btn-primary">提交</button>
			</div>
		</div>
	</div>
</div>