<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="panel panel-info">
	<div class="panel-heading " style="height: 50px;">
		持久化文件&nbsp;(显示最近5条) 
		<div style="float: right;">
			<div class="btn-group btn-group-sm" role="group" aria-label="...">
				<button type="button" onclick="loadPersistFiles()"
					class="btn btn-default">刷新</button>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<div id="persisteTipId"
			class="center-block text-center blocktip-title">
			<div style="text-align: center;">加载中...</div>
		</div>
		<div class="row" id="PersistFileListId" style="padding: 20px;">
			<!-- 文件列表 -->
		</div>
	</div>

</div>
<script type="text/javascript">
	$(function() {
		loadPersistFiles();
	});
	//加载10条持久化文件
	function loadPersistFiles() {
		$("#PersistFileListId").html('');
		$.post('document/persists.do', {
			cpage : 1,
			size : 5
		}, function(data) {
			$('#persisteTipId').hide();
			$(data.fileViews).each(
					function(i, obj) {
						addFileNode(obj.iconUrl, obj.file.title, obj.file.id,
								"PersistFileListId")
					});
		}, 'json');
	}
	//查看某文件
	function viewFile(fileId) {
		window.location="document/view.do?fileid="+fileId;
	}
</script>