<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>文件上传- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<!-- /.carousel -->
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading center-block webfile-buttonplus"
							style="height: 50px;">
							临时文件&nbsp;(显示最近5条)
							<div style="float: right;">
								<jsp:include page="commons/includeManyUploadButtonHtml5.jsp"></jsp:include>
							</div>
						</div>
						<div class="panel-body">
							<div id="fileQueue">
								<div class="center-block text-center blocktip-title">
									<i class="glyphicon glyphicon-paperclip"></i> 请上传文件 <br /> <span>
										允许上传附件类型为 <b> <PF:ParameterValue
												key="config.doc.upload.types" />
									</b> ，最大<%=Long.valueOf(FarmParameterService.getInstance().getParameter("config.doc.upload.length.max"))
					/ 1024 / 1024%>M
									</span>
								</div>
							</div>
							<div class="row" id="fileListId" style="padding: 20px;">
								<!-- 文件列表 -->
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<jsp:include page="commons/includePersistFiles.jsp"></jsp:include>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>