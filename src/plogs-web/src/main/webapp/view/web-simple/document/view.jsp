<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>文件信息- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="logoLeftBoxId">
					<ol class="breadcrumb">
						<li><a>wdap</a></li>
						<li><a>${fileview.file.title}</a></li>
						<li class="active">文件</li>
					</ol>
					<jsp:include page="commons/includeFileTitleBox.jsp"></jsp:include>
				</div>
				<div class="col-md-8" style="border-left: 1px dashed #cccccc;">
					<jsp:include page="../operat/includeFileOperatBox.jsp"></jsp:include>
					<table class="table table-bordered  table-striped"
						style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
						<thead>
							<tr>
								<th>属性</th>
								<th>值</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2" style="background-color: #dddddd; padding: 1px;"></td>
							</tr>
							<tr>
								<td style="min-width: 70px;">ID</td>
								<td style="word-break: break-all;">${fileview.file.id}</td>
							</tr>
							<tr>
								<td>名称</td>
								<td style="word-break: break-all;">${fileview.file.title}</td>
							</tr>
							<tr>
								<td>文件类型</td>
								<td style="word-break: break-all;">${fileview.file.exname}</td>
							</tr>
							<tr>
								<td>大小</td>
								<td style="word-break: break-all;">${fileview.sizelabel}</td>
							</tr>
							<tr>
								<td style="min-width: 70px;">状态</td>
								<td>${fileview.statelabel}</td>
							</tr>
							<tr>
								<td colspan="2" style="background-color: #dddddd; padding: 1px;"></td>
							</tr>
							<tr>
								<td>文件名称</td>
								<td style="word-break: break-all;">${fileview.file.filename}</td>
							</tr>
							<tr>
								<td>相对目录</td>
								<td style="word-break: break-all;">${fileview.file.relativepath}</td>
							</tr>
							<tr>
								<td>绝对地址</td>
								<td style="word-break: break-all;">${fileview.realpath}</td>
							</tr>
							<tr>
								<td>物理状态</td>
								<td style="word-break: break-all;">${fileview.physicsExist}</td>
							</tr>
							<tr>
								<td colspan="2" style="background-color: #dddddd; padding: 1px;"></td>
							</tr>
							<tr>
								<td>文件模型</td>
								<td style="word-break: break-all;">${fileview.modellabel}</td>
							</tr>
							<tr>
								<td>资源库</td>
								<td style="word-break: break-all;">${fileview.resource.title}</td>
							</tr>
							<tr>
								<td>校验码</td>
								<td style="word-break: break-all;">${fileview.file.secret}</td>
							</tr>
							<tr>
								<td>来源</td>
								<td style="word-break: break-all;">${fileview.file.sysname}</td>
							</tr>
							<tr>
								<td>创建时间</td>
								<td style="word-break: break-all;"><PF:FormatTime
										date="${fileview.file.ctime}"
										yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
							</tr>
							<tr>
								<td>创建者</td>
								<td style="word-break: break-all;">${fileview.file.cusername}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>