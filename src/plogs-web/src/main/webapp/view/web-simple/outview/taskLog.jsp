<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>任务日志- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content { 
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="logoLeftBoxId">
					<div
						style="color: #999999; text-align: center; margin: auto; margin-top: 80px;">
						<div
							style="height: 128px; display: flex; justify-content: center; align-items: Center;">
							<img style="max-width: 256px; max-height: 128px;"
								src="${fileview.iconUrl}" alt="..." class="img-rounded">
						</div>
						<div>
							<h2 style="margin-left: 0px;">${fileview.file.title}</h2>
						</div>
					</div>
				</div>
				<div class="col-md-8" style="border-left: 1px dashed #cccccc;">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped"
								style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
								<tr>
									<th>原始文件</th>
									<td>${omodel.title}</td>
								</tr>
								<tr>
									<th>目标文件</th>
									<td>${tmodel.title}</td>
								</tr>
								<tr>
									<th>转换器</th>
									<td>${convertor.title}[${convertor.classkey}]</td>
								</tr>
								<tr>
									<th>入队时间</th>
									<td><PF:FormatTime date="${task.ctime}"
													yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
								</tr>
								<tr>
									<th>开始时间</th>
									<td><PF:FormatTime date="${task.stime}"
													yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
								</tr>
								<tr>
									<th>结束时间</th>
									<td><PF:FormatTime date="${task.etime}"
													yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
								</tr>
							</table>
							<table class="table table-bordered  table-striped"
								style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
								<thead>
									<tr>
										<th>日志时间</th>
										<th>日志内容</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${logs}" var="node">
										<tr>
											<td><PF:FormatTime date="${node.ctime}"
													yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
											<td>${node.pcontent}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<c:forEach items="${viewdocs}" var="node">
							<div class="col-sm-3">
								<div class="thumbnail">
									<div
										style="height: 64px; margin-top: 20px; text-align: center;">
										<img
											style="max-height: 64px; max-width: 128px; display: block; margin: auto;"
											src="${node.ICONURL}">
									</div>
									<div class="caption">
										<div style="text-align: center;">
											<span>&nbsp;${node.MODELTITLE}</span>
										</div>
										<div class="btn-group btn-group-justified  btn-group-xs"
											role="group" style="margin-top: 4px;"
											aria-label="Justified button group">
											<c:if test="${node.VIEWIS=='1'&&node.VIEWURL!=null}">
												<a href="${node.VIEWURL}" class="btn btn-default"
													role="button">预览</a>
											</c:if>
											<c:if test="${node.DOWNURL!=null}">
												<a href="${node.DOWNURL}" class="btn btn-default"
													role="button">下载</a>
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>

				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>