<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>任务信息- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4" id="logoLeftBoxId">
					<div
						style="color: #999999; text-align: center; margin: auto; margin-top: 80px;">
						<div
							style="height: 128px; display: flex; justify-content: center; align-items: Center;">
							<img style="max-width: 256px; max-height: 128px;"
								src="${fileview.iconUrl}" alt="..." class="img-rounded">
						</div>
						<div>
							<h2 style="margin-left: 0px;">${fileview.file.title}</h2>
						</div>
					</div>
				</div>
				<div class="col-md-8" style="border-left: 1px dashed #cccccc;">
					<div class="row">
						<div class="col-sm-12" >
							<table class="table table-bordered  table-striped"
								style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
								<thead>
									<tr>
										<th>模型</th>
										<th>入队时间</th>
										<th>状态</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${viewtasks}" var="node">
										<tr>
											<td>${node.DTITLE}</td>
											<td>${node.CTIME}</td>
											<td>${node.STATETITLE}</td>
											<td style="width: 80px;"><a
												href="include/Publog.do?taskid=${node.ID}"
												class="btn btn-default btn-xs" role="button">查看信息</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered  table-striped"
								style="background-color: #ffffff; font-size: 12px; color: #999999;">
								<thead>
									<tr>
										<th>属性</th>
										<th>值</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="2"
											style="background-color: #dddddd; padding: 1px;"></td>
									</tr>
									<tr>
										<td style="min-width: 70px;">ID</td>
										<td style="word-break: break-all;">${fileview.file.id}</td>
									</tr>
									<tr>
										<td>名称</td>
										<td style="word-break: break-all;">${fileview.file.title}</td>
									</tr>
									<tr>
										<td>文件类型</td>
										<td style="word-break: break-all;">${fileview.file.exname}</td>
									</tr>
									<tr>
										<td>大小</td>
										<td style="word-break: break-all;">${fileview.sizelabel}</td>
									</tr>
								</tbody>
							</table>
							<table class="table table-bordered  table-striped"
								style="background-color: #ffffff; font-size: 12px; color: #999999;">
								<tr>
									<td style="min-width: 70px;">文本信息</td>
									<td>${filetext.completeis=='1'?'有':'无'}</td>
								</tr>
								<tr>
									<td>信息来源</td>
									<td>${filetext.pcontent}</td>
								</tr>
								<tr>
									<td>内容</td>
									<td><div style="max-height: 200px; overflow: auto;">${filetext.filetext}</div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>