<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>文件预览- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="commons/head.jsp"></jsp:include><jsp:include
		page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 hidden-xs" id="logoLeftBoxId">
					<div
						style="color: #999999; text-align: center; margin: auto; margin-top: 60px;">
						<div>
							<img style="max-width: 96px; max-height: 96px;"
								src="view/web-simple/atext/png/icon" alt="..."
								class="img-rounded">
						</div>
						<div style="font-size: 18px;">
								<PF:ParameterValue key="config.sys.title" /> 
						</div>
					</div> 
				</div>
				<div class="col-sm-8"
					style="border-left: 1px dashed #cccccc; padding-top: 60px;">
					<div class="row">
						<c:forEach items="${viewdocs}" var="node">
							<c:if test="${node.VIEWIS=='1'&&node.VIEWURL!=null}">
								<div class="col-sm-12" style="margin: auto; text-align: center;">
									<div class="thumbnail">
										<div
											style="height: 64px; margin-top: 20px; text-align: center;">
											<img
												style="max-height: 64px; max-width: 128px; display: block; margin: auto;"
												src="${node.ICONURL}">
										</div>
										<div class="caption">
											<div style="text-align: center;">
												<span>&nbsp;${node.MODELTITLE}</span>
											</div>
											<div class="btn-group btn-group-justified  btn-group"
												role="group" style="margin-top: 4px;"
												aria-label="Justified button group">
												<a href="${node.VIEWURL}" class="btn btn-default"
													role="button">预览</a>
											</div>
										</div>
									</div>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<script type="text/javascript">
		$(function() {
			$(window).resize(function() {
				$('.containerbox').css('min-height', $(window).height() - 170);
			});
			$('.containerbox').css('min-height', $(window).height() - 170);
		});
	</script>
</body>
</html>