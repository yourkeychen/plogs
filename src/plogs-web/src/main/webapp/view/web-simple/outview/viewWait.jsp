<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="/view/conf/wdaptag.tld" prefix="WDAP"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>文件预览- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}
</style>
</head>
<body>
	<jsp:include page="commons/head.jsp"></jsp:include><jsp:include
		page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12" id="logoLeftBoxId">
					<div
						style="color: #999999; text-align: center; margin: auto; margin-top: 60px;">
						<div>
							<img style="max-width: 96px; max-height: 96px;"
								src="view/web-simple/atext/png/icon" alt="..."
								class="img-rounded">
						</div>
						<div style="font-size: 18px;">
							(刷新倒计时<span id="waitTime"></span>)<br /> 预览文件生成中,请等待....
						</div>
						<div style="font-size: 18px; padding-top:20px;">
							<WDAP:viewNotAble fileid="${fileid}">
								<a href="download/Pubfile.do?id=${fileid}&secret=${secret}"><i
									class="glyphicon glyphicon-cloud-download"></i> 当前文件无法预览请下载后查看</a>
							</WDAP:viewNotAble>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<table class="table table-bordered  table-striped"
							style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
							<thead>
								<tr>
									<th>模型</th>
									<th>入队时间</th>
									<th>状态</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${viewtasks}" var="node">
									<tr>
										<td>${node.DTITLE}</td>
										<td>${node.CTIME}</td>
										<td>${node.STATETITLE}</td>
										<td style="width: 80px;"><a
											href="include/Publog.do?taskid=${node.ID}"
											class="btn btn-default btn-xs" role="button">查看信息</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<script type="text/javascript">
		$(function() {
			$(window).resize(function() {
				$('.containerbox').css('min-height', $(window).height() - 170);
			});
			$('.containerbox').css('min-height', $(window).height() - 170);
		});
	</script>
	<script language="javascript" type="text/javascript">
		var waitTime = 3;
		$(function() {
			$('#waitTime').text(waitTime);
			setInterval("redirect()", 1000);
		});
		function redirect() {
			if (waitTime >= 0) {
				$('#waitTime').text(waitTime);
			}
			if (waitTime < 0) {
				//include/Pubview.do?fileid=4028b8816c2403e1016c240dd3000043
				location.href = 'include/Pubview.do?fileid=${fileid}';
			}
			waitTime--;
		}
	</script>
</body>
</html>