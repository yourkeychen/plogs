<%@page import="com.farm.wcp.util.ThemesUtil"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

.taskFileNodeBox {
	margin: 4px;
	text-align: left;
	float: left;
	clear: right;
	border: 1px dashed #d9534f;
	padding: 8px;
	background-color: #ffffff;
	padding: 8px;
	text-align: left;
}

.fileIcon {
	margin: auto;
	width: 64px;
	height: 64px;
}
code{
	color: #999999;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px; overflow: hidden;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row" style="margin-top: 50px;">
						<div class="col-lg-2"></div>
						<div class="col-lg-8">
							<form action="fileSearch/search.do" id="searchFormId"
								method="post">
								<div class="input-group ">
									<input type="hidden" id="taskTypeIdInputId" name="taskTypeId"
										value="${taskTypeId}"> <input type="hidden"
										id="exnameInputId" name="exname" value="${exname}"> <input
										type="text" class="form-control" id="nameInputId" name="name"
										placeholder="请输入关键字..." value="${name}"> <input
										type="hidden" id="pageInputId" name="page" value="${page}">
									<div class="input-group-btn">
										<button class="btn btn-default" onclick="doSearch()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
										<button class="btn btn-default" onclick="clearSearch()">
											<i class="glyphicon glyphicon-refresh"></i>
										</button>
										<button type="button" class="btn btn-default dropdown-toggle"
											data-toggle="dropdown" aria-haspopup="true"
											aria-expanded="false">
											文件类型<span id="exnameTitleId"></span> &nbsp;<span
												class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right"
											style="max-height: 200px; overflow: auto;">
											<c:forEach items="${exnames}" var="node">
												<li><a onclick="choooseFileExName('${node}')">${node}</a></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</form>
						</div>
						<div class="col-lg-2"></div>
					</div>
					<div class="row" style="margin-top: 20px; padding: 10px;">
						<c:if test="${empty result.resultList}">
							<div class="table-responsive"
								style="text-align: center; padding: 50px;">
								<img alt="" src="text/img/plogs/searchBack.png">
							</div>
							<c:if test="${!empty message}">
								<div style="color: red;">${message}</div>
							</c:if>
						</c:if>
						<c:if test="${result.totalPage>0}">
							<c:forEach items="${result.resultList}" var="node">
								<c:if test="${node.FILEID!=null&&node.FILEID!=''}">
									<div class="col-sm-12">
										<div class="taskFileNodeBox">
											<table style="table-layout: fixed; width: 100%;">
												<tr>
													<td rowspan="2" width="72"><img
														class="fileIcon img-rounded"
														src="download/Pubicon.do?id=${node.FILEID}" /></td>
													<td
														style="overflow: hidden; padding: 4px; white-space: nowrap; text-overflow: ellipsis;">
														<span title="${node.FILENAME}" style="color: #555555;">${node.FILENAME}</span>
														<code>${node.EXNAME}</code> <code>
															<c:if test="${node.SOURCETYPE=='1'}">任务描述</c:if>
															<c:if test="${node.SOURCETYPE=='2'}">描述附件</c:if>
															<c:if test="${node.SOURCETYPE=='3'}">任务日志</c:if>
															<c:if test="${node.SOURCETYPE=='4'}">日志附件</c:if>
														</code> &nbsp;<code>${node.TASKNAME}</code>
														<div style="float: right; font-size: 12px;">
															<!-- 预览地址 -->
															<WDAP:viewAble fileid="${node.FILEID}">
																<%
																	if (ThemesUtil.isMobile(request)) {
																%>
																<!-- 移動端 -->
																<a href="include/Pubview.do?fileid=${node.FILEID}">
																	预览附件</a>
																<%
																	} else {
																%>
																<!-- PC端 -->
																<a target="_blank"
																	href="include/Pubview.do?fileid=${node.FILEID}">
																	预览附件</a>
																<%
																	}
																%>
															</WDAP:viewAble>
															<!-- 下载地址： -->
															<a
																href="download/Pubfile.do?id=${node.FILEID}&secret=${node.SECRET}">
																下载附件</a> <a
																href="projectView/taskinfo.do?taskid=${node.TASKID}">
																查看任务</a> <a
																href="projectView/projectInfo.do?projectId=${node.PROJECTID}">
																查看项目</a>
													</td>
												</tr>
												<tr>
													<td><div
															style="font-size: 12px; font-weight: 200; color: #9f9f9f;">
															${node.TEXT}</div></td>
												</tr>
											</table>
										</div>
									</div>
								</c:if>
							</c:forEach>
						</c:if>
					</div>
					<c:if test="${!empty result.resultList}">
						<div style="text-align: center;">
							<nav aria-label="Page navigation"
								style="margin-bottom: 0px; padding-bottom: 0px;">
								<ul class="pagination">
									<c:if test="${result.currentPage >1}">
										<li><a
											href="javascript:doSearch(${result.currentPage-1})"
											aria-label="Previous"> <span aria-hidden="true">上一页</span>
										</a></li>
									</c:if>
									<c:if test="${result.currentPage <result.totalPage}">
										<li><a
											href="javascript:doSearch(${result.currentPage+1})"
											aria-label="Next"> <span aria-hidden="true">下一页</span>
										</a></li>
									</c:if>
								</ul>
							</nav>
							<div style="color: #9f9f9f;">当前第${result.currentPage}页,共${result.totalPage}页</div>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {
		choooseFileExName($('#exnameInputId').val());
	});
	function doSearch(page) {
		if (page) {
			$('#pageInputId').val(page);
		}
		$('#searchFormId').submit();
	}
	function choooseFileExName(exname) {
		if (exname) {
			$('#exnameTitleId').html(":&nbsp;" + exname);
			$('#exnameInputId').val(exname);
		}
	}
	function clearSearch() {
		$('#exnameTitleId').html("");
		$('#exnameInputId').val("");
		$('#nameInputId').val("");
		doSearch();
	}
	function doSort(uuid) {
		$('#fieldUuidId').val(uuid);
		if ($('#sortTypeId').val() == 'ASC') {
			$('#sortTypeId').val('DESC');
		} else {
			$('#sortTypeId').val('ASC');
		}
		doSearch();
	}
</script>
</html>