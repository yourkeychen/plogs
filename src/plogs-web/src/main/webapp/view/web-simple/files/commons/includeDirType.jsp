<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<script type="text/javascript" src="text/javascript/wcpTypes.js"></script>
<style>
.wcp_pkm_userinfos {
	border-bottom: 1px solid #ccc;
}

.wcp_pkm_userphoto {
	float: left;
	width: 80px;
	height: 80px;
}

.wcp_pkm_info {
	float: left;
	clear: right;
	padding-left: 20px;
	max-width: 170px;
}

.wcp_pkm_title {
	font-weight: 700;
}

.typeSort {
	font-size: 12px;
	color: #ccc;
	margin-left: 20px;
}
</style>
<!-- 分类树 -->
<div class="modal fade bs-example-modal-sm" id="DirTypeTreeId"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">选择文件夹</h4>
			</div>
			<div class="modal-body">
				<div id="loadingDivId">加载分类中...</div>
				<ul id="docRoottypeUl" class="doctypeUl"></ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var type_collapse_level = 3;
	var types;
	$(function() {
		$.post('dir/types.do', {}, function(flag) {
			loadPrivateTypes(flag);
		});
	});
	function loadPrivateTypes(flag) {
		var typeobj = $.parseJSON(flag);
		//所有分类数据
		types = typeobj.types;
		if (types.length == 0) {
			//无分类数据
			$('#loadingDivId')
					.html(
							"<div class='alert alert-warning'>无分类信息<br/>请在后台添加分类!</div>");
		} else {
			//有分类数据
			//加载第一层和第二次分类
			loadTypes('#docRoottypeUl', "00000000000000000000000000000000",
					true, 1);
		}
	}

	//点击分类执行的事件
	function clickType(obj) {
		if (obj.id) {
			chooseDirTypeHandle(obj.id);
		}
	}
</script>