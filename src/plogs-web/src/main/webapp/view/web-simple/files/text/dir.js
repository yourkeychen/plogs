var basepath, cdirId;

// 获得多选的文件或文件夹ID集合
function getchooseFileAndDirIds() {
	var ids = '';
	$('.deskFile').each(function(i, obj) {
		if ($(obj).hasClass("active")) {
			var type = '';
			if ($(obj).hasClass("wfsDir")) {
				type = 'DIR-';
			}
			if ($(obj).hasClass("wfsFile")) {
				type = 'FILE-';
			}
			ids = ids + "," + type + $(obj).attr('id');
		}
	});
	return ids;
}

// 重置桌面，清空选中文件或文件夹
function resetDesk() {
	$('.deskFile').removeClass("active");
	// loadDirInfo(cdirId);
}

// 单击文件
function clickFile(obj, e, isOpen) {
	if (clickFlag) {// 取消上次延时未执行的方法
		clickFlag = clearTimeout(clickFlag);
	}
	var fileid = $(obj).attr('id');
	clickFlag = setTimeout(function() {
		if (!e.shiftKey) {
			// 单选单击
			$('.deskFile').removeClass("active");
			$(obj).addClass("active");
			if (isOpen) {
				openOperatorWin('wfile/fileInfo.do?wfileid=' + fileid);
			}
		} else {
			// 多选单击
			$(obj).addClass("active");
			clickManyDirAndFile(isOpen);
		}
	}, 200);
	if (e && e.stopPropagation) { // 非IE
		e.stopPropagation();
	} else { // IE
		window.event.cancelBubble = true;
	}
}
// 选中多个文件或文件夹
function clickManyDirAndFile(isOpen) {
	if (isOpen) {
		openOperatorWin('dir/manyFileInfo.do?fileids='
				+ getchooseFileAndDirIds());
	}
}

// 双击文件
function dbClickFile(obj, e) {
	if (clickFlag) {// 取消上次延时未执行的方法
		clickFlag = clearTimeout(clickFlag);
	}
	var fileid = $(obj).attr('id');
	// 下载或预览文件
	window.open(basepath + 'wfile/intoFile.do?wfileid=' + fileid, "_blank");
}
// 双击文件夹
function dbClickDir(obj, e) {
	if (clickFlag) {// 取消上次延时未执行的方法
		clickFlag = clearTimeout(clickFlag);
	}
	var dirid = $(obj).attr('id');
	intoDir(dirid);
}
// 单击文件夹
function clickDir(obj, e, isOpen) {
	if (clickFlag) {// 取消上次延时未执行的方法
		clickFlag = clearTimeout(clickFlag);
	}
	var dirid = $(obj).attr('id');
	clickFlag = setTimeout(function() {
		if (!e.shiftKey) {
			// 单选单击
			$('.deskFile').removeClass("active");
			$(obj).addClass("active");
			if (isOpen) {
				loadDirInfo(dirid);
			}
		} else {
			// 多选单击
			$(obj).addClass("active");
			clickManyDirAndFile(isOpen);
		}
	}, 200);
	if (e && e.stopPropagation) { // 非IE
		e.stopPropagation();
	} else { // IE
		window.event.cancelBubble = true;
	}
}

// 進入文件夾
function intoDir(dirId) {
	window.location = basepath + "dir/dir.do?dirid=" + dirId;
}

// 加载文件夹信息
function loadDirInfo(dirId) {
	openOperatorWin('dir/dirInfo.do?dirid=' + dirId + '&cdirid=' + cdirId);
}
// 打开文件夹信息表单
function openCreatDirFrom(dirid, parentid) {
	if (!dirid) {
		dirid = "";
	}
	var url = 'dir/form.do?dirid=' + dirid + "&parentid=" + parentid;
	loadForm(url);
}
// 打开文件编辑表单
function openFileEditForm(wfileid) {
	var url = "wfile/form.do?wfileid=" + wfileid;
	loadForm(url)
}
// 加载并打开一个表单
function loadForm(url) {
	$('#deskFormWinId').modal('show')
	$('#dirFormContentId').html("<div class='loadDiv'>loading...<div>");
	$('#dirFormContentId').load(url);
}
// 刪除一個文件夾
function delDir(dirid) {
	if (confirm("是否刪除文件夾，刪除后將不可恢復?")) {
		$.post('dir/delDir.do', {
			'dirid' : dirid
		}, function(flag) {
			if (flag.STATE == '0') {
				$("#" + dirid).remove();
				resetDesk();
				closeOperatorWin();
			} else {
				alert(flag.MESSAGE);
			}
		}, 'json');
	}
}
function delManyFile(dirid) {
	if (confirm("是否刪除文件夾，刪除后將不可恢復?")) {
		$.post('dir/delDir.do', {
			'dirid' : dirid
		}, function(flag) {
			if (flag.STATE == '0') {
				window.location.reload();
				closeOperatorWin();
			} else {
				alert(flag.MESSAGE);
			}
		}, 'json');
	}
}

// 删除一个文件
function delWfile(wfileid) {
	if (confirm("是否刪除文件，刪除后將不可恢復?")) {
		$.post('wfile/delWFile.do', {
			'wfileids' : wfileid
		}, function(flag) {
			if (flag.STATE == '0') {
				$("#" + wfileid).remove();
				resetDesk();
				closeOperatorWin();
			} else {
				alert(flag.MESSAGE);
			}
		}, 'json');
	}

}
// 打开文件移动窗口
function openMoveFileWin(wfileid) {
	$('#DirTypeTreeId').modal('show');
	chooseDirTypeHandle = function(toDirid) {
		$.post('wfile/move.do', {
			'wfileid' : wfileid,
			'toDirid' : toDirid
		}, function(flag) {
			if (flag.STATE == '0') {
				window.location.reload();
			} else {
				alert(flag.MESSAGE);
			}
		}, 'json');
	};
}
// 打开文件夹分类树窗口
function openMoveDirWin(dirid) {
	$('#DirTypeTreeId').modal('show');
	chooseDirTypeHandle = function(toDirid) {
		$.post('dir/move.do', {
			'dirid' : dirid,
			'toDirid' : toDirid
		}, function(flag) {
			if (flag.STATE == '0') {
				window.location.reload();
			} else {
				alert(flag.MESSAGE);
			}
		}, 'json');
	};
}
// 树形目录中打开一个目录
function chooseDirOpenWin() {
	$('#DirTypeTreeId').modal('show');
	chooseDirTypeHandle = function(dirid) {
		intoDir(dirid)
	};
}

//打开操作窗口
function openOperatorWin(url) {
	$('#logoLeftBoxId').html("<div class='loadDiv'>loading...<div>");
	$('#logoLeftBoxId').load(url, {}, function() {
		$('#myModal').modal('show');
	});
}
//关闭窗口
function closeOperatorWin() {
	$('#myModal').modal('hide');
}