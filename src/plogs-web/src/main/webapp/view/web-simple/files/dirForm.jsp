<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<c:if test="${dir.id==null}">
	<span id="loadOpTitleId" style="display: none;">新建文件夹</span>
</c:if>
<c:if test="${dir.id!=null}">
	<span id="loadOpTitleId" style="display: none;">编辑文件夹</span>
</c:if>
<form id="dirFomrId">
	<div class="form-group">
		<label for="dirNameId">名称</label> <input type="text"
			value="${dir.name}" class="form-control" id="dirNameId" name="name"
			placeholder="文件夾名称">
	</div>
	<input type="hidden" value="${dir.id}" name="id"> <input
		type="hidden" value="${parentid}" name="parentid">
	<button id="dirFormSubmitButton" type="button" class="btn btn-primary">提交</button>
</form>
<script>
	$(function() {
		$('#dirFormTitleId').text($('#loadOpTitleId').text());
		validateInput('dirNameId', function(id, val, obj) {
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 64)) {
				return {
					valid : false,
					msg : '长度不能大于' + 64
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		$('#dirFormSubmitButton').bind('click', function() {
			var parmArray = $('#dirFomrId').serializeArray();
			var parmStringNew = {};
			$.each(parmArray, function(index, data) {
				var name = data.name;
				var value = data.value;
				parmStringNew[name] = value;
			});
			if (validate('dirFomrId')) {
				if (confirm("是否提交表单?")) {
					$.post('dir/formSubmit.do', parmStringNew, function(flag) {
						if (flag.STATE == '0') {
							window.location.reload();
						} else {
							alert(flag.MESSAGE);
						}
					}, 'json');
				}
			}
		});
	});
</script>