<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div
	style="color: #999999; text-align: center; margin: auto;">
	<div>
		<c:if test="${dir.id=='NONE'}">
			<img style="max-width: 128px; max-height: 128px;"
				src="view/web-simple/atext/png/icon" alt="..." class="img-rounded">
		</c:if>
		<c:if test="${dir.id=='SEARCH'}">
			<img style="max-width: 128px; max-height: 128px;"
				src="text/img/filesearch.png" alt="..." class="img-rounded">
		</c:if>
		<c:if test="${dir.id!='NONE'&&dir.id!='SEARCH'}">
			<img style="max-width: 128px; max-height: 128px;"
				src="text/img/diricon.png" alt="..." class="img-rounded">
		</c:if>
	</div>
	<c:if test="${dir.id=='NONE'||cdirid==dir.id}">
		<div>
			<h1>${dir.name}</h1>
		</div>
	</c:if>
	<c:if test="${dir.id!='NONE'&&cdirid!=dir.id}">
		<table class="table table-bordered  table-striped"
			style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
			<tbody>
				<tr>
					<td style="min-width: 100px;">类型</td>
					<td>文件夹</td>
				</tr>
				<tr>
					<td>名称</td>
					<td>${dir.name}</td>
				</tr>
				<tr>
					<td>创建时间</td>
					<td><PF:FormatTime date="${dir.ctime}"
							yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
				</tr>
				<tr>
					<td>修改时间</td>
					<td><PF:FormatTime date="${dir.etime}"
							yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
				</tr>
				<!-- <tr>
					<td>大小</td>
					<td>${dir.dirsize}</td>
				</tr> -->
			</tbody>
		</table>
		<div class="btn-group btn-group-justified btn-group-sm" role="group"
			aria-label="Justified button group with nested dropdown">
			<a href="javascript:intoDir('${dir.id}');" class="btn btn-default"
				role="button"><i class="glyphicon glyphicon-folder-open"></i> 打开</a>
			<!--  -->
			<a href="javascript:openCreatDirFrom('${dir.id}', '');"
				class="btn btn-default" role="button"><i
				class="glyphicon glyphicon-pencil"></i> 修改</a>
			<!--  -->
			<a href="javascript:openMoveDirWin('${dir.id}');"
				class="btn btn-default" role="button"><i
				class="glyphicon glyphicon-share-alt"></i> 移动</a>
			<!--  -->
			<a href="javascript:delDir('${dir.id}', '');" class="btn btn-danger"
				role="button"><i class="glyphicon glyphicon-trash"
				style="color: #ffffff;"></i> 刪除</a>
		</div>
	</c:if>
</div>