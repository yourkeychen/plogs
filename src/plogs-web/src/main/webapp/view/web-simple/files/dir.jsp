<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<link href="view/web-simple/files/text/dir.css" rel="stylesheet">
<script src="view/web-simple/files/text/dir.js"></script>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="border-left: 1px dashed #cccccc;">
					<div class="panel panel-default">
						<c:if test="${cdir.id!='SEARCH'}">
							<div class="panel-body" style="padding-bottom: 8px;">
								<div class="btn-group btn-group-justified btn-group-sm"
									role="group"
									aria-label="Justified button group with nested dropdown">
									<a href="dir/dir.do?dirid=${cdir.id}" class="btn btn-default"
										role="button"><i class="glyphicon glyphicon-refresh"></i>
										刷新</a> <a href="javascript:OpenUploadWin();"
										class="btn btn-default" role="button"><i
										class="glyphicon glyphicon-cloud-upload"></i> 上传文件</a>
									<div class="btn-group btn-group-sm" role="group">
										<a class="btn btn-default dropdown-toggle"
											data-toggle="dropdown" role="button" aria-haspopup="true"
											aria-expanded="false"><i
											class="glyphicon glyphicon-folder-close "></i>新建<span
											class="caret"></span> </a>
										<ul class="dropdown-menu">
											<li><a
												href="javascript:openCreatDirFrom('','${cdir.id}');"><i
													class="glyphicon glyphicon-plus"></i> 新建子文件夹</a></li>
											<li>
										</ul>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${cdir.id=='SEARCH'}">
							<br />
						</c:if>
						<div class="panel-body"
							style="padding-top: 8px; margin-bottom: 0px; padding-bottom: 0px;">
							<ol class="breadcrumb" style="margin-bottom: 4px;">
								<li><a href="javascript:chooseDirOpenWin();"><i
										class="glyphicon glyphicon-th"></i></a></li>
								<c:forEach items="${path}" var="dir" varStatus="status">
									<c:if test="${!status.last}">
										<li><a href="<PF:basePath/>dir/dir.do?dirid=${dir.id}">${dir.name}</a></li>
									</c:if>
									<c:if test="${status.last}">
										<li class="active">${dir.name}</li>
									</c:if>
								</c:forEach>
							</ol>
						</div>
						<div class="panel-body" id="fileDeskId"
							style="min-height: 200px; padding: 24px; padding-top: 4px;">
							<c:forEach items="${dirs}" var="dir">
								<div class="deskFile wfsDir" id="${dir.id}" title="${dir.name}">
									<img
										style="width: 72px; height: 72px; margin-left: 12px; margin-top: 12px;"
										alt="" src="text/img/diricon.png">
									<div class="title deskFileName">${dir.name}</div>
								</div>
							</c:forEach>
							<c:forEach items="${wfiles}" var="file">
								<div class="deskFile wfsFile" id="${file.id}"
									title="${file.title}">
									<img
										style="width: 72px; height: 72px; margin-left: 12px; margin-top: 12px;"
										alt="" src="${iconUrlBase}${file.wdapfileid}">
									<div class="title deskFileName">${file.title}</div>
								</div>
							</c:forEach>
						</div>
					</div>
					<div style="color: #bfbfbf;line-height: 1.5em;">
						<i class=" glyphicon glyphicon-info-sign "></i>&nbsp;使用Shift快捷鍵可以选择多个文件或文件夹;<br />
						<i class=" glyphicon glyphicon-info-sign "></i>&nbsp;双击文件夹或文件进行快捷操作;<br />
						<i class=" glyphicon glyphicon-info-sign "></i>&nbsp;鼠标右键弹出操作菜单
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">文件操作</h4>
				</div>
				<div class="modal-body" id="logoLeftBoxId"></div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var clickFlag = null;//是否点击标识（定时器编号）
		var chooseDirTypeHandle = null;
		var basepath, cdirId;
		$(function() {
			document.oncontextmenu = function() {
				return false;
			}
			basepath = '<PF:basePath/>';
			cdirId = '${cdir.id}';
			$('#fileDeskId').bind('click', function() {
				//清除選中樣式
				resetDesk();
			});
			//---------------------------------------
			$('.wfsDir').bind('click', function(e) {
				//单击文件夹，加载文件夹信息
				clickDir($(this), e, false);
			});
			$(".wfsDir").bind("contextmenu", function(e) {
				e.preventDefault();
				clickDir($(this), e, true);
			});
			$('.wfsDir').bind('dblclick ', function(e) {
				//双击文件夹，进入文件夹
				dbClickDir($(this), e)
			});
			//---------------------------------------
			$('.wfsFile').bind('click', function(e) {
				//单击文件，加载文件信息
				clickFile($(this), e, false);
			});
			$(".wfsFile").bind("contextmenu", function(e) {
				e.preventDefault();
				clickFile($(this), e, true);
			});
			$('.wfsFile').bind('dblclick ', function(e) {
				//双击文件，预览或下载
				dbClickFile($(this), e)
			});

		});
	</script>
	<jsp:include page="commons/includeFormWindow.jsp"></jsp:include>
	<jsp:include page="commons/includeDirType.jsp"></jsp:include>
	<jsp:include page="commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
</html>