<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div style="color: #999999; text-align: center; margin: auto;">
	<div>
		<img style="max-width: 128px; max-height: 128px;"
			src="${iconUrlBase}${wfile.wdapfileid}" alt="..." class="img-rounded">
	</div>
	<table class="table table-bordered  table-striped"
		style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
		<tbody>
			<tr>
				<td style="min-width: 100px;">类型</td>
				<td>${wfile.ftype}文件</td>
			</tr>
			<tr>
				<td>名称</td>
				<td>${wfile.title}</td>
			</tr>
			<tr>
				<td>大小</td>
				<td>${sizeLable}</td>
			</tr>
			<tr>
				<td>创建时间</td>
				<td><PF:FormatTime date="${wfile.ctime}"
						yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
			</tr>
			<tr>
				<td>修改时间</td>
				<td><PF:FormatTime date="${wfile.utime}"
						yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
			</tr>
		</tbody>
	</table>
	<div class="btn-group btn-group-justified btn-group-sm" role="group"
		aria-label="Justified button group with nested dropdown">
		<a href="${downUrl}" class="btn btn-default" role="button"><i
			class="glyphicon glyphicon-download-alt"></i> 下载</a>
		<c:if test="${isView}">
			<a href="${viewUrl}" target="_blank" class="btn btn-default"
				role="button"><i class="glyphicon glyphicon-eye-open"></i> 预览</a>
		</c:if>
		<!--  -->
		<div class="btn-group btn-group-sm" role="group">
			<a class="btn btn-default dropdown-toggle" data-toggle="dropdown"
				role="button" aria-haspopup="true" aria-expanded="false"><i
				class="glyphicon glyphicon-cog"></i> 編輯<span class="caret"></span> </a>
			<ul class="dropdown-menu" style="text-align: left;">
				<li><a href="javascript:openFileEditForm('${wfile.id}');"><i
						class="glyphicon glyphicon-pencil"></i> 文件重命名</a></li>
				<li>
				<li><a href="javascript:reViewFile('${wfile.wdapfileid}');"><i
						class="glyphicon glyphicon-eye-close"></i> 重置预览文件</a></li>
				<li>
				<li><a href="javascript:openMoveFileWin('${wfile.id}');"><i
						class="glyphicon glyphicon-share-alt"></i> 移动到文件夾</a></li>
				<li>
			</ul>
		</div>
		<!--  -->
		<a href="javascript:delWfile('${wfile.id}');" class="btn btn-danger"
			role="button"><i class="glyphicon glyphicon-trash"
			style="color: #ffffff;"></i> 刪除</a>
	</div>
</div>
<script>
	$(function() {
		loadDirInfoId = '${dir.id}';
	});
	function reViewFile(wdapfileid) {
		$.post('wfile/reView.do', {
			'wdapfileid' : wdapfileid
		}, function(flag) {
			if (flag.STATE == '0') {
				alert("已启动重建任务!");
			} else {
				alert(flag.MESSAGE);
			}
		}, 'json');
	}
</script>