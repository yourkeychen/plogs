<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div
	style="color: #999999; text-align: center; margin: auto;">
	<div>
		<img style="max-width: 128px; max-height: 128px;"
			src="text/img/manyfile.png" alt="..." class="img-rounded">
	</div>
	<table class="table table-bordered  table-striped"
		style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
		<tbody>
			<tr>
				<td style="min-width: 100px;">类型</td>
				<td>多个文件或文件夹</td>
			</tr>
			<tr>
				<td>文件数量</td>
				<td>${fileNum}</td>
			</tr>
			<tr>
				<td>文件夹数量</td>
				<td>${dirNum}</td>
			</tr>
		</tbody>
	</table>
	<div class="btn-group btn-group-justified btn-group-sm" role="group"
		aria-label="Justified button group with nested dropdown">
		<a href="javascript:openMoveDirWin('${fileids}');"
			class="btn btn-default" role="button"><i
			class="glyphicon glyphicon-share-alt"></i> 移动</a>
		<!--  -->
		<a href="javascript:delManyFile('${fileids}');" class="btn btn-danger"
			role="button"><i class="glyphicon glyphicon-trash"
			style="color: #ffffff;"></i> 刪除</a>
	</div>
</div>