<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<span id="loadOpTitleId" style="display: none;">修改文件</span>
<form id="fileFomrId">
	<div class="form-group">
		<label for="dirNameId">名称</label> <input type="text"
			value="${wfile.title}" class="form-control" id="fileTitleId"
			name="title" placeholder="文件名称">
	</div>
	<input type="hidden" value="${wfile.id}" name="id">
	<button id="fileFormSubmitButton" type="button" class="btn btn-primary">提交</button>
</form>
<script>
	$(function() {
		$('#dirFormTitleId').text($('#loadOpTitleId').text());
		validateInput('fileTitleId', function(id, val, obj) {
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 128)) {
				return {
					valid : false,
					msg : '长度不能大于' + 128
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		});
		$('#fileFormSubmitButton').bind('click', function() {
			var parmArray = $('#fileFomrId').serializeArray();
			var parmStringNew = {};
			$.each(parmArray, function(index, data) {
				var name = data.name;
				var value = data.value;
				parmStringNew[name] = value;
			});
			if (validate('fileFomrId')) {
				if (confirm("是否提交表单?")) {
					$.post('wfile/edit.do', parmStringNew, function(flag) {
						if (flag.STATE == '0') {
							window.location.reload();
						} else {
							alert(flag.MESSAGE);
						}
					}, 'json');
				}
			}
		});
	});
</script>