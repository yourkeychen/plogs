<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div
	style="color: #999999; text-align: center; margin: auto; margin-top: 100px;">
	<div>
		<img style="max-width: 128px; max-height: 128px;"
			src="view/web-simple/atext/png/icon" alt="..." class="img-rounded">
	</div>
	<div>
		<h1>
			<PF:ParameterValue key="config.sys.title" />
		</h1>
	</div>
</div>