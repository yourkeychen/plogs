<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="hidden-xs">
	<table class="table table-bordered table-striped " id="regularTableId"
		style="background-color: #ffffff; font-size: 12px; margin-top: 30px; color: #999999;">
		<thead>
			<tr>
				<th>原文件类型</th>
				<th>转换条件</th>
				<th>超时时间</th>
				<th>支持预览</th>
				<th>预览方式</th>
				<th>转换器</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${regulars}" var="node">
				<tr>
					<td valign="middle">${node.FILEEXNAME}</td>
					<td>${node.SIZEMIN}-${node.SIZEMAX}</td>
					<td>${node.OUTTIMENUM}</td>
					<td>${node.VIEWIS}</td>
					<td>${node.TITLE}</td>
					<td>${node.BTITLE}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<script>
	$(function() {
		table_rowspan("regularTableId", 1);
	});
	/** 
	 * @ function：合并指定表格列（表格id为table_id）指定列（列数为table_colnum）的相同文本的相邻单元格 
	 * @ param：table_id 为需要进行合并单元格的表格的id。如在HTMl中指定表格 id="data" ，此参数应为 #data 
	 * @ param：table_colnum 为需要合并单元格的所在列。为数字，从最左边第一列为1开始算起。 
	 */
	function table_rowspan(table_id, table_colnum) {
		table_firsttd = "";
		table_currenttd = "";
		table_SpanNum = 0;
		table_Obj = $("#" + table_id + " tr td:nth-child(" + table_colnum + ")");
		table_Obj.each(function(i) {
			if (i == 0) {
				table_firsttd = $(this);
				table_SpanNum = 1;
			} else {
				table_currenttd = $(this);
				if (table_firsttd.text() == table_currenttd.text()) {
					//这边注意不是val（）属性，而是text（）属性 //td内容为空的不合并 
					if (table_firsttd.text() != "") {
						table_SpanNum++;
						table_currenttd.hide();
						table_firsttd.attr("rowSpan", table_SpanNum);
					}
				} else {
					table_firsttd = $(this);
					table_SpanNum = 1;
				}
			}
		});
	};
</script>