<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!--点击任务后弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">
				<c:if test="${empty view}">添加分组视图</c:if>
				<c:if test="${!empty view}">更新分组视图</c:if>
			</h4>
		</div>
		<div class="modal-body" style="padding-bottom: 0px;">
			<form class="form-horizontal" id="addViewFormId">
				<input type="hidden" id="viewformViewId" value="${view.id}">
				<div class="form-group" id="ViewNAmeGroupId">
					<label for="ViewNAme" class="col-sm-3 control-label">名称<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="ViewNAme"
							value="${view.name}" placeholder="分组名称">
					</div>
				</div>
				<div class="form-group">
					<label for="taskState" class="col-sm-3 control-label">排序号<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="ViewSort" class="form-control" val="${view.sort}">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
						</select>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<span class="alertMsgClass" id="errormessageShowviewboxId"></span>
			<button type="button" onclick="submitTaskForm()"
				class="btn btn-primary">
				<c:if test="${empty view}">添加视图</c:if>
				<c:if test="${!empty view}">更新视图</c:if>
			</button>
		</div>
	</div>
</div>
<script>
	$(function() {
		validateInput('ViewNAme', function(id, val, obj) {
			// 名称
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 64)) {
				return {
					valid : false,
					msg : '长度不能大于' + 64
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowviewboxId', 'ViewNAmeGroupId');
		validateInput('ViewSort', function(id, val, obj) {
			// 排序
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowviewboxId', 'titleGroupId');
		$('select', '#addViewFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
	});
	//提交表单
	function submitTaskForm() {
		if (!validate('addViewFormId')) {
			$('#errormessageShowviewboxId').text('信息录入有误，请检查！');
		} else {
			$('#errormessageShowviewboxId').text('');
			pConfirm("是否提交数据?", function() {
				var name = $('#ViewNAme').val();
				var sort = $('#ViewSort').val();
				var viewid = $('#viewformViewId').val();
				$.post('workplan/viewSubmit.do', {
					'sort' : sort,
					'name' : name,
					'viewid' : viewid
				}, function(flag) {
					if (flag.STATE == '0') {
						pAlert(flag.view.name + ":任务提交成功!");
						//此處刷新
						reloadPage();
					} else {
						pAlert(flag.MESSAGE);
					}
				}, 'json');

			});
		}
	}
</script>