<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- 计划中 -->
<div class="ui-layout-north">
	<div class="plogs-view-title type1">未分组计划</div>
</div>
<div class="ui-layout-center">
	<div class="plogs-backbox plogs-view-tasks">
		<div class="plogs-view-type6box taskAutoHeightBox"
			style="overflow: auto;"></div>
	</div>
</div>
