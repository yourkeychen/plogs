<%@page import="com.farm.wcp.util.ThemesUtil"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="/view/conf/wdaptag.tld" prefix="WDAP"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>人员任务信息- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

h2 {
	color: #d9534f;
	border-left: 8px solid #d9534f;
	padding-left: 10px;
	margin-left: 0px;
	font-weight: 500;
}

h3 {
	border-left: 4px solid #d9534f;
	color: #d9534f;
	padding-left: 10px;
	margin-left: 10px;
	font-weight: 200;
	font-size: 16px;
}

code {
	margin-left: 0.5em;
}

.tip {
	font-weight: 200;
	color: #c7254e;
	font-size: 14px;
	white-space: nowrap;
}

table {
	background-color: #ffffff;
}

.taskFilesBox {
	margin-left: 20px;
}

.taskFilesBox .col-sm-4 {
	padding: 0px;
}

.taskFilesBox code {
	font-size: 0.8em;
}

.taskFileNodeBox {
	margin: 4px;
	text-align: left;
	height: 68px;
	float: left;
	clear: right;
	border: 1px dashed #d9534f;
	padding: 8px;
	background-color: #ffffff;
	padding: 8px;
	text-align: left;
}

.taskFilesBox .filename {
	white-space: nowrap;
	overflow: hidden;
	padding-top: 4px;
	margin-right: 10px;
	margin-left: 10px;
}

.fileIcon {
	margin: auto;
	width: 48px;
	height: 48px;
	width: 48px;
}

h3 code {
	font-size: 14px;
}

.panel-body .doc_vavigat_h1 {
	font-weight: 700;
	border-left: 5px solid #cb254e;
}

.taskBox {
	margin-top: 10px;
	border: 1px dashed #ccc;
	background-color: #ffffff;
	padding: 10px;
}

.taskBox a {
	color: #333333;
}

.taskBox a:HOVER {
	color: #666666;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-3 hidden-xs hidden-sm">
					<div class="panel panel-default affix-top" id="docContentMenuId"
						style="z-index: 1000; width: 230px; border: 1px dashed #ddd;">
						<div class="panel-body" id="docContentTitlelbody">
							<span class="glyphicon glyphicon-list-alt"></span> &nbsp; <b>目录</b>
						</div>
						<div class="panel-body" id="docContentPanelbody"
							style="list-style: none; padding: 4px; margin-left: -10px; border-top: 1px dashed #ddd;">
							<ul id="ContentMenuId" class="doc_vavigat"
								style="height: 310px; overflow-y: auto;">
								<li class="doc_vavigat_h1"><a
									onclick="scrollToMarkName('top0')" id="linkmark0">人员信息 </a></li>
								<li class="doc_vavigat_h1"><a
									onclick="scrollToMarkName('top1')" id="linkmark0">时间进度 </a></li>
								<li class="doc_vavigat_h1"><a
									onclick="scrollToMarkName('top2')" id="linkmark1">详细任务信息 </a></li>
								<c:forEach items="${projects}" var="project">
									<li class="doc_vavigat_h2"><a
										onclick="scrollToMarkName('project-${project.key}')"
										id="linkmark1">${project.value}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9" style="">
					<div class="row">
						<div class="col-md-12">
							<c:if test="${!empty stime&&!empty etime}">
								<h1>
									<code>查询区间[${stime}</code>
									<code>${etime}]</code>
								</h1>
							</c:if>
							<h1>${user.name}
								<a name="top0"></a>
								<code>${org.name}</code>
							</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div>
								<h2>
									<a name="top1"></a>任务时间进度
								</h2>
								<div class="table-responsive">
									<table id="taskTimeDataId"
										class="table table-striped table-bordered"
										style="font-size: 12px;">
										<tbody>
											<tr>
												<th style="width: 120px;">实际开始时间</th>
												<th style="width: 120px;">实际结束时间</th>
												<th>任务名称</th>
												<th style="width: 70px;">任务状态</th>
												<th>项目名称</th>
											</tr>
											<c:forEach items="${tasks}" var="task">
												<tr> 
													<th valign="middle"><PF:FormatTime
															date="${task.dostime }" yyyyMMddHHmmss="yyyy-MM-dd | WW" /></th>
													<td><PF:FormatTime date="${task.doetime }"
															yyyyMMddHHmmss="yyyy-MM-dd | WW" /></td>
													<td><a onclick="scrollToMarkName('task-${task.id}')">${task.title}</a></td>
													<td><c:if test="${task.pstate=='1'}">
															计划中
														</c:if> <c:if test="${task.pstate=='2'}">
															待开始
														</c:if> <c:if test="${task.pstate=='3'}">
															处理中
														</c:if> <c:if test="${task.pstate=='4'}">
															已完成
														</c:if> <c:if test="${task.pstate=='5'}">
															已关闭
														</c:if></td>
													<td>${task.pcontent}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h2>
								<a name="top2"></a> 任务详细信息
							</h2>
							<div>
								<c:forEach items="${projects}" var="project">
									<h3>
										<a name="project-${project.key}"></a>${project.value }
									</h3>
									<c:forEach items="${tasks}" var="task">
										<c:if test="${project.key==task.projectid }">
											<!-- 任务 -->
											<div class="taskBox">
												<div class="row">
													<div class="col-md-9"
														style="font-size: 14px; font-weight: 700;">
														<a name="task-${task.id}"
															href="projectView/taskinfo.do?taskid=${task.id}">
															${task.title} </a><span class="tip">
															${task.tasktypename}</span>
														<c:set var="isHaveFile" value="false"></c:set>
														<c:forEach items="${files}" var="file">
															<c:if
																test="${task.id==file.TASKID&&file.EXNAME!='ihtml'}">
																<img class="img-rounded"
																	style="height: 2em; width: 2em; position: relative; top: -2px;"
																	src="download/Pubicon.do?id=${file.FILEID}" />
																<c:set var="isHaveFile" value="true"></c:set>
															</c:if>
														</c:forEach>
													</div>
													<div class="col-md-3" style="text-align: right;">
														<span class="tip"
															style="position: relative; top: 2px; margin-right: 4px;">
															<PF:FormatTime date="${task.dostime}"
																yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
														</span>
														<c:if test="${!isHaveFile}">
															<span style="margin-right: 22px;"></span>
														</c:if>
														<div class="btn-group btn-group-xs" role="group"
															aria-label="Extra-small button group">
															<c:if test="${isHaveFile}">
																<button type="button" class="btn btn-default"
																	onclick="showTaskFile('${task.id}');">
																	<span class="glyphicon glyphicon-chevron-down"></span>
																</button>
															</c:if>
															<a href="projectView/taskinfo.do?taskid=${task.id}"
																target="_blank" type="button" class="btn btn-default">
																<span class="glyphicon glyphicon-info-sign"></span>
															</a>
														</div>
													</div>
												</div>
											</div>
											<div class="taskFilesBox row" id="taskFiles-${task.id}"
												style="margin-left: 4px; margin-right: 4px; display: none;">
												<c:forEach items="${files}" var="file">
													<c:if test="${task.id==file.TASKID&&file.EXNAME!='ihtml'}">
														<div class="col-sm-6">
															<div class="taskFileNodeBox">
																<table style="table-layout: fixed; width: 100%;">
																	<tr>
																		<td rowspan="2" width="50"><img
																			class="fileIcon img-rounded"
																			src="download/Pubicon.do?id=${file.FILEID}" /></td>
																		<td width="240"
																			style="overflow: hidden; padding: 4px; white-space: nowrap; text-overflow: ellipsis;">
																			<span title="${file.FILETITLE}"
																			style="color: #555555;">${file.FILETITLE}</span>
																		</td>
																	</tr>
																	<tr>
																		<td><code>${file.EXNAME}</code> <code>
																				<c:if test="${file.SOURCETYPE=='1'}">任务描述</c:if>
																				<c:if test="${file.SOURCETYPE=='2'}">描述附件</c:if>
																				<c:if test="${file.SOURCETYPE=='3'}">任务日志</c:if>
																				<c:if test="${file.SOURCETYPE=='4'}">日志附件</c:if>
																			</code>
																			<div style="float: right; font-size: 12px;">
																				<!-- 预览地址 -->
																				<WDAP:viewAble fileid="${file.FILEID}">
																					<%
																						if (ThemesUtil.isMobile(request)) {
																					%>
																					<!-- 移動端 -->
																					<a href="include/Pubview.do?fileid=${file.FILEID}">
																						预览</a>
																					<%
																						} else {
																					%>
																					<!-- PC端 -->
																					<a target="_blank"
																						href="include/Pubview.do?fileid=${file.FILEID}">
																						预览</a>
																					<%
																						}
																					%>
																				</WDAP:viewAble>
																				<!-- 下载地址： -->
																				<a
																					href="download/Pubfile.do?id=${file.FILEID}&secret=${file.SECRET}">
																					下载</a>
																			</div></td>
																	</tr>
																</table>
															</div>
														</div>
													</c:if>
												</c:forEach>
											</div>
										</c:if>
									</c:forEach>
								</c:forEach>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {
		$('#docContentMenuId').affix({
			offset : {
				top : 50,
				bottom : 5
			}
		})
		uniteTable('taskTimeDataId', 1);
	});
	function showTaskFile(taskId) {
		if ($('#taskFiles-' + taskId).is(":hidden")) {
			$('#taskFiles-' + taskId).show();
		} else {
			$('#taskFiles-' + taskId).hide();
		}
	}
	//滾動到指定位置
	function scrollToMarkName(markName) {
		$('html,body').animate({
			scrollTop : $("a[name='" + markName + "']").offset().top - 70
		});
	}

	function uniteTable(tableId, colLength) {//表格ID，表格列数
		var tb = document.getElementById(tableId);
		tb.style.display = '';
		var i = 0;
		var j = 0;
		var rowCount = tb.rows.length; //   行数 
		var colCount = tb.rows[0].cells.length; //   列数 
		var obj1 = null;
		var obj2 = null;
		//为每个单元格命名 
		for (i = 0; i < rowCount; i++) {
			for (j = 0; j < colCount; j++) {
				tb.rows[i].cells[j].id = "tb__" + i.toString() + "_"
						+ j.toString();
			}
		}
		//合并行 
		for (i = 0; i < colLength; i++) {
			obj1 = document.getElementById("tb__0_" + i.toString())
			for (j = 1; j < rowCount; j++) {
				obj2 = document.getElementById("tb__" + j.toString() + "_"
						+ i.toString());
				if (obj1.innerText == obj2.innerText
						&& ((obj2.innerText != "" || obj1.innerText != "") && (obj1.innerText != "-" || obj2.innerText != "-"))) {
					obj1.rowSpan++;
					obj2.parentNode.removeChild(obj2);
				} else {
					obj1 = document.getElementById("tb__" + j.toString() + "_"
							+ i.toString());
				}
			}
		}

		//合并列
		for (i = 1; i < rowCount; i++) {
			colCount = tb.rows[i].cells.length;
			obj1 = document.getElementById(tb.rows[i].cells[0].id);
			for (j = 1; j < colLength; j++) {
				if (obj1.colSpan >= colLength)
					break;
				obj2 = document.getElementById(tb.rows[i].cells[j].id);
				if (obj1.innerText == obj2.innerText
						&& ((obj2.innerText != "" || obj1.innerText != "") && (obj1.innerText != "-" || obj2.innerText != "-"))) {
					obj1.colSpan++;
					obj2.parentNode.removeChild(obj2);
				} else {
					obj1 = obj2;
				}
			}
		}
	}
</script>
</html>