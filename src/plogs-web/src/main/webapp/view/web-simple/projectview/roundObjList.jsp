<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>查询对象选择</title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.min.js"></script>
<link
	href="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.css"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px; overflow: hidden;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3" style="text-align: center;">
									<div style="width: 240px; margin: auto;">
										<div class="" id="form_dayId" style="margin: 20px;"></div>
									</div>
								</div>
								<div class="col-lg-9">
									<div class="hidden-xs hidden-sm"
										style="text-align: center; padding-bottom: 20px;">
										<div style="height: 30px; width: 100%;"></div>
										<h1>
											<c:if test="${objType=='user' }">人员</c:if>
											<c:if test="${objType=='project' }">项目</c:if>
											<c:if test="${roundUnit=='WEEK' }">周</c:if>
											<c:if test="${roundUnit=='MONTH' }">月</c:if>
											<c:if test="${roundUnit=='DAY' }">日</c:if>
											报
										</h1>
										<h1>日期:&nbsp;${stime}&nbsp;至&nbsp;${etime}</h1>
									</div>
									<c:if test="${objType=='project'}">
										<div class="row">
											<div class="col-lg-12">
												<table class="table table-striped">
													<thead>
														<tr>
															<th>名称</th>
															<th>阶段</th>
															<th>任务数</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach items="${projects}" var="node">
															<tr>
																<td><a
																	href="projectView/projectInfo.do?projectId=${node.id}&stime=${stime}&etime=${etime}">
																		${node.name}</a></td>
																<td>${node.gradationname}</td>
																<td>${node.pcontent}</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</c:if>
									<c:if test="${objType=='user'}">
										<div class="row">
											<div class="col-lg-2"></div>
											<div class="col-lg-8">
												<table class="table table-striped">
													<thead>
														<tr>
															<th>姓名</th>
															<th>隶属机构</th>
															<th>任务数</th>
														</tr>
													</thead>
													<tbody>
														<!-- ID,NAME,TASKNUM,ORGNAME -->
														<c:forEach items="${users}" var="node">
															<tr>
																<td><a
																	href="projectView/personInfo.do?userId=${node.ID}&stime=${stime}&etime=${etime}">
																		${node.NAME}</a></td>
																<td>${node.ORGNAME}</td>
																<td>${node.TASKNUM}</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
										</div>
									</c:if>
									<div style="text-align: center;">
										<a
											href="projectView/chooseRound.do?objType=${objType}&baseDate=${baseDate}"
											type="button" class="btn btn-default">返回设置日期范围</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form action="<PF:basePath/>projectView/chooseObj.do" id="submitFormId">
		<input type="hidden" name="objType" value="${objType}">
		<!--  -->
		<input type="hidden" name="baseDate" id="baseDateId">
		<!--  -->
		<input type="hidden" name="roundUnit" id="roundUnitId">
	</form>
	<jsp:include page="../document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {
		$("#form_dayId").datetimepicker({
			format : "yyyy-mm-DD",
			showMeridian : true,
			autoclose : true,
			todayBtn : true,
			minView : 2,
			startView : 2
		}).on(
				'changeDate',
				function(ev) {
					setCDate(formateDates(new Date($("#form_dayId").data(
							"datetimepicker").getDate())));
				});
		$("#form_dayId").datetimepicker("setDate",
				new Date(eval(Number('${baseDate}'))));
	});
	//界面表单中回显日期
	function setCDate(YYYYMMDD) {
		$('#baseDateId').val(YYYYMMDD);
		$('#cdateId').text(YYYYMMDD);
		$('#roundUnitId').val('${roundUnit}');
		$('#submitFormId').submit();
	}
	//日期对象格式化为日期字符串
	function formateDates(date) {
		date_value = date.getFullYear() + '-' + (date.getMonth() + 1) + '-'
				+ date.getDate();
		return date_value;
	}
</script>
</html>