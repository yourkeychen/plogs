<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>查询周期选择</title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.min.js"></script>
<link
	href="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.css"
	rel="stylesheet">
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px; overflow: hidden;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-3" style="text-align: center;">
									<div style="width: 240px; margin: auto;">
										<div class="" id="form_dayId" style="margin: 20px;"></div>
									</div>
								</div>
								<div class="col-lg-9">
									<div class="hidden-xs hidden-sm"
										style="text-align: center; padding-bottom: 20px;">
										<div style="height: 30px; width: 100%;"></div>
										<h1>
											<c:if test="${objType=='user' }">人员周报</c:if>
											<c:if test="${objType=='project' }">项目周报</c:if>
										</h1>
										<h1>
											基准日期:<span id="cdateId"></span>
										</h1>
									</div>
									<div class="btn-group btn-group-justified  btn-group-lg"
										role="group" aria-label="Justified button group">
										<a href="javascript:submitToUnit('DAY')"
											class="btn btn-default" role="button"><i
											class="glyphicon glyphicon-search"></i> 日报</a>
										<!--  -->
										<a href="javascript:submitToUnit('WEEK')" href="#"
											class="btn btn-default" role="button"><i
											class="glyphicon glyphicon-search"></i> 周报</a>
										<!--  -->
										<a href="javascript:submitToUnit('MONTH')" href="#"
											class="btn btn-default" role="button"><i
											class="glyphicon glyphicon-search"></i> 月报</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form action="<PF:basePath/>projectView/chooseObj.do" id="submitFormId">
		<input type="hidden" name="objType" value="${objType}">
		<!--  -->
		<input type="hidden" name="baseDate" id="baseDateId">
		<!--  -->
		<input type="hidden" name="roundUnit" id="roundUnitId">
	</form>
	<jsp:include page="../document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {
		$("#form_dayId").datetimepicker({
			format : "yyyy-mm-DD",
			showMeridian : true,
			autoclose : true,
			todayBtn : true,
			minView : 2,
			startView : 2
		}).on(
				'changeDate',
				function(ev) {
					setCDate(formateDates(new Date($("#form_dayId").data(
							"datetimepicker").getDate())));
				});
		//回显后台传出日期
		if ('${baseDate}' != '') {
			$("#form_dayId").datetimepicker("setDate",
					new Date(eval(Number('${baseDate}'))));
		}

		setCDate(formateDates(new Date($("#form_dayId").data("datetimepicker")
				.getDate())));
	});
	//日期对象格式化为日期字符串
	function formateDates(date) {
		date_value = date.getFullYear() + '-' + (date.getMonth() + 1) + '-'
				+ date.getDate();
		return date_value;
	}
	//界面表单中回显日期
	function setCDate(YYYYMMDD) {
		$('#baseDateId').val(YYYYMMDD);
		$('#cdateId').text(YYYYMMDD);
	}

	function submitToUnit(roundUnit) {
		$('#roundUnitId').val(roundUnit);
		$('#submitFormId').submit();
	}
</script>
</html>