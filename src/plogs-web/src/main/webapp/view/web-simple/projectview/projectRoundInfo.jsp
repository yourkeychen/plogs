<%@page import="com.farm.wcp.util.ThemesUtil"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="/view/conf/wdaptag.tld" prefix="WDAP"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>项目任务信息- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

h2 {
	color: #d9534f;
	border-left: 8px solid #d9534f;
	padding-left: 10px;
	margin-left: 0px;
	font-weight: 500;
}

h3 {
	border-left: 4px solid #d9534f;
	color: #d9534f;
	padding-left: 10px;
	margin-left: 10px;
	font-weight: 200;
	font-size: 16px;
}

code {
	margin-left: 0.5em;
}

.tip {
	font-weight: 200;
	color: #c7254e;
	font-size: 14px;
	white-space: nowrap;
}

table {
	background-color: #ffffff;
}

.taskFilesBox {
	margin-left: 20px;
}

.taskFilesBox .col-sm-4 {
	padding: 0px;
}

.taskFilesBox code {
	font-size: 0.8em;
}

.taskFileNodeBox {
	margin: 4px;
	text-align: left;
	height: 68px;
	float: left;
	clear: right;
	border: 1px dashed #d9534f;
	padding: 8px;
	background-color: #ffffff;
	padding: 8px;
	text-align: left;
}

.taskFilesBox .filename {
	white-space: nowrap;
	overflow: hidden;
	padding-top: 4px;
	margin-right: 10px;
	margin-left: 10px;
}

.fileIcon {
	margin: auto;
	width: 48px;
	height: 48px;
	width: 48px;
}

h3 code {
	font-size: 14px;
}

.panel-body .doc_vavigat_h1 {
	font-weight: 700;
	border-left: 5px solid #cb254e;
}

.taskBox {
	margin-top: 10px;
	border: 1px dashed #ccc;
	background-color: #ffffff;
	padding: 10px;
}

.taskBox a {
	color: #333333;
}

.taskBox a:HOVER {
	color: #666666;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-3 hidden-xs hidden-sm">
					<div class="panel panel-default affix-top" id="docContentMenuId"
						style="z-index: 1000; width: 230px; border: 1px dashed #ddd;">
						<div class="panel-body" id="docContentTitlelbody">
							<span class="glyphicon glyphicon-list-alt"></span> &nbsp; <b>目录</b>
						</div>
						<div class="panel-body" id="docContentPanelbody"
							style="list-style: none; padding: 4px; margin-left: -10px; border-top: 1px dashed #ddd;">
							<ul id="ContentMenuId" class="doc_vavigat"
								style="height: 310px; overflow-y: auto;">
								<c:if test="${!empty projects }">
									<li class="doc_vavigat_h1"><a
										onclick="scrollToMarkName('top3')" id="linkmark1">机构项目 </a></li>
								</c:if>
								<li class="doc_vavigat_h1"><a
									onclick="scrollToMarkName('top1')" id="linkmark0">项目信息 </a></li>
								<li class="doc_vavigat_h1"><a
									onclick="scrollToMarkName('top2')" id="linkmark1">项目属性 </a></li>
								<c:forEach items="${gradations }" var="gradation">
									<li class="doc_vavigat_h1"><a
										onclick="scrollToMarkName('${gradation.id}')" id="linkmark1">${gradation.name}
									</a></li>
									<c:forEach items="${tasktypes }" var="tasktype">
										<c:if test="${tasktype.GRADATIONID==gradation.id}">
											<li class="doc_vavigat_h2"><a
												onclick="scrollToMarkName('${gradation.id}${tasktype.TASKTYPEID}')"
												id="linkmark1">${tasktype.TASKTYPENAME} </a></li>
										</c:if>
									</c:forEach>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9" style="">
					<div class="row">
						<div class="col-md-12">
							<c:if test="${!empty stime&&!empty etime}">
								<h1>
									<code>查询区间[${stime}</code>
									<code>${etime}]</code>
								</h1>
							</c:if>
							<h1>${company.name}${(!empty company)?' : ':''}${project.name}</h1>
						</div>
					</div>
					<c:if test="${!empty projects }">
						<div class="row">
							<div class="col-md-12">
								<h2>
									<a name="top3"></a>机构项目
								</h2>
								<div class="table-responsive"
									style="max-height: 350px; overflow: auto;">
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th style="width: 150px;">活跃时间</th>
												<th>项目名称</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${projects}" var="node">
												<tr>
													<td><PF:FormatTime date="${node.livetime}"
															yyyyMMddHHmmss="yyyy-MM-dd" /></td>
													<th><a
														href="<PF:basePath/>projectView/projectInfo.do?projectId=${node.id}">${node.name}</a>
													</th>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</c:if>
					<div class="row">
						<div class="col-md-12">
							<div>
								<h2>
									<a name="top1"></a>项目信息
								</h2>
								<div class="table-responsive">
									<table class="table table-striped table-bordered">
										<tbody>
											<tr>
												<th>创建时间</th>
												<td><PF:FormatTime date="${project.ctime}"
														yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
												<th>创建人</th>
												<td>${cuser.name}<code>${project.cuser}</code></td>
											</tr>
											<tr>
												<th>活跃时间</th>
												<td><PF:FormatTime date="${project.livetime}"
														yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
												<th>项目类型</th>
												<td>${projectType.name}<code>${project.typeid}</code></td>
											</tr>
											<tr>
												<th>当前阶段</th>
												<td>${project.gradationname}<code>${project.gradationid}</code></td>
												<th>项目状态</th>
												<td><c:if test="${project.state=='0'}">停用</c:if> <c:if
														test="${project.state=='1'}">激活</c:if> <c:if
														test="${project.state=='2'}">关闭</c:if> <c:if
														test="${project.state=='3'}">删除</c:if> <code>${project.state}</code></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h2>
								<a name="top2"></a> 项目属性
								<c:if test="${USEROBJ.type=='3' }">
									<a style="float: right;"
										href="workproject/fieldEditPage.do?projectId=${project.id}"
										class="btn btn-primary btn-xs">編輯属性</a>
								</c:if>
							</h2>
							<div class="table-responsive"
								style="max-height: 350px; overflow: auto;">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th style="width: 150px;">属性名称</th>
											<th>属性值</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${fields}" var="node">
											<tr>
												<th>${node.name}</th>
												<td>${node.valtitle}<code>${node.val}</code></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<c:forEach items="${gradations }" var="gradation">
						<div class="row">
							<div class="col-md-12">
								<h2>
									<!-- 项目阶段 -->
									<a name="${gradation.id}"></a> ${gradation.name}
								</h2>
								<div>
									<c:forEach items="${tasktypes }" var="tasktype">
										<c:if test="${tasktype.GRADATIONID==gradation.id}">
											<h3>
												<!-- 任务分类 -->
												<a name="${gradation.id}${tasktype.TASKTYPEID}"></a>
												${tasktype.TASKTYPENAME}
											</h3>
											<div>
												<c:forEach items="${tasks}" var="task">
													<c:if
														test="${task.gradationid==gradation.id&&task.tasktypeid==tasktype.TASKTYPEID}">
														<!-- 任务 -->
														<div class="taskBox">
															<div class="row">
																<div class="col-md-9"
																	style="font-size: 14px; font-weight: 700;">
																	<a href="projectView/taskinfo.do?taskid=${task.id}">
																		${task.title} </a><span class="tip">
																		${task.tasktypename}</span>
																	<c:set var="isHaveFile" value="false"></c:set>
																	<c:forEach items="${files}" var="file">
																		<c:if
																			test="${task.id==file.TASKID&&file.EXNAME!='ihtml'}">
																			<img class="img-rounded"
																				style="height: 2em; width: 2em; position: relative; top: -2px;"
																				src="download/Pubicon.do?id=${file.FILEID}" />
																			<c:set var="isHaveFile" value="true"></c:set>
																		</c:if>
																	</c:forEach>
																</div>
																<div class="col-md-3" style="text-align: right;">
																	<span class="tip"
																		style="position: relative; top: 2px; margin-right: 4px;">
																		<PF:FormatTime date="${task.dostime}"
																			yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
																	</span>
																	<c:if test="${!isHaveFile}">
																		<span style="margin-right: 22px;"></span>
																	</c:if>
																	<div class="btn-group btn-group-xs" role="group"
																		aria-label="Extra-small button group">
																		<c:if test="${isHaveFile}">
																			<button type="button" class="btn btn-default"
																				onclick="showTaskFile('${task.id}');">
																				<span class="glyphicon glyphicon-chevron-down"></span>
																			</button>
																		</c:if>
																		<a href="projectView/taskinfo.do?taskid=${task.id}"
																			target="_blank" type="button" class="btn btn-default">
																			<span class="glyphicon glyphicon-info-sign"></span>
																		</a>
																	</div>
																</div>
															</div>
														</div>
														<div class="taskFilesBox row" id="taskFiles-${task.id}"
															style="margin-left: 4px; margin-right: 4px; display: none;">
															<c:forEach items="${files}" var="file">
																<c:if
																	test="${task.id==file.TASKID&&file.EXNAME!='ihtml'}">
																	<div class="col-sm-6">
																		<div class="taskFileNodeBox">
																			<table style="table-layout: fixed; width: 100%;">
																				<tr>
																					<td rowspan="2" width="50"><img
																						class="fileIcon img-rounded"
																						src="download/Pubicon.do?id=${file.FILEID}" /></td>
																					<td width="240"
																						style="overflow: hidden; padding: 4px; white-space: nowrap; text-overflow: ellipsis;">
																						<span title="${file.FILETITLE}"
																						style="color: #555555;">${file.FILETITLE}</span>
																					</td>
																				</tr>
																				<tr>
																					<td><code>${file.EXNAME}</code> <code>
																							<c:if test="${file.SOURCETYPE=='1'}">任务描述</c:if>
																							<c:if test="${file.SOURCETYPE=='2'}">描述附件</c:if>
																							<c:if test="${file.SOURCETYPE=='3'}">任务日志</c:if>
																							<c:if test="${file.SOURCETYPE=='4'}">日志附件</c:if>
																						</code>
																						<div style="float: right; font-size: 12px;">
																							<!-- 预览地址 -->
																							<WDAP:viewAble fileid="${file.FILEID}">
																								<%
																									if (ThemesUtil.isMobile(request)) {
																								%>
																								<!-- 移動端 -->
																								<a
																									href="include/Pubview.do?fileid=${file.FILEID}">
																									预览</a>
																								<%
																									} else {
																								%>
																								<!-- PC端 -->
																								<a target="_blank"
																									href="include/Pubview.do?fileid=${file.FILEID}">
																									预览</a>
																								<%
																									}
																								%>
																							</WDAP:viewAble>
																							<!-- 下载地址： -->
																							<a
																								href="download/Pubfile.do?id=${file.FILEID}&secret=${file.SECRET}">
																								下载</a>
																						</div></td>
																				</tr>
																			</table>
																		</div>
																	</div>
																</c:if>
															</c:forEach>
														</div>
													</c:if>
												</c:forEach>

											</div>
										</c:if>
									</c:forEach>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {
		$('#docContentMenuId').affix({
			offset : {
				top : 50,
				bottom : 5
			}
		})
	});
	function showTaskFile(taskId) {
		if ($('#taskFiles-' + taskId).is(":hidden")) {
			$('#taskFiles-' + taskId).show();
		} else {
			$('#taskFiles-' + taskId).hide();
		}
	}
	//滾動到指定位置
	function scrollToMarkName(markName) {
		$('html,body').animate({
			scrollTop : $("a[name='" + markName + "']").offset().top - 70
		});
	}
</script>
</html>