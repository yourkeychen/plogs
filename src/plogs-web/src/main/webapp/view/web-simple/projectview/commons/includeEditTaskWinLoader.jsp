<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!--点击任务后弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">修改任务属性</h4>
		</div>
		<div class="modal-body" style="padding-bottom: 0px;">
			<form class="form-horizontal" id="addTaskFormId">
				<input type="hidden" id="taskformTaskId" value="${task.id}">
				<div class="form-group" id="taskGradationGroupID">
					<label for="taskGradation" class="col-sm-3 control-label">阶段<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="taskGradation" class="form-control"
							val='${task.gradationid}'>
							<c:forEach var="node" items="${gradations}">
								<option value="${node.id}">${node.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group" id="taskTypeGroupId">
					<label for="taskType" class="col-sm-3 control-label">类型<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="taskType" class="form-control"
							val='${task.tasktypeid}'>
							<c:forEach var="node" items="${taskTypes}">
								<option value="${node.id}">${node.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group" id="titleGroupId">
					<label for="taskTitle" class="col-sm-3 control-label">标题<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="taskTitle"
							value="${task.title}" placeholder="任务标题">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<span class="alertMsgClass" style="float: left;" id="errormessageShowboxId"></span>
			<button type="button" onclick="submitTaskForm()"
				class="btn btn-primary">更新任务</button>
		</div>
	</div>
</div>
<script>
	$(function() {
		//階段选定事件
		$('#taskGradation').change(function() {
			loadTaskTypes($(this).val());
		});
		$('select', '#addTaskFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
		validateInput('taskTitle', function(id, val, obj) {
			// 标题
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 256)) {
				return {
					valid : false,
					msg : '长度不能大于' + 256
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'titleGroupId');
		validateInput('taskGradation', function(id, val, obj) {
			// 阶段
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'taskGradationGroupID');
	});
	//提交表单
	function submitTaskForm() {
		if (!validate('addTaskFormId')) {
			$('#errormessageShowboxId').text('信息录入有误，请检查！');
		} else {
			$('#errormessageShowboxId').text('');
			pConfirm("是否提交数据?", function() {
				var title = $('#taskTitle').val();
				var gradationid = $('#taskGradation').val();
				var tasktypeid = $('#taskType').val();
				var taskid = $('#taskformTaskId').val();
				$.post('worktask/doTaskEdit.do', {
					'title' : title,
					'gradationid' : gradationid,
					'tasktypeid' : tasktypeid,
					'taskid' : taskid
				}, function(flag) {
					if (flag.STATE == '0') {
						//pAlert(flag.task.title + ":任务提交成功!");
						$('#taskAddFormBox').modal('hide');
						 location.reload();
					} else {
						pAlert(flag.MESSAGE);
					}
				}, 'json');

			});
		}
	}
	//加载远程阶段类型
	function loadGradations(projectId) {
		$.post('worktask/loadGradations.do', {
			'projectId' : projectId
		}, function(data) {
			if (data.STATE == '0') {
				$('#taskGradation').html('<option value="">~选择项目~</option>');
				$('#taskType').html('<option value="">~选择阶段~</option>');
				$(data.gradations).each(
						function(i, obj) {
							$('#taskGradation').append(
									'<option value="'+obj.id+'">' + obj.name
											+ '</option>');
						});
				$('#taskGradation').val(data.gradationid);
				loadTaskTypes(data.gradationid);
			} else {
				pAlert(data.MESSAGE);
			}

		}, 'json');
	}

	//加载任务类型
	function loadTaskTypes(gradationId) {
		//加载阶段
		$.post('worktask/loadTaskType.do', {
			'gradationId' : gradationId
		}, function(data) {
			if (data.STATE == '0') {
				$('#taskType').html('<option value="">~选择任务类型~</option>');
				$(data.taskTypes).each(
						function(i, obj) {
							$('#taskType').append(
									'<option value="'+obj.id+'">' + obj.name
											+ '</option>');
						});
			} else {
				pAlert(data.MESSAGE);
			}

		}, 'json');
	}
</script>