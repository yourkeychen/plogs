<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

table tr td {
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
	-o-text-overflow: ellipsis;
	-moz-text-overflow: ellipsis;
	-webkit-text-overflow: ellipsis;
	max-width: 200px;
}

table tr th {
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
	-o-text-overflow: ellipsis;
	-moz-text-overflow: ellipsis;
	-webkit-text-overflow: ellipsis;
	max-width: 200px;
}
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px; overflow: hidden;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row" style="margin-top: 50px;">
						<div class="col-lg-2"></div>
						<div class="col-lg-8">
							<form action="projectView/index.do" id="searchFormId"
								method="post">
								<div class="input-group ">
									<div class="input-group-btn">
										<button type="button" class="btn btn-default dropdown-toggle"
											data-toggle="dropdown" aria-haspopup="true"
											aria-expanded="false">
											项目状态&nbsp;<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right">
											<li><a onclick="choooseProjectState('')">全部</a></li>
											<li><a onclick="choooseProjectState('1')">激活</a></li>
											<li><a onclick="choooseProjectState('0')">停用</a></li>
											<li><a onclick="choooseProjectState('2')">关闭</a></li>
										</ul>
									</div>
									<input type="hidden" id="fieldUuidId" name="fieldUuid"
										value="${fieldUuid}"> <input type="hidden"
										id="sortTypeId" name="sortType" value="${sortType}"><input
										type="hidden" id="pageInputId" name="page" value="${page}">
									<input type="text" class="form-control" id="nameInputId"
										name="name" placeholder="请输入项目名称或id" value="${name}">
									<input type="hidden" class="form-control" id="stateInputId"
										name="state" value="${state}"> <input type="hidden"
										class="form-control" id="fieldLimitId" name="fieldLimits"
										value="${fieldLimits}">
									<div class="input-group-btn">
										<button class="btn btn-default" onclick="doSearch()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
										<button class="btn btn-default" onclick="clearSearch()">
											<i class="glyphicon glyphicon-refresh"></i>
										</button>
										<button type="button" class="btn btn-default dropdown-toggle"
											data-toggle="dropdown" aria-haspopup="true"
											aria-expanded="false">
											项目类型&nbsp;<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right">
											<c:forEach items="${types}" var="node">
												<li><a onclick="choooseProjectType('${node.id}')">${node.name}</a></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</form>
						</div>
						<div class="col-lg-2"></div>
					</div>
					<jsp:include page="commons/includeFieldImits.jsp"></jsp:include>
					<div class="row" style="padding: 10px;">
						<div class="table-responsive">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th style="width: 200px;">项目名称</th>
										<th style="width: 200px;">业务机构</th>
										<th style="width: 200px;">当前阶段</th>
										<th style="width: 200px;">状态</th>
										<c:forEach items="${fields}" var="node">
											<th class="active" style="width: 100px;"><a
												onclick="doSort('${node.uuid}')">${node.name}</a></th>
										</c:forEach>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${projects.resultList}" var="node">
										<tr>
											<td><a target="_blank"
												href="projectView/projectInfo.do?projectId=${node.ID}">${node.NAME}</a></td>
											<td>${node.COMPANYNAME}</td>
											<td>${node.GRADATIONNAME}</td>
											<td>${node.STATETITLE}</td>
											<c:forEach items="${fields}" var="tdtitle">
												<td class="success" title="${node[tdtitle.uuid]}">${node[tdtitle.uuid]}</td>
											</c:forEach>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div style="text-align: center;">
						<nav aria-label="Page navigation"
							style="margin-bottom: 0px; padding-bottom: 0px;">
							<ul class="pagination">
								<c:if test="${projects.currentPage >1}">
									<li><a
										href="javascript:doSearch(${projects.currentPage-1})"
										aria-label="Previous"> <span aria-hidden="true">上一页</span>
									</a></li>
								</c:if>
								<c:if test="${projects.currentPage <projects.totalPage}">
									<li><a
										href="javascript:doSearch(${projects.currentPage+1})"
										aria-label="Next"> <span aria-hidden="true">下一页</span>
									</a></li>
								</c:if>
							</ul>
						</nav>
						<div style="color: #9f9f9f;">当前第${projects.currentPage}页,共${projects.totalPage}页</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {

	});
	function doSearch(page) {
		if (page) {
			$('#pageInputId').val(page);
		}
		$('#fieldLimitId').val(getFieldLimits());
		$('#searchFormId').submit();
	}

	function choooseProjectType(typeid) {
		$('#nameInputId').val(typeid);
		$('#pageInputId').val(1);
		doSearch();
	}
	function choooseProjectState(typeid) {
		$('#stateInputId').val(typeid);
		$('#pageInputId').val(1);
		doSearch();
	}
	function clearSearch() {
		$('#nameInputId').val('');
		$('#pageInputId').val(1);
		doSearch();
	}
	function doSort(uuid) {
		$('#fieldUuidId').val(uuid);
		if ($('#sortTypeId').val() == 'ASC') {
			$('#sortTypeId').val('DESC');
		} else {
			$('#sortTypeId').val('ASC');
		}
		doSearch();
	}
</script>
</html>