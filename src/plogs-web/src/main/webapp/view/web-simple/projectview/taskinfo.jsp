<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<link rel="stylesheet" href="<PF:basePath/>text/lib/viewer/viewer.css" />
<script charset="utf-8" src="<PF:basePath/>text/lib/viewer/viewer.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/alert/sweetalert.min.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<style type="text/css">
</style>
</head>
<body>
	<jsp:include page="../commons/head.jsp"></jsp:include>
	<jsp:include page="../commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div style="padding: 8px;">
						<div style="float: left;">
							<!-- 1.计划，2开始，3处理，4结束,5关闭 -->
							<c:if test="${task.pstate=='1'}">
								<img src="text/img/plogs/taskPlan.png" title="计划"
									style="height: 64px; width: 64px;">
							</c:if>
							<c:if test="${task.pstate=='2'}">
								<img src="text/img/plogs/taskStar.png" title="开始"
									style="height: 64px; width: 64px;">
							</c:if>
							<c:if test="${task.pstate=='3'}">
								<img src="text/img/plogs/taskJustDo.png" title="处理"
									style="height: 64px; width: 64px;">
							</c:if>
							<c:if test="${task.pstate=='4'}">
								<img src="text/img/plogs/taskComplete.png" title="结束"
									style="height: 64px; width: 64px;">
							</c:if>
							<c:if test="${task.pstate=='5'}">
								<!-- 歸檔 -->
								<img src="text/img/plogs/backup.png" title="归档"
									style="height: 64px; width: 64px;">
							</c:if>
						</div>
						<div
							style="padding-left: 60px; padding-top: 8px; margin-left: 20px;">
							<c:if test="${USEROBJ.type=='3' }">
								<!-- 各种弹出窗口 -->
								<div class="openWin">
									<!-- 添加任务弹出窗口 -->
									<jsp:include page="commons/includeEditTaskWin.jsp"></jsp:include>
								</div>
								<a style="float: right;"
									href="javascript:editTask('${task.id}')"
									class="btn btn-primary btn-xs">修改属性</a>
								<script type="text/javascript">
									//修改任務
									function editTask(taskId) {
										pAlert("loading...", 10000);
										$('#taskAddFormBox')
												.load(
														'worktask/loadEditTaskFormWin.do',
														{
															'taskId' : taskId
														},
														function() {
															swal.close();
															$(
																	'#taskOpratorMenuBox')
																	.modal(
																			'hide');
															$('#taskAddFormBox')
																	.modal(
																			'show');
														});
									}
								</script>
							</c:if>
							<div style="font-size: 18px; font-weight: 700;">
								${task.title}&nbsp;&nbsp;
								<code>${task.tasktypename}</code>
							</div>
							<div style="font-size: 14px; font-weight: 200; margin-top: 6px;">
								${company.name}${(!empty company)?' : ':''}${project.name}&nbsp;&nbsp;
								<code title="项目阶段">${task.gradationname}</code>
							</div>
						</div>
						<div class="table-responsive" style="margin-top: 20px;">
							<table class="table table-striped table-bordered">
								<tbody>
									<tr>
										<th>创建时间</th>
										<td><code>
												<PF:FormatTime date="${task.ctime}"
													yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" />
											</code></td>
										<th>创建人</th>
										<td><code>${user.name}</code></td>
									</tr>
									<tr>
										<th>已用时</th>
										<td><code> ${task.dotimes==null?0:task.dotimes}小时
											</code></td>
										<th></th>
										<td></td>
									</tr>
									<c:if test="${!empty task.completion}">
										<tr>
											<th>完成度</th>
											<td colspan="3"><div class="progress"
													style="margin: 0px;">
													<div
														class="progress-bar progress-bar-success progress-bar-striped"
														role="progressbar" aria-valuenow="60" aria-valuemin="0"
														aria-valuemax="100" style="width: ${task.completion}%;">${task.completion}%</div>
												</div></td>
										</tr>
									</c:if>
									<tr>
										<th>实际开始时间</th>
										<td><code>
												<PF:FormatTime date="${task.dostime}"
													yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" />
											</code></td>
										<th>实际结束时间</th>
										<td><code>
												<PF:FormatTime date="${task.doetime}"
													yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" />
											</code></td>
									</tr>
									<c:if test="${!empty task.planstime||!empty task.planetime}">
										<tr>
											<th>计划开始时间</th>
											<td><code>
													<PF:FormatTime date="${task.planstime}"
														yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" />
												</code></td>
											<th>计划结束时间</th>
											<td><code>
													<PF:FormatTime date="${task.planetime}"
														yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" />
												</code></td>
										</tr>
									</c:if>
									<tr>
										<th>管理人</th>
										<td><c:forEach items="${managerUsers }" var="node">
												<code> ${node.USERNAME } </code>
											</c:forEach></td>
										<th>执行人</th>
										<td><c:forEach items="${doUsers }" var="node">
												<code> ${node.USERNAME } </code>
											</c:forEach></td>
									</tr>
								</tbody>
							</table>
						</div>
						<c:if test="${!empty text}">
							<div class="panel panel-default" style="margin-top: 20px;">
								<div class="panel-heading">
									<b>任务说明</b>
								</div>
								<div class="panel-body">
									<div class="ke-content" style="overflow: auto;">${text}</div>
								</div>
							</div>
						</c:if>
						<c:if test="${!empty logs}">
							<c:forEach var="log" items="${logs}">
								<div class="panel panel-default" style="margin-top: 20px;">
									<div class="panel-heading">
										<b>${log.datekey}-日志</b>
									</div>
									<div class="panel-body">
										<div class="ke-content" style="overflow: auto;">${log.text}</div>
									</div>
								</div>
							</c:forEach>
						</c:if>
						<div class="panel panel-default" style="margin-top: 20px;">
							<div class="panel-heading">
								<b>任务附件</b>
							</div>
							<div class="panel-body">
								<c:forEach var="taskfile" items="${files}">
									<p>
										<a href="document/view.do?fileid=${taskfile.file.id }"> <img
											src="download/Pubicon.do?id=${taskfile.file.id }" width="16"
											height="16"> ${taskfile.file.title } <code>
												<c:if test="${taskfile.sourcetype=='1'}">任务描述</c:if>
												<c:if test="${taskfile.sourcetype=='2'}">描述附件</c:if>
												<c:if test="${taskfile.sourcetype=='3'}">任务日志</c:if>
												<c:if test="${taskfile.sourcetype=='4'}">日志附件</c:if>
											</code>&nbsp;<code>${taskfile.file.id}</code> &nbsp;<code>${taskfile.file.filesize}b</code>
										</a>
									</p>
								</c:forEach>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../commons/footServer.jsp"></jsp:include>
	<jsp:include page="../commons/foot.jsp"></jsp:include>
	<script type="text/javascript">
		$(function() {//图片点击
			$('.ke-content img').each(function(i, obj) {
				try {
					$(obj).css("cursor", "pointer");
					var gallery = new Viewer(obj, {
						navbar : false,
						toolbar : {
							zoomIn : 4,
							zoomOut : 4,
							oneToOne : 4,
							reset : 4,
							prev : 0,
							play : {
								show : 0,
								size : 'large',
							},
							next : 0,
							rotateLeft : 4,
							rotateRight : 4,
							flipHorizontal : 4,
							flipVertical : 4
						},
						keyboard : false,
					});
				} catch (e) {
				}
			});
		});
	</script>
	<script>
		//询问框
		function pConfirm(tip, okFunc, noFunc) {
			swal({
				text : tip,
				buttons : true,
				buttons : {
					'確定' : true,
					cancel : "取消"
				},
			}).then(function(willDelete) {
				if (willDelete) {
					okFunc();
				} else {
					if (noFunc) {
						noFunc();
					}
				}
			});
		}
		//消息框
		function pAlert(tip, timenum) {
			if (timenum) {
				swal({
					text : tip,
					timer : timenum,
					buttons : false
				});
			} else {
				swal(tip);
			}
		}
	</script>
</body>
</html>