<%@page import="java.util.Date"%>
<%@page import="com.farm.core.time.TimeTool"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${project.name}-<PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script src="text/lib/layout/jquery.layout-latest.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh-CN.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-kindeditor.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-file-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/echarts/echarts.all.2.2.7.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-multiimage-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/htmltags.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/alert/sweetalert.min.js"></script>
<!-- 附件批量上传组件 -->
<script src="<PF:basePath/>text/lib/fileUpload/jquery.ui.widget.js"></script>
<script
	src="<PF:basePath/>text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.css"
	rel="stylesheet">
<script charset="utf-8"
	src="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.min.js"></script>
<link
	href="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.css"
	rel="stylesheet">
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

.plogs-backbox {
	background-color: #f4f4f4;
	height: 100%;
	width: 100%;
}

.plogs-view-title {
	padding: 10px;
	font-weight: 700;
}

.plogs-view-tasks {
	padding: 0px;
}

.plogs-task-box {
	height: 45px;
	border: 1px dashed;
	padding: 4px;
	background-color: #ffffff;
	cursor: pointer;
	border-left: 8px solid;
	margin-bottom: 4px;
	line-height: 1.3em;
}

.plogs-task-box .plogs-projec-title {
	font-size: 10px;
	font-weight: 200;
	color: #666;
}

.plogs-task-box .plogs-task-title {
	font-size: 14px;
	font-weight: 600;
	color: #888;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

.plogs-view-type3box .plogs-task-box .plogs-task-title {
	color: #000;
}

.plogs-task-box code {
	font-size: 0.8em;
	font-weight: 200;
	float: right;
	padding: 0px;
	padding-right: 4px;
	padding-left: 4px
}

.plogs-projec-petime {
	font-size: 10px;
	margin-left: 4px;
	color: #c7254e;
}
</style>
</head>
<body>
	<!-- 各种弹出窗口 -->
	<div class="openWin">
		<!-- 属性编辑弹出窗口 -->
		<jsp:include page="../worktask/commons/includeFieldEditWin.jsp"></jsp:include>
	</div>
	<div class="ui-layout-center" id="plogs-current-task">
		<div class="ui-layout-center">
			<div class="ui-layout-east" id="taskFieldloadBoxId">
				<div class="plogs-backbox" style="padding: 20px;">loading...</div>
			</div>
		</div>
	</div>
	<div class="ui-layout-west">
		<div style="background-color: #f4f4f4;">
			<div style="overflow: auto; background-color: #ffffff;">
				<div class="table-responsive">
					<div
						style="font-size: 20px; margin-top: 50px; margin-bottom: 10px; font-weight: 700; text-align: center;">${project.name}
					</div>
					<table class="table table-striped table-bordered">
						<tbody>

							<tr>
								<th style="width: 25%">创建时间</th>
								<td><PF:FormatTime date="${project.ctime}"
										yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
							</tr>
							<tr>
								<th>创建人</th>
								<td>${cuser.name}<code>${project.cuser}</code></td>
							</tr>
							<tr>
								<th>活跃时间</th>
								<td><PF:FormatTime date="${project.livetime}"
										yyyyMMddHHmmss="yyyy-MM-dd HH:mm:ss" /></td>
							</tr>
							<tr>
								<th>项目类型</th>
								<td>${projectType.name}<code>${project.typeid}</code></td>
							</tr>
							<tr>
								<th>当前阶段</th>
								<td>${project.gradationname}<code>${project.gradationid}</code></td>
							</tr>
							<tr>
								<th>项目状态</th>
								<td><c:if test="${project.state=='0'}">停用</c:if> <c:if
										test="${project.state=='1'}">激活</c:if> <c:if
										test="${project.state=='2'}">关闭</c:if> <c:if
										test="${project.state=='3'}">删除</c:if> <code>${project.state}</code></td>
							</tr>
						</tbody>
					</table>
					<div
						style="font-size: 20px; margin-top: 50px; margin-bottom: 10px; font-weight: 700; text-align: center;">
						<a href="projectView/projectInfo.do?projectId=${project.id}"
							class="btn btn-primary ">返回项目</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui-layout-north">
		<!-- 顶部头  -->
		<jsp:include page="../commons/head.jsp"></jsp:include>
	</div>
	<div class="ui-layout-south">
		<div style="background-color: #000000;">
			<!-- 底部权限 -->
			<jsp:include page="../commons/foot.jsp"></jsp:include>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(function() {
		//主体分隔
		$('body').layout({
			applyDefaultStyles : true,
			north__closable : false,//可以被关闭  
			north__resizable : false,//可以改变大小
			north__size : 50,
			//---------------------------
			south__closable : false,//可以被关闭  
			south__resizable : false,//可以改变大小
			south__size : 36,
			south__spacing_open : 1,
			//---------------------------
			west__closable : true,//可以被关闭  
			west__resizable : false,//可以改变大小
			west__size : 400,
			west__spacing_open : 8,
		});
		//任務操作区
		$('#plogs-current-task').layout({
			applyDefaultStyles : true
		});
	});
</script>
<script>
	$(function() {
		loadTaskField();
	});
	//返回任务列表
	function backCurrentTasks() {
		saveCurentTask(function() {
			window.location = "<PF:basePath/>worktask/index.do";
		});
	}
	//完成任务
	function completeCurrentTask() {
		saveCurentTask(function() {
			doUpdateTaskState('${task.id}', "4", function() {
				window.location = "<PF:basePath/>worktask/index.do";
			});
		});
	}
	//执行任务状态改变
	function doUpdateTaskState(taskId, state, func) {
		$.post('worktask/updateTaskState.do', {
			'taskId' : taskId,
			'state' : state
		}, function(flag) {
			if (flag.STATE == '0') {
				func();
			} else {
				pAlert(flag.MESSAGE);
			}
		}, 'json');
	}
	//保存当前任务
	function saveCurentTask(func) {
		//('保存当前任务:' + currentTaskId);
		//獲得text内容
		eval('currentLogTexteditor').sync();
		var text = $('#currentLogText').val();
		//提交到後臺
		$.post('worktask/ctlogSubmit.do', {
			'text' : text,
			'taskid' : '${task.id}'
		},
				function(flag) {
					if (flag.STATE == '0') {
						if (flag.log != null) {
							$('#currentLogOpLogsId').text(
									"日志保存成功:" + flag.task.title + "-"
											+ flag.log.etime);
						} else {
							$('#currentLogOpLogsId').text("日志无需保存");
						}
						if (func) {
							func();
						}
					} else {
						pAlert(flag.MESSAGE);
					}
				}, 'json');
	}
	//加载任务属性
	function loadTaskField() {
		$('#taskFieldloadBoxId').load('workproject/loadFieldWin.do', {
			'projectId' : '${project.id}'
		}, function() {
		});
	}
	//編輯任务属性
	function editFieldVal(fieldUuid, taskid) {
		$('#fieldEditFormBox').load('worktask/loadFieldFormWin.do', {
			'uuid' : fieldUuid,
			'projectid' : '${project.id}'
		}, function() {
			$('#fieldEditFormBox').modal('show');
		});
	}
	//添加任务属性实例
	function addFieldInsVal(fieldUuid, taskid) {
		$('#fieldEditFormBox').load('worktask/addFieldIns.do', {
			'fieldUuid' : fieldUuid,
			'projectid' : '${project.id}'
		}, function() {
			$('#fieldEditFormBox').modal('show');
		});
	}
	//删除任务属性实例
	function delFieldInsVal(fieldInsid, taskid) {
		pConfirm("确定删除一条属性?", function() {
			$('#fieldEditFormBox').load('worktask/delFieldIns.do', {
				'fieldinsId' : fieldInsid,
				'projectid' : '${project.id}'
			}, function() {
				loadTaskField();
				$('#fieldEditFormBox').modal('show');
			});
		});
	}
	//編輯任务属性实例
	function editFieldInsVal(fieldInsid, taskid) {
		$('#fieldEditFormBox').load('worktask/loadFieldInsFormWin.do', {
			'fieldinsId' : fieldInsid,
			'taskid' : taskid
		}, function() {
			$('#fieldEditFormBox').modal('show');
		});
	}
</script>
<script>
	//询问框
	function pConfirm(tip, okFunc, noFunc) {
		swal({
			text : tip,
			buttons : true,
			buttons : {
				'確定' : true,
				cancel : "取消"
			},
		}).then(function(willDelete) {
			if (willDelete) {
				okFunc();
			} else {
				if (noFunc) {
					noFunc();
				}
			}
		});
	}
	//消息框
	function pAlert(tip, timenum) {
		if (timenum) {
			swal({
				text : tip,
				timer : timenum,
				buttons : false
			});
		} else {
			swal(tip);
		}
	}
</script>
</html>