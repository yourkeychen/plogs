<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<c:if test="${!empty fields}">
	<div class="row"
		style="padding: 10px; background-color: #ffffff; margin: 10px; border: 1px dashed #999999; padding-top: 20px;">
		<!-- 2附件，3多行文本，4单行文本，5枚举单选 10日期 -->
		<c:forEach items="${fields}" var="node">
			<c:if
				test="${node.valtype=='3'||node.valtype=='4'||node.valtype=='5'||node.valtype=='10'}">
				<div class="col-md-2">
					<div class="form-group">
						<label for="limit-${node.uuid}">${node.name}</label>
						<c:if
							test="${node.valtype=='3'||node.valtype=='4'||node.valtype=='10'}">
							<input type="text" class="form-control fieldlimit"
								id="limit-${node.uuid}" name="${node.uuid}"
								placeholder="${node.name}">
						</c:if>
						<c:if test="${node.valtype=='5'}">
							<select class="form-control fieldlimit" id="limit-${node.uuid}"
								name="${node.uuid}">
								<option value=""></option>
								<c:forEach items="${node.enumlist}" var="enode">
									<option value="${enode[0]}">${enode[1]}</option>
								</c:forEach>
							</select>
						</c:if>
					</div>
				</div>
			</c:if>
		</c:forEach>
	</div>
</c:if>
<script type="text/javascript">
	function getFieldLimits() {
		var limits = '';
		$('.fieldlimit').each(
				function(i, obj) {
					var name = $(obj).attr('name').replace('%,', ',').replace(
							'%:', ':');
					var val = $(obj).val().replace('%,', ',')
							.replace('%:', ':');
					limits = limits + "%," + name + "%:" + val;
				});
		return limits;
	}
	$(function() {
		var fieldlimit = $('#fieldLimitId').val();
		$(fieldlimit.split("%,")).each(function(i, obj) {
			if (obj && obj.trim()) {
				var node = obj.split('%:');
				if (node.length == 2) {
					var name = node[0];
					var val = node[1];
					if (name && val) {
						$("#limit-" + name).val(val);
					}
				}
			}
		});
	});
</script>
