<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--点击任务后弹出的操作窗口 -->
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">项目人员管理</h4>
		</div>
		<div class="modal-body" style="padding: 20px;">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">项目成员</div>
						<div class="panel-body" style="padding: 0px; padding-top: 10px;">
							<div style="height: 362px; overflow: auto;">
								<table class="table table-striped table-condensed"
									style="font-size: 12px;">
									<tbody id="projectUserListId">

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">选择人员</div>
						<div class="panel-body" style="padding: 0px;">
							<div class="input-group input-group-sm"
								style="padding: 10px; padding-top: 4px; padding-bottom: 4px;">
								<input type="text" class="form-control"
									id="loadUserSearchLimitId" placeholder="姓名/机构名称"> <span
									class="input-group-btn">
									<button class="btn btn-default" type="button"
										onclick="loadUser()">查询</button>
								</span>
							</div>
							<!-- /input-group -->
							<div style="height: 300px; overflow: auto;">
								<table class="table table-striped table-condensed"
									style="font-size: 12px;">
									<tbody id="allUserChooseListId">

									</tbody>
								</table>
							</div>
							<nav aria-label="..." style="margin: 0px; text-align: center;">
								<ul class="pagination pagination-sm" style="margin: 0px;">
									<li id="allUserChoosePagelastId"><a
										onclick="loadAllUser(-1)" aria-label="Previous"><span
											aria-hidden="true">«</span></a></li>
									<li><a id="allUserChoosePageId"></a></li>
									<li id="allUserChoosePageNextId"><a
										onclick="loadAllUser(1)" aria-label="Next"><span
											aria-hidden="true">»</span></a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<span class="alertMsgClass" id="errormessageShowboxId"></span>
			<button type="button" onclick="backtoProjectMenu()"
				class="btn btn-primary">返回</button>
		</div>
	</div>
</div>
<script>
	$(function() {
		loadAllUser();
		loadProjectUser();
	});

	function backtoProjectMenu() {
		pAlert("loading...", 10000);
		$('#projectUsersFormBox').modal('hide');
		openProjectMenuByTaskId('${projectId}');
	}
	//添加项目用户
	function addUserToProject(userid, poptype) {
		$.post('workproject/addUserToProject.do', {
			'userid' : userid,
			'projectid' : '${projectId}',
			'poptype' : poptype
		}, function(flag) {
			if (flag.STATE == '0') {
				loadProjectUser();
				pAlert("操作成功!");
			} else {
				pAlert(flag.MESSAGE);
			}
		}, 'json');
		
	}
	//修改用户类型
	function editUserToProject(projectuserid, poptype) {
		$.post('workproject/editUserToProject.do', {
			'projectUserId' : projectuserid,
			'poptype' : poptype
		}, function(flag) {
			if (flag.STATE == '0') {
				loadProjectUser();
				pAlert("操作成功!");
			} else {
				pAlert(flag.MESSAGE);
			}
		}, 'json');
	}
	//删除项目用户
	function delUserToProject(projectuserid) {
		$.post('workproject/delUserToProject.do', {
			'projectUserId' : projectuserid
		}, function(flag) {
			if (flag.STATE == '0') {
				loadProjectUser();
				pAlert("操作成功!");
			} else {
				pAlert(flag.MESSAGE);
			}
		}, 'json');
	}

	function loadProjectUser() {
		$.post('workproject/queryProjectUser.do',
						{
							projectid : '${projectId}'
						},
						function(backdb) {
							$('#projectUserListId').html('');
							$(backdb.datas.resultList)
									.each(
											function(i, obj) {
												var htmlmodel = '<div class="btn-group btn-group-xs" role="group">'
														+ '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
														+ '设置<span class="caret"></span></button>'
														+ '<ul class="dropdown-menu">'
														+ '<li><a onclick="editUserToProject(\''
														+ obj.ID
														+ '\',\'2\')">设置为：成员</a></li>'
														+ '<li><a onclick="editUserToProject(\''
														+ obj.ID
														+ '\',\'1\')">设置为：管理员</a></li>'
														+ '  <li role="separator" class="divider"></li><li><a onclick="delUserToProject(\''
														+ obj.ID
														+ '\')">删除</a></li>'
														+ '</ul></div>';
												var trmodel = '<tr><td >'
														+ htmlmodel
														+ '</td><td title="'+obj.LOGINNAME+'">'
														+ obj.NAME
														+ '</td><td>'
														+ obj.POPTYPE
														+ '</td></tr>';
												$('#projectUserListId').append(
														trmodel);
											});
						}, 'json');
	}

	function loadAllUser(addpage) {
		var cpage = $('#allUserChoosePageId').text();
		if (addpage) {
			cpage = Number(cpage) + Number(addpage);
		}
		$.post('workproject/queryAllUser.do',
						{
							name : $('#loadUserSearchLimitId').val(),
							page : cpage
						},
						function(backdb) {
							$('#allUserChooseListId').html('');
							$('#allUserChoosePageId').text(
									backdb.datas.currentPage);
							if (backdb.datas.currentPage > 1) {
								$('#allUserChoosePagelastId').show();
							} else {
								$('#allUserChoosePagelastId').hide();
							}
							if (backdb.datas.currentPage < backdb.datas.totalPage) {
								$('#allUserChoosePageNextId').show();
							} else {
								$('#allUserChoosePageNextId').hide();
							}
							$(backdb.datas.resultList)
									.each(
											function(i, obj) {
												var htmlmodel = '<div class="btn-group btn-group-xs" role="group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">添加<span class="caret"></span></button><ul class="dropdown-menu">'
														+ '<li><a onclick="addUserToProject(\''
														+ obj.USERID
														+ '\',\'2\')">添加为：成员</a></li>'
														+ '<li><a  onclick="addUserToProject(\''
														+ obj.USERID
														+ '\',\'1\')">添加为：管理员</a></li></ul></div>';
												var trmodel = '<tr><td>'
														+ htmlmodel
														+ '</td><td title="'+obj.LOGINNAME+'">'
														+ obj.USERNAME
														+ '</td><td>'
														+ (obj.ORGNAME ? obj.ORGNAME
																: '')
														+ '</td></tr>';
												$('#allUserChooseListId')
														.append(trmodel);
											});
						}, 'json');
	}
</script>