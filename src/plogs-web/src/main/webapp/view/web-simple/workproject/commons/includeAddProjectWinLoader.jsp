<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--点击任务后弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">
				<c:if test="${empty project}">添加项目</c:if>
				<c:if test="${!empty project}">更新项目</c:if>
			</h4>
		</div>
		<div class="modal-body" style="padding-bottom: 0px;">
			<form class="form-horizontal" id="addProjectFormId">
				<input type="hidden" id="projectformProjectId" value="${project.id}">
				<input type="hidden" id="projectCompanyId" value="${company.id}">
				<!-- 分割线 -->
				<div class="form-group" id="companyNameGroupId">
					<label for="companyName" class="col-sm-3 control-label">机构名称<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<div class="input-group">
							<input type="text" class="form-control" aria-label="..."
								id="companyName" placeholder="机构名称 ">
							<div class="input-group-btn">
								<button id="companyNameChooseButton" type="button"
									class="btn btn-default dropdown-toggle" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">
									<span id="companyNameTitle" style="font-size: 12px;"></span>
									<span class="glyphicon glyphicon-search"></span>
								</button>
								<ul id="companyNameListUl"
									class="dropdown-menu dropdown-menu-right"
									style="font-size: 12px;">
									<!-- <li><a href="#">Action</a></li> -->
								</ul>
							</div>
							<!-- /btn-group -->
						</div>
						<!-- /input-group -->
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<div class="col-sm-9" style="color: green; font-size: 12px;">可直接录入新的业务机构名称创建机构，或通过关键字筛选已有机构</div>
				</div>
				<div class="form-group" id="companyTypeGroupId">
					<label for="companyType" class="col-sm-3 control-label">机构类型<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="companyType" class="form-control" val=${company.type}>
							<option value="">~选择机构类型~</option>
							<PF:OptionDictionary index="COMPANY_TYPE_ENUM"
								isTextValue="false" />
						</select>
					</div>
				</div>
				<!-- 分割綫 -->



				<hr style="margin: 20px;" />
				<div class="form-group" id="projectTypeGroupId">
					<label for="projectType" class="col-sm-3 control-label">项目分类<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="projectType" class="form-control" val=${project.typeid}>
							<option value="">~选择项目分类~</option>
							<c:forEach var="node" items="${projectTypes}">
								<option value="${node.id}">${node.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group" id="gradationGroupID">
					<label for="projectGradation" class="col-sm-3 control-label">阶段<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<select id="projectGradation" class="form-control"
							val='${project.gradationid}'>
							<option value="">~先选择项目分类~</option>
							<c:forEach var="node" items="${gradations}">
								<option value="${node.id}">${node.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group" id="titleGroupId">
					<label for="projectName" class="col-sm-3 control-label">标题<span
						class="alertMsgClass">*</span></label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="projectName"
							value="${project.name}" placeholder="项目名称">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<span class="alertMsgClass" id="errormessageShowboxId"></span>
			<button type="button" onclick="submitTaskForm()"
				class="btn btn-primary">
				<c:if test="${empty project}">添加项目</c:if>
				<c:if test="${!empty project}">更新项目</c:if>
			</button>
		</div>
	</div>
</div>
<script>
	$(function() {
		//項目选定事件
		$('#projectType').change(function() {
			loadGradations($(this).val());
		});
		$('select', '#addProjectFormId').each(function(i, obj) {
			var val = $(obj).attr('val');
			$(obj).val(val);
		});
		$(".form_day").datetimepicker({
			format : "yyyy-mm-dd",
			showMeridian : true,
			autoclose : true,
			todayBtn : true,
			minView : 2,
			startView : 3
		});
		validateInput('projectName', function(id, val, obj) {
			// 标题
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 256)) {
				return {
					valid : false,
					msg : '长度不能大于' + 256
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'titleGroupId');
		validateInput('projectType', function(id, val, obj) {
			// 项目
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'projectTypeGroupId');

		validateInput('companyName', function(id, val, obj) {
			// 机构名称
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			if (valid_maxLength(val, 256)) {
				return {
					valid : false,
					msg : '长度不能大于' + 256
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'companyNameGroupId');

		validateInput('companyType', function(id, val, obj) {
			// 机构类型
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'companyTypeGroupId');

		validateInput('projectGradation', function(id, val, obj) {
			// 阶段
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'gradationGroupID');
		validateInput('taskType', function(id, val, obj) {
			// 类型
			if (valid_isNull(val)) {
				return {
					valid : false,
					msg : '不能为空'
				};
			}
			return {
				valid : true,
				msg : '正确'
			};
		}, 'errormessageShowboxId', 'taskTypeGroupId');
	});
	//提交表单
	function submitTaskForm() {
		if (!validate('addProjectFormId')) {
			$('#errormessageShowboxId').text('信息录入有误，请检查！');
		} else {
			$('#errormessageShowboxId').text('');
			pConfirm("是否提交数据?", function() {
				var name = $('#projectName').val();
				var projectTypeid = $('#projectType').val();
				var gradationid = $('#projectGradation').val();
				var projectId = $('#projectformProjectId').val();

				var companyid = $('#projectCompanyId').val();
				var companyname = $('#companyName').val();
				var companytype = $('#companyType').val();

				$.post('workproject/projectSubmit.do', {
					'id' : projectId,
					'name' : name,
					'gradationid' : gradationid,
					'typeid' : projectTypeid,
					'companyid' : companyid,
					'companyname' : companyname,
					'companytype' : companytype
				}, function(flag) {
					if (flag.STATE == '0') {
						pAlert(flag.project.name + ":项目提交成功!");
						loadUserProject();
						$('#projectAddFormBox').modal('hide');
					} else {
						pAlert(flag.MESSAGE);
					}
				}, 'json');

			});
		}
	}
	//加载远程阶段类型
	function loadGradations(projectTypeId) {
		$.post('workproject/loadGradations.do', {
			'projectTypeid' : projectTypeId
		},
				function(data) {
					if (data.STATE == '0') {
						$('#projectGradation').html(
								'<option value="">~选择项目~</option>');
						$(data.gradations).each(
								function(i, obj) {
									$('#projectGradation').append(
											'<option value="'+obj.id+'">'
													+ obj.name + '</option>');
								});
					} else {
						pAlert(data.MESSAGE);
					}
				}, 'json');
	}
</script>

<script type="text/javascript">
	//加载项目机构
	function loadCompany(id, name, type) {
		$('#projectCompanyId').val(id);
		$('#companyName').val(name);
		$('#companyType').val(type);
		$('#companyNameTitle').text(name);
	}
	$(function() {
		$('#companyNameChooseButton').bind(
				'click',
				function() {
					//	alert('檢索已有項目');
					$('#companyNameListUl').html('');
					$('#companyNameListUl').html('<li><a>loading...</a></li>');
					$.post('workproject/queryCompany.do', {
						'name' : $('#companyName').val()
					}, function(flag) {
						$('#companyNameListUl').html('');
						$(flag.companys).each(
								function(i, obj) {
									$('#companyNameListUl').append(
											'<li><a onclick="loadCompany(\''
													+ obj.id + '\', \''
													+ obj.name + '\', \''
													+ obj.type + '\')">'
													+ obj.name + '</a></li>');
								});
						$('#companyNameListUl').append(
								'<li role="separator" class="divider"></li>');
						$('#companyNameListUl').append(
								'<li><a onclick="loadCompany(\'\', \'\', \'\')">清理当前机构</a></li>');
					}, 'json');
				});
	});
</script>
<c:if test="${!empty company }">
	<script type="text/javascript">
		$(function() {
			loadCompany('${company.id}', '${company.name}', '${company.type}');
		});
	</script>
</c:if>