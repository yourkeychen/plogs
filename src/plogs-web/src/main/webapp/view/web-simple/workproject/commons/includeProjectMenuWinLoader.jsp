<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--点击任务后弹出的操作窗口 -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div class="modal-title" id="myModalLabel" style="height: 38px;">
				<div style="padding-top: 8px; margin-left: 20px;">
					<div
						style="font-size: 18px; font-weight: 700; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
						<a target="_blank"
							href="projectView/projectInfo.do?projectId=${project.id}">
							${company.name}${(!empty company)?' : ':''}${project.name}</a>&nbsp;&nbsp;
						<code>${project.gradationname}</code>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-body"
			style="margin-bottom: 0px; padding-bottom: 0px;">
			<table class="table table-striped table-bordered"
				style="margin-bottom: 0px;">
				<tbody>
					<tr>
						<th>创建时间</th>
						<td><code>
								<PF:FormatTime date="${project.ctime}"
									yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
							</code></td>
						<th>创建人</th>
						<td><code>${user.name}</code></td>
					</tr>
					<tr>
						<th>项目分类</th>
						<td><code> ${type.name} </code></td>
						<th>当前阶段</th>
						<td><code> ${project.gradationname} </code></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="modal-body">
			<div class="btn-group btn-group-justified btn-group-xs" role="group"
				aria-label="Justified button group">
				<!--  -->
				<a onclick="updateProjectState('${project.id}','1')"
					class="btn btn-info" role="button">激活</a>
				<!--  -->
				<a onclick="updateProjectState('${project.id}','0')"
					class="btn btn-warning" role="button">停用</a>
				<!--  -->
				<a onclick="updateProjectState('${project.id}','2')"
					class="btn btn-success" role="button">关闭 </a>
			</div>
			<div class="btn-group btn-group-justified btn-group-xs" role="group"
				style="margin-top: 4px;" aria-label="Justified button group">
				<!--  -->
				<a onclick="updateProjectState('${project.id}','3')"
					class="btn btn-primary" role="button"><i
					class="glyphicon glyphicon-trash" style="color: #ffffff;"></i>&nbsp;删除项目</a>
				<!--  -->
				<a onclick="editProject('${project.id}')" class="btn btn-primary"
					role="button"><i class="glyphicon glyphicon-pencil"
					style="color: #ffffff;"></i>&nbsp;修改项目</a>
				<a onclick="editProjectUser('${project.id}')" class="btn btn-primary"
					role="button"><i class="glyphicon glyphicon-user"
					style="color: #ffffff;"></i>&nbsp;人员管理(${usersize})</a>	
				
			</div>
		</div>
		<div class="modal-footer">
			<a target="_blank"
				href="projectView/projectInfo.do?projectId=${project.id}"
				class="btn btn-primary" role="button">查看详情</a>
			<button type="button" class="btn btn-default" data-dismiss="modal">关闭窗口</button>
		</div>
	</div>
</div>