<%@page import="java.util.Date"%>
<%@page import="com.farm.core.time.TimeTool"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>项目管理- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="../atext/include-web.jsp"></jsp:include>
<script src="text/lib/layout/jquery.layout-latest.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/themes/default/default.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/super-validate/validate.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/kindeditor-all-min.js"></script>
<script charset="utf-8" src="<PF:basePath/>text/lib/kindeditor/zh-CN.js"></script>
<link rel="stylesheet"
	href="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-kindeditor.css" />
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-file-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/echarts/echarts.all.2.2.7.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/wcpplug/wcp-multiimage-kindeditor.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/kindeditor/htmltags.js"></script>
<script charset="utf-8"
	src="<PF:basePath/>text/lib/alert/sweetalert.min.js"></script>
<!-- 附件批量上传组件 -->
<script src="<PF:basePath/>text/lib/fileUpload/jquery.ui.widget.js"></script>
<script
	src="<PF:basePath/>text/lib/fileUpload/jquery.iframe-transport.js"></script>
<script src="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.js"></script>
<link href="<PF:basePath/>text/lib/fileUpload/jquery.fileupload.css"
	rel="stylesheet">
<script charset="utf-8"
	src="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.min.js"></script>
<link
	href="<PF:basePath/>text/lib/bootstrapdatepicker/bootstrap-datetimepicker.css"
	rel="stylesheet">
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

.plogs-backbox {
	background-color: #f4f4f4;
	height: 100%;
	width: 100%;
}

.plogs-view-title {
	padding: 10px;
	font-weight: 700;
}
/*
* 激活
*/
.plogs-view-title.type1 {
	background-color: #d9edf7;
	color: #337ab7;
}

.plogs-view-type1box .plogs-task-box {
	border-color: #337ab7;
}

.plogs-view-type1box .plogs-task-box:HOVER {
	background-color: #d9edf7;
}
/*
* 停用
*/
.plogs-view-title.type0 {
	background-color: #fcf8e3;
	color: #f0ad4e;
}

.plogs-view-type0box .plogs-task-box {
	border-color: #f0ad4e;
}

.plogs-view-type0box .plogs-task-box:HOVER {
	background-color: #fcf8e3;
}
/*
*完成
*/
.plogs-view-title.type2 {
	background-color: #dff0d8;
	color: #5cb85c;
}

.plogs-view-type2box .plogs-task-box {
	border-color: #5cb85c;
}

.plogs-view-type2box .plogs-task-box:HOVER {
	background-color: #dff0d8;
}

.plogs-view-tasks {
	padding: 0px;
}

.plogs-task-box {
	height: 45px;
	border: 1px dashed;
	padding: 4px;
	background-color: #ffffff;
	cursor: pointer;
	border-left: 8px solid;
	margin-bottom: 4px;
	line-height: 1.3em;
}

.plogs-task-box .plogs-projec-title {
	font-size: 10px;
	font-weight: 200;
	color: #666;
}

.plogs-task-box .plogs-task-title {
	font-size: 14px;
	font-weight: 600;
	color: #888;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

.plogs-view-type3box .plogs-task-box .plogs-task-title {
	color: #000;
}

.plogs-task-box code {
	font-size: 0.8em;
	font-weight: 200;
	float: right;
	padding: 0px;
	padding-right: 4px;
	padding-left: 4px
}

.plogs-projec-petime {
	font-size: 10px;
	margin-left: 4px;
	color: #c7254e;
}
</style>
</head>
<body>
	<!-- 各种弹出窗口 -->
	<div class="openWin">
		<!-- 操作項目弹出窗口 -->
		<jsp:include page="commons/includeProjectMenuWin.jsp"></jsp:include>
		<!-- 添加項目弹出窗口 -->
		<jsp:include page="commons/includeAddProjectWin.jsp"></jsp:include>
		<!-- 添加人员管理弹出窗口 -->
		<jsp:include page="commons/includeAddUsersWin.jsp"></jsp:include>
	</div>
	<div class="ui-layout-center" id="plogs-current-task">
		<div
			style="float: left; width: 33%; background-color: #f1f7fa; height: 100%;"><jsp:include
				page="commons/includType1.jsp"></jsp:include></div>
		<div
			style="float: left; width: 33%; background-color: #fefcf4; height: 100%;"><jsp:include
				page="commons/includType0.jsp"></jsp:include></div>
		<div
			style="float: left; width: 34%; background-color: #faf2f2; height: 100%;"><jsp:include
				page="commons/includType2.jsp"></jsp:include></div>
	</div>
	<div class="ui-layout-north">
		<!-- 顶部头  -->
		<jsp:include page="../commons/head.jsp"></jsp:include>
	</div>
	<div class="ui-layout-west">
		<div style="background-color: #f4f4f4; height: 98%;">
			<div style="text-align: center; padding-top: 20px;">
				<h1 style="margin: 0px; padding: 0px;">项目管理台</h1>
			</div>
			<div style="padding: 10px;">
				<div class="panel panel-default">
					<div class="panel-body">
						<jsp:include page="commons/includSearchForm.jsp"></jsp:include>
					</div>
				</div>
			</div>
			<div class="list-group" style="margin: 4px;">
				<!--  -->
				<a id="addProjectButtonId" class="list-group-item"><i
					class="glyphicon glyphicon-plus-sign list-group-item-success"></i>&nbsp;添加项目</a>
				<!--  -->
				<a id="refrashProjectButtonId" class="list-group-item"><i
					class="glyphicon glyphicon-refresh list-group-item-success"></i>&nbsp;刷新项目</a>
			</div>
			<!-- <div class="list-group" style="margin: 4px;">
				
				<a id="gotoTaskViewButtonId" class="list-group-item"><i
					class="glyphicon glyphicon-log-out"></i>&nbsp;返回工作台</a>
			</div> -->
		</div>
	</div>
	<div class="ui-layout-south">
		<div style="background-color: #000000;">
			<!-- 底部权限 -->
			<jsp:include page="../commons/foot.jsp"></jsp:include>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(function() {
		//主体分隔
		$('body').layout({
			applyDefaultStyles : true,
			north__closable : false,//可以被关闭  
			north__resizable : false,//可以改变大小
			north__size : 50,
			north__spacing_open : 1,
			//---------------------------
			south__closable : false,//可以被关闭  
			south__resizable : false,//可以改变大小
			south__size : 36,
			south__spacing_open : 1,
			//---------------------------
			west__closable : true,//可以被关闭  
			west__resizable : false,//可以改变大小
			west__size : 250,
			west__spacing_open : 8
		});
	});
</script>
<script>
	$(function() {
		//返回任务控制台
		$('#gotoTaskViewButtonId').bind('click', function() {
			gotoTaskView();
		});

		//加载项目
		loadUserProject();
		$('#addProjectButtonId').bind('click', function() {
			addProject();
		});
		$('#refrashProjectButtonId').bind('click', function() {
			var obj = pAlert("loading...", 500);
			loadUserProject();
		});

		$(".taskAutoHeightBox").height($(window).height() - 130);
		$(window).resize(function() {
			$(".taskAutoHeightBox").height($(window).height() - 130);
		});
	});

	//返回工作台
	function gotoTaskView() {
		window.location = "<PF:basePath/>worktask/index.do";
	}

	//创建一个项目
	function addProject() {
		$('#projectAddFormBox').load('workproject/loadProjectFormWin.do', {},
				function() {
					$('#projectAddFormBox').modal('show');
				});
	}

	//加载视图项目
	function loadUserProject() {
		pAlert("loading...", 10000);
		$.post('workproject/loadProject.do', {
			'typeid' : $('#searchTypeInputId').val(),
			'year4' : $('#searchCtimeInputId').val(),
			'name' : $('#searchNameInputId').val()
		}, function(flag) {
			swal.close();
			//ID,CTIME,NAME,STATE,LIVETIME,GRADATIONID,GRADATIONNAME,TYPEID
			$('.plogs-task-box').remove();
			var willOpenTaskId;
			$(flag.projects.resultList).each(function(i, obj) {
				appendProjectToView(obj, obj.STATE);
			});
		}, 'json');
	}
	//添加一个任务
	function appendProjectToView(node, viewtype) {
		$('.plogs-view-type' + viewtype + 'box').append(
				creatProjectDiv(node, viewtype));
	}
	//创建任务元素
	function creatProjectDiv(node, viewtype) {
		var div = '<div class="plogs-task-box" id="' + node.ID
				+ '" onClick="openProjectMenu(this);" >';
		div = div + '    <div class="plogs-task-title">' + node.COMPANYNAME+':'+ node.NAME + '</div>';
		div = div + '    <div  class="plogs-task-title">';
		div = div + '        <span class="plogs-projec-title">' + node.CTIME
				+ '</span>' + "<span class='plogs-projec-title'>-"
				+ node.GRADATIONNAME + "</span>";
		div = div + '        <code>' + node.TYPENAME + '</code>';
		div = div + '    </div>';
		div = div + '</div>';
		return div;
	}
	//打开任务的操作菜单 (任务信息)
	function openProjectMenu(projectBox) {
		var projectid = $(projectBox).attr('id');
		openProjectMenuByTaskId(projectid);
	}
	//打开任务的操作菜单 (任务信息)
	function openProjectMenuByTaskId(projectid) {
		pAlert("loading...", 10000);
		$('#taskOpratorMenuBox').load('workproject/openProjectMenuWin.do', {
			'projectid' : projectid
		}, function() {
			swal.close();
			$('#taskOpratorMenuBox').modal('show');
		});
	}
	//修改任務狀態
	function updateProjectState(taskId, state) {
		if (state == '3') {
			pConfirm('项目被删除，不可恢复，确认执行？', function() {
				doupdateProjectState(taskId, state);
			});
			return;
		}
		doupdateProjectState(taskId, state);
	}
	//执行状态改变
	function doupdateProjectState(projectid, state) {
		pAlert("loading...", 10000);
		$.post('workproject/updateProjectState.do', {
			'projectid' : projectid,
			'state' : state
		}, function(flag) {
			swal.close();
			if (flag.STATE == '0') {
				//('修改成功');重新加載任務
				$('.plogs-task-box').remove();
				loadUserProject();
				$('#taskOpratorMenuBox').modal('hide');
			} else {
				pAlert(flag.MESSAGE);
			}
		}, 'json');
	}

	//修改项目
	function editProject(projectid) {
		pAlert("loading...", 10000);
		$('#projectAddFormBox').load('workproject/loadProjectFormWin.do', {
			'projectId' : projectid
		}, function() {
			swal.close();
			$('#taskOpratorMenuBox').modal('hide');
			$('#projectAddFormBox').modal('show');
		});
	}
	
	//人员管理
	function editProjectUser(projectid){
		pAlert("loading...", 10000);
		$('#projectUsersFormBox').load('workproject/loadProjectUsersFormWin.do', {
			'projectId' : projectid
		}, function() {
			swal.close();
			$('#taskOpratorMenuBox').modal('hide');
			$('#projectUsersFormBox').modal('show');
		});
	}
</script>
<script>
	//询问框
	function pConfirm(tip, okFunc, noFunc) {
		swal({
			text : tip,
			buttons : true,
			buttons : {
				'確定' : true,
				cancel : "取消"
			},
		}).then(function(willDelete) {
			if (willDelete) {
				okFunc();
			} else {
				if (noFunc) {
					noFunc();
				}
			}
		});
	}
	//消息框
	function pAlert(tip, timenum) {
		if (timenum) {
			swal({
				text : tip,
				timer : timenum,
				buttons : false
			});
		} else {
			swal(tip);
		}
	}
</script>
</html>