<%@page import="java.util.Date"%>
<%@page import="com.farm.core.time.TimeTool"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<form>
	<div class="form-group">
		<label>项目类型</label> <select id="searchTypeInputId"
			class="form-control"><option value="">~全部~</option>
			<c:forEach var="node" items="${types}">
				<option value="${node.id}">${node.name}</option>
			</c:forEach>
		</select>
	</div>
	<div class="form-group">
		<label>创建时间</label> <select id="searchCtimeInputId"
			class="form-control">
			<option value="">~全部~</option>
			<c:forEach var="node" items="${years}">
				<option value="${node[0]}">${node[1]}</option>
			</c:forEach>
		</select>
	</div>
	<div class="form-group">
		<label>项目名称</label> <input id="searchNameInputId" type="text"
			class="form-control" placeholder="业务机构或项目名称">
	</div>
	<button type="button" onclick="searchProjects()"
		class="btn btn-primary">查询</button>
	&nbsp;&nbsp;
	<button type="button" onclick="clearSearchForm()"
		class="btn btn-default">清除条件</button>
</form>
<script type="text/javascript">
	function searchProjects() {
		loadUserProject();
	}
	function clearSearchForm() { 
		$('#searchTypeInputId').val('');
		$('#searchCtimeInputId').val('');
		$('#searchNameInputId').val('');
		loadUserProject();
	} 
</script>


