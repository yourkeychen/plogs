<%@page import="java.util.Date"%>
<%@page import="com.farm.core.time.TimeTool"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- 完成项目 -->
<div class="ui-layout-north">
	<div class="plogs-view-title type2" title="[2]项目完成后关闭，必须全部任务完成">关闭项目</div>
</div>
<div class="ui-layout-center">
	<div class="plogs-backbox plogs-view-tasks">
		<div class="plogs-view-type2box taskAutoHeightBox"
			style="overflow: auto; width: 100%; background-color: #f2f9ef;"></div>
	</div>
</div>
