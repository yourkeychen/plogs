<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- 停用项目 -->
<div class="ui-layout-north">
	<div class="plogs-view-title type0" title="[0]项目暂时停用，未完成任务可继续处理，不可新建任务">停用项目</div>
</div>
<div class="ui-layout-center">
	<div class="plogs-backbox plogs-view-tasks">
		<div class="plogs-view-type0box taskAutoHeightBox"
			style="overflow: auto; background-color: #fefcf4;"></div>
	</div>
</div>