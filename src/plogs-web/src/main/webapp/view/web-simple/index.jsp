<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页- <PF:ParameterValue key="config.sys.title" /></title>
<meta name="robots" content="index,follow">
<jsp:include page="atext/include-web.jsp"></jsp:include>
<style type="text/css">
.super_content {
	border-bottom: 0px;
}

.plogs-home-iconbox {
	margin-top: 100px;
	min-height: 400px;
	text-align: center;
}

.plogs-home-icon {
	padding: 20px;
	margin: 20px;
	border-radius: 25px;
}

.plogs-home-icon:HOVER {
	background-color: #eeeeee;
	cursor: pointer;
}

.plogs-home-iconbox .plogs-home-icon img {
	width: 96px;
	height: 96px;
}

.plogs-home-iconbox .plogs-home-icon label {
	margin-top: 20px;
	color: #999999;
}

.plogs-home-iconbox .plogs-home-icon:HOVER label {
	color: #666666;
	cursor: pointer;
}
</style>
</head>
<body>
	<jsp:include page="commons/head.jsp"></jsp:include>
	<jsp:include page="commons/superContent.jsp"></jsp:include>
	<div class="containerbox "
		style="background-color: #f4f4f4; border-top: 1px solid #eeeeee; padding-bottom: 50px; padding-top: 20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-4 hidden-xs hidden-sm" id="logoLeftBoxId">
					<jsp:include page="commons/HomeLogo.jsp"></jsp:include>
				</div>
				<div class="col-md-8" style="border-left: 1px dashed #cccccc;">
					<div class="row plogs-home-iconbox">
						<div class="col-md-4">
							<div class="plogs-home-icon" funcurl="worktask/index.do">
								<img src="text/img/plogs/work.png" alt="..." class="img-rounded">
								<div>
									<label>开始工作</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="plogs-home-icon" funcurl="dir/dir.do">
								<img src="text/img/plogs/webfile.png" alt="..."
									class="img-rounded">
								<div>
									<label>我的网盘</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="plogs-home-icon" funcurl="fileSearch/search.do">
								<img src="text/img/plogs/filesearch.png" alt="..."
									class="img-rounded">
								<div>
									<label>文件搜索</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="plogs-home-icon" funcurl="projectView/index.do">
								<img src="text/img/plogs/project.png" alt="..."
									class="img-rounded">
								<div>
									<label>项目查询</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="plogs-home-icon" funcurl="taskQuery/index.do">
								<img src="text/img/plogs/uweek.png" alt="..."
									class="img-rounded">
								<div>
									<label>任务统计(周/日报)</label>
								</div>
							</div>
						</div>
						<PF:UserIsAdmin userid="${USEROBJ.id}">
							<div class="col-md-4">
								<div class="plogs-home-icon" funcurl="docview/Pubindex.do">
									<img src="text/img/plogs/files.png" alt="..."
										class="img-rounded">
									<div>
										<label>文件系統</label>
									</div>
								</div>
							</div>
						</PF:UserIsAdmin>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="document/commons/includeUploadBox.jsp"></jsp:include>
	<jsp:include page="commons/footServer.jsp"></jsp:include>
	<jsp:include page="commons/foot.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(function() {
		$('#gotoFileButtonId')
				.click(
						function() {
							if (!isnull($('#gotoFileId').val())) {
								window.location = "<PF:basePath/>document/view.do?fileid="
										+ $('#gotoFileId').val();
							}
						});

		$('.plogs-home-icon').click(function() {
			var url = $(this).attr('funcurl');
			if (!url) {
				alert('未设置跳转地址!');
				return;
			}
			window.location = '<PF:basePath/>' + url;
		});

	});
	//表单是否为空
	function isnull(val) {
		var str = val.replace(/(^\s*)|(\s*$)/g, '');//去除空格;
		if (str == '' || str == undefined || str == null) {
			return true;
		} else {
			return false;
		}
	}
</script>
</html>