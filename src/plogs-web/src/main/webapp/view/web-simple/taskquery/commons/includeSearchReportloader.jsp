<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="table-responsive" style="border: 1px solid #cccccc;">
	<table class="table table-bordered" style="margin: 0px;">
		<thead>
			<tr>
				<th>执行时间</th>
				<th style="max-width: 100px;">任务名称</th>
				<th style="border-left: 1px solid #cccccc;">项目名称</th>
				<th>业务机构</th>
				<th>执行人</th>
				<th>完成度</th>
				<th>耗费工时</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${datas}" var="node">
				<c:if test="${!empty node.LOGS}">
					<tr>
						<th scope="row" rowspan="2">${node.DOSTIME}</th>
						<td rowspan="2" style="max-width: 200px;"><a
							href="projectView/taskinfo.do?taskid=${node.TASKID}"
							target="_blank">${node.TASKTITLE}</a></td>
						<td style="border-left: 1px solid #cccccc;"><a
							href="projectView/projectInfo.do?projectId=${node.PROJECTID}"
							target="_blank">${node.PROJECTNAME}</a></td>
						<td>${node.COMPANYNAME}</td>
						<td>${node.USERNAME}</td>
						<td><c:if test="${!empty node.COMPLETION}">
						${node.COMPLETION}%</c:if></td>
						<td><c:if test="${!empty node.DOTIMES}">
						${node.DOTIMES}小时</c:if></td>
					</tr>
					<tr>
						<td colspan="5" style="border-left: 1px solid #cccccc;"><c:forEach
								items="${node.LOGS}" var="log">
								<div style="margin-left: 10px; font-size: 12px; color: #999999;">
									<PF:FormatTime date="${log.etime}"
										yyyyMMddHHmmss="yyyy-MM-dd HH:mm" />
								</div>
								<div class="ke-content"
									style="border: 1px dashed #8a6d3b; border-radius: 4px; padding: 4px; background-color: #fcf8e3;">
									<div class="ke-content"
										style="max-height: 100px; overflow: auto;">${log.text}</div>
								</div>
							</c:forEach></td>
					</tr>
				</c:if>
				<c:if test="${empty node.LOGS}">
					<tr>
						<th scope="row">${node.DOSTIME}</th>
						<td><a href="projectView/taskinfo.do?taskid=${node.TASKID}"
							target="_blank">${node.TASKTITLE}</a></td>
						<td style="border-left: 1px solid #cccccc;"><a
							href="projectView/projectInfo.do?projectId=${node.PROJECTID}"
							target="_blank">${node.PROJECTNAME}</a></td>
						<td>${node.COMPANYNAME}</td>
						<td>${node.USERNAME}</td>
						<td><c:if test="${!empty node.COMPLETION}">
						${node.COMPLETION}%</c:if></td>
						<td><c:if test="${!empty node.DOTIMES}">
						${node.DOTIMES}小时</c:if></td>
					</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
</div>
<div style="text-align: center;">
	<nav aria-label="Page navigation">
		<ul class="pagination">
			<c:if test="${result.currentPage>1}">
				<li><a href="javascript:doSearch(${result.currentPage-1 })"
					aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
				</a></li>
			</c:if>
			<li><a>${result.currentPage }/${result.totalPage}</a></li>
			<c:if test="${result.currentPage<result.totalPage}">
				<li><a href="javascript:doSearch(${result.currentPage+1 })"
					aria-label="Next"> <span aria-hidden="true">&raquo;</span>
				</a></li>
			</c:if>
		</ul>
	</nav>
</div>