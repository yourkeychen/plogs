<%@page import="java.text.DecimalFormat"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<form class="form-horizontal">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<h1 style="text-align: center; margin-bottom: 50px;">任务日志查询</h1>
					<div class="row" style="margin-top: 10px;">
						<div class="col-lg-6" style="text-align: center;">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">项目名称</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="projectname"
										placeholder="项目名称">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">执行人</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="userid" value="" placeholder="执行人">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6" style="text-align: center;">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">业务机构</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" class="form-control" aria-label="..."
											id="companyName" placeholder="机构名称 ">
										<div class="input-group-btn">
											<button id="companyNameChooseButton" type="button"
												class="btn btn-default dropdown-toggle"
												data-toggle="dropdown" aria-haspopup="true"
												aria-expanded="false">
												<span class="glyphicon glyphicon-search"></span>
											</button>
											<ul id="companyNameListUl"
												class="dropdown-menu dropdown-menu-right"
												style="font-size: 12px;">
												<!-- <li><a href="#">Action</a></li> -->
											</ul>
										</div>
										<!-- /btn-group -->
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<label for="inputEmail3" class="col-sm-4 control-label"></label>
							<div class="col-sm-8"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6" style="text-align: center;">
							<div class="form-group">
								<label for="stime" class="col-sm-4 control-label">执行时间范围开始</label>
								<div class="col-sm-8">
									<input type="text" class="form-control form_day" id="stime"
										placeholder="时间开始">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="etime" class="col-sm-4 control-label">执行时间范围结束</label>
								<div class="col-sm-8">
									<input type="text" class="form-control form_day" id="etime"
										placeholder="时间结束">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<label for="inputEmail3" class="col-sm-2 control-label"></label>
							<div class="col-sm-10"
								style="padding-bottom: 20px; padding-left: 4px;">
								<div class="btn-group" role="group" style=""
									aria-label="Default button group">
									<button type="button" onclick="lastDay()"
										class="btn btn-default">前一天</button>
									<button type="button" onclick="timeToday() "
										class="btn btn-success">今天</button>
									<button type="button" onclick="nextDay()"
										class="btn btn-default">后一天</button>
								</div>
								<div class="btn-group" role="group" style=""
									aria-label="Default button group">
									<button type="button" onclick="lastWeek()"
										class="btn btn-default">前一周</button>
									<button type="button" onclick="timeToweek() "
										class="btn btn-success">本周</button>
									<button type="button" onclick="nextWeek()"
										class="btn btn-default">后一周</button>
								</div>
							</div>
						</div>
					</div>
					<h2
						style="text-align: center; margin-bottom: 20px; margin-top: 50px;">
						<button type="button" class="btn btn-primary" onclick="doSearch(1);">
							&nbsp;&nbsp;&nbsp;&nbsp;执行查询&nbsp;&nbsp;<i
								class=" glyphicon glyphicon-search "></i>&nbsp;&nbsp;&nbsp;&nbsp;
						</button>
					</h2>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	//前一天
	function lastDay() {
		var cdate = getDateFromStr($('#stime').val());
		$('#stime').val(addDate(cdate, -1));
		$('#etime').val(addDate(cdate, -1));
	}
	//后一天
	function nextDay() {
		var cdate = getDateFromStr($('#stime').val());
		$('#stime').val(addDate(cdate, 1));
		$('#etime').val(addDate(cdate, 1));
	}
	//选择今天
	function timeToday(date) {
		var day2 = new Date();
		if (date) {
			day2 = date;
		}
		day2.setTime(day2.getTime());
		var s2 = day2.getFullYear() + "-"
				+ formatTime((day2.getMonth() + 1), 2) + "-"
				+ formatTime(day2.getDate(), 2);
		$('#stime').val(s2);
		$('#etime').val(s2);
	}
	//选择本周
	function timeToweek() {
		var monday = getMonday(new Date());
		var mondayStr = getStrFromDate(monday);
		$('#stime').val(mondayStr);
		$('#etime').val(addDate(monday, 6));
	}
	//前一周
	function lastWeek() {
		var cdate1 = getDateFromStr($('#stime').val());
		$('#stime').val(addDate(cdate1, -7));
		var cdate2 = getDateFromStr($('#etime').val());
		$('#etime').val(addDate(cdate2, -7));
	}
	//后一周
	function nextWeek() {
		var cdate1 = getDateFromStr($('#stime').val());
		$('#stime').val(addDate(cdate1, 7));
		var cdate2 = getDateFromStr($('#etime').val());
		$('#etime').val(addDate(cdate2, 7));
	}
	//字符串转日期格式，strDate要转为日期格式的字符串
	function getDateFromStr(strDate) {
		var date = eval('new Date('
				+ strDate.replace(/\d+(?=-[^-]+$)/, function(a) {
					return parseInt(a, 10) - 1;
				}).match(/\d+/g) + ')');
		return date;
	}
	//日期转字符串
	function getStrFromDate(date) {
		var month = date.getMonth() + 1;
		var day = date.getDate();
		return date.getFullYear() + '-' + getFormatDate(month) + '-'
				+ getFormatDate(day);
	}
	//date加几天后的字符串日期
	function addDate(date, days) {
		if (days == undefined || days == '') {
			days = 1;
		}
		var date = new Date(date);
		date.setDate(date.getDate() + days);
		var month = date.getMonth() + 1;
		var day = date.getDate();
		return date.getFullYear() + '-' + getFormatDate(month) + '-'
				+ getFormatDate(day);
	}
	//格式化为两位数
	function getFormatDate(arg) {
		if (arg == undefined || arg == '') {
			return '';
		}
		var re = arg + '';
		if (re.length < 2) {
			re = '0' + re;
		}

		return re;
	}
	//获得一个周一
	function getMonday(date) {
		var day = date.getDay();
		var deltaDay;
		if (day == 0) {
			deltaDay = 6;
		} else {
			deltaDay = day - 1;
		}
		var monday = new Date(date.getTime() - deltaDay * 24 * 60 * 60 * 1000);
		monday.setHours(0);
		monday.setMinutes(0);
		monday.setSeconds(0);
		return monday; //返回本周的周一的0时0分0秒
	}
	//格式化月，日
	function formatTime(str, num) {
		str = String("00" + str);
		return str.substring(str.length - num);
	}
</script>
<script type="text/javascript">
	function doSearch(page){
		$('#taskReportBoxId').html('<div style="text-align: center;background-color: #eeeeee;padding: 20px;border: 1px dashed #cccccc;color: #999999;">查询中...</div>');
		$('#taskReportBoxId').load('taskQuery/loadReport.do',{
			'projectname':$('#projectname').val(),
			'user':$('#userid').val(),
			'companyname':$('#companyName').val(),
			'etime':$('#etime').val(),
			'stime':$('#stime').val(),
			'page':page
		},function(flag){});
	}
	//加载项目机构
	function loadCompany(id, name, type) {
		$('#companyName').val(name);
	}
	$(function() {
		$(".form_day").datetimepicker({
			format : "yyyy-mm-dd",
			showMeridian : true,
			autoclose : true,
			todayBtn : true,
			minView : 2,
			startView : 3
		});
		$('#companyNameChooseButton')
				.bind(
						'click',
						function() {
							//	alert('檢索已有項目');
							$('#companyNameListUl').html('');
							$('#companyNameListUl').html(
									'<li><a>loading...</a></li>');
							$
									.post(
											'workproject/queryCompany.do',
											{
												'name' : $('#companyName')
														.val()
											},
											function(flag) {
												$('#companyNameListUl')
														.html('');
												$(flag.companys)
														.each(
																function(i, obj) {
																	$(
																			'#companyNameListUl')
																			.append(
																					'<li><a onclick="loadCompany(\''
																							+ obj.id
																							+ '\', \''
																							+ obj.name
																							+ '\', \''
																							+ obj.type
																							+ '\')">'
																							+ obj.name
																							+ '</a></li>');
																});
												$('#companyNameListUl')
														.append(
																'<li role="separator" class="divider"></li>');
												$('#companyNameListUl')
														.append(
																'<li><a onclick="loadCompany(\'\', \'\', \'\')">清理当前机构</a></li>');
											}, 'json');
						});
	});
</script>