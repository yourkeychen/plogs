<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="btn-group btn-group-sm btn-group-justified" role="group"
	aria-label="Justified button group with nested dropdown">
	<!--  -->
	<a href="document/view.do?fileid=${fileview.file.id}"
		class="btn btn-info" role="button"><span
		class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;信息</a>
	<!--  -->
	<a href="docview/info.do?fileid=${fileview.file.id}"
		class="btn btn-warning" role="button"><span
		class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>&nbsp;预览</a>
	<!--  -->
	<a href="${fileview.downloadUrl}" class="btn btn-success" role="button"><span
		class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>&nbsp;下载</a>
	<!--  -->
	<a href="javascript:window.location.reload();" class="btn btn-primary" role="button"><span
		class=" glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;刷新</a>
</div>