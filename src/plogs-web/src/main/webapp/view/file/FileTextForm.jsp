<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--文件文本表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> 浏览文件文本 </span>
	</div>
	<div data-options="region:'center'">
		${text.filetext}
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<a id="dom_cancle_formFiletext" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionFiletext = 'filetext/add.do';
	var submitEditActionFiletext = 'filetext/edit.do';
	var currentPageTypeFiletext = '${pageset.operateType}';
	var submitFormFiletext;
	$(function() {
		//表单组件对象
		submitFormFiletext = $('#dom_formFiletext').SubmitForm({
			pageType : currentPageTypeFiletext,
			grid : gridFiletext,
			currentWindowId : 'winFiletext'
		});
		//关闭窗口
		$('#dom_cancle_formFiletext').bind('click', function() {
			$('#winFiletext').window('close');
		});
		//提交新增数据
		$('#dom_add_entityFiletext').bind('click', function() {
			submitFormFiletext.postSubmit(submitAddActionFiletext);
		});
		//提交修改数据
		$('#dom_edit_entityFiletext').bind('click', function() {
			submitFormFiletext.postSubmit(submitEditActionFiletext);
		});
	});
//-->
</script>