<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--资源库表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formFileresource">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">资源库名称:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[128]']"
						id="entity_title" name="title" value="${entity.title}"></td>
				</tr>
				<tr>
					<td class="title">目录:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[512]']"
						id="entity_path" name="path" value="${entity.path}"></td>
				</tr>
				<tr>
					<td class="title">状态:</td>
					<!-- 1读写，2只读，0禁用 -->
					<td colspan="3"><select name="state" id="entity_state"
						val="${entity.state}"><option value="1">读写</option>
							<option value="2">只读</option>
							<option value="0">禁用</option></select></td>
				</tr>
				<tr>
					<td class="title">备注:</td>
					<td colspan="3"><textarea style="width: 360px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
							name="pcontent">${entity.pcontent}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityFileresource" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityFileresource" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formFileresource" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionFileresource = 'fileresource/add.do';
	var submitEditActionFileresource = 'fileresource/edit.do';
	var currentPageTypeFileresource = '${pageset.operateType}';
	var submitFormFileresource;
	$(function() {
		//表单组件对象
		submitFormFileresource = $('#dom_formFileresource').SubmitForm({
			pageType : currentPageTypeFileresource,
			grid : gridFileresource,
			currentWindowId : 'winFileresource'
		});
		//关闭窗口
		$('#dom_cancle_formFileresource').bind('click', function() {
			$('#winFileresource').window('close');
		});
		//提交新增数据
		$('#dom_add_entityFileresource').bind('click', function() {
			submitFormFileresource.postSubmit(submitAddActionFileresource);
		});
		//提交修改数据
		$('#dom_edit_entityFileresource').bind('click', function() {
			submitFormFileresource.postSubmit(submitEditActionFileresource);
		});
	});
//-->
</script>