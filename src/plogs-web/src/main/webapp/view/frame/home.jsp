<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html>
<head>
<title><PF:ParameterValue key="config.sys.title" /></title>
<jsp:include page="/view/conf/include.jsp"></jsp:include>
<script type="text/javascript" src="text/javascript/echarts.min.js"></script>
</head>
<body class="easyui-layout">
	<div
		style="text-align: right; color: #000000; background: #fff; padding: 20px; height: 100%;">
		<table style="width: 100%;">
			<tr>
				<td width="50%" style="text-align: center;"><img
					src="<PF:basePath/>view/web-simple/atext/png/icon" style="max-width: 128px;"></td>
				<td width="50%"><div id="main"
						style="width: 100%; height: 400px;"></div></td>
			</tr>
		</table>
	</div>
</body>
<script>
	
</script>
</html>