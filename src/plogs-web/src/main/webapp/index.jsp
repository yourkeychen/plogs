<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.farm.parameter.FarmParameterService"%>
<%@page import="com.farm.core.Context"%>
<%@page import="com.farm.util.spring.BeanFactory"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title><PF:ParameterValue key="config.sys.title" /></title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="robots" content="index,follow">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="text/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="text/lib/bootstrap/css/bootstrap-theme.min.css"
	rel="stylesheet">
<script type="text/javascript" src="text/javascript/jquery-1.8.0.min.js"></script>
<script src="text/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="text/lib/bootstrap/respond.min.js"></script>
</head>
<body style="background-color: #000000;">
	<div class="containerbox ">
		<div class="container">
			<div class="row">
				<div
					style="color: #ffffff; text-align: center; margin: auto; margin-top: 200px;">
					<div>
						<img style="max-width: 128px; max-height: 128px;"
							src="view/web-simple/atext/png/icon" alt="..."
							class="img-rounded">
					</div>
					<div>
						<h1>
							<PF:ParameterValue key="config.sys.title" />
						</h1>
						<h3 style="color: #cccccc; font-size: 14px;">
							页面加载中(倒计时<span id="waitTime"></span>)....
						</h3>
					</div>
					<div>
						<a style="color: #ffffff;" href="<PF:defaultIndexPage/>">立即加载</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
	<script language="javascript" type="text/javascript">
		var waitTime = 1;
		$(function() {
			$('#waitTime').text(waitTime);
			setInterval("redirect()", 1000);
		});
		function redirect() {
			if (waitTime >= 0) {
				$('#waitTime').text(waitTime);
			}
			if (waitTime < 0) {
				location.href = '<PF:defaultIndexPage/>';
			}
			waitTime--;
		}
	</script>
</html>