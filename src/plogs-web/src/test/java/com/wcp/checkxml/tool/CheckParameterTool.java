package com.wcp.checkxml.tool;

import java.io.File;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CheckParameterTool {

	public static void main(String[] args) throws IOException {
		Document xml = Jsoup.parse(new File("D:\\test\\WdapWebConfig.xml"), "UTF-8");

		Elements elements = xml.getElementsByTag("parameter");

		for (Element node : elements) {
			String name = node.attr("name");
			System.out.println("<parameter versions='0.1.0'>"+name+"</parameter>");
		}

	}

}
