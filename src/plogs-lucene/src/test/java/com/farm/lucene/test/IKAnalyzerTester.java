package com.farm.lucene.test;

import java.io.IOException;
import java.io.StringReader;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

public class IKAnalyzerTester {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String s = "SDCZ60-016G-AW46OZ  SDCZ60-016G-AW46PZ";
		StringReader input = new StringReader(s.trim());
		IKSegmenter ikseg = new IKSegmenter(input, true);
		for (Lexeme lexeme = ikseg.next(); lexeme != null; lexeme = ikseg.next()) {
			System.out.println(lexeme.getLexemeText() + "|");
		}
	}

}
