package com.farm.authority.password;

import com.farm.authority.password.impl.SafePasswordProvider;

/**
 * 获得密码创建器的实现
 * 
 * @author macpl
 *
 */
public class PasswordProviderService {
	public static PasswordProviderInter getInstanceProvider() {
		return new SafePasswordProvider();
	}

	/**
	 * @param type
	 *            SIMPLE:简单类型,SAFE:安全类型
	 * @return
	 */
	public static PasswordProviderInter getInstanceProvider(String type) {
		return new SafePasswordProvider();
	}
}
