package com.farm.wfs.service;

import com.farm.wfs.domain.Wfile;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：网盘文件服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface WfileServiceInter {
	public void addFile(String wdapfileid, String dirid, LoginUser currentUser);

	/**
	 * 修改实体管理实体
	 * 
	 * @param entity
	 */
	public Wfile editWfileEntity(Wfile entity, LoginUser user);

	/**
	 * 删除实体管理实体
	 * 
	 * @param entity
	 */
	public void deleteWfile(String id, LoginUser user);

	/**
	 * 获得实体管理实体
	 * 
	 * @param id
	 * @return
	 */
	public Wfile getWfileEntity(String id);

	/**
	 * 创建一个基本查询用来查询当前实体管理实体
	 * 
	 * @param query
	 *            传入的查询条件封装
	 * @return
	 */
	public DataQuery createWfileSimpleQuery(DataQuery query);

	/**
	 * @param dirid
	 * @param id
	 * @return
	 */
	public List<Wfile> getFiles(String dirid, String userid);

	/**
	 * 移动一个文件到文件夹
	 * 
	 * @param wfileid
	 * @param toDirid
	 */
	public void moveToDir(String wfileid, String toDirid);

	/**
	 * 搜索文件
	 * 
	 * @param word
	 * @param userid
	 * @return
	 */
	public List<Wfile> searchFiles(String word, String userid);

}