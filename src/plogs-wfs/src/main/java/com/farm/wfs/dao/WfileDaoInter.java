package com.farm.wfs.dao;

import com.farm.wfs.domain.Wfile;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;

/* *
 *功能：网盘文件数据库持久层接口
 *详细：
 *
 *版本：v0.1
 *作者：Farm代码工程自动生成
 *日期：20150707114057
 *说明：
 */
public interface WfileDaoInter {
	/**
	 * 删除一个网盘文件实体
	 * 
	 * @param entity
	 *            实体
	 */
	public void deleteEntity(Wfile wfile);

	/**
	 * 由网盘文件id获得一个网盘文件实体
	 * 
	 * @param id
	 * @return
	 */
	public Wfile getEntity(String wfileid);

	/**
	 * 插入一条网盘文件数据
	 * 
	 * @param entity
	 */
	public Wfile insertEntity(Wfile wfile);

	/**
	 * 获得记录数量
	 * 
	 * @return
	 */
	public int getAllListNum();

	/**
	 * 修改一个网盘文件记录
	 * 
	 * @param entity
	 */
	public void editEntity(Wfile wfile);

	/**
	 * 获得一个session
	 */
	public Session getSession();

	/**
	 * 执行一条网盘文件查询语句
	 */
	public DataResult runSqlQuery(DataQuery query);

	/**
	 * 条件删除网盘文件实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param rules
	 *            删除条件
	 */
	public void deleteEntitys(List<DBRule> rules);

	/**
	 * 条件查询网盘文件实体，依据对象字段值,当rules为空时查询全部(一般不建议使用该方法)
	 * 
	 * @param rules
	 *            查询条件
	 * @return
	 */
	public List<Wfile> selectEntitys(List<DBRule> rules);

	/**
	 * 条件修改网盘文件实体，依据对象字段值(一般不建议使用该方法)
	 * 
	 * @param values
	 *            被修改的键值对
	 * @param rules
	 *            修改条件
	 */
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules);

	/**
	 * 条件合计网盘文件:count(*)
	 * 
	 * @param rules
	 *            统计条件
	 */
	public int countEntitys(List<DBRule> rules);

	/**
	 * 获得用户文件
	 * 
	 * @param dirid
	 * @param userid
	 * @return
	 */
	public List<Wfile> getFiles(String dirid, String userid);

	public List<Wfile> selectEntitys(List<DBRule> rules, int maxnum);
}