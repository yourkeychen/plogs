package com.farm.wfs.dao.impl;

import java.math.BigInteger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.utils.HibernateSQLTools;
import com.farm.wfs.dao.DirTypeDaoInter;
import com.farm.wfs.domain.DirType;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;

/* *
 *功能：专题分类持久层实现
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Repository
public class DirTypeDaoImpl extends HibernateSQLTools<DirType>implements DirTypeDaoInter {
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFatory;

	@Override
	public void deleteEntity(DirType DirType) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.delete(DirType);
	}

	@Override
	public int getAllListNum() {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery("select count(*) from wfs_dirtype");
		BigInteger num = (BigInteger) sqlquery.list().get(0);
		return num.intValue();
	}

	@Override
	public DirType getEntity(String DirTypeid) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		return (DirType) session.get(DirType.class, DirTypeid);
	}

	@Override
	public DirType insertEntity(DirType DirType) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.save(DirType);
		return DirType;
	}

	@Override
	public void editEntity(DirType DirType) {
		// TODO 自动生成代码,修改后请去除本注释
		Session session = sessionFatory.getCurrentSession();
		session.update(DirType);
	}

	@Override
	public Session getSession() {
		// TODO 自动生成代码,修改后请去除本注释
		return sessionFatory.getCurrentSession();
	}

	@Override
	public DataResult runSqlQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			return query.search(sessionFatory.getCurrentSession());
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void deleteEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		deleteSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public List<DirType> selectEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		return selectSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	@Override
	public List<DirType> selectEntitys(List<DBRule> rules, int maxnum) {
		// TODO 自动生成代码,修改后请去除本注释
		return selectSqlFromFunction(sessionFatory.getCurrentSession(), rules, maxnum);
	}

	@Override
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		updataSqlFromFunction(sessionFatory.getCurrentSession(), values, rules);
	}

	@Override
	public int countEntitys(List<DBRule> rules) {
		// TODO 自动生成代码,修改后请去除本注释
		return countSqlFromFunction(sessionFatory.getCurrentSession(), rules);
	}

	public SessionFactory getSessionFatory() {
		return sessionFatory;
	}

	public void setSessionFatory(SessionFactory sessionFatory) {
		this.sessionFatory = sessionFatory;
	}

	@Override
	protected Class<?> getTypeClass() {
		return DirType.class;
	}

	@Override
	protected SessionFactory getSessionFactory() {
		return sessionFatory;
	}

	@Override
	public boolean isHasChildNode(String typeid) {
		int num = countEntitys(DBRuleList.getInstance().add(new DBRule("PARENTID", typeid, "="))
				.add(new DBRule("PSTATE", "1", "=")).toList());
		return num > 0;
	}

	@Override
	public boolean isHasDocs(String typeid) {
		DirType type = getEntity(typeid);
		Session session = sessionFatory.getCurrentSession();
		SQLQuery sqlquery = session.createSQLQuery(
				"SELECT COUNT(*) AS NUM FROM wfs_dirtype A LEFT JOIN FARM_DOC_SPECIALLIST B ON B.TYPEID = A.ID WHERE A.TREECODE LIKE '"
						+ type.getTreecode() + "%' AND B.ID IS NOT NULL;");
		@SuppressWarnings("rawtypes")
		List types = sqlquery.list();
		return ((BigInteger) types.get(0)).intValue() > 0;
	}

	@Override
	public List<DirType> allSubType(String typeid) {
		DirType type = getEntity(typeid);
		return selectEntitys(
				DBRuleList.getInstance().add(new DBRule("TREECODE", type.getTreecode(), "like-")).toList());
	}
}
