package com.farm.wfs.service;

import com.farm.wfs.domain.DirType;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：专题服务层接口
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
public interface DirTypeServiceInter {

	/**
	 * 获得专题分类
	 * 
	 * @param specialId
	 * @param isShowMoreInfo
	 *            是否顯示更多信息在分類名稱中【如，排序】
	 * @return
	 */
	public List<DirType> getAllDirs(String userid);

	/**
	 * 獲得分類的全路径
	 * 
	 * @param types
	 *            用户的私有知识库分类（可以通过getPrivateKnowType函数获得）
	 * @param typeid
	 *            分类id
	 * @return
	 */
	public List<DirType> getTypeAllParent(List<DirType> types, String typeid);

	/**
	 * 获得专题分类
	 * 
	 * @param typeid
	 * @return
	 */
	public DirType getDir(String userid, String typeid);

	/**
	 * 編輯分類
	 * 
	 * @param id
	 * @param name
	 * @param sort
	 * @param currentUser
	 */
	public DirType editDir(String typeid, String name, int sort, LoginUser currentUser);

	/**
	 * 刪除分類
	 * 
	 * @param typeid
	 * @param currentUser
	 */
	public void delDir(String dirid, LoginUser currentUser);

	/**
	 * 移動分類
	 * 
	 * @param specialId
	 * @param typeid
	 *            移动的分类
	 * @param otypeid
	 *            目标分类
	 */
	public void moveType(String specialId, String typeid, String otypeid);

	/**
	 * 获得一个目录的子目录
	 * 
	 * @param dirid
	 * @param userid
	 * @return
	 */
	public List<DirType> getDirs(String dirid, String userid);

	/**
	 * 創建分類
	 * 
	 * @param name
	 * @param sort
	 * @param parentId
	 * @param specialId
	 * @param currentUser
	 */
	public DirType createDir(String name, int sort, String parentId, LoginUser currentUser);

	/**
	 * 获得目录路径
	 * 
	 * @param dirid
	 * @return
	 */
	public List<DirType> getDirPath(String dirid);

	/**
	 * 搜索文件夹
	 * 
	 * @param word
	 * @param userid
	 * @return
	 */
	public List<DirType> searchDirs(String word, String userid);
}