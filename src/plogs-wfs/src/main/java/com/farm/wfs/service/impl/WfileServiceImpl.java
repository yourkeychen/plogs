package com.farm.wfs.service.impl;

import com.farm.wfs.domain.Wfile;
import com.farm.core.time.TimeTool;
import com.farm.file.WdapFileServiceInter;
import com.farm.file.domain.FileBase;
import com.farm.view.WdapViewServiceInter;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;

import com.farm.wfs.dao.WfileDaoInter;
import com.farm.wfs.service.WfileServiceInter;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DBRuleList;
import com.farm.core.sql.query.DataQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileNotFoundException;
import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;

/* *
 *功能：网盘文件服务层实现类
 *详细：
 *
 *版本：v0.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Service
public class WfileServiceImpl implements WfileServiceInter {
	@Resource
	private WfileDaoInter wfileDaoImpl;
	@Resource
	private WdapFileServiceInter wdapFileServiceImpl;
	@Resource
	private WdapViewServiceInter wdapViewServiceImpl;
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(WfileServiceImpl.class);

	@Override
	@Transactional
	public Wfile editWfileEntity(Wfile entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		Wfile entity2 = wfileDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setUserid(entity.getUserid());
		entity2.setUsername(entity.getUsername());
		entity2.setTypeid(entity.getTypeid());
		entity2.setUtime(entity.getUtime());
		entity2.setCtime(entity.getCtime());
		entity2.setFtype(entity.getFtype());
		entity2.setFsize(entity.getFsize());
		entity2.setTitle(entity.getTitle());
		entity2.setWdapfileid(entity.getWdapfileid());
		entity2.setId(entity.getId());
		wfileDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteWfile(String id, LoginUser user) {
		Wfile wfile = wfileDaoImpl.getEntity(id);
		wfileDaoImpl.deleteEntity(wfile);
		try {
			wdapFileServiceImpl.freeFile(wfile.getWdapfileid());
			wdapFileServiceImpl.delFileByLogic(wfile.getWdapfileid());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	@Transactional
	public Wfile getWfileEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return wfileDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createWfileSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WFS_FILE",
				"ID,USERID,USERNAME,TYPEID,UTIME,CTIME,FTYPE,FSIZE,TITLE,WDAPFILEID");
		return dbQuery;
	}

	@Override
	@Transactional
	public void addFile(String wdapfileid, String dirid, LoginUser currentUser) {
		try {

			FileBase filebase = wdapFileServiceImpl.getFileBase(wdapfileid);
			Wfile wfile = new Wfile();
			wfile.setCtime(TimeTool.getTimeDate14());
			wfile.setFsize(Long.valueOf(filebase.getFilesize()));
			wfile.setFtype(filebase.getExname());
			wfile.setTitle(filebase.getTitle());
			if (StringUtils.isNotBlank(dirid) && !dirid.toUpperCase().equals("NONE")) {
				wfile.setTypeid(dirid);
			}
			wfile.setUserid(currentUser.getId());
			wfile.setUsername(currentUser.getName());
			wfile.setUtime(TimeTool.getTimeDate14());
			wfile.setWdapfileid(filebase.getId());
			wfileDaoImpl.insertEntity(wfile);
			wdapFileServiceImpl.submitFile(filebase.getId());
			if (wdapViewServiceImpl.fileConvertAble(filebase.getExname(), filebase.getFilesize())) {
				wdapViewServiceImpl.reLoadFileViewTask(filebase.getId());
			}
		} catch (FileNotFoundException | JSONException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	@Transactional
	public List<Wfile> getFiles(String dirid, String userid) {
		if (StringUtils.isBlank(dirid) || dirid.toUpperCase().equals("NONE")) {
			dirid = null;
		}
		return wfileDaoImpl.getFiles(dirid, userid);
	}

	@Override
	@Transactional
	public void moveToDir(String wfileid, String toDirid) {
		Wfile wfile = wfileDaoImpl.getEntity(wfileid);
		wfile.setTypeid(toDirid);
		wfileDaoImpl.editEntity(wfile);
	}

	@Override
	@Transactional
	public List<Wfile> searchFiles(String word, String userid) {
		return wfileDaoImpl.selectEntitys(DBRuleList.getInstance().add(new DBRule("TITLE", word, "like"))
				.add(new DBRule("USERID", userid, "=")).toList(), 100);
	}
}
