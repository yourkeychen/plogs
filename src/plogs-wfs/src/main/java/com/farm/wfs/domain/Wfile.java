package com.farm.wfs.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/* *
 *功能：网盘文件类
 *详细：
 *
 *版本：v2.1
 *作者：FarmCode代码工程
 *日期：20150707114057
 *说明：
 */
@Entity(name = "WFile")
@Table(name = "wfs_file")
public class Wfile implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "USERID", length = 32, nullable = false)
	private String userid;
	@Column(name = "USERNAME", length = 64, nullable = false)
	private String username;
	@Column(name = "TYPEID", length = 32, nullable = true)
	private String typeid;
	@Column(name = "UTIME", length = 14, nullable = false)
	private String utime;
	@Column(name = "CTIME", length = 14, nullable = false)
	private String ctime;
	@Column(name = "FTYPE", length = 16, nullable = false)
	private String ftype;
	@Column(name = "FSIZE", length = 19, nullable = false)
	private Long fsize;
	@Column(name = "TITLE", length = 256, nullable = false)
	private String title;
	@Column(name = "WDAPFILEID", length = 32, nullable = false)
	private String wdapfileid;

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTypeid() {
		return this.typeid;
	}

	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}

	public String getUtime() {
		return this.utime;
	}

	public void setUtime(String utime) {
		this.utime = utime;
	}

	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getFtype() {
		return this.ftype;
	}

	public void setFtype(String ftype) {
		this.ftype = ftype;
	}

	public Long getFsize() {
		return this.fsize;
	}

	public void setFsize(Long fsize) {
		this.fsize = fsize;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWdapfileid() {
		return this.wdapfileid;
	}

	public void setWdapfileid(String wdapfileid) {
		this.wdapfileid = wdapfileid;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}