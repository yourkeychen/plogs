package com.farm.web.fileview;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.farm.util.spring.BeanFactory;
import com.farm.view.WdapViewServiceInter;

public class ViewNotAble extends TagSupport {
	private String fileid;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	@Override
	public int doStartTag() throws JspException {
		WdapViewServiceInter wdapView = (WdapViewServiceInter) BeanFactory.getBean("wdapViewServiceImpl");
		// 权限未注册或用户有权限或权限不检查
		if (!wdapView.isView(fileid)) {
			return EVAL_BODY_INCLUDE;
		}
		return SKIP_BODY;
	}

	public String getFileid() {
		return fileid;
	}

	public void setFileid(String fileid) {
		this.fileid = fileid;
	}

}
